import { env } from 'process';

const path = require('path');

export const DEV_HOST = process.env.HOST || 'http://localhost';
export const DEV_PORT = process.env.DEV_PORT || '3017';
export const MOCK_API_PORT = '3004';

export const CLIENT_ENTRY_PATH = path.join(__dirname, '..', 'src', 'client.js');
export const SERVER_ENTRY_PATH = path.join(__dirname, '..', 'src', 'server.js');
export const REGISTRATION_BANNER_ENTRY_PATH = path.join(__dirname, '..', 'src', 'registrationBanner.js');
export const POINTS_ENTRY_PATH = path.join(__dirname, '..', 'src', 'points.js');
export const KV_ENTRY_PATH = path.join(__dirname, '..', 'src', 'kv.js');
export const NUMBER_ENTRY_PATH = path.join(__dirname, '..', 'src', 'number.js');
export const OUTPUT_PATH = path.join(__dirname, '..', 'dist');

export const POLYFILL_PATH = path.join(__dirname, 'polyfills');
export const SSR_POLYFILL_PATH = path.join(__dirname, 'ssr_polyfills');

export const IMAGE_OUTPUT_PATH = 'assets/images';
export const JS_OUTPUT_PATH = 'assets/js';
export const CSS_OUTPUT_PATH = 'assets/css';

export const VENDOR_SCRIPTS = ['react', 'react-dom'];

export const CDN_HOST = 'https://shop.rewe-static.de';
export const CDN_PATH = 'payback-frontend';
export const CDN_PB_FULL_PATH = `${CDN_HOST}/${CDN_PATH}/${env.GIT_HASH}/`;

export const resolveExtensions = ['.js', '.jsx', '.scss'];
