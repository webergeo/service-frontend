/* Empty stubs for SSR mode - we still need the "global" object definitions in the component code
 * even though these are not used on the server side
 */

/* eslint-disable no-unused-vars */
const logger = {
  debug: (message) => {},
  info: (message) => {},
  warn: (message) => {},
  error: (message) => {}
};

global.XRD = {
  tracking: {
    trackEvent: () => {}
  },
  logging: {
    getLogger: serviceName => logger
  }
};

/* eslint-enable no-unused-vars */
