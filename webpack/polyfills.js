// Fallback for Tracking API
if (!window.XRD) {
  window.XRD = {};
}

if (!window.XRD.tracking) {
  window.XRD.tracking = {
    trackEvent: (eventName, payload) => {
      if (process.env.NODE_ENV !== 'production') {
        // eslint-disable-next-line no-unused-vars,no-console
        console.log('Called XRD.tracking.trackEvent', eventName, payload);
      }
    }
  };
}

if (!window.XRD.logging) {
  window.XRD.logging = {};
}

if (!window.XRD.logging.getLogger) {
  window.XRD.logging.getLogger = serviceName => ({
    debug: (message) => {
      if (process.env.NODE_ENV !== 'production') {
        console.debug('Called XRD.logging.getLogger.debug service-name: %s, message: %s', serviceName, message);
      }
    },

    info: (message) => {
      if (process.env.NODE_ENV !== 'production') {
        console.info('Called XRD.logging.getLogger.info service-name: %s, message: %s', serviceName, message);
      }
    },

    warn: (message) => {
      if (process.env.NODE_ENV !== 'production') {
        console.warn('Called XRD.logging.getLogger.warn service-name: %s, message: %s', serviceName, message);
      }
    },

    error: (message) => {
      if (process.env.NODE_ENV !== 'production') {
        console.error('Called XRD.logging.getLogger.error service-name: %s, message: %s', serviceName, message);
      }
    }
  });
}
