import {
  CDN_PB_FULL_PATH,
  IMAGE_OUTPUT_PATH,
  NUMBER_ENTRY_PATH,
  OUTPUT_PATH,
  POLYFILL_PATH,
  JS_OUTPUT_PATH,
  CSS_OUTPUT_PATH
} from './constants';

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const plugins = [
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      SERVICE_PORT: JSON.stringify(process.env.SERVICE_PORT)
    }
  }),

  new webpack.optimize.ModuleConcatenationPlugin(),

  new HtmlWebpackPlugin({
    template: 'src/templates/number.ejs',
    filename: 'templates/number.ejs',
    ejsVarInject: {
      markup: '<%- markup -%>',
      initialState: '<%- initialState -%>'
    },
    inject: false,
    minify: {
      removeComments: true,
      collapseWhitespace: true
    }
  }),

  new MiniCssExtractPlugin({
    filename: `${CSS_OUTPUT_PATH}/[name].min.css`,
    chunkFilename: '[id].css'
  })
];

module.exports = {
  target: 'web',
  entry: {
    number: [POLYFILL_PATH, NUMBER_ENTRY_PATH]
  },
  output: {
    path: OUTPUT_PATH,
    publicPath: CDN_PB_FULL_PATH,
    filename: `${JS_OUTPUT_PATH}/[name].min.js`
  },
  externals: {
    react: 'React',
    'react-dom': 'ReactDOM'
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: false,
        terserOptions: {}
      })
    ]
  },
  plugins,
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader?limit=10000'
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: `${IMAGE_OUTPUT_PATH}/[name].[ext]`
            }
          }
        ]
      },
      {
        test: /\.scss/,
        exclude: [/node_modules/],
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      }
    ]
  }
};
