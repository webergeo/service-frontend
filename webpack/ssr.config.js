import { CDN_PB_FULL_PATH, IMAGE_OUTPUT_PATH, SERVER_ENTRY_PATH, SSR_POLYFILL_PATH, OUTPUT_PATH } from './constants';

const nodeExternals = require('webpack-node-externals');

module.exports = {
  entry: [SSR_POLYFILL_PATH, SERVER_ENTRY_PATH],
  output: {
    path: OUTPUT_PATH,
    filename: 'server.js',
    libraryTarget: 'commonjs2',
    publicPath: CDN_PB_FULL_PATH
  },
  target: 'node',
  mode: 'production',
  node: {
    //console: false,
    global: false,
    //process: false,
    //Buffer: false,
    __filename: false,
    __dirname: false
  },
  externals: nodeExternals(),
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: { limit: 10000},
      },
      // minify image files
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: `${IMAGE_OUTPUT_PATH}/[name].[ext]`,
              emitFile: false // We emit the files with the client.config
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        exclude: [/node_modules/, /(global\.scss)$/],
        use: ['css-loader', 'sass-loader']
      }
    ]
  },
  plugins: []
};
