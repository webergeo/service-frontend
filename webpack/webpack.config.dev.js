import {
  DEV_HOST,
  DEV_PORT,
  OUTPUT_PATH,
  CLIENT_ENTRY_PATH,
  IMAGE_OUTPUT_PATH,
  POLYFILL_PATH,
  NUMBER_ENTRY_PATH,
  KV_ENTRY_PATH,
  REGISTRATION_BANNER_ENTRY_PATH,
  POINTS_ENTRY_PATH
} from './constants';

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const { loadConfiguration } = require('../dev-settings/dev-config');

/**
 * This performs an early loading of environment variables from the .env file as determined by the value of
 * BACKEND_MODE environment variable. The environment will be subsequently exported into the browser by the DefinePlugin.
 *
 */
const backendMode = process.env.BACKEND_MODE;
if (typeof backendMode !== 'undefined') {
  console.info(`Backend mode:${backendMode}`);
  loadConfiguration(backendMode);
}

let indexTemplate;
let entry;
switch (process.env.DEV_MODE) {
  case 'number':
    indexTemplate = 'src/templates-dev/number-index.html';
    entry = {
      app: [POLYFILL_PATH, NUMBER_ENTRY_PATH]
    };
    break;
  case 'kv':
    indexTemplate = 'src/templates-dev/kv-index.html';
    entry = {
      app: [POLYFILL_PATH, KV_ENTRY_PATH]
    };
    break;
  case 'registrationBanner':
    indexTemplate = 'src/templates-dev/registrationBanner-index.html';
    entry = {
      app: [POLYFILL_PATH, REGISTRATION_BANNER_ENTRY_PATH]
    };
    break;
  case 'points':
    indexTemplate = 'src/templates-dev/points-index.html';
    entry = {
      app: [
        `webpack-dev-server/client?${DEV_HOST}:${DEV_PORT}`,
        'webpack/hot/dev-server',
        POLYFILL_PATH,
        POINTS_ENTRY_PATH
      ]
    };
    break;
  default:
    indexTemplate = 'src/templates-dev/mydata-index.html';
    entry = {
      app: [
        `webpack-dev-server/client?${DEV_HOST}:${DEV_PORT}`,
        'webpack/hot/dev-server',
        POLYFILL_PATH,
        CLIENT_ENTRY_PATH
      ]
    };
}

module.exports = {
  devtool: 'cheap-module-source-map',
  target: 'web',
  mode: 'development',
  entry,
  output: {
    path: OUTPUT_PATH,
    publicPath: '/',
    filename: '[name].js',
    devtoolModuleFilenameTemplate: 'webpack:///[absolute-resource-path]'
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: { limit: 10000},
      },
      // minify image files
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: `${IMAGE_OUTPUT_PATH}/[name].[ext]`
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        include: path.resolve(__dirname, '..', 'src'),
        exclude: [/node_modules/, /(global\.scss)$/],
        use: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(), // stops the build if there is an error.
   // new webpack.NamedModulesPlugin(),
    new HtmlWebpackPlugin({
      //hash: true,
      title: 'My Awesome application',
      myPageHeader: 'Hello World',

      template: indexTemplate,
      inject: 'body',
      filename: 'index.html'
    }),
    /* "globally" define the entire process environment configuration
     * to transparently support getEnvironmentConfig() calls and the like on the browser side
    */
    new webpack.DefinePlugin({
      'process.env': JSON.stringify(process.env),
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    }),
    new webpack.optimize.ModuleConcatenationPlugin()
  ]
};
