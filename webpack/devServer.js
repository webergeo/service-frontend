require('@babel/register');
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const webpackDevConfig = require('./webpack.config.dev');
const { getEnvironmentConfig } = require('../src/config/appConfig');
const { DEV_HOST, MOCK_API_PORT, DEV_PORT } = require('./constants');

const compiler = webpack(webpackDevConfig);
const webpackServerConfig = {
  contentBase: '../src',
  historyApiFallback: true,
  // enable HMR
  hot: true,
  // embed the webpack-dev-server runtime into the bundle
  inline: true,
  publicPath: webpackDevConfig.output.publicPath,
  open: true,
  proxy: {
    '/mydata/payback/redirectionendpoint': {
      target: `${getEnvironmentConfig('PAYBACK_SERVICE_BASE_URL')}`,
      secure: false,
      changeOrigin: true
    },
    '/pb/qa_oauth_accountbinding_rewe': {
      target: `${getEnvironmentConfig('PAYBACK_SERVICE_BASE_URL')}`,
      secure: false,
      changeOrigin: true
    },
    '/api/token': {
      target: `${getEnvironmentConfig('AUTH_SERVICE_BASE_URL')}`,
      secure: false,
      changeOrigin: true
    },
    '/api/customers/*': {
      target: `${getEnvironmentConfig('PAYBACK_SERVICE_BASE_URL')}`,
      secure: false,
      changeOrigin: true,
      // extrawurst for PB iframe token redirect
      router: {
        '/tokenredirect': `${DEV_HOST}:${MOCK_API_PORT}`
      }
    },
    '/api/payback/*': {
      target: `${getEnvironmentConfig('PAYBACK_SERVICE_BASE_URL')}`,
      secure: false,
      changeOrigin: true
    },
    '/api/receipts*': {
      target: `${getEnvironmentConfig('REWE_EBON_SERVICE_BASE_URL')}`,
      secure: false,
      changeOrigin: true
    }
  },
  stats: {
    colors: true,
    hash: false,
    timings: true,
    chunks: false,
    chunkModules: false,
    modules: false
  }
};

const server = new WebpackDevServer(compiler, webpackServerConfig);

server.listen(DEV_PORT, '0.0.0.0');
console.info('==> Forwarding Payback API calls to %s', getEnvironmentConfig('PAYBACK_SERVICE_BASE_URL'));
console.info('==> Forwarding Ebon API calls to %s', getEnvironmentConfig('REWE_EBON_SERVICE_BASE_URL'));
console.info('==> Forwarding auth calls to %s', getEnvironmentConfig('AUTH_SERVICE_BASE_URL'));
console.info('==> Server listening on port %s', DEV_PORT);
