import {
  VENDOR_SCRIPTS,
  CDN_PB_FULL_PATH,
  IMAGE_OUTPUT_PATH,
  CLIENT_ENTRY_PATH,
  OUTPUT_PATH,
  POLYFILL_PATH,
  JS_OUTPUT_PATH,
  CSS_OUTPUT_PATH
} from './constants';

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const plugins = [
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      SERVICE_PORT: JSON.stringify(process.env.SERVICE_PORT)
    }
  }),

  new webpack.optimize.ModuleConcatenationPlugin(),

  new HtmlWebpackPlugin({
    template: 'src/templates/mydata-paybackcontent.ejs',
    filename: 'templates/mydata-paybackcontent.ejs',
    ejsVarInject: {
      markup: '<%- markup -%>',
      initialState: '<%- initialState -%>',
      paybackMcApiScript: '<%- paybackMcApiScript -%>'
    },
    inject: false,
    minify: {
      removeComments: true,
      collapseWhitespace: true
    }
  }),

  new HtmlWebpackPlugin({
    template: 'src/templates/mydata-payback-dev.ejs',
    filename: 'templates/mydata-payback-dev.ejs',
    ejsVarInject: {
      markup: '<%- markup -%>',
      initialState: '<%- initialState -%>',
      paybackMcApiScript: '<%- paybackMcApiScript -%>'
    },
    inject: false,
    minify: {
      removeComments: true,
      collapseWhitespace: true
    }
  }),

  new CopyWebpackPlugin([
    { from: './src/templates/payback-gallery.ejs', to: 'templates' },
    { from: './src/templates/coupons.ejs', to: 'templates' },
    { from: './src/templates/coupons-mobile.ejs', to: 'templates' },
    { from: './src/templates/voucher.ejs', to: 'templates' },
    { from: './src/templates/voucher_disabled.ejs', to: 'templates' }
  ]),

  new MiniCssExtractPlugin({
    filename: `${CSS_OUTPUT_PATH}/[name].min.css`,
    chunkFilename: '[id].css'
  })
];

module.exports = {
  target: 'web',
  entry: {
    app: [POLYFILL_PATH, CLIENT_ENTRY_PATH],
    vendor: VENDOR_SCRIPTS
  },
  output: {
    path: OUTPUT_PATH,
    publicPath: CDN_PB_FULL_PATH,
    filename: `${JS_OUTPUT_PATH}/[name].min.js`
  },
  externals: {
    react: 'React',
    'react-dom': 'ReactDOM'
  },
  optimization: {
    splitChunks: {
      //  identifies common modules and put them into a commons vendor chunk
      name: 'vendor',
      filename: `${JS_OUTPUT_PATH}/vendor.min.js`,
      minChunks: Infinity
    },
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: false,
        terserOptions: {}
      })
    ]
  },
  plugins,
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader?limit=10000'
      },
      // minify image files
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: `${IMAGE_OUTPUT_PATH}/[name].[ext]`
            }
          }
          // temporarily disabled until docker / MacOS build problems are resolved
          // {
          //   loader: 'image-webpack-loader',
          //   options: {
          //     bypassOnDebug: true,
          //     mozjpeg: {
          //       enabled: false
          //     },
          //     optipng: {
          //       optimizationLevel: 7
          //     },
          //     pngquant: {
          //       quality: '90',
          //       speed: 3
          //     },
          //     svgo: {
          //       // some SVGs are currently causing trouble here so this optimizer is disabled for now
          //       enabled: false,
          //       removeMetadata: true,
          //       cleanupAttrs: true,
          //       removeComments: true
          //     }
          //   }
          // }
        ]
      },
      {
        test: /\.scss/,
        exclude: [/node_modules/],
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      }
    ]
  }
};
