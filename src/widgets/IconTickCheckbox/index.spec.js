import test from 'ava';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';
import IconTickCheckbox from '../IconTickCheckbox';

configure({ adapter: new Adapter() });

const iconTickCheckboxComponent = props => <IconTickCheckbox labelText="default label" handleCheckboxClick={() => { }} {...props && { ...props }} />;

test('renders default IconCheckCheckbox with checked state', (t) => {
  const optedInBlock = render.create(iconTickCheckboxComponent({ checkboxId: 'default-checkbox' })).toJSON();
  t.snapshot(optedInBlock);
  const wrapper = mount(iconTickCheckboxComponent({ checkboxId: 'default-checkbox' }));
  t.is(wrapper.instance().props.isChecked, false);
});

test('uncheck the checked checkbox', (t) => {
  const wrapper = mount(iconTickCheckboxComponent({ checkboxId: 'unchecked-checkbox', isChecked: true }));
  t.is(wrapper.instance().props.isChecked, true);
});

test('correct labeltext is passed to checkbox label', (t) => {
  const wrapper = mount(iconTickCheckboxComponent({ checkboxId: 'dummy-checkboxId', labelText: 'dummy label' }));
  t.is(wrapper.instance().props.labelText, 'dummy label');
});
