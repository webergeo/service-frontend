import PropTypes from 'prop-types';
import React, { Component } from 'react';
import './IconTickCheckbox.scss';

class IconTickCheckbox extends Component {
  constructor() {
    super();
    this.toggleCheckboxClick = this.toggleCheckboxClick.bind(this);
  }

  toggleCheckboxClick(e) {
    const { handleCheckboxClick } = this.props;
    handleCheckboxClick({ checked: e.target.checked });
  }

  render() {
    const { labelText, checkboxId, isChecked } = this.props;
    return (
      <label htmlFor={checkboxId} className="rs-payback-form__checkbox-icon-label">
        <input
          type="checkbox"
          name={checkboxId}
          checked={isChecked}
          id={checkboxId}
          onChange={e => this.toggleCheckboxClick(e)}
        />
        <IconTick /> {labelText}
      </label>
    );
  }
}

const IconTick = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="#000000">
    <path d="M10 14.586l8.293-8.293a1 1 0 0 1 1.414 1.414l-9 9a.997.997 0 0 1-1.414 0l-5-5a1 1 0 0 1 1.414-1.414L10 14.586z"/>
  </svg>
);

IconTickCheckbox.defaultProps = {
  isChecked: false
};

IconTickCheckbox.propTypes = {
  handleCheckboxClick: PropTypes.func.isRequired,
  isChecked: PropTypes.bool,
  labelText: PropTypes.string.isRequired,
  checkboxId: PropTypes.string.isRequired
};

export default IconTickCheckbox;
