import test from 'ava';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import Accordion from '../Accordion';

configure({ adapter: new Adapter() });

const Content = () => (
  <div className="container1">
    <h1>This is a content title for accordion</h1>
    <p>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed
      diam voluptua.
    </p>
  </div>
);

const defaultComponent = () => (
  <Accordion title="Dummy Accordion">
    <Content />
  </Accordion>
);

test('renders the default Accordion Component', (t) => {
  const accordionTree = render.create(defaultComponent()).toJSON();
  t.snapshot(accordionTree);
  const wrapper = shallow(defaultComponent());
  t.is(wrapper.instance().props.title, 'Dummy Accordion');
  t.is(wrapper.instance().props.alternateTitle, '');
  t.is(wrapper.instance().props.initialPanelState, false);
  t.is(wrapper.instance().state.open, false);
});

test('show the accordion content with alternative title when clicked', (t) => {
  const wrapper = shallow(
    <Accordion title="Dummy Accordion" alternateTitle="I am different">
      <Content />
    </Accordion>);
  t.is(wrapper.instance().state.open, false);
  t.is(wrapper.instance().props.alternateTitle, 'I am different');
  wrapper.find('.rs-accordion__link').simulate('click', { preventDefault: () => undefined });
  t.is(wrapper.instance().state.open, true);
  t.is(wrapper.find('.rs-accordion__link').text(), 'I am different');
});

test('render the children when passed in', (t) => {
  const wrapper = shallow(
    <Accordion title="Dummy Accordion" alternateTitle="I am different">
      <Content />
    </Accordion>);
  t.is(wrapper.find(Content).length, 1);
});
