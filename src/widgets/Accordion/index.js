import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classNames from 'classnames';

import './Accordion.scss';

class Accordion extends Component {
  constructor(props) {
    super(props);
    this.state = { open: null };
    this.onAccordionClick = this.onAccordionClick.bind(this);
  }

  componentWillMount() {
    const { initialPanelState } = this.props;
    this.setState({ open: initialPanelState });
  }

  onAccordionClick(e) {
    e.preventDefault();
    const { open } = this.state;
    this.setState({ open: !open });
  }

  renderTitle() {
    const { open } = this.state;
    const { alternateTitle, title } = this.props;
    return (
      <div className="rs-accordion__title">
        <a
          href="#rs-accordion"
          className="rs-accordion__link rs-qa-accordion__link"
          onClick={this.onAccordionClick}
        >
          {open && alternateTitle !== '' ? alternateTitle : title}
        </a>
      </div>
    );
  }

  renderContent() {
    return (
      <div className="rs-accordion__content">
        {this.props.children}
      </div>
    );
  }

  render() {
    const { open } = this.state;
    const accordionContainerClass = classNames(
      'rs-accordion',
      open ? 'rs-accordion--active' : null
    );
    return (
      <div className={accordionContainerClass}>
        {this.renderTitle()}
        {this.renderContent()}
      </div>
    );
  }
}

Accordion.propTypes = {
  alternateTitle: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string
  ]).isRequired,
  initialPanelState: PropTypes.bool
};

Accordion.defaultProps = { alternateTitle: '', initialPanelState: false };

export default Accordion;
