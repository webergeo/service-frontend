import test from 'ava';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import RSInfoIcon from '../RSInfoIcon';

const rsInfoIconComponent = props => <RSInfoIcon {...props && { ...props }} />;

configure({ adapter: new Adapter() });
test('renders the default snapshot', (t) => {
  const modalTree = render.create(rsInfoIconComponent()).toJSON();
  t.snapshot(modalTree);
});

test('renders default props', (t) => {
  const wrapper = shallow(rsInfoIconComponent());
  t.is(wrapper.props().height, 16);
  t.is(wrapper.props().width, 16);
});

test('renders the single svg with path elements', (t) => {
  const wrapper = shallow(rsInfoIconComponent());
  t.is(wrapper.find('svg').length, 1);
  t.is(wrapper.find('path').length, 2);
});

test('renders the RSInfoIcon with matching dimension', (t) => {
  const wrapper = shallow(rsInfoIconComponent({ height: 20, width: 25 }));
  t.is(wrapper.props().height, 20);
  t.is(wrapper.props().width, 25);
});
