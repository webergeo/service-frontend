import PropTypes from 'prop-types';
import React from 'react';
import './RSInfoIcon.scss';

const RSInfoIcon = ({ height, width, stroke, styles }) => (
  <svg className={styles} xmlns="http://www.w3.org/2000/svg" height={height} width={width} viewBox={(-stroke) + ' ' + (-stroke) + ' ' + (580 + stroke) + ' ' + (580 + stroke)}>
    <path
      className="rs-payback-infoicon-circle"
      fillRule="evenodd"
      clipRule="evenodd"
      //eslint-disable-next-line
      d="M290 3.743C131.905 3.743 3.743 131.905 3.743 290S131.905 576.257 290 576.257 576.257 448.095 576.257 290 448.095 3.743 290 3.743"
    />

    <path
      className="rs-payback-infoicon-info"
      fillRule="evenodd"
      clipRule="evenodd"
      //eslint-disable-next-line
      d="M290 3.743zm35.782 465.168h-71.564V218.436h71.564v250.475zm0-286.257h-71.564V111.09h71.564v71.564z"
    />
  </svg>
);

RSInfoIcon.propTypes = {
  height: PropTypes.number,
  width: PropTypes.number,
  styles: PropTypes.string,
  stroke: PropTypes.number
};

RSInfoIcon.defaultProps = {
  height: 16,
  width: 16,
  stroke: 0,
  styles: 'rs-infoicon'
};

export default RSInfoIcon;
