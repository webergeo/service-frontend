import test from 'ava';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import LoadingSpinner from '.';
import RSLoadingSpinnerIcon from './RSLoadingSpinnerIcon';

configure({ adapter: new Adapter() });
const loadingSpinnerComponent = () => <LoadingSpinner />;

test('renders the default snapshot', (t) => {
  const modalTree = render.create(loadingSpinnerComponent()).toJSON();
  t.snapshot(modalTree);
});

test('always renders the div with class "rs-loading-spinner"', (t) => {
  const wrapper = shallow(loadingSpinnerComponent());
  t.is(wrapper.find('div.rs-loading-spinner').length, 1);
});

test('always renders RSLoadingSpinnerIcon component as child of "rs-loading-spinner__svg" div', (t) => {
  const wrapper = shallow(loadingSpinnerComponent());
  const loadingSpinnerSVGDiv = wrapper.find('div.rs-loading-spinner__svg');
  t.is(loadingSpinnerSVGDiv.children(RSLoadingSpinnerIcon).length, 1);
});

test('renders 18 fading bubble animation of type span elements', (t) => {
  const wrapper = shallow(loadingSpinnerComponent());
  const fadingBubbles = wrapper.find('div.rs-fading-bubbles');
  t.is(fadingBubbles.find('span').length, 18);
});
