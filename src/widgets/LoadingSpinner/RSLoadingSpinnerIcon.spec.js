import test from 'ava';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import RSLoadingSpinnerIcon from './RSLoadingSpinnerIcon';

const rsLoadingSpinnerIconComponent = () => <RSLoadingSpinnerIcon />;

configure({ adapter: new Adapter() });

test('renders the default snapshot', (t) => {
  const modalTree = render.create(rsLoadingSpinnerIconComponent()).toJSON();
  t.snapshot(modalTree);
});

test('contains no props', (t) => {
  const wrapper = mount(rsLoadingSpinnerIconComponent());
  t.deepEqual(wrapper.props(), {});
});

test('renders the single svg with path element', (t) => {
  const wrapper = shallow(rsLoadingSpinnerIconComponent());
  t.is(wrapper.find('svg').length, 1);
  t.is(wrapper.find('path').length, 1);
});
