import React from 'react';

import { arrayGenerator } from '../../utils/commons';
import RSLoadingSpinnerIcon from './RSLoadingSpinnerIcon';

import './LoadingSpinner.scss';

const LoadingSpinner = () => {
  const bubbleItems = arrayGenerator(18).map((_, i) => (
    <span
      // eslint-disable-next-line
      key={`rs-fading-bubbles__bubble_${i}`}
      className={
        `rs-fading-bubbles__bubble--${i + 1} rs-fading-bubbles__bubble`
      }
    />
  ));
  return (
    <div className="rs-loading-spinner">
      <div className="rs-loading-spinner__background" />
      <div className="rs-loading-spinner__svg">
        <RSLoadingSpinnerIcon />
      </div>
      <div className="rs-fading-bubbles">
        {bubbleItems}
      </div>
    </div>
  );
};

export default LoadingSpinner;
