import React from 'react';
import PropTypes from 'prop-types';

import { Button } from '../../widgets';
import Accordion from '../Accordion';
import OptedBlock from '../OptedBlock';

const OptedOutBlock = ({
  accordionProperties,
  blockDescription,
  optedOutButtonLabel,
  handleOptedOutButtonClick,
  children: optedOutContent
}) => (
  <OptedBlock>
    <p>{blockDescription || optedOutContent}</p>
    <Button handleClick={handleOptedOutButtonClick} big color="green" label={optedOutButtonLabel} target="_blank" />
    {accordionProperties && <Accordion {...accordionProperties}>{optedOutContent}</Accordion>}
  </OptedBlock>
);

OptedOutBlock.defaultProps = {
  blockDescription: '',
  accordionProperties: null
};

OptedOutBlock.propTypes = {
  blockDescription: PropTypes.string,
  optedOutButtonLabel: PropTypes.string.isRequired,
  handleOptedOutButtonClick: PropTypes.func.isRequired,
  accordionProperties: PropTypes.shape({
    alternateTitle: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
  }),
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default OptedOutBlock;
