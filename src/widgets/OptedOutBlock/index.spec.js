import test from 'ava';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import OptedOutBlock from '../OptedOutBlock';

const defaultComponent = () => {
  const accordionProperties = {
    alternateTitle: 'Weniger Informationen',
    title: 'Mehr Informationen'
  };
  return (
    <OptedOutBlock
      accordionProperties={accordionProperties}
      optedOutButtonLabel="Dummy Button Label"
      blockDescription="Dummy description"
      handleOptedOutButtonClick={() => { }}
    >
      <div>Dummy optedout content</div>
    </OptedOutBlock>
  );
};

configure({ adapter: new Adapter() });
test('renders default OptedOutBlock component', (t) => {
  const optedOutBlock = render.create(defaultComponent()).toJSON();
  t.snapshot(optedOutBlock);
});

test('renders OptedBlock as parent block', (t) => {
  const wrapper = shallow(defaultComponent());
  t.is(wrapper.find('OptedBlock').length, 1);
});

test('renders Accordion as child component', (t) => {
  const wrapper = shallow(defaultComponent());
  t.is(wrapper.find('Accordion').length, 1);
});

test('renders Button as child component', (t) => {
  const wrapper = shallow(defaultComponent());
  t.is(wrapper.find('Button').length, 1);
});

test('renders received children as accordion content', (t) => {
  const wrapper = mount(defaultComponent());
  const accordionChildren = wrapper.find('Accordion').props().children;
  t.deepEqual(wrapper.props().children, accordionChildren);
});
