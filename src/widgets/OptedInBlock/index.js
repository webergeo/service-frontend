import PropTypes from 'prop-types';
import React from 'react';

import OptedBlock from '../OptedBlock';
import Accordion from '../Accordion';

const OptedInBlock = ({ accordionProperties, checkboxGroup, children: optedInContent }) => (
  <OptedBlock>
    {checkboxGroup}
    <Accordion {...accordionProperties}>
      {optedInContent}
    </Accordion>
  </OptedBlock>
);

OptedInBlock.propTypes = {
  checkboxGroup: PropTypes.element.isRequired,
  accordionProperties: PropTypes.shape({
    alternateTitle: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
  }).isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default OptedInBlock;
