import test from 'ava';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import OptedInBlock from '../OptedInBlock';
import CheckboxGroup from '../CheckboxGroup';

const defaultComponent = () => {
  const accordionProperties = {
    alternateTitle: 'Weniger Informationen',
    title: 'Mehr Informationen'
  };
  const checkboxGroup = <CheckboxGroup labelText="Dummy label" checkboxId="dummyId" handleCheckboxClick={() => { }} />;
  return (
    <OptedInBlock accordionProperties={accordionProperties} checkboxGroup={checkboxGroup}>
      <div>Dummy optedin content</div>
    </OptedInBlock>
  );
};

configure({ adapter: new Adapter() });
test('renders default OptedInBlock component', (t) => {
  const optedInBlock = render.create(defaultComponent()).toJSON();
  t.snapshot(optedInBlock);
});

test('renders OptedBlock as parent block', (t) => {
  const wrapper = shallow(defaultComponent());
  t.is(wrapper.find('OptedBlock').length, 1);
});

test('renders Accordion as child component', (t) => {
  const wrapper = shallow(defaultComponent());
  t.is(wrapper.find('Accordion').length, 1);
});

test('renders CheckboxGroup as child component', (t) => {
  const wrapper = shallow(defaultComponent());
  t.is(wrapper.find('CheckboxGroup').length, 1);
});

test('renders received children as accordion content', (t) => {
  const wrapper = mount(defaultComponent());
  const accordionChildren = wrapper.find('Accordion').props().children;
  t.deepEqual(wrapper.props().children, accordionChildren);
});
