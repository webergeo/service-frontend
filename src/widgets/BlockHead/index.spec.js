import test from 'ava';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import BlockHead from '../BlockHead';

const BLOCKHEAD_TITLE_CLASS = '.rs-qa-payback__section-headline';
const BLOCKHEAD_SUBTITLE_CLASS = '.rs-qa-ribbon';

configure({ adapter: new Adapter() });
test('renders the default BlockHead Component', (t) => {
  const blockHeadTree = render
    .create(<BlockHead title="Awesome BlockHead title" />)
    .toJSON();
  t.snapshot(blockHeadTree);
  const wrapper = mount(<BlockHead title="Awesome BlockHead title" />);
  t.is(wrapper.props().title, 'Awesome BlockHead title');
  t.is(wrapper.find(BLOCKHEAD_TITLE_CLASS).length, 1);
  t.is(wrapper.find(BLOCKHEAD_SUBTITLE_CLASS).length, 0);
});

test('renders the BlockHead Component with title and subtitle', (t) => {
  const wrapper = mount(<BlockHead title="Awesome BlockHead title" subTitle="I am a subtitle" />);
  t.is(wrapper.props().title, 'Awesome BlockHead title');
  t.is(wrapper.props().subTitle, 'I am a subtitle');
  t.is(wrapper.find(BLOCKHEAD_TITLE_CLASS).length, 1);
  t.is(wrapper.find(BLOCKHEAD_SUBTITLE_CLASS).length, 1);
});
