import PropTypes from 'prop-types';
import React from 'react';

import './BlockHead.scss';

/**
 * Create a reusable blockhead component
 * @param {String}  title       - title of head component
 * @param {String}  subTitle    - optional subtitle
 */
const BlockHead = ({ title, subTitle }) => {
  const titleLayout = (
    <h3 className="rs-payback__section-headline rs-qa-payback__section-headline">
      {title}
    </h3>
  );
  const subTitleLayout = subTitle === ''
    ? null
    : (<span className="rs-ribbon rs-qa-ribbon">
      {subTitle}
    </span>);
  return (
    <div className="rs-payback__section-head">
      {titleLayout}
      {subTitleLayout}
    </div>
  );
};

BlockHead.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string
};

BlockHead.defaultProps = { subTitle: '' };

export default BlockHead;
