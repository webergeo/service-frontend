import test from 'ava';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import TeaserImgText from '../TeaserImgText';

const PAYBACK_TEASERIMGTEXT_HEADLINE = '.rs-qa-payback__teaserImgText-headline';
const PAYBACK_TEASERIMGTEXT_TEXT = '.rs-qa-payback__teaserImgText-text';
const PAYBACK_TEASERIMGTEXT_IMG = '.rs-qa-payback__teaserImgText-img';

const defaultComponent = props => <TeaserImgText {...props} />;

configure({ adapter: new Adapter() });
test('renders default TeaserImgText component', (t) => {
  const containerBlock = render.create(defaultComponent()).toJSON();
  t.snapshot(containerBlock);
});

test('renders the TeaserImgText Component with content', (t) => {
  const wrapper = mount(<TeaserImgText title="title" paragraph="Lorem Ipsum dolor" img="Image" />);
  t.is(wrapper.props().title, 'title');
  t.is(wrapper.props().paragraph, 'Lorem Ipsum dolor');
  t.is(wrapper.props().img, 'Image');
  t.is(wrapper.find(PAYBACK_TEASERIMGTEXT_HEADLINE).length, 1);
  t.is(wrapper.find(PAYBACK_TEASERIMGTEXT_TEXT).length, 1);
  t.is(wrapper.find(PAYBACK_TEASERIMGTEXT_IMG).length, 1);
});
