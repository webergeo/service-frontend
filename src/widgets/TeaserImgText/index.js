import PropTypes from 'prop-types';
import React from 'react';

import './TeaserImgText.scss';

const TeaserImgText = (props) => {
  const { title = '', paragraph = '', img = '' } = props;

  return (
    <div className="rs-payback__teaserImgText">
      {img !== '' && (
        <div className="rs-payback__teaserImgText-imgContainer">
          <img src={img} className="rs-payback__teaserImgText-img rs-qa-payback__teaserImgText-img" alt="" />
        </div>
      )}
      <div className="rs-payback__teaserImgText-content">
        <div className="rs-payback__teaserImgText-textContainer">
          {title !== '' && <h2 className="rs-payback__teaserImgText-headline rs-qa-payback__teaserImgText-headline">{title}</h2>}
          {paragraph !== '' && <p className="rs-payback__teaserImgText-text rs-qa-payback__teaserImgText-text">{paragraph}</p>}
        </div>
      </div>
    </div>
  );
};

TeaserImgText.propTypes = {
  title: PropTypes.string,
  paragraph: PropTypes.string.isRequired,
  img: PropTypes.string.isRequired
};

export default TeaserImgText;
