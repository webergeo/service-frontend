import React from 'react';

import './LoadingIndicator.scss';

const LoadingIndicator = () => (
  <div className="lds-default">
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
  </div>
);

export default LoadingIndicator;
