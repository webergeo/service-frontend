import test from 'ava';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import OptedBlock from '../OptedBlock';

configure({ adapter: new Adapter() });
const defaultComponent = () => (
  <OptedBlock>
    <div>Dummy opted child1</div>
    <div>Dummy opted child2</div>
  </OptedBlock>);

test('render multiple children when passed in', (t) => {
  const optedBlock = render.create(defaultComponent()).toJSON();
  t.snapshot(optedBlock);
  const wrapper = shallow(defaultComponent());
  t.true(wrapper.contains(<div>Dummy opted child1</div>));
  t.true(wrapper.contains(<div>Dummy opted child2</div>));
  t.is(wrapper.props().children.length, 2);
});
