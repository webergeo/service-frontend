import PropTypes from 'prop-types';
import React from 'react';

import './OptedBlock.scss';

const OptedBlock = props => (
  <div className="rs-payback__section-subcontent">
    {props.children}
  </div>
);

OptedBlock.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default OptedBlock;
