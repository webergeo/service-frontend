import test from 'ava';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';
import CheckboxGroup from '../CheckboxGroup';

configure({ adapter: new Adapter() });

const checkboxGroupComponent = props => <CheckboxGroup labelText="default label" handleCheckboxClick={() => { }} {...props && { ...props }} />;

test('renders default CheckboxGroup with checked state', (t) => {
  const optedInBlock = render.create(checkboxGroupComponent({ checkboxId: 'default-checkbox' })).toJSON();
  t.snapshot(optedInBlock);
  const wrapper = mount(checkboxGroupComponent({ checkboxId: 'default-checkbox' }));
  t.is(wrapper.instance().props.isChecked, false);
});

test('uncheck the checked checkbox', (t) => {
  const wrapper = mount(checkboxGroupComponent({ checkboxId: 'unchecked-checkbox', isChecked: true }));
  t.is(wrapper.instance().props.isChecked, true);
});

test('correct labeltext is passed to checkbox label', (t) => {
  const wrapper = mount(checkboxGroupComponent({ checkboxId: 'dummy-checkboxId', labelText: 'dummy label' }));
  t.is(wrapper.instance().props.labelText, 'dummy label');
});
