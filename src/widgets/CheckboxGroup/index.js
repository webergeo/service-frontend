import PropTypes from 'prop-types';
import React, { Component } from 'react';

import './CheckboxGroup.scss';

class CheckboxGroup extends Component {
  constructor() {
    super();
    this.toggleCheckboxClick = this.toggleCheckboxClick.bind(this);
  }

  toggleCheckboxClick(e) {
    const { handleCheckboxClick } = this.props;
    e.preventDefault();
    handleCheckboxClick({ checked: e.target.checked });
  }

  render() {
    const { labelText, checkboxId, isChecked } = this.props;
    return (
      <div className="rs-payback-form__checkbox-group">
        <input
          type="checkbox"
          name={checkboxId}
          id={checkboxId}
          className="rs-payback-form__checkbox"
          checked={isChecked}
          onChange={e => this.toggleCheckboxClick(e)}
        />
        <label htmlFor={checkboxId} className="rs-payback-form__checkbox-label">
          {labelText}
        </label>
      </div>
    );
  }
}

CheckboxGroup.defaultProps = {
  isChecked: false
};

CheckboxGroup.propTypes = {
  handleCheckboxClick: PropTypes.func.isRequired,
  isChecked: PropTypes.bool,
  labelText: PropTypes.string.isRequired,
  checkboxId: PropTypes.string.isRequired
};

export default CheckboxGroup;
