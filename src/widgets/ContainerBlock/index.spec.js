import test from 'ava';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import ContainerBlock from '../ContainerBlock';

const BLOCKHEAD_TITLE_CLASS = '.rs-qa-payback__containerblock-headline';
const BLOCKHEAD_SUBTITLE_CLASS = '.rs-qa-payback__containerblock-subline';
const BLOCKHEAD_PARAGRAPH_CLASS = '.rs-qa-payback__containerblock-text';
const BLOCKHEAD_IMG_CLASS = '.rs-qa-payback__containerblock-img';

const Content = () => <div>Dummy content</div>;

const defaultComponent = props => (
  <ContainerBlock {...props}>
    <Content />
  </ContainerBlock>
);

configure({ adapter: new Adapter() });
test('renders default ContainerBlock component', (t) => {
  const containerBlock = render.create(defaultComponent()).toJSON();
  t.snapshot(containerBlock);
});

test('renders the ContainerBlock Component with title, subtitle, paragraph and img', (t) => {
  const wrapper = mount(
    <ContainerBlock title="Awesome BlockHead title" subtitle="I am a subtitle" paragraph="Lorem Ipsum dolor" img="Image">
      <Content />
    </ContainerBlock>);
  t.is(wrapper.props().title, 'Awesome BlockHead title');
  t.is(wrapper.props().subtitle, 'I am a subtitle');
  t.is(wrapper.props().paragraph, 'Lorem Ipsum dolor');
  t.is(wrapper.props().img, 'Image');
  t.is(wrapper.find(BLOCKHEAD_TITLE_CLASS).length, 1);
  t.is(wrapper.find(BLOCKHEAD_SUBTITLE_CLASS).length, 1);
  t.is(wrapper.find(BLOCKHEAD_PARAGRAPH_CLASS).length, 1);
  t.is(wrapper.find(BLOCKHEAD_IMG_CLASS).length, 1);
  t.is(wrapper.find(Content).length, 1);
});
