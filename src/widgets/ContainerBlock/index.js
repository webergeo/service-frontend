import PropTypes from 'prop-types';
import React from 'react';

import './ContainerBlock.scss';

const ContainerBlock = (props) => {
  const { title = '', subtitle = '', paragraph = '', img = '', nodivider = false, children } = props;

  const contentClass = nodivider
    ? 'rs-payback__containerblock-content rs-payback__containerblock-content--no-divider'
    : 'rs-payback__containerblock-content';

  return (
    <div className="rs-payback__containerblock bg-pbblue">
      <div className={contentClass}>
        <div className="rs-payback__containerblock-item">
          {title !== '' && <h2 className="rs-payback__containerblock-headline rs-qa-payback__containerblock-headline">{title}</h2>}
          {subtitle !== '' && <p className="rs-payback__containerblock-subline rs-qa-payback__containerblock-subline">{subtitle}</p>}
          {paragraph !== '' && <p className="rs-payback__containerblock-text rs-qa-payback__containerblock-text hidden-xs hidden-sm">{paragraph}</p>}
        </div>
        {img !== '' && (
          <div className="rs-payback__containerblock-item hidden-xs hidden-sm">
            <img src={img} className="rs-payback__containerblock-img rs-qa-payback__containerblock-img" alt="" />
          </div>
        )}
      </div>
      <div className="rs-payback__containerblock-content">{children}</div>
    </div>
  );
};

ContainerBlock.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  paragraph: PropTypes.string,
  img: PropTypes.string,
  nodivider: PropTypes.bool,
  children: PropTypes.element.isRequired
};

export default ContainerBlock;
