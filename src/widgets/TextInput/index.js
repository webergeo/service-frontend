import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import './TextInput.scss';

/* (!) The new TextInput Component is in /components */
const TextInput = ({ uniqueID, isDisabled, onTextValueChange, textInputValue, validation, placeHolderText, qaClassName, focus, personal }) => {
  const inputClass = classNames('rs-payback__text-input', qaClassName, {
    'rs-payback__text-input--invalid': validation && validation.error,
    'no-mouseflow': personal
  });
  const labelClass = classNames('rs-payback__text-label', 'rs-qa-payback__text-label');

  const optionalLabelParams = (validation && validation.error)
    ? { title: validation.message }
    : {};

  return (
    <div className="rs-payback-form__form-row rs-payback-form__form-row--input">
      <input
        className={inputClass}
        type="text"
        id={uniqueID}
        name="paybackNumber"
        onChange={onTextValueChange}
        value={textInputValue}
        required
        {...isDisabled && { readOnly: true }}
        autoFocus={focus}
        {...(personal && { 'data-mf-replace': '' })}
      />
      <label
        htmlFor={uniqueID}
        className={labelClass}
        data-label={placeHolderText}
        {...optionalLabelParams}
      >
        {validation && validation.error ? validation.message : placeHolderText}
      </label>
    </div>
  );
};

TextInput.propTypes = {
  uniqueID: PropTypes.string.isRequired,
  isDisabled: PropTypes.bool,
  onTextValueChange: PropTypes.func.isRequired,
  placeHolderText: PropTypes.string.isRequired,
  textInputValue: PropTypes.string,
  validation: PropTypes.shape({
    error: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired
  }),
  qaClassName: PropTypes.string,
  focus: PropTypes.bool,
  personal: PropTypes.bool
};

TextInput.defaultProps = {
  isDisabled: false,
  onTextValueChange: undefined,
  textInputValue: '',
  validation: {
    error: false,
    message: ''
  },
  qaClassName: '',
  focus: false,
  personal: false
};

export default TextInput;
