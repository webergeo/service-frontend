import test from 'ava';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import TextInput from '../TextInput';

const textInputComponent = props => (
  <TextInput placeHolderText="Awesome label" onTextValueChange={() => { }} uniqueID="some_unique_id" {...props && { ...props }} />
);

configure({ adapter: new Adapter() });
test('renders the default TextInput', (t) => {
  const modalTree = render.create(textInputComponent()).toJSON();
  t.snapshot(modalTree);
  const wrapper = mount(textInputComponent());
  t.is(wrapper.props().textInputValue, '');
  t.is(wrapper.props().placeHolderText, 'Awesome label');
  t.is(wrapper.props().uniqueID, 'some_unique_id');
  t.is(wrapper.props().qaClassName, '');
  t.false(wrapper.props().focus);
});

test('always renders the div with class "rs-payback-form__form-row"', (t) => {
  const wrapper = shallow(textInputComponent());
  t.true(wrapper.find('div.rs-payback-form__form-row').length > 0);
});

test('always renders the input with uniqueID', (t) => {
  const wrapper = mount(textInputComponent());
  const input = wrapper.find('input');
  t.is(input.props().id, wrapper.props().uniqueID);
});

test('always renders the matching label for input id', (t) => {
  const wrapper = shallow(textInputComponent());
  const input = wrapper.find('input');
  const label = wrapper.find('label');
  t.is(input.props().id, label.props().htmlFor);
});

test('renders the disabled input', (t) => {
  const wrapper = shallow(textInputComponent({ isDisabled: true }));
  const input = wrapper.find('input');
  t.true(input.props().readOnly);
});

test('renders the autoFocused input', (t) => {
  const wrapper = shallow(textInputComponent({ focus: true }));
  const input = wrapper.find('input');
  t.true(input.props().autoFocus);
});

test('renders the input with placeholder text', (t) => {
  const wrapper = mount(textInputComponent({ placeHolderText: 'Awesome placeholder' }));
  const label = wrapper.find('label');
  t.is(label.props().children, 'Awesome placeholder');
});

test('always render the data-label attribute of label same as placeHolderText', (t) => {
  const wrapper = mount(textInputComponent());
  const label = wrapper.find('label');
  t.is(wrapper.props().placeHolderText, label.props()['data-label']);
});

test('uses the correct class on validation error', (t) => {
  const wrapper = shallow(textInputComponent({ validation: { error: true, message: 'default error' } }));
  const input = wrapper.find('input');
  t.true(input.hasClass('rs-payback__text-input--invalid'));
});

test('on error, renders error message as label content', (t) => {
  const wrapper = shallow(textInputComponent({ validation: { error: true, message: 'default error' } }));
  const label = wrapper.find('label');
  t.is(label.props().children, 'default error');
});

test('applies the data-mf-replace attribute and no-mouseflow class iff input is personal', (t) => {
  const wrapper = shallow(textInputComponent({ personal: true }));
  const input = wrapper.find('input');
  t.true(input.props()['data-mf-replace'] === '');
  t.true(input.hasClass('no-mouseflow'));

  const wrapper2 = shallow(textInputComponent({ personal: false }));
  const input2 = wrapper2.find('input');
  t.true(input2.props()['data-mf-replace'] === undefined);
  t.false(input2.hasClass('no-mouseflow'));
});
