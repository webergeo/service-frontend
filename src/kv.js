import 'core-js/features/object/values';
import ReactDOM from 'react-dom';
import React from 'react';
import { parse } from 'query-string';
import logger from './utils/ducLogger';
import Kv from './components/Kv';

/* eslint-disable global-require */
if (process.env.NODE_ENV !== 'production') {
  require('./assets/scss/_development_fonts.scss');
  require('./assets/scss/_development_box_model.scss');
}
/* eslint-enable */

if (module.hot) {
  module.hot.accept();
}

window.addEventListener('load', () => {
  try {
    /* eslint-disable-next-line */
    const initialState = window.__INITIAL_PB_STATE__ || {};
    const query = parse(window.location.search);
    let status = Array.isArray(query.accountBindingStatus) ?
      query.accountBindingStatus[0] :
      query.accountBindingStatus;

    const buttons = document.querySelectorAll('.payback-kv-button');

    /* eslint-disable-next-line */
    for (let i = 0; i < buttons.length; i++) {
      ReactDOM.render(
        /* eslint-disable-next-line */
        <Kv {...initialState} accountBindingStatus={status} />,
        buttons[i]
      );

      // Hacky trick to only pass the status to the first button.
      status = undefined;
    }
  } catch (error) {
    logger.error(`Error during kv-app rendering. URL: ${window.location.href} Message: ${error.message}`);
  }
});
