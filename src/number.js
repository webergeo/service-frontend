import 'core-js/features/object/values';
import ReactDOM from 'react-dom';
import React from 'react';
import logger from './utils/ducLogger';

import NumberApp from './containers/NumberApp';

/* eslint-disable global-require */
if (process.env.NODE_ENV !== 'production') {
  require('./assets/scss/_development_fonts.scss');
  require('./assets/scss/_development_box_model.scss');
}
/* eslint-enable */

window.addEventListener('load', () => {
  try {
    const appMountPoint = window.document.getElementById('rs-payback-number');
    if (appMountPoint) {
      // eslint-disable-next-line no-underscore-dangle
      ReactDOM.render(<NumberApp {...window.__INITIAL_PB_STATE__} />, appMountPoint);
    }
  } catch (error) {
    logger.error(`Error during number-app rendering. URL: ${window.location.href} Message: ${error.message}`);
  }
});
