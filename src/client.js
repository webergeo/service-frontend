import 'core-js/features/object/values';
import ReactDOM from 'react-dom';
import React from 'react';
import { parse } from 'query-string';
import logger from './utils/ducLogger';
import App from './containers/AppMY';

/* eslint-disable global-require */
if (process.env.NODE_ENV !== 'production') {
  require('./assets/scss/_development_fonts.scss');
  require('./assets/scss/_development_box_model.scss');
}
/* eslint-enable */

window.addEventListener('load', () => {
  try {
    const appMountPoint = window.document.getElementById('rs-payback');
    if (appMountPoint) {
      // eslint-disable-next-line no-underscore-dangle
      ReactDOM.render(<App {...window.__INITIAL_PB_STATE__} defaultQueryParams={parse(window.location.search)} />, appMountPoint);
    }
  } catch (error) {
    logger.error(`Error during app rendering. URL: ${window.location.href} Message: ${error.message}`);
  }
});
