/* global __INITIAL_PB_STATE__ */
//import { env } from 'process';
const { env } = require('process')

const isProduction = process.env.NODE_ENV === 'production';

/**
 * Retrieves an environment variable by specified key.
 *
 * In production mode:
 * - Will try to transparently check first if the global "PB initial state" object is defined
 * in the browser context and see if the variable is available there. When operating in server mode
 * the actual process environment is examined.
 *
 * In developement mode (in the browser) a somewhat unusual mechanism is responsible for this method:
 * As the server-side process environment is of course not avaialable in the browser, it is instead artificially
 * exposed as a "compile-time" JS object by Webpack (using the define plugin). This make the entire process.env "globally"
 * available in the browser, and this method transparently accesses this object in the last return statement below.
 *
 * For details on ENV configuration loading see the webpack.config.dev.js
 *
 * Will throw an error if the specified key cannot be found in any of the above locations.
 *
 * @param {*} key name of an environment parameter.
 * @returns {object} - configuration for matching key
 */
export const getEnvironmentConfig = (key) => {
  if (isProduction) {
    if (typeof __INITIAL_PB_STATE__ !== 'undefined') {
      return __INITIAL_PB_STATE__.environment[key];
    } else if (typeof env[key] !== 'undefined') {
      // console.info(`Key: ${key} -> env[value]: ${env[key]}`);
      // console.info(`Key: ${key} -> process.env[value]: ${process.env[key]}`);
      return env[key];
    }
    throw new Error(`${key} Environment variable is not configured`);
  }
  // careful with this! "process.env" is exactly how it is supposed to be here as Webpack's DefinePlugin exports
  // the environment under this precise name!
  return process.env[key];
};

export const isEnvParameterTruthy = (key) => {
  const value = getEnvironmentConfig(key);
  return value === true || value === 'true' || value === '1';
};

/**
 * Checks whether an environment variable is defined. Semantics similar to getEnvironmentConfig(), but this method is
 * exception-safe (i.e. it will never thrown an error), and will simply return false if it can't find a variable.
 *
 * @param {*} key
 */
export const isEnvParameterDefined = (key) => {
  let defined = false;
  if (isProduction) {
    if (typeof __INITIAL_PB_STATE__ !== 'undefined') {
      defined = typeof __INITIAL_PB_STATE__.environment[key] !== 'undefined';
    } else {
      defined = typeof env[key] !== 'undefined';
    }
  } else {
    // dev mode
    defined = typeof process.env[key] !== 'undefined';
  }
  return defined;
};
