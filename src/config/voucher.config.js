export default {
  voucherAmounts: [2, 5, 10, 20],
  showIndividualVoucher: true,
  decimalPlacesForbidden: false
};
