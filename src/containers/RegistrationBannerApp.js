import React from 'react';
import '../assets/scss/base.scss';
import RegistrationBanner from '../meso-components/RegistrationBanner';
import PropTypes from "prop-types";
import PaybackTeaserBlock from "../components/PaybackTeaserBlock";


const RegistrationBannerApp = (props) => (
  <div>
    {props.layout === 'checkout' ? <RegistrationBanner /> : <PaybackTeaserBlock kv={false} />}
  </div>);

RegistrationBannerApp.propTypes = {
  layout: PropTypes.string
};

RegistrationBannerApp.defaultProps = {
  layout: 'checkout'
};

export default RegistrationBannerApp;
