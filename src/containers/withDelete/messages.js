const MESSAGE = {
  error: {
    fallbackTitle: 'Es sind Fehler aufgetreten.'
  },
  delete: {
    title: 'Möchten Sie die PAYBACK Kundennummer wirklich aus Ihrem REWE Kundenkonto entfernen?',
    withoutAccountBinding: 'Damit stehen Ihnen folgende PAYBACK Services auf REWE.de nicht mehr zur Verfügung: automatisch Punkte sammeln bei' +
      ' jedem Einkauf.',
    items: ['automatisch Punkte sammeln bei jedem Einkauf', 'Punktestand einsehen', 'persönliche eCoupons erhalten', 'Guthaben aktivieren', 'REWE Bonus Coupon',
      'REWE eBon'],
    main: 'Damit stehen Ihnen folgende PAYBACK Services nicht mehr zur Verfügung:',
    bottom: 'Sofern Sie das Marketing Einverständnis erteilt haben, wird dieses ebenfalls automatisch widerrufen.',
    bottomHint: 'Sollten Sie REWE gegenüber keine aktuelle Einwilligung in Werbung und Marktforschung erteilt haben, stehen Ihnen damit die personalisierten Wochenangebote am REWE Service-Punkt nicht mehr zur Verfügung.',
    success: 'Ihre PAYBACK Kundennummer wurde erfolgreich aus Ihrem REWE Kundenkonto entfernt!',
    successContent: 'Ab jetzt stehen Ihnen die PAYBACK Services (z.B. eBon) nicht mehr zur Verfügung. Sofern Sie das Marketing Einverständnis' +
      ' erteilt haben, wurde dies automatisch widerrufen.'
  },
  deleteAfterUnbind: {
    main: 'Möchten Sie auch Ihre PAYBACK Kundennummer aus Ihrem REWE Kundenkonto entfernen?',
    top: 'Damit stehen Ihnen folgende PAYBACK Services auf REWE.de nicht mehr zur Verfügung: automatisch Punkte sammeln bei jedem Einkauf'
  },
  unbind: {
    main: 'Möchten Sie wirklich auf PAYBACK Services auf REWE.de verzichten?',
    top: 'Damit stehen Ihnen folgende PAYBACK Services nicht mehr zur Verfügung:',
    items: ['automatisch Punkte sammeln bei jedem Einkauf', 'Punktestand einsehen', 'persönliche eCoupons erhalten', 'Guthaben aktivieren', 'REWE Bonus Coupon', 'REWE eBon'],
    bottom: 'Sofern Sie das Marketing Einverständnis erteilt haben, wird dieses ebenfalls automatisch widerrufen.',
    bottomHint: 'Sollten Sie REWE gegenüber keine aktuelle Einwilligung in Werbung und Marktforschung erteilt haben, stehen Ihnen damit die personalisierten Wochenangebote am REWE Service-Punkt nicht mehr zur Verfügung.',
    success_message_content: `Ab jetzt stehen Ihnen die PAYBACK Services (z.B. eBon) nicht mehr zur Verfügung. 
      Sofern Sie das Marketing Einverständnis erteilt haben, wurde dies automatisch widerrufen.`
  }
};

export default MESSAGE;
