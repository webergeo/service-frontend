import React from 'react';
import PropTypes from 'prop-types';

import LoadingSpinner from '../../widgets/LoadingSpinner';
import Modal from '../../components/Modal';
import { deletePaybackAccountBinding, deletePaybackAccountInfo } from '../../api/Accounts';
import { HTTP_OK, HTTP_FORBIDDEN, HTTP_NO_CONTENT } from '../../utils/constants';
import { parsePaybackErrorMessage } from '../../utils/apiResponseFormatter';
import logger from '../../utils/ducLogger';
import MESSAGE from './messages';

// displayName returns a non-empty (display) name for Component.
const displayName = Component => (Component && (Component.displayName || Component.name)) || 'Component';

// withDelete returns a component that provides WrappedComponent with
// callbacks to unbind a PAYBACK account from a REWE account and delete a
// PAYBACK number from a REWE account. WrappedComponent may be undefined or
// null.
const withDelete = (WrappedComponent) => {
  class WithDelete extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        // spinning indicates a pending response from the back-end.
        // Render shows a full-screen LoadingSpinner if spinning is truthy.
        spinning: false,
        // modal is the active modal component, if any.
        // Render includes the modal if it is truthy.
        modal: undefined
      };

      this.showSpinner = this.showSpinner.bind(this);
      this.showModal = this.showModal.bind(this);
      this.done = this.done.bind(this);
      this.doneUpdate = this.doneUpdate.bind(this);
      this.handleDelete = this.handleDelete.bind(this);
      this.handleDeleteAfterUnbind = this.handleDeleteAfterUnbind.bind(this);
      this.handleUnbind = this.handleUnbind.bind(this);
      this.deletePaybackNumber = this.deletePaybackNumber.bind(this);
      this.unbindPaybackAccount = this.unbindPaybackAccount.bind(this);
    }

    // showSpinner enables the spinner and disables any modal.
    showSpinner() {
      this.setState({
        spinning: true,
        modal: undefined
      });
    }

    // showModal disables the spinner and shows a Modal with the given props.
    showModal(modalProps) {
      this.setState({
        spinning: false,
        modal: <Modal handleClose={this.done} handleSubmit={this.done} handleCancel={this.done} {...modalProps} />
      });
    }

    // done disables any spinner and modal.
    done() {
      this.setState({
        spinning: false,
        modal: undefined
      });
    }

    // doneUpdate disables any spinner and modal, and updates the parent
    // component.
    doneUpdate() {
      this.done();
      this.props.onUpdateParentView();
    }

    // handleDelete shows a Modal that asks the user to confirm his wish to
    // delete the PAYBACK number from his account.
    handleDelete() {
      const { paybackAccountBound } = this.props.accountInfo;

      const content = !paybackAccountBound ? MESSAGE.delete.withoutAccountBinding : (
        <div>
          <p>{MESSAGE.delete.main}</p>
          <ul className="rs-modal__list">
            {MESSAGE.delete.items instanceof Array ? (
              MESSAGE.delete.items.map(item => (
                <li className="rs-modal__listItem" key={item}>
                  <span className="rs-modal__listItemBullet" />{item}
                </li>
              ))
            ) : (
              <li />
            )}
          </ul>
          <p>{MESSAGE.delete.bottom}</p>
          <p>{MESSAGE.delete.bottomHint}</p>
        </div>
      );

      this.showModal({
        title: MESSAGE.delete.title,
        content,
        type: 'warning',
        handleSubmit: () => { this.deletePaybackNumber(false); }
      });
    }

    // handleDelete shows a Modal that asks the user whether he wishes to
    // delete the PAYBACK number from his REWE account -- after he has
    // unbound his payback account.
    handleDeleteAfterUnbind() {
      this.showModal({
        title: MESSAGE.deleteAfterUnbind.main,
        content: MESSAGE.deleteAfterUnbind.top,
        type: 'warning',
        handleSubmit: () => { this.deletePaybackNumber(true); }
      });
    }

    // handleUnbind shows a Modal that asks the user to confirm his wish to
    // unbind his REWE account from his PAYBACK account.
    handleUnbind() {
      const content = (
        <div>
          <p>{MESSAGE.unbind.top}</p>
          <ul className="rs-modal__list">
            {MESSAGE.unbind.items instanceof Array ? (
              MESSAGE.unbind.items.map((i, v) => (
                <li className="rs-modal__listItem" key={i}>
                  <span className="rs-modal__listItemBullet" />
                  {MESSAGE.unbind.items[v]}
                </li>
              ))
            ) : (
              <li />
            )}
          </ul>
          <p>{MESSAGE.unbind.bottom}</p>
          <p>{MESSAGE.unbind.bottomHint}</p>
        </div>
      );
      this.showModal({
        title: MESSAGE.unbind.main,
        content,
        type: 'warning',
        handleSubmit: this.unbindPaybackAccount
      });
    }

    // deletePaybackNumber asks the back-end to delete the PAYBACK number
    // and subsequently shows either a success or error Modal.
    async deletePaybackNumber(afterUnbind = false) {
      const { paybackAccountBound } = this.props.accountInfo; // We need the initial state before anything happens
      try {
        this.showSpinner();
        const response = await deletePaybackAccountInfo();
        await this.props.onUpdateParentView();

        if (response.status === HTTP_NO_CONTENT) {
          this.showModal({
            title: MESSAGE.delete.success,
            content: (paybackAccountBound && !afterUnbind) ? MESSAGE.delete.successContent : '',
            type: 'success'
          });
        } else if (response.status >= 400) { // When error occurred
          if (response.status === HTTP_FORBIDDEN) {
            this.done();
          } else {
            const errorMessage = await response.json();
            this.showModal({
              title: MESSAGE.error.fallbackTitle,
              content: parsePaybackErrorMessage(errorMessage),
              type: 'error'
            });
          }
        } else {
          logger.warn(`Unexpected HTTP status code in response to payback number deletion request: ${response.status}`);
          this.done();
        }
      } catch (error) {
        logger.error(`Failed to delete payback number, error: ${error.message}`);
        this.showModal({
          title: MESSAGE.error.fallbackTitle,
          content: MESSAGE.error.fallbackTitle,
          type: 'error',
          handleSubmit: this.doneUpdate
        });
      }
    }

    // unbindPaybackAccount asks the back-end to unbind the PAYBACK account
    // and subsequently shows either a success or error Modal.
    async unbindPaybackAccount() {
      const errorModalProps = error => ({
        title: MESSAGE.error.fallbackTitle,
        content: parsePaybackErrorMessage(error),
        type: 'error',
        showClose: false,
        handleSubmit: this.doneUpdate
      });

      try {
        this.showSpinner();
        const response = await deletePaybackAccountBinding();
        await this.props.onUpdateParentView();

        if (response.status === HTTP_OK) {
          this.showModal({
            title: MESSAGE.unbind.success_message_content,
            type: 'success',
            showClose: false,
            handleSubmit: this.handleDeleteAfterUnbind
          });
        } else if (response.status >= 400) {
          if (response.status === HTTP_FORBIDDEN) {
            this.done();
          } else {
            const error = await response.json();
            this.showModal(errorModalProps(error));
          }
        } else {
          logger.warn(`Unexpected HTTP status code in response to payback account unbinding request: ${response.status}`);
          this.done();
        }
      } catch (error) {
        logger.error(`Failed to unbind payback account, error: ${error.message}`);
        this.showModal(errorModalProps(error));
      }
    }

    render() {
      const { spinning, modal } = this.state;
      return (
        <React.Fragment>
          {spinning && <LoadingSpinner />}
          {modal}
          {WrappedComponent && (
            <WrappedComponent
              onDeletePaybackNumber={this.handleDelete}
              onUnbindPaybackAccount={this.handleUnbind}
              {...this.props}
            />
          )}
        </React.Fragment>
      );
    }
  }

  WithDelete.displayName = `WithDelete(${displayName(WrappedComponent)})`;

  WithDelete.propTypes = {
    environment: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
    accountInfo: PropTypes.shape({
      paybackAccountBound: PropTypes.bool
    }).isRequired,
    onUpdateParentView: PropTypes.func.isRequired
  };

  return WithDelete;
};

export default withDelete;
