import test from 'ava';
import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { stub, spy } from 'sinon';
import { noop } from '../../utils/commons';
import Modal from '../../components/Modal';
import LoadingSpinner from '../../widgets/LoadingSpinner';
import * as accountsApi from '../../api/Accounts';
import withDelete from '.';
import MESSAGE from './messages';

configure({ adapter: new Adapter() });

// Dummy is a dummy WrappedComponent for withDelete -- used to access the
// callbacks provided by withDelete.
const Dummy = () => <span />;

const showsDummyOnly = (t, wrapper) => {
  t.is(wrapper.find(LoadingSpinner).length, 0);
  t.is(wrapper.find(Modal).length, 0);
  t.is(wrapper.find(Dummy).length, 1);
};

const showsSpinnerAndDummy = (t, wrapper) => {
  t.is(wrapper.find(LoadingSpinner).length, 1);
  t.is(wrapper.find(Modal).length, 0);
  t.is(wrapper.find(Dummy).length, 1);
};

const showsModalAndDummy = (t, wrapper) => {
  t.is(wrapper.find(LoadingSpinner).length, 0);
  t.is(wrapper.find(Modal).length, 1);
  t.is(wrapper.find(Dummy).length, 1);
};

const callDelete = async (wrapper) => {
  wrapper.find(Dummy).props().onDeletePaybackNumber();
  await wrapper.update();
};

const callUnbind = async (wrapper) => {
  wrapper.find(Dummy).props().onUnbindPaybackAccount();
  await wrapper.update();
};

const click = async (wrapper, modalTitle, buttonLabel) => {
  wrapper
    .find(`Modal[title="${modalTitle}"]`)
    .find(`Button[label="${buttonLabel}"]`)
    .simulate('click');
  await wrapper.update();
};

const cancelDelete = async wrapper => click(wrapper, MESSAGE.delete.title, 'Nein');

const cancelUnbind = async wrapper => click(wrapper, MESSAGE.unbind.main, 'Nein');

const cancelDeleteAfterUnbind = async wrapper => click(wrapper, MESSAGE.deleteAfterUnbind.main, 'Nein');

const confirmDelete = async wrapper => click(wrapper, MESSAGE.delete.title, 'Ja');

const confirmUnbind = async wrapper => click(wrapper, MESSAGE.unbind.main, 'Ja');

const confirmDeleteAfterUnbind = async wrapper => click(wrapper, MESSAGE.deleteAfterUnbind.main, 'Ja');

const closeError = async wrapper => click(wrapper, MESSAGE.error.fallbackTitle, 'Weiter');

const closeDeleteSuccess = async wrapper => click(wrapper, MESSAGE.delete.success, 'Weiter');

const closeUnbindSuccess = async wrapper => click(wrapper, MESSAGE.unbind.success_message_content, 'Weiter');

const apiResponse = async (wrapper) => {
  await wrapper.update();
  await wrapper.update();
  await wrapper.update();
};

test.beforeEach('stub accountsApi', () => {
  stub(accountsApi, 'deletePaybackAccountInfo');
  stub(accountsApi, 'deletePaybackAccountBinding');
});

test.afterEach.always('restore accountsApi', () => {
  accountsApi.deletePaybackAccountInfo.restore();
  accountsApi.deletePaybackAccountBinding.restore();
});

test.serial('copes with undefined WrapperComponent', (t) => {
  const WithDelete = withDelete();
  const wrapper = mount(<WithDelete accountInfo={{ paybackAccountBound: false }} onUpdateParentView={noop} />);
  t.is(wrapper.children().length, 0);
});

test.serial('copes with null WrapperComponent', (t) => {
  const WithDelete = withDelete(null);
  const wrapper = mount(<WithDelete accountInfo={{ paybackAccountBound: false }} onUpdateParentView={noop} />);
  t.is(wrapper.children().length, 0);
});

test.serial('renders WrappedComponent', (t) => {
  const WithDelete = withDelete(Dummy);
  const wrapper = mount(<WithDelete accountInfo={{ paybackAccountBound: false }} onUpdateParentView={noop} />);
  showsDummyOnly(t, wrapper);
});

test.serial('handles onDeletePaybackNumber happy path', async (t) => {
  accountsApi.deletePaybackAccountInfo.resolves({ status: 204 });
  const WithDelete = withDelete(Dummy);
  const w = mount(
    <WithDelete accountInfo={{ paybackAccountBound: false }} onUpdateParentView={spy()} />
  );
  showsDummyOnly(t, w);

  await callDelete(w);
  showsModalAndDummy(t, w);

  await confirmDelete(w);
  showsSpinnerAndDummy(t, w);
  t.true(accountsApi.deletePaybackAccountInfo.calledOnce);
  t.true(w.props().onUpdateParentView.calledOnce);

  await apiResponse(w);
  showsModalAndDummy(t, w);

  await closeDeleteSuccess(w);
  showsDummyOnly(t, w);
});

test.serial('handles onDeletePaybackNumber cancel', async (t) => {
  accountsApi.deletePaybackAccountInfo.resolves({ status: 204 });
  const WithDelete = withDelete(Dummy);
  const w = mount(
    <WithDelete accountInfo={{ paybackAccountBound: false }} onUpdateParentView={spy()} />
  );
  showsDummyOnly(t, w);

  await callDelete(w);
  showsModalAndDummy(t, w);

  await cancelDelete(w);
  showsDummyOnly(t, w);
  t.true(accountsApi.deletePaybackAccountInfo.notCalled);
  t.true(w.props().onUpdateParentView.notCalled);
});

test.serial('handles onDeletePaybackNumber 403 Forbidden', async (t) => {
  accountsApi.deletePaybackAccountInfo.resolves({ status: 403 });
  const WithDelete = withDelete(Dummy);
  const w = mount(
    <WithDelete accountInfo={{ paybackAccountBound: false }} onUpdateParentView={spy()} />
  );
  showsDummyOnly(t, w);

  await callDelete(w);
  showsModalAndDummy(t, w);

  await confirmDelete(w);
  showsSpinnerAndDummy(t, w);
  t.true(accountsApi.deletePaybackAccountInfo.calledOnce);
  t.true(w.props().onUpdateParentView.calledOnce);

  await apiResponse(w);
  showsDummyOnly(t, w);
});

test.serial('handles onDeletePaybackNumber 400 Bad Request', async (t) => {
  accountsApi.deletePaybackAccountInfo.resolves({
    status: 400,
    json: () => ({
      _status: {
        validationMessages: [{ message: 'ZONG' }]
      }
    })
  });
  const WithDelete = withDelete(Dummy);
  const w = mount(
    <WithDelete accountInfo={{ paybackAccountBound: false }} onUpdateParentView={spy()} />
  );
  showsDummyOnly(t, w);

  await callDelete(w);
  showsModalAndDummy(t, w);

  await confirmDelete(w);
  showsSpinnerAndDummy(t, w);
  t.true(accountsApi.deletePaybackAccountInfo.calledOnce);
  t.true(w.props().onUpdateParentView.calledOnce);

  await apiResponse(w);
  showsModalAndDummy(t, w);

  await closeError(w);
  showsDummyOnly(t, w);
});

test.serial('handles onDeletePaybackNumber exception', async (t) => {
  accountsApi.deletePaybackAccountInfo.rejects('ZONG');
  const WithDelete = withDelete(Dummy);
  const w = mount(
    <WithDelete accountInfo={{ paybackAccountBound: false }} onUpdateParentView={spy()} />
  );
  showsDummyOnly(t, w);

  await callDelete(w);
  showsModalAndDummy(t, w);

  await confirmDelete(w);
  showsSpinnerAndDummy(t, w);
  t.true(accountsApi.deletePaybackAccountInfo.calledOnce);
  t.true(w.props().onUpdateParentView.notCalled);

  await apiResponse(w);
  showsModalAndDummy(t, w);

  await closeError(w);
  showsDummyOnly(t, w);
  t.true(w.props().onUpdateParentView.calledOnce);
});

test.serial('handles onUnbindPaybackAccount happy path', async (t) => {
  accountsApi.deletePaybackAccountBinding.resolves({ status: 200 });
  accountsApi.deletePaybackAccountInfo.resolves({ status: 204 });
  const WithDelete = withDelete(Dummy);
  const w = mount(
    <WithDelete accountInfo={{ paybackAccountBound: true }} onUpdateParentView={spy()} />
  );
  showsDummyOnly(t, w);

  await callUnbind(w);
  showsModalAndDummy(t, w);

  await confirmUnbind(w);
  showsSpinnerAndDummy(t, w);
  t.true(accountsApi.deletePaybackAccountBinding.calledOnce);
  t.true(accountsApi.deletePaybackAccountInfo.notCalled);
  t.true(w.props().onUpdateParentView.calledOnce);

  await apiResponse(w);
  showsModalAndDummy(t, w);

  await closeUnbindSuccess(w);
  showsModalAndDummy(t, w);

  await confirmDeleteAfterUnbind(w);
  t.true(accountsApi.deletePaybackAccountBinding.calledOnce);
  t.true(accountsApi.deletePaybackAccountInfo.calledOnce);
  t.true(w.props().onUpdateParentView.calledTwice);

  await apiResponse(w);
  showsModalAndDummy(t, w);

  await closeDeleteSuccess(w);
  showsDummyOnly(t, w);
});

test.serial('handles onUnbindPaybackAccount unbind cancel', async (t) => {
  accountsApi.deletePaybackAccountBinding.resolves({ status: 200 });
  accountsApi.deletePaybackAccountInfo.resolves({ status: 204 });
  const WithDelete = withDelete(Dummy);
  const w = mount(
    <WithDelete accountInfo={{ paybackAccountBound: true }} onUpdateParentView={spy()} />
  );
  showsDummyOnly(t, w);

  await callUnbind(w);
  showsModalAndDummy(t, w);

  await cancelUnbind(w);
  showsDummyOnly(t, w);
  t.true(accountsApi.deletePaybackAccountBinding.notCalled);
  t.true(accountsApi.deletePaybackAccountInfo.notCalled);
  t.true(w.props().onUpdateParentView.notCalled);
});

test.serial('handles onUnbindPaybackAccount unbind 403 Forbidden', async (t) => {
  accountsApi.deletePaybackAccountBinding.resolves({ status: 403 });
  accountsApi.deletePaybackAccountInfo.resolves({ status: 204 });
  const WithDelete = withDelete(Dummy);
  const w = mount(
    <WithDelete accountInfo={{ paybackAccountBound: true }} onUpdateParentView={spy()} />
  );
  showsDummyOnly(t, w);

  await callUnbind(w);
  showsModalAndDummy(t, w);

  await confirmUnbind(w);
  showsSpinnerAndDummy(t, w);
  t.true(accountsApi.deletePaybackAccountBinding.calledOnce);
  t.true(accountsApi.deletePaybackAccountInfo.notCalled);
  t.true(w.props().onUpdateParentView.calledOnce);

  await apiResponse(w);
  showsDummyOnly(t, w);
});

test.serial('handles onUnbindPaybackAccount unbind 400 Bad Request', async (t) => {
  accountsApi.deletePaybackAccountBinding.resolves({
    status: 400,
    json: () => ({
      _status: {
        validationMessages: [{ message: 'ZONG' }]
      }
    })
  });
  accountsApi.deletePaybackAccountInfo.resolves({ status: 204 });
  const WithDelete = withDelete(Dummy);
  const w = mount(
    <WithDelete accountInfo={{ paybackAccountBound: true }} onUpdateParentView={spy()} />
  );
  showsDummyOnly(t, w);

  await callUnbind(w);
  showsModalAndDummy(t, w);

  await confirmUnbind(w);
  showsSpinnerAndDummy(t, w);
  t.true(accountsApi.deletePaybackAccountBinding.calledOnce);
  t.true(accountsApi.deletePaybackAccountInfo.notCalled);
  t.true(w.props().onUpdateParentView.calledOnce);

  await apiResponse(w);
  showsModalAndDummy(t, w);

  await closeError(w);
  showsDummyOnly(t, w);
});

test.serial('handles onUnbindPaybackAccount unbind exception', async (t) => {
  accountsApi.deletePaybackAccountBinding.rejects('ZONG');
  accountsApi.deletePaybackAccountInfo.resolves({ status: 204 });
  const WithDelete = withDelete(Dummy);
  const w = mount(
    <WithDelete accountInfo={{ paybackAccountBound: true }} onUpdateParentView={spy()} />
  );
  showsDummyOnly(t, w);

  await callUnbind(w);
  showsModalAndDummy(t, w);

  await confirmUnbind(w);
  showsSpinnerAndDummy(t, w);

  await apiResponse(w);
  showsModalAndDummy(t, w);
  t.true(accountsApi.deletePaybackAccountBinding.calledOnce);
  t.true(accountsApi.deletePaybackAccountInfo.notCalled);
  t.true(w.props().onUpdateParentView.notCalled);

  await closeError(w);
  showsDummyOnly(t, w);
  t.true(w.props().onUpdateParentView.calledOnce);
});

test.serial('handles onUnbindPaybackAccount delete cancel', async (t) => {
  accountsApi.deletePaybackAccountBinding.resolves({ status: 200 });
  accountsApi.deletePaybackAccountInfo.resolves({ status: 204 });
  const WithDelete = withDelete(Dummy);
  const w = mount(
    <WithDelete accountInfo={{ paybackAccountBound: true }} onUpdateParentView={spy()} />
  );
  showsDummyOnly(t, w);

  await callUnbind(w);
  showsModalAndDummy(t, w);

  await confirmUnbind(w);
  showsSpinnerAndDummy(t, w);

  await apiResponse(w);
  showsModalAndDummy(t, w);
  t.true(accountsApi.deletePaybackAccountBinding.calledOnce);
  t.true(accountsApi.deletePaybackAccountInfo.notCalled);
  t.true(w.props().onUpdateParentView.calledOnce);

  await closeUnbindSuccess(w);
  showsModalAndDummy(t, w);

  await cancelDeleteAfterUnbind(w);
  showsDummyOnly(t, w);
  t.true(accountsApi.deletePaybackAccountBinding.calledOnce);
  t.true(accountsApi.deletePaybackAccountInfo.notCalled);
  t.true(w.props().onUpdateParentView.calledOnce);
});

test.serial('handles onUnbindPaybackAccount delete 403 Forbidden', async (t) => {
  accountsApi.deletePaybackAccountBinding.resolves({ status: 200 });
  accountsApi.deletePaybackAccountInfo.resolves({ status: 403 });
  const WithDelete = withDelete(Dummy);
  const w = mount(
    <WithDelete accountInfo={{ paybackAccountBound: true }} onUpdateParentView={spy()} />
  );
  showsDummyOnly(t, w);

  await callUnbind(w);
  showsModalAndDummy(t, w);

  await confirmUnbind(w);
  showsSpinnerAndDummy(t, w);

  await apiResponse(w);
  showsModalAndDummy(t, w);
  t.true(accountsApi.deletePaybackAccountBinding.calledOnce);
  t.true(accountsApi.deletePaybackAccountInfo.notCalled);
  t.true(w.props().onUpdateParentView.calledOnce);

  await closeUnbindSuccess(w);
  showsModalAndDummy(t, w);

  await confirmDeleteAfterUnbind(w);
  showsSpinnerAndDummy(t, w);

  await apiResponse(w);
  showsDummyOnly(t, w);
  t.true(accountsApi.deletePaybackAccountBinding.calledOnce);
  t.true(accountsApi.deletePaybackAccountInfo.calledOnce);
  t.true(w.props().onUpdateParentView.calledTwice);
});

test.serial('handles onUnbindPaybackAccount delete 400 Bad Request', async (t) => {
  accountsApi.deletePaybackAccountBinding.resolves({ status: 200 });
  accountsApi.deletePaybackAccountInfo.resolves({
    status: 400,
    json: () => ({
      _status: {
        validationMessages: [{ message: 'ZONG' }]
      }
    })
  });
  const WithDelete = withDelete(Dummy);
  const w = mount(
    <WithDelete accountInfo={{ paybackAccountBound: true }} onUpdateParentView={spy()} />
  );
  showsDummyOnly(t, w);

  await callUnbind(w);
  showsModalAndDummy(t, w);

  await confirmUnbind(w);
  showsSpinnerAndDummy(t, w);

  await apiResponse(w);
  showsModalAndDummy(t, w);
  t.true(accountsApi.deletePaybackAccountBinding.calledOnce);
  t.true(accountsApi.deletePaybackAccountInfo.notCalled);
  t.true(w.props().onUpdateParentView.calledOnce);

  await closeUnbindSuccess(w);
  showsModalAndDummy(t, w);

  await confirmDeleteAfterUnbind(w);
  showsSpinnerAndDummy(t, w);

  await apiResponse(w);
  showsModalAndDummy(t, w);
  t.true(accountsApi.deletePaybackAccountBinding.calledOnce);
  t.true(accountsApi.deletePaybackAccountInfo.calledOnce);
  t.true(w.props().onUpdateParentView.calledTwice);

  await closeError(w);
  showsDummyOnly(t, w);
});

test.serial('handles onUnbindPaybackAccount delete exception', async (t) => {
  accountsApi.deletePaybackAccountBinding.resolves({ status: 200 });
  accountsApi.deletePaybackAccountInfo.rejects('ZONG');
  const WithDelete = withDelete(Dummy);
  const w = mount(
    <WithDelete accountInfo={{ paybackAccountBound: true }} onUpdateParentView={spy()} />
  );
  showsDummyOnly(t, w);

  await callUnbind(w);
  showsModalAndDummy(t, w);

  await confirmUnbind(w);
  showsSpinnerAndDummy(t, w);

  await apiResponse(w);
  showsModalAndDummy(t, w);
  t.true(accountsApi.deletePaybackAccountBinding.calledOnce);
  t.true(accountsApi.deletePaybackAccountInfo.notCalled);
  t.true(w.props().onUpdateParentView.calledOnce);

  await closeUnbindSuccess(w);
  showsModalAndDummy(t, w);

  await confirmDeleteAfterUnbind(w);
  showsSpinnerAndDummy(t, w);

  await apiResponse(w);
  showsModalAndDummy(t, w);
  t.true(accountsApi.deletePaybackAccountBinding.calledOnce);
  t.true(accountsApi.deletePaybackAccountInfo.calledOnce);
  t.true(w.props().onUpdateParentView.calledOnce);

  await closeError(w);
  showsDummyOnly(t, w);
});
