import React, { Component } from 'react';

import { getAccountInfo } from '../services/PaybackAccountService';
import '../assets/scss/base.scss';
import PaybackNumberIncludeComponent from '../meso-components/PaybackNumberIncludeComponent';


export default class NumberApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accountInfo: {
        errorStatus: undefined,
        paybackNumber: undefined
      }
    };
    this.updateView = this.updateView.bind(this);
    this.initializeState = this.initializeState.bind(this);
    this.initializeState(props);
  }

  componentDidMount() {
    // avoid unnecessary view updates if initial state is already defined in SSR output
    if (this.state.accountInfo.paybackNumber === undefined) {
      this.updateView();
    }
  }

  async updateView() {
    const accountInfo = await getAccountInfo();
    this.setState({ accountInfo });
  }


  // initial state as delivered by SSR, if available
  initializeState(props) {
    if (props) {
      this.state.accountInfo = {
        errorStatus: props.errorStatus,
        paybackNumber: props.paybackNumber
      };
      /* eslint-disable no-underscore-dangle */
    } else if (window.__INITIAL_PB_STATE__) {
      this.state.accountInfo = {
        errorStatus: window.__INITIAL_PB_STATE__.errorStatus,
        paybackNumber: window.__INITIAL_PB_STATE__.paybackNumber
      };
    }
    /* eslint-enable no-underscore-dangle */
  }

  render() {
    return (
      <div>
        <PaybackNumberIncludeComponent accountNumber={this.state.accountInfo.paybackNumber} />
      </div>
    );
  }
}
