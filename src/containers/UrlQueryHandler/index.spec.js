import test from 'ava';
import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { parse } from 'query-string';

import UrlQueryHandler from '.';
import { Button } from '../../widgets';
import Modal from '../../components/Modal';

configure({ adapter: new Adapter() });

const urlQueryHandlerWithParams = (
  queryString = '?accountBindingStatus=success',
  paybackNumber = '0000000001'
) => (
  <UrlQueryHandler
    environment={{}}
    accountInfo={{ paybackNumber }}
    queryParams={parse(queryString)}
  />
);

const prepareDomHistoryState = (t, pathname, query) => {
  window.history.pushState({}, '', pathname + query);
  t.is(window.location.pathname, pathname);
  t.is(window.location.search, query);
};

test('renders account binding status modal if query parameter is available', (t) => {
  const wrapper = mount(urlQueryHandlerWithParams());
  const modal = wrapper.find(Modal);
  modal.text().includes('0000000001');
  t.is(modal.length, 1);
});

test('renders empty value if query parameter is not available', (t) => {
  const wrapper = mount(urlQueryHandlerWithParams(''));
  t.true(wrapper.isEmptyRender());
});

test('removes account binding status modal on submission', (t) => {
  const wrapperForSubmission = mount(urlQueryHandlerWithParams());
  wrapperForSubmission.find(Modal).find(Button).simulate('click');

  t.is(wrapperForSubmission.find(Modal).length, 0);
});

test('removes account binding status modal on close', (t) => {
  const wrapperForSubmission = mount(urlQueryHandlerWithParams());
  wrapperForSubmission.find(Modal).find('.rs-modal__closelink').simulate('click');

  t.is(wrapperForSubmission.find(Modal).length, 0);
});

test.serial('removes url query params when account binding status modal is submitted', (t) => {
  prepareDomHistoryState(t, '/mypayback', '?accountBindingStatus=success');

  const wrapper = mount(urlQueryHandlerWithParams(window.location.search));
  wrapper.find(Modal).find(Button).simulate('click');

  t.is(window.location.pathname, '/mypayback');
  t.is(window.location.search, '');
});

test.serial('removes url query params when account binding status modal is closed', (t) => {
  prepareDomHistoryState(t, '/mypayback', '?accountBindingStatus=success');
  const wrapper = mount(urlQueryHandlerWithParams(window.location.search));
  wrapper.find(Modal).find('.rs-modal__closelink').simulate('click');

  t.is(window.location.pathname, '/mypayback');
  t.is(window.location.search, '');
});
