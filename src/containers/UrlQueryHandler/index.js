import React, { Component } from 'react';
import PropTypes from 'prop-types';

import AccountBindingStatusModal from '../../components/AccountBindingStatusModal';

class UrlQueryHandler extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAccountBindingStatusModal: !!props.queryParams.accountBindingStatus
    };
    this.handleAccountBindingStatusModalClose = this.handleAccountBindingStatusModalClose.bind(this);
  }

  handleAccountBindingStatusModalClose(e) {
    e.preventDefault();
    window.history.replaceState({}, '', window.location.pathname);
    this.setState({ showAccountBindingStatusModal: false });
  }

  render() {
    return this.state.showAccountBindingStatusModal
      ? (
        <AccountBindingStatusModal
          environment={this.props.environment}
          paybackNumber={this.props.accountInfo.paybackNumber}
          accountBindingStatus={this.props.queryParams.accountBindingStatus}
          onClose={this.handleAccountBindingStatusModalClose}
          onSubmit={this.handleAccountBindingStatusModalClose}
        />
      )
      : null;
  }
}

UrlQueryHandler.propTypes = {
  environment: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  accountInfo: PropTypes.shape({
    paybackNumber: PropTypes.string
  }).isRequired,
  queryParams: PropTypes.shape({
    accountBindingStatus: PropTypes.string
  }).isRequired
};

export default UrlQueryHandler;
