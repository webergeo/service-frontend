import test from 'ava';
import React from 'react';
import render from 'react-test-renderer';

import RegistrationBannerApp from "./RegistrationBannerApp";

test('renders the after page snapshot', (t) => {
  const testRegistrationBannerAppAfterPage = render.create(
    <RegistrationBannerApp layout={'checkout'} />
  ).toJSON();
  t.snapshot(testRegistrationBannerAppAfterPage);
});

test('renders the payback page snapshot', (t) => {
  const testRegistrationBannerAppNonAfterPage = render.create(
    <RegistrationBannerApp layout={'payback-page'} />
  ).toJSON();
  t.snapshot(testRegistrationBannerAppNonAfterPage);
});
