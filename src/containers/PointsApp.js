import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { getAccountInfo } from '../services/PaybackAccountService';
import Points from '../components/Points';
import UrlQueryHandler from './UrlQueryHandler';

import '../assets/scss/base.scss';
import points from './points';

const ViewContainer = points(Points);

class PointsApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      environment: {},
      accountInfo: {
        errorStatus: undefined,
        paybackNumber: undefined,
        eBonOptInExists: undefined,
        bonusCouponOptInExists: undefined,
        groupEweOptInExists: undefined,
        ewe16OptInExists: undefined,
        paybackAccountBound: undefined
      }
    };
    this.initializeState(props);
    this.initializeState = this.initializeState.bind(this);
    this.updateView = this.updateView.bind(this);
  }

  componentDidMount() {
    // avoid unnecessary view updates if initial state is already defined in SSR output
    if (this.state.accountInfo.paybackNumber === undefined) {
      this.updateView();
    }
  }

  async updateView() {
    const accountInfo = await getAccountInfo();
    this.setState({ accountInfo });
  }

  // initial state as delivered by SSR, if available
  initializeState(props) {
    if (props) {
      this.state.environment = props.environment || {};
      this.state.accountInfo = {
        errorStatus: props.errorStatus,
        paybackNumber: props.paybackNumber,
        eBonOptInExists: props.eBonOptInExists,
        bonusCouponOptInExists: props.bonusCouponOptInExists,
        groupEweOptInExists: props.groupEweOptInExists,
        ewe16OptInExists: props.ewe16OptInExists,
        paybackAccountBound: props.paybackAccountBound
      };
      /* eslint-disable no-underscore-dangle */
    } else if (window.__INITIAL_PB_STATE__) {
      this.state.environment = window.__INITIAL_PB_STATE__.environment;
      this.state.accountInfo = {
        errorStatus: window.__INITIAL_PB_STATE__.errorStatus,
        paybackNumber: window.__INITIAL_PB_STATE__.paybackNumber,
        eBonOptInExists: window.__INITIAL_PB_STATE__.eBonOptInExists,
        bonusCouponOptInExists: window.__INITIAL_PB_STATE__.bonusCouponOptInExists,
        groupEweOptInExists: window.__INITIAL_PB_STATE__.groupEweOptInExists,
        ewe16OptInExists: window.__INITIAL_PB_STATE__.ewe16OptInExists,
        paybackAccountBound: window.__INITIAL_PB_STATE__.paybackAccountBound
      };
    }
    /* eslint-enable no-underscore-dangle */
  }

  render() {
    return (
      <div data-hello="hello">
        <ViewContainer
          environment={this.state.environment}
          accountInfo={this.state.accountInfo}
          onUpdateParentView={this.updateView}
        />
        <UrlQueryHandler
          environment={this.state.environment}
          accountInfo={this.state.accountInfo}
          queryParams={this.props.defaultQueryParams} />
      </div>
    );
  }
}

PointsApp.propTypes = {
  defaultQueryParams: PropTypes.shape({
    accountBindingStatus: PropTypes.string
  }).isRequired
};

export default PointsApp;
