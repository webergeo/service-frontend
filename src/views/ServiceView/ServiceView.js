import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { createBus } from 'suber';

import PaybackNumberBlock from '../../components/PaybackNumberBlock';
import PaybackPointsBlock from '../../components/PaybackPointsBlock';
import PaybackModuleBlock from '../../components/PaybackModuleBlock';
import ConsentFormBlock from '../../components/ConsentFormBlock';
import EBonBlock from '../../components/EBonBlock';
import BonusCouponBlock from '../../components/BonusCouponBlock';

import { isEnvParameterDefined, isEnvParameterTruthy } from '../../config/appConfig';

import './ServiceView.scss';

// global event bus
const eventBus = createBus();

/**
 * This renders the "full" payback page when the user has a valid PB account binding.
 */
class ServiceView extends Component {
  constructor(props) {
    super(props);
    this.state = props;

    // feature togglez
    this.TOGGLE_ECOUPON = isEnvParameterTruthy('FEATURE_TOGGLE_ECOUPON');
    this.TOGGLE_EBON = isEnvParameterTruthy('FEATURE_TOGGLE_EBON');
    this.TOGGLE_BONUS_COUPON = isEnvParameterDefined('FEATURE_TOGGLE_BONUS_COUPON') ?
      isEnvParameterTruthy('FEATURE_TOGGLE_BONUS_COUPON') :
      props.environment.FEATURE_TOGGLE_BONUS_COUPON;
    this.TOGGLE_DELETE_EBON_CHECKBOX = isEnvParameterDefined('FEATURE_TOGGLE_DELETE_EBON_CHECKBOX') ?
      isEnvParameterTruthy('FEATURE_TOGGLE_DELETE_EBON_CHECKBOX') :
      props.environment.FEATURE_TOGGLE_DELETE_EBON_CHECKBOX;
  }

  render() {
    // stub
    return (
      <div className="rs-payback__serviceView">
        <div className="rs-payback__serviceView-container">
          {
            <PaybackNumberBlock
              paybackNumber={this.props.accountInfo.paybackNumber}
              onPaybackNumberDeleteRequest={this.props.onDeletePaybackNumber}
            />
          }
          {<PaybackPointsBlock eventBus={eventBus} onUpdateParentView={this.props.onUpdateParentView} />}
        </div>
        {this.TOGGLE_BONUS_COUPON && <BonusCouponBlock accountInfo={this.props.accountInfo} onUpdateParentView={this.props.onUpdateParentView} /> }
        {this.TOGGLE_EBON && <EBonBlock accountInfo={this.props.accountInfo} onUpdateParentView={this.props.onUpdateParentView} deleteBonCheckBox={this.TOGGLE_DELETE_EBON_CHECKBOX} />}
        {this.TOGGLE_ECOUPON && <PaybackModuleBlock eventBus={eventBus} />}
        <ConsentFormBlock
          accountInfo={this.props.accountInfo}
          onUpdateParentView={this.props.onUpdateParentView}
          onUnbindPaybackAccount={this.props.onUnbindPaybackAccount}
        />
      </div>
    );
  }
}

ServiceView.propTypes = {
  environment: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  accountInfo: PropTypes.shape({
    paybackNumber: PropTypes.string,
    paybackAccountBound: PropTypes.bool,
    ewe16OptInExists: PropTypes.bool,
    eBonOptInExists: PropTypes.bool,
    bonusCouponOptInExists: PropTypes.bool
  }).isRequired,
  onUpdateParentView: PropTypes.func.isRequired,
  onDeletePaybackNumber: PropTypes.func.isRequired,
  onUnbindPaybackAccount: PropTypes.func.isRequired
};

export default ServiceView;
