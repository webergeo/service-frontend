import test from 'ava';
import React from 'react';
import render from 'react-test-renderer';
import { stub } from 'sinon';

import ServiceView from './ServiceView';
import { noop } from '../../utils/commons';
import * as accountsApi from '../../api/Accounts';

test('renders ServiceView default snapshot', (t) => {
  const getGroupEweStub = stub(accountsApi, 'getGroupEwe');
  getGroupEweStub.returns({
    status: 200,
    json: () => ({ status: true })
  });

  const serviceViewTree = render.create(
    <ServiceView
      environment={{}}
      accountInfo={{
        paybackNumber: '1234567890',
        paybackAccountBound: true
      }}
      onUpdateParentView={noop}
      onPaybackNumberDeleteRequest={noop}
    />
  ).toJSON();
  t.snapshot(serviceViewTree);
  getGroupEweStub.restore();
});
