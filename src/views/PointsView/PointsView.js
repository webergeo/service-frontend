import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { createBus } from 'suber';

import PaybackPointsWithWelcomingBlock from '../../components/PaybackPointsWithWelcomingBlock';

import { isEnvParameterDefined, isEnvParameterTruthy } from '../../config/appConfig';

import './PointsView.scss';

// global event bus
const eventBus = createBus();

/**
 * This renders the "full" payback page when the user has a valid PB account binding.
 */
class PointsView extends Component {
  constructor(props) {
    super(props);
    this.state = props;

    // feature togglez
    this.TOGGLE_DELETE_EBON_CHECKBOX = isEnvParameterDefined('FEATURE_TOGGLE_DELETE_EBON_CHECKBOX') ?
      isEnvParameterTruthy('FEATURE_TOGGLE_DELETE_EBON_CHECKBOX') :
      props.environment.FEATURE_TOGGLE_DELETE_EBON_CHECKBOX;
  }

  render() {
    // stub
    return (
      <div className="rs-payback__serviceView">
        <div className="rs-payback__serviceView-container">
          <PaybackPointsWithWelcomingBlock eventBus={eventBus} onUpdateParentView={this.props.onUpdateParentView} />
        </div>
      </div>
    );
  }
}

PointsView.propTypes = {
  environment: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  accountInfo: PropTypes.shape({
    paybackNumber: PropTypes.string,
    paybackAccountBound: PropTypes.bool,
    ewe16OptInExists: PropTypes.bool,
    eBonOptInExists: PropTypes.bool,
    bonusCouponOptInExists: PropTypes.bool
  }).isRequired,
  onUpdateParentView: PropTypes.func.isRequired,
  onDeletePaybackNumber: PropTypes.func.isRequired,
  onUnbindPaybackAccount: PropTypes.func.isRequired
};

export default PointsView;
