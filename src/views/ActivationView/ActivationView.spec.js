import test from 'ava';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';
import { stub, spy } from 'sinon';

import * as tracking from '../../utils/tracking';
import * as commons from '../../utils/commons';
import * as accountsApi from '../../api/Accounts';
import ActivationView from './ActivationView';
import MESSAGES from './messages';
import PaybackTeaserBlock from '../../components/PaybackTeaserBlock';
import BenefitTeaserBlock from '../../components/BenefitTeaserBlock';
import BenefitBlock from '../../components/BenefitBlock';
import Modal from '../../components/Modal';
import LoadingSpinner from '../../widgets/LoadingSpinner';
import PaybackNumberModal from '../../components/PaybackNumberModal';
import PaybackNumberIntroBlock from '../../components/PaybackNumberIntroBlock';
import QuestionsBlock from '../../components/QuestionsBlock';

const activationView = props => (
  <ActivationView
    onUpdateParentView={commons.noop}
    onPaybackNumberDeleteRequest={commons.noop}
    {...props}
  />
);

configure({ adapter: new Adapter() });

test('renders view without payback number', (t) => {
  const activationViewTree = render
    .create(activationView({
      accountInfo: {
        paybackNumber: undefined
      }
    }))
    .toJSON();
  t.snapshot(activationViewTree);
});

test('renders view without payback number and kv enrollment', (t) => {
  const activationViewTree = render
    .create(activationView({
      environment: { kv: true },
      accountInfo: {
        paybackNumber: undefined
      }
    }))
    .toJSON();
  t.snapshot(activationViewTree);
});

test('renders view with payback number', (t) => {
  const activationViewTree = render
    .create(activationView({
      accountInfo: {
        paybackNumber: '1234567890'
      }
    }))
    .toJSON();
  t.snapshot(activationViewTree);
});

test('rendered view without payback number contains expected blocks', (t) => {
  const emptyPaybackNumbers = [undefined, null, ''];

  emptyPaybackNumbers.forEach((paybackNumber) => {
    const wrapper = shallow(activationView({
      accountInfo: { paybackNumber }
    }));
    t.is(wrapper.find(BenefitTeaserBlock).length, 1);
    t.is(wrapper.find(PaybackTeaserBlock).length, 1);
    t.is(wrapper.find(PaybackNumberIntroBlock).length, 0);
    t.is(wrapper.find(BenefitBlock).length, 1);
    t.is(wrapper.find(QuestionsBlock).length, 1);
  });
});

test('rendered view with payback number contains expected blocks', (t) => {
  const wrapper = shallow(activationView({
    accountInfo: {
      paybackNumber: '1234565'
    }
  }));
  t.is(wrapper.find(PaybackTeaserBlock).length, 0);
  t.is(wrapper.find(BenefitTeaserBlock).length, 0);
  t.is(wrapper.find(PaybackNumberIntroBlock).length, 1);
  t.is(wrapper.find(BenefitBlock).length, 1);
  t.is(wrapper.find(QuestionsBlock).length, 1);
});

const mountWithLoadingViewState = (t, updateParentViewHandler) => {
  const wrapper = mount(activationView({
    accountInfo: {
      paybackNumber: '1234565'
    },
    onUpdateParentView: updateParentViewHandler,
    defaultShowSpinner: true
  }));
  t.is(wrapper.find(Modal).length, 0);
  t.is(wrapper.find(LoadingSpinner).length, 1);

  return wrapper;
};

test.serial('When the payback number addition failed, the action should not be tracked', async (t) => {
  const trackSpy = spy(tracking, 'trackPaybackNumberAddition');
  const updatePaybackAccountInfoStub = stub(accountsApi, 'updatePaybackAccountInfo');
  updatePaybackAccountInfoStub.returns({ status: 400 });

  let previousPaybackNumber = '';
  let wrapper = shallow(activationView(({
    accountInfo: {
      paybackNumber: previousPaybackNumber
    }
  }))).instance();
  await wrapper.handleAddPaybackNumber('0101010101');
  t.false(trackSpy.called);

  trackSpy.resetHistory();

  previousPaybackNumber = null;
  wrapper = shallow(activationView(({
    accountInfo: {
      paybackNumber: previousPaybackNumber
    }
  }))).instance();
  await wrapper.handleAddPaybackNumber('0101010101');
  t.false(trackSpy.called);

  trackSpy.restore();
  updatePaybackAccountInfoStub.restore();
});

test.serial('When a payback number is changed, the action should not be tracked', async (t) => {
  const trackSpy = spy(tracking, 'trackPaybackNumberAddition');
  const updatePaybackAccountInfoStub = stub(accountsApi, 'updatePaybackAccountInfo');
  updatePaybackAccountInfoStub.returns({ status: 200 });

  const previousPaybackNumber = '0123456789';
  const wrapper = shallow(activationView(({
    accountInfo: {
      paybackNumber: previousPaybackNumber
    }
  }))).instance();
  await wrapper.handleChangePaybackNumber('101010101');
  t.false(trackSpy.called);

  trackSpy.restore();
  updatePaybackAccountInfoStub.restore();
});

test.serial('When a payback number is (newly) added, the action should be tracked', async (t) => {
  const trackSpy = spy(tracking, 'trackPaybackNumberAddition');
  const updatePaybackAccountInfoStub = stub(accountsApi, 'updatePaybackAccountInfo');
  updatePaybackAccountInfoStub.returns({ status: 200 });

  let previousPaybackNumber = '';
  let wrapper = shallow(activationView(({
    accountInfo: {
      paybackNumber: previousPaybackNumber
    }
  }))).instance();
  await wrapper.handleAddPaybackNumber('1010101010');
  t.true(trackSpy.called);

  trackSpy.resetHistory();

  previousPaybackNumber = null;
  wrapper = shallow(activationView(({
    accountInfo: {
      paybackNumber: previousPaybackNumber
    }
  }))).instance();
  await wrapper.handleAddPaybackNumber('1010101010');
  t.true(trackSpy.called);

  trackSpy.restore();
  updatePaybackAccountInfoStub.restore();
});

test.serial('payback number addition/edit failed status 403, onUpdateParentView is called', async (t) => {
  const testCase = async (isChange) => {
    const onUpdateParentView = spy();
    const updatePaybackAccountInfoStub = stub(accountsApi, 'updatePaybackAccountInfo');
    updatePaybackAccountInfoStub.returns({ status: 403 });

    const wrapper = shallow(activationView(({
      accountInfo: {
        paybackNumber: '1010101010'
      },
      onUpdateParentView
    })));

    t.is(wrapper.find(PaybackNumberModal).length, 0);

    if (isChange) {
      await wrapper.instance().handleChangePaybackNumber('0101010101');
    } else {
      await wrapper.instance().handleAddPaybackNumber('0101010101');
    }
    wrapper.update();

    t.true(onUpdateParentView.called);
    t.is(wrapper.find(PaybackNumberModal).length, 1);

    updatePaybackAccountInfoStub.restore();
  };

  await testCase(false);
  await testCase(true);
});

test.serial('payback number addition/edit throw exception', async (t) => {
  const testCase = async (isChange) => {
    const updatePaybackAccountInfoStub = stub(accountsApi, 'updatePaybackAccountInfo');
    updatePaybackAccountInfoStub.throws();

    const wrapper = mount(activationView(({
      accountInfo: {
        paybackNumber: '1010101010'
      }
    })));

    t.is(wrapper.find(Modal).length, 0);

    if (isChange) {
      await wrapper.instance().handleChangePaybackNumber('0101010101');
    } else {
      await wrapper.instance().handleAddPaybackNumber('0101010101');
    }
    wrapper.update();

    const additionFailureModal = wrapper.find(`Modal[title="${MESSAGES.edit.failed}"]`);
    t.is(additionFailureModal.length, 1);

    updatePaybackAccountInfoStub.restore();
  };

  await testCase(false);
  await testCase(true);
});

test.serial('payback number ADDITION success', async (t) => {
  const updatePaybackAccountInfoStub = stub(accountsApi, 'updatePaybackAccountInfo');
  updatePaybackAccountInfoStub.returns({ status: 200 });
  const initiateAccountBindingPopUpStub = stub(ActivationView.prototype, 'initiateAccountBindingPopUp');
  initiateAccountBindingPopUpStub.returns({});

  const wrapper = shallow(activationView(({
    accountInfo: {
      paybackNumber: '1010101010'
    }
  })));

  await wrapper.instance().handleAddPaybackNumber('1010101010');
  wrapper.update();

  t.is(wrapper.find(PaybackNumberModal).length, 0);
  t.is(wrapper.find(LoadingSpinner).length, 0);
  t.true(initiateAccountBindingPopUpStub.calledOnce);

  updatePaybackAccountInfoStub.restore();
  initiateAccountBindingPopUpStub.restore();
});

test.serial('payback number ADDITION fails due to blocked popups', async (t) => {
  const updatePaybackAccountInfoStub = stub(accountsApi, 'updatePaybackAccountInfo');
  updatePaybackAccountInfoStub.returns({ status: 200 });
  const generateSyncCsrfTokenStub = stub(accountsApi, 'generateSyncCsrfToken');
  generateSyncCsrfTokenStub.returns({ status: 200, response: '{}' });
  const renderAccountBindingPopUpStub = stub(commons, 'renderAccountBindingPopUp');
  renderAccountBindingPopUpStub.returns(false);

  const wrapper = shallow(activationView(({
    onDeletePaybackNumber: () => {},
    accountInfo: {
      paybackNumber: '1010101010'
    }
  })));

  await wrapper.instance().handleAddPaybackNumber('1010101010');
  wrapper.update();

  t.is(wrapper.find(PaybackNumberModal).length, 0);
  const popupBlockedModal = wrapper.find(`Modal[content="${MESSAGES.edit.popupBlocked}"]`);
  t.is(popupBlockedModal.find(Modal).length, 1);

  t.true(renderAccountBindingPopUpStub.calledOnce);

  updatePaybackAccountInfoStub.restore();
  generateSyncCsrfTokenStub.restore();
  renderAccountBindingPopUpStub.restore();
});

test.serial('payback number EDIT success', async (t) => {
  const updatePaybackAccountInfoStub = stub(accountsApi, 'updatePaybackAccountInfo');
  updatePaybackAccountInfoStub.returns({ status: 200 });
  const initiateAccountBindingPopUpStub = stub(ActivationView.prototype, 'initiateAccountBindingPopUp');
  initiateAccountBindingPopUpStub.returns({});

  const wrapper = shallow(activationView(({
    accountInfo: {
      paybackNumber: '1010101010'
    }
  })));

  await wrapper.instance().handleChangePaybackNumber('1010101010');
  wrapper.update();

  t.is(wrapper.find(PaybackNumberModal).length, 0);
  t.is(wrapper.find(LoadingSpinner).length, 0);

  // in contrast to a successful addition
  const additionSuccessModal = wrapper.find(`Modal[title="${MESSAGES.edit.success}"]`);
  t.is(additionSuccessModal.find(Modal).length, 1);
  t.false(initiateAccountBindingPopUpStub.called);

  updatePaybackAccountInfoStub.restore();
  initiateAccountBindingPopUpStub.restore();
});

test.serial('payback number addition/edit failed', async (t) => {
  const testCase = async (isChange) => {
    const updatePaybackAccountInfoStub = stub(accountsApi, 'updatePaybackAccountInfo');
    const errorMessage = 'Some random error message';
    updatePaybackAccountInfoStub.returns(
      {
        status: 400,
        json: () => ({
          _status: {
            validationMessages: [{ message: errorMessage }]
          }
        })
      }
    );
    const initiateAccountBindingPopUpStub = stub(ActivationView.prototype, 'initiateAccountBindingPopUp');
    initiateAccountBindingPopUpStub.returns({});

    const onUpdateParentViewSpy = spy();
    const wrapper = mountWithLoadingViewState(t, onUpdateParentViewSpy);

    if (isChange) {
      await wrapper.instance().handleChangePaybackNumber('0101010101');
    } else {
      await wrapper.instance().handleAddPaybackNumber('0101010101');
    }
    wrapper.update();

    t.is(wrapper.find(PaybackNumberModal).length, 0);
    t.is(wrapper.find(LoadingSpinner).length, 0);
    t.is(wrapper.find(Modal).length, 1);
    t.true(initiateAccountBindingPopUpStub.notCalled);

    const additionFailureModal = wrapper.find(`Modal[content="${errorMessage}"]`);
    t.is(additionFailureModal.length, 1);

    additionFailureModal.find('Button[label="Weiter"]').simulate('click');

    wrapper.update();

    t.is(wrapper.find(Modal).length, 0);

    updatePaybackAccountInfoStub.restore();
    initiateAccountBindingPopUpStub.restore();
  };

  await testCase(false);
  await testCase(true);
});

test('Benefit activation render Payback activation Popup', (t) => {
  const initiateAccountBindingPopUpStub = stub(ActivationView.prototype, 'initiateAccountBindingPopUp');
  initiateAccountBindingPopUpStub.returns({});

  const wrapper = shallow(activationView({
    accountInfo: {
      paybackNumber: '1234565'
    }
  })).instance();

  wrapper.handelNowUnlockAllBenefits();

  t.true(initiateAccountBindingPopUpStub.calledOnce);

  initiateAccountBindingPopUpStub.restore();
});

test('renders view with invalid token status', (t) => {
  const activationViewTree = render
    .create(activationView({
      accountInfo: {
        paybackNumber: '1234567890',
        tokenStatus: 'invalid'
      }
    }))
    .toJSON();
  t.snapshot(activationViewTree);
});
