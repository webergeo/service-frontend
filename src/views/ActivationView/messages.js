const MESSAGES = {
  edit: {
    failed: 'Ihre PAYBACK Kundennummer konnte nicht gespeichert werden',
    success: 'Ihre PAYBACK Kundennummer wurde erfolgreich gespeichert!',
    popupBlocked: 'Ihr Browser hat das Öffnen des PAYBACK Pop-ups verhindert. Bitte erstellen Sie für PAYBACK eine Ausnahme in Ihren Browsereinstellungen, um eine vollständige Nutzung der Services zu ermöglichen.'
  },
  tokenError: {
    modalContent: 'Bitte geben Sie Ihre PAYBACK Zugangsdaten erneut ein, um weiterhin die PAYBACK Services nutzen zu können. Dies ist aus Sicherheitsgründen einmal alle 18 Monate notwendig.',
    modalButton: 'PAYBACK Zugangsdaten eingeben'
  }
};

export default MESSAGES;
