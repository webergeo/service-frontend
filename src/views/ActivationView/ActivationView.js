import React, { Component } from 'react';
import PropTypes from 'prop-types';

import PaybackNumberIntroBlock from '../../components/PaybackNumberIntroBlock';
import PaybackTeaserBlock from '../../components/PaybackTeaserBlock';
import BenefitTeaserBlock from '../../components/BenefitTeaserBlock';

import './ActivationView.scss';
import MESSAGES from './messages';
import BenefitBlock from '../../components/BenefitBlock';
import QuestionsBlock from '../../components/QuestionsBlock';
import PaybackNumberModal from '../../components/PaybackNumberModal';
import MainTitle from '../../components/MainTitle';
import { parsePaybackErrorMessage } from '../../utils/apiResponseFormatter';
import logger from '../../utils/ducLogger';
import Modal from '../../components/Modal';
import LoadingSpinner from '../../widgets/LoadingSpinner';
import { HTTP_FORBIDDEN, HTTP_OK } from '../../utils/constants';
import { popupName, popupOptions, renderAccountBindingPopUp } from '../../utils/commons';
import { trackAccountBindingStart, trackPaybackNumberAddition } from '../../utils/tracking';
import { generateSyncCsrfToken, updatePaybackAccountInfo } from '../../api/Accounts';
import { isEnvParameterDefined, isEnvParameterTruthy, getEnvironmentConfig } from '../../config/appConfig';

class ActivationView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // pending indicates a pending response from the back-end.
      // If true, render shows a full-screen LoadingSpinner.
      pending: props.defaultShowSpinner,
      // modal is the active modal component, if any.
      // If truthy, render shows it.
      modal: undefined,
      // If tokenErrorModal close button was clicked.
      tokenErrorModalDismissed: false
    };

    this.closeModal = this.closeModal.bind(this);
    this.renderErrorOnAddPaybackNumber = this.renderErrorOnAddPaybackNumber.bind(this);
    this.renderErrorOnChangePaybackNumber = this.renderErrorOnChangePaybackNumber.bind(this);
    this.renderSuccess = this.renderSuccess.bind(this);
    this.renderAddPaybackNumber = this.renderAddPaybackNumber.bind(this);
    this.renderChangePaybackNumber = this.renderChangePaybackNumber.bind(this);
    this.handleAddPaybackNumber = this.handleAddPaybackNumber.bind(this);
    this.initiateAccountBindingPopUp = this.initiateAccountBindingPopUp.bind(this);
    this.handleChangePaybackNumber = this.handleChangePaybackNumber.bind(this);
    this.handelNowUnlockAllBenefits = this.handelNowUnlockAllBenefits.bind(this);
    this.renderTokenErrorModal = this.renderTokenErrorModal.bind(this);
    this.TOGGLE_KV_ENROLLMENT = isEnvParameterDefined('FEATURE_TOGGLE_KV_ENROLLMENT') ?
      isEnvParameterTruthy('FEATURE_TOGGLE_KV_ENROLLMENT') :
      props.environment.FEATURE_TOGGLE_KV_ENROLLMENT;
  }

  closeModal() {
    this.setState({ modal: undefined });
  }

  async handleChangePaybackNumber(paybackNumber) {
    this.closeModal();

    try {
      this.setState({ pending: true });
      const payload = { paybackNumber };
      const response = await updatePaybackAccountInfo({ payload });

      // When error occurred
      if (response.status >= 400) {
        if (response.status === HTTP_FORBIDDEN) {
          await this.props.onUpdateParentView();
          this.setState({
            pending: false,
            modal: this.renderChangePaybackNumber()
          });
          return;
        }
        const error = await response.json();
        this.setState({
          pending: false,
          modal: this.renderErrorOnChangePaybackNumber(parsePaybackErrorMessage(error))
        });
        return;
      }

      // Don't track changes.
      this.props.onUpdateParentView();

      this.setState({
        pending: false,
        modal: this.renderSuccess()
      });
    } catch (error) {
      logger.error(`Failed to change a payback number, error: ${error.message}`);
      this.setState({
        pending: false,
        modal: this.renderErrorOnChangePaybackNumber(error.message)
      });
    }
  }

  async handleAddPaybackNumber(paybackNumber) {
    this.closeModal();

    // HACK: Open the payback popup here to "help" the browser associate the
    // call to window.open with the user's click. Otherwise the browser may
    // suppress the popup. BEWARE that you MUST close the popup manually if
    // saving the payback number fails.
    //
    // NESTED HACK: Do not open a popup in local development mode, see function
    // initiateAccountBindingPopUp below.
    //
    // We SHOULD work towards a solution without popups to improve the
    // usability and get rid of this hack. See for example
    // https://developer.mozilla.org/en-US/docs/Web/API/Window/open#Usability_issues
    const popup = (process.env.NODE_ENV === 'dev' && process.env.BACKEND_MODE === 'local')
      ? null
      : window.open('', popupName, popupOptions());

    try {
      this.setState({ pending: true });
      const payload = { paybackNumber };
      const response = await updatePaybackAccountInfo({ payload });

      // When error occurred
      if (response.status >= 400) {
        if (popup != null) {
          popup.close();
        }
        if (response.status === HTTP_FORBIDDEN) {
          await this.props.onUpdateParentView();
          this.setState({
            pending: false,
            modal: this.renderAddPaybackNumber()
          });
          return;
        }
        const error = await response.json();
        this.setState({
          pending: false,
          modal: this.renderErrorOnAddPaybackNumber(parsePaybackErrorMessage(error))
        });
        return;
      }

      // Track payback number addition on success
      trackPaybackNumberAddition();
      this.initiateAccountBindingPopUp(paybackNumber);
      this.props.onUpdateParentView();

      this.setState({
        pending: false,
        modal: undefined
      });
    } catch (error) {
      logger.error(`Failed to add a payback number, error: ${error.message}`);
      if (popup != null) {
        popup.close();
      }
      this.setState({
        pending: false,
        modal: this.renderErrorOnAddPaybackNumber(error.message)
      });
    }
  }

  initiateAccountBindingPopUp(paybackNumber) {
    trackAccountBindingStart();
    const xhr = generateSyncCsrfToken();
    if (xhr.status !== HTTP_OK) {
      this.setState(() => ({
        accountBindingStatus: 'failure'
      }));

      return;
    }
    const { csrfToken } = JSON.parse(xhr.response);

    let renderPopUp = renderAccountBindingPopUp;

    if (process.env.NODE_ENV === 'dev' && process.env.BACKEND_MODE === 'local') {
      renderPopUp = () => {
        const fetch = require('isomorphic-fetch'); // eslint-disable-line global-require
        fetch(`${getEnvironmentConfig('PAYBACK_SERVICE_BASE_URL')}/mock-api/accountbinding`)
          .then(res => res.json())
          .then((body) => {
            window.location.href = `${window.location.pathname}?accountBindingStatus=${body.mockedStatus}`;
          });

        return true;
      };
    }

    const success = renderPopUp(csrfToken, paybackNumber);

    if (!success) {
      throw new Error(MESSAGES.edit.popupBlocked);
    }
  }

  handelNowUnlockAllBenefits() {
    this.initiateAccountBindingPopUp(this.props.accountInfo.paybackNumber);
  }

  renderErrorOnAddPaybackNumber(errorMessage) {
    const onClick = () => {
      this.setState({
        modal: this.renderAddPaybackNumber()
      });
    };
    return (
      <Modal
        title={MESSAGES.edit.failed}
        content={errorMessage}
        type="error"
        handleCancel={onClick}
        handleSubmit={onClick}
      />
    );
  }

  renderErrorOnChangePaybackNumber(errorMessage) {
    const onClick = () => {
      this.setState({
        modal: this.renderChangePaybackNumber()
      });
    };
    return (
      <Modal
        title={MESSAGES.edit.failed}
        content={errorMessage}
        type="error"
        handleCancel={onClick}
        handleSubmit={onClick}
      />
    );
  }

  renderSuccess() {
    return (
      <Modal
        title={MESSAGES.edit.success}
        type="success"
        handleCancel={this.closeModal}
        handleSubmit={this.closeModal}
      />
    );
  }

  renderAddPaybackNumber(errorMessage) {
    return (
      <PaybackNumberModal
        onCancel={this.closeModal}
        onSubmit={this.handleAddPaybackNumber}
        defaultValidation={{ error: !!errorMessage, message: errorMessage || '' }}
      />
    );
  }

  renderChangePaybackNumber(errorMessage) {
    return (
      <PaybackNumberModal
        onCancel={this.closeModal}
        onSubmit={this.handleChangePaybackNumber}
        defaultValidation={{ error: !!errorMessage, message: errorMessage || '' }}
        activateService={false}
      />
    );
  }

  /* eslint-disable class-methods-use-this */

  renderTokenErrorModal() {
    const footerConfig = {
      confirmAction: { label: MESSAGES.tokenError.modalButton }
    };

    return (
      <Modal
        type="warning"
        title={MESSAGES.tokenError.modalContent}
        footerConfig={footerConfig}
        handleSubmit={() => this.setState({ modal: this.handelNowUnlockAllBenefits() })}
        handleClose={() => this.setState({ tokenErrorModalDismissed: true })}
      />
    );
  }

  /* eslint-enable class-methods-use-this */

  render() {
    const { paybackNumber, tokenStatus } = this.props.accountInfo;
    const { pending, modal, tokenErrorModalDismissed } = this.state;

    return (
      <div className="rs-payback__activationView">
        <MainTitle className="rs-payback__activationView-title">Jetzt exklusive PAYBACK Services bei REWE nutzen</MainTitle>
        <div className="rs-payback__activationView-content">
          {!paybackNumber && (
            <div className="rs-payback__activationView-content-sibling">
              <BenefitTeaserBlock onClick={() => { this.setState({ modal: this.renderAddPaybackNumber() }); }} />
            </div>
          )}
          {!paybackNumber && (
            <div className="rs-payback__activationView-content-sibling">
              <PaybackTeaserBlock kv={this.TOGGLE_KV_ENROLLMENT} />
            </div>
          )}
          {paybackNumber && <PaybackNumberIntroBlock
            paybackNumber={paybackNumber}
            onEditButtonClick={() => { this.setState({ modal: this.renderChangePaybackNumber() }); }}
            onDeleteButtonClick={this.props.onDeletePaybackNumber}
          />
          }
        </div>

        {!paybackNumber && <MainTitle className="rs-payback__activationView-title">Ihre PAYBACK Vorteile</MainTitle>}
        <div className="rs-payback__activationView-content">
          <BenefitBlock
            withFirstHighlighted={!!paybackNumber}
            onClick={() => {
              // Add new payback number or bind an existing payback number.
              if (!paybackNumber) {
                this.setState({ modal: this.renderAddPaybackNumber() });
                return;
              }
              this.handelNowUnlockAllBenefits();
            }}
          />
        </div>

        <MainTitle className="rs-payback__activationView-title">Fragen zu PAYBACK?</MainTitle>
        <div className="rs-payback__activationView-content">
          <QuestionsBlock />
        </div>

        {/* Render the active modal, if any. */}
        {modal}

        {!modal && !tokenErrorModalDismissed && tokenStatus && tokenStatus !== 'valid' && this.renderTokenErrorModal()}

        {/* Render a full-screen spinner, if pending. */}
        {pending && <LoadingSpinner />}
      </div>
    );
  }
}

ActivationView.propTypes = {
  environment: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  accountInfo: PropTypes.shape({
    paybackNumber: PropTypes.string,
    tokenStatus: PropTypes.string
  }).isRequired,
  onUpdateParentView: PropTypes.func.isRequired,
  defaultShowSpinner: PropTypes.bool,
  onDeletePaybackNumber: PropTypes.func.isRequired
};

ActivationView.defaultProps = {
  environment: {},
  defaultShowSpinner: false
};

export default ActivationView;
