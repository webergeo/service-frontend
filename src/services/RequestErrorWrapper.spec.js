import test from 'ava';

import { hasError } from './RequestErrorWrapper';

test('hasError works as expected', (t) => {
  const response = {
    data: 'someData'
  };

  t.false(hasError(response));

  response.errorStatus = 'error';
  t.true(hasError(response));
});
