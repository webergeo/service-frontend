/**
 * Service layer for Payback account data requests. Provides response unpacking, exception handling and common
 * facilities.
 *
 * TODO: updateAccount, deleteAccount
 */
import { getPaybackAccountInfo, getPaybackPoints } from '../api/Accounts';
import {
  hasEBonOptInFlag,
  hasBonusCouponOptInFlag,
  hasEwe16OptInFlag,
  hasPaybackAccountBinding,
  getPaybackNumber,
  hasGroupEweOptInFlag,
  getTokenStatus
} from '../utils/apiResponseFormatter';
import { apiError, generalError } from './RequestErrorWrapper';
import { HTTP_FORBIDDEN } from '../utils/constants';

import logger from '../utils/ducLogger';

export const getAccountInfo = async (token = {}) => {
  try {
    const response = await getPaybackAccountInfo({ token });
    if (response.status >= 400) {
      if (response.status !== HTTP_FORBIDDEN) {
        logger.error(`Error getting account info, status: ${response.status}`);
      }
      return apiError(response);
    }

    const paybackNumber = getPaybackNumber(response);
    const paybackAccountBound = hasPaybackAccountBinding(response);
    const ewe16OptInExists = hasEwe16OptInFlag(response);
    const eBonOptInExists = hasEBonOptInFlag(response);
    const bonusCouponOptInExists = hasBonusCouponOptInFlag(response);
    const groupEweOptInExists = hasGroupEweOptInFlag(response);
    const tokenStatus = getTokenStatus(response);
    return {
      paybackNumber,
      paybackAccountBound,
      ewe16OptInExists,
      eBonOptInExists,
      bonusCouponOptInExists,
      groupEweOptInExists,
      tokenStatus
    };
  } catch (error) {
    logger.error(`Failed to get account info, error: ${error.message}`);
    return generalError(error);
  }
};

export const getPoints = async (token = {}) => {
  try {
    const response = await getPaybackPoints({ token });
    if (response.status >= 400) {
      return apiError(response);
    }
    return response;
  } catch (error) {
    logger.error(`Failed to get points, error: ${error.message}`);
    return generalError(error);
  }
};
