/**
 *
 * @param {*} errorCode
 */
export const apiError = (httpResponse = {}) => ({
  errorStatus: httpResponse.status,
  errorMessage: 'HTTP error accessing payback API'
});

/**
 *
 * @param {*} error
 */
export const generalError = (error = {}) => ({
  errorStatus: error.name,
  errorMessage: error.message
});

export const hasError = (data = {}) => !!data.errorStatus;
