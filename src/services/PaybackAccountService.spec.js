import { serial as test } from 'ava';
import nock from 'nock';
import { spy } from 'sinon';
import authTokenMock from '../mock-data/authTokenMock';
import paybackPointMock from '../mock-data/paybackPointMock';
import paybackAccountMock from '../mock-data/paybackAccountMock';
import { loadConfiguration } from '../../dev-settings/dev-config';
import { getAccountInfo, getPoints } from './PaybackAccountService';

test.before(() => {
  loadConfiguration('test');
  nock('http://localhost:3004')
    .post('/api/token', {
      scope: 'read',
      grant_type: 'client_credentials'
    })
    .reply(200, authTokenMock)
    .get('/api/customers/me/paybackaccount')
    .reply(200, paybackAccountMock)
    .get('/api/customers/me/paybackaccount/points')
    .reply(200, paybackPointMock);
});

test('PaybackAccountService# getAccountInfo OK response check', async (t) => {
  const response = await getAccountInfo();
  t.is(response.paybackNumber, paybackAccountMock.paybackNumber);
  t.is(response.ewe16OptInExists, false);
  t.is(response.eBonOptInExists, true);
});

test('PaybackAccountService# getPoints OK response check', async (t) => {
  const response = await getPoints();
  t.deepEqual(response, paybackPointMock);
});

test('PaybackAccountService# getPoints HTTP error response check', async (t) => {
  nock('http://localhost:3004')
    .get('/api/customers/me/paybackaccount/points')
    .reply(400, 'Bad request');
  const response = await getPoints();
  t.true(response.errorStatus !== undefined);
  t.true(response.errorMessage !== undefined);
});

test('PaybackAccountService# getPoints HTTP-FORBIDDEN error response check', async (t) => {
  nock('http://localhost:3004')
    .get('/api/customers/me/paybackaccount/points')
    .reply(403, 'Forbidden');
  const loggerSpy = spy(XRD.logging.getLogger(), 'error');
  const response = await getPoints();
  t.true(response.errorStatus !== undefined);
  t.true(response.errorMessage !== undefined);
  t.false(loggerSpy.calledWith('Error getting points, status: 403'));
  loggerSpy.restore();
});

test('PaybackAccountService# getPoints general error response check', async (t) => {
  process.env.URL_PAYBACK_POINTS = undefined;
  const loggerSpy = spy(XRD.logging.getLogger(), 'error');
  const response = await getPoints();
  t.true(response.errorStatus !== undefined);
  t.true(response.errorMessage !== undefined);
  t.true(loggerSpy.called);
  loggerSpy.restore();
});
