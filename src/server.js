import { env } from 'process';
import express from 'express';
import { GracefulShutdownManager } from '@moebius/http-graceful-shutdown';

import app from './ssr';
import logger from './ssr/logger';

const SSR_PORT = env.SERVICE_PORT || '8080';
const SSR_ENV = env.NODE_ENV || 'development';

app.set('views', 'templates');
app.use('/assets', express.static('assets'));

// start the server
const server = app.listen(SSR_PORT, (err) => {
  if (err) {
    logger.error(err);
  }
  logger.info(`Server running @ ${SSR_PORT} [${SSR_ENV}]`);
});

// ------------------------------- Process shutdown handlers ------------------------------------

const shutdownManager = new GracefulShutdownManager(server);

const shutDown = () => {
  shutdownManager.terminate(() => {
    logger.info('ExpressJS has been shut down gracefully!');
  });
};

process.on('SIGINT', shutDown);
process.on('SIGTERM', shutDown);
