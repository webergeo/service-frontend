import { buildPaybackAccountResponse } from './utils';

module.exports = {
  paybackaccount: [buildPaybackAccountResponse({
    pbNumber: '3434342232',
    withEbon: true,
    withEwe16: true,
    withAccountBinding: true,
    withBonusCoupon: true,
    withGroupEwe: false,
    tokenStatus: 'valid'
  })],
  tokenGenerator: [
    {
      id: 'unique-csrftoken',
      csrfToken: '2fcf5c92-625d-407d-abf4-d253653a6d51'
    }
  ]
};
