const jsonServer = require('json-server');  //  https://github.com/typicode/json-server

const authMiddleware = require('./middlewares/auth');
const paybackMiddleware = require('./middlewares/payback');
const accountBindingMiddleware = require('./middlewares/accountBinding');

const paybackTokenRedirectMiddleware = require('./middlewares/paybackTokenRedirect');

const mockDb = require('./initialMockState');
const URL_SETTINGS = require('./urls');

const appConfig = require('../config/appConfig');

const server = jsonServer.create();         //  Returns an Express server.
const router = jsonServer.router(mockDb);   //  Returns JSON Server router.
const middlewares = jsonServer.defaults();  //  Returns middlewares used by JSON Server.

const PORT = 3004;

/**
 * Use middleware
 */
server.use(jsonServer.bodyParser);          //  bodyParser enable body-parser middleware (default: true)
// Custom middleware
// http://expressjs.com/de/api.html#app.use
server.use((req, res, next) => {
  // Header for both SSR and CSR API mock
  res.setHeader('Access-Control-Allow-Origin', '*');
  next();                                   //  continue to JSON Server router
});

/* ************** API responses (start) *************** */

server.post(`${URL_SETTINGS.URL_AUTH_SERVICE}/token`, authMiddleware.authToken);
server.get(URL_SETTINGS.URL_PAYBACK_ACCOUNT, paybackMiddleware.getAccount);

/**
 * Mock-API for mocking PAYBACK account binding response on local development
 */
server.get(URL_SETTINGS.URL_ACCOUNT_BINDING_STATUS_MOCK, accountBindingMiddleware.accountBindingSuccess);
// server.get(URL_SETTINGS.URL_ACCOUNT_BINDING_STATUS_MOCK, accountBindingMiddleware.accountBindingFailed);

/**
 * Payback number create/update request
 */
server.put(URL_SETTINGS.URL_PAYBACK_ACCOUNT, paybackMiddleware.updateAccount); // Build-in error for payback number 0000000000

/**
 * Payback number deletion request
 */
server.delete(URL_SETTINGS.URL_PAYBACK_ACCOUNT, paybackMiddleware.deleteAccountSucceeded);
// server.delete(URL_SETTINGS.URL_PAYBACK_ACCOUNT, paybackMiddleware.deleteAccountFailed);
// server.delete(URL_SETTINGS.URL_PAYBACK_ACCOUNT, paybackMiddleware.forbidden('error message'));

/**
 * PointsApp
 */
server.get(URL_SETTINGS.URL_PAYBACK_POINTS, paybackMiddleware.getPointsSucceeded);
// server.get(URL_SETTINGS.URL_PAYBACK_POINTS, paybackMiddleware.getPointsFailed);
// server.get(URL_SETTINGS.URL_PAYBACK_POINTS, paybackMiddleware.forbidden('expired'));

/**
 * GroupEWE
 */
server.get(URL_SETTINGS.URL_PAYBACK_GROUP_EWE, paybackMiddleware.getGroupEweSucceeded);
// server.get(URL_SETTINGS.URL_PAYBACK_GROUP_EWE, paybackMiddleware.getGroupEweFailed);
// server.get(URL_SETTINGS.URL_PAYBACK_GROUP_EWE, paybackMiddleware.forbidden('error message'));

/**
 * Ebon and Ewe16
 */
server.put(URL_SETTINGS.URL_PAYBACK_EWE16, paybackMiddleware.toggleEwe16Succeeded);
// server.put(URL_SETTINGS.URL_PAYBACK_EWE16, paybackMiddleware.toggleEbonFailed);
// server.put(URL_SETTINGS.URL_PAYBACK_EWE16, paybackMiddleware.forbidden('error message'));

server.put(URL_SETTINGS.URL_PAYBACK_EBON, paybackMiddleware.toggleEbonSucceeded);
// server.put(URL_SETTINGS.URL_PAYBACK_EBON, paybackMiddleware.toggleEbonFailed);
// server.put(URL_SETTINGS.URL_PAYBACK_EBON, paybackMiddleware.forbidden('error message'));

/**
 * Delete Ebon Receipts
 */
server.delete(URL_SETTINGS.URL_EBON_RECEIPTS, paybackMiddleware.ebonReceiptDeletionSucceeds);
//server.delete(URL_SETTINGS.URL_EBON_RECEIPTS, paybackMiddleware.ebonReceiptDeletionFailes);

server.get(URL_SETTINGS.URL_PAYBACK_EBON_OPT_IN_AVAILABILITY, paybackMiddleware.ebonOptInAvailable);
// server.get(URL_SETTINGS.URL_PAYBACK_EBON_OPT_IN_AVAILABILITY, paybackMiddleware.ebonOptInNotAvailable);
// server.get(URL_SETTINGS.URL_PAYBACK_EBON_OPT_IN_AVAILABILITY, paybackMiddleware.forbidden('error message'));

server.put(URL_SETTINGS.URL_PAYBACK_BONUS_COUPON, paybackMiddleware.toggleBonusCouponSucceeded);
// server.put(URL_SETTINGS.URL_PAYBACK_BONUS_COUPON, paybackMiddleware.toggleBonusCouponFailed);
// server.put(URL_SETTINGS.URL_PAYBACK_BONUS_COUPON, paybackMiddleware.forbidden('error message'));

/**
 * Account unbinding
 */
server.delete(URL_SETTINGS.URL_PAYBACK_ACCOUNT_BINDING, paybackMiddleware.accountUnbindSucceeded);
// server.delete(URL_SETTINGS.URL_PAYBACK_ACCOUNT_BINDING, paybackMiddleware.accountUnbindFailed);
// server.delete(URL_SETTINGS.URL_PAYBACK_ACCOUNT_BINDING, paybackMiddleware.forbidden('error message'));

/**
 * Token redirect
 */
server.get(URL_SETTINGS.URL_PAYBACK_SESSION_TOKEN_REDIRECT, paybackTokenRedirectMiddleware.tokenRedirect);

server.get('/api/payback/csrfToken/web', async (req, res) => {
  /* eslint-disable-next-line */
  mockDb.paybackaccount[0]._embedded.ebon.optIn = req.query.ebon === 'true';
  res.json({
    csrfToken: '2fcf5c92-625d-407d-abf4-d253653a6d51'
  });
});

server.get('/pb/qa_oauth_accountbinding_rewe', async (req, res) => {
  res.send(`<!doctype html>
    <a href="/mydata/payback/redirectionendpoint">Continue</a>
  `);
});

server.get('/mydata/payback/redirectionendpoint', async (req, res) => {
  await fetch('http://localhost:3004/mock-api/accountbinding');
  res.send(`<!doctype html>
    <script>
      if (window.opener) {
        var redirectUrl=window.opener.location.href +
          (window.opener.location.search ? '&' : '?') +
          'accountBindingStatus=success';
        window.opener.location.href=redirectUrl;
        window.close();
      }
    </script>`);
});

/* ************** API responses (end) *************** */

server.use(middlewares);
server.use(router);

server.listen(PORT, () => {
  console.info(`🔆  Mock API JSON-Server is running under ${PORT} 🔆`);
});
