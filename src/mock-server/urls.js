module.exports = {
  URL_AUTH_SERVICE: '/api',
  URL_PAYBACK_ACCOUNT_INITIAL_STATE: '/api/customers/me/paybackaccount',
  URL_PAYBACK_ACCOUNT: '/api/customers/me/paybackaccount',
  URL_PAYBACK_EWE16: '/api/customers/me/paybackaccount/ewe16',
  URL_PAYBACK_CSRF_TOKEN_WEB: '/api/payback/csrfToken/web',
  URL_PAYBACK_EBON: '/api/customers/me/paybackaccount/ebon',
  URL_PAYBACK_EBON_OPT_IN_AVAILABILITY: '/api/customers/me/ebonOptInAvailability',
  URL_PAYBACK_BONUS_COUPON: '/api/customers/me/paybackaccount/bonus-coupon',
  URL_PAYBACK_ACCOUNT_BINDING: '/api/customers/me/paybackaccount/accountbinding',
  URL_PAYBACK_SESSION_TOKEN_REDIRECT: '/api/customers/me/paybackaccount/tokenredirect',
  URL_PAYBACK_POINTS: '/api/customers/me/paybackaccount/points',
  URL_PAYBACK_GROUP_EWE: '/api/customers/me/paybackaccount/group-ewe',
  URL_ACCOUNT_BINDING_STATUS_MOCK: '/mock-api/accountbinding',
  URL_EBON_RECEIPTS: '/api/receipts'
};
