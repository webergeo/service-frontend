/* eslint-disable no-underscore-dangle */
import paybackAccountMock from '../mock-data/paybackAccountMock';

export function buildPaybackAccountResponse({
  pbNumber = '0123456789',
  withEbon = false,
  withEwe16 = false,
  withAccountBinding = false,
  withBonusCoupon = false,
  withGroupEwe = false,
  tokenStatus = 'valid'
} = {}) {
  const paybackAccount = Object.assign({}, paybackAccountMock);
  paybackAccount.id = pbNumber;
  paybackAccount.paybackNumber = pbNumber;
  paybackAccount._embedded.ebon.optIn = withEbon;
  paybackAccount._embedded.bonusCoupon.optIn = withBonusCoupon;
  paybackAccount._embedded.groupEwe.optIn = withGroupEwe;
  paybackAccount._embedded.ewe16.optIn = withEwe16;
  paybackAccount._embedded.accountBindingExists = withAccountBinding;
  paybackAccount._embedded.tokenStatus = tokenStatus;
  return paybackAccount;
}

export function buildErrorResponse(message = 'Mocked validation message') {
  return {
    _status: {
      validationMessages: [{ message }]
    }
  };
}
