/* eslint-disable no-underscore-dangle */
import mockDb from '../initialMockState';

/**
 * Mock-API for mocking account binding response on local development
 */
export const accountBindingSuccess = (req, res) => {
  // FIXME: mockDb.paybackaccount may be an empty array
  mockDb.paybackaccount[0]._embedded.accountBindingExists = true;
  mockDb.paybackaccount[0]._embedded.tokenStatus = 'valid';
  res.status(200).send({ mockedStatus: 'success' });
};

export const accountBindingFailed = (req, res) => {
  res.status(200).send({ mockedStatus: 'failure' });
};
