/* eslint-disable no-underscore-dangle */
import { buildPaybackAccountResponse, buildErrorResponse } from '../utils';
import mockDb from '../initialMockState';
import groupEweMock from '../../mock-data/groupEweMock';

const INVALID_PAYBACK_NUMBER = '0000000000';
const NOTAVAIBLE_PAYBACK_NUMBER = '1111111111';

export const forbidden = tokenStatus => (req, res) => {
  mockDb.paybackaccount[0]._embedded.tokenStatus = tokenStatus;
  res.status(403).send(buildErrorResponse(`Token ${tokenStatus}`));
};

export const getAccount = (req, res) => {
  if (mockDb.paybackaccount.length > 0) {
    res.status(200).send(mockDb.paybackaccount[0]);
  } else {
    res.status(204).end();
  }
};

export const updateAccount = (req, res) => {
  const { paybackNumber } = req.body;

  if (paybackNumber === INVALID_PAYBACK_NUMBER) {
    res.status(400).send(buildErrorResponse('Mock: Update failed with 400'));
  } else if (paybackNumber === NOTAVAIBLE_PAYBACK_NUMBER) {
    res.status(503).send(buildErrorResponse('Mock: PBS not avaible with 503'));
  } else {
    const newResponse = buildPaybackAccountResponse({ pbNumber: paybackNumber });
    mockDb.paybackaccount = [newResponse];
    res.status(200).end();
  }
};

export const deleteAccountSucceeded = (req, res) => {
  mockDb.paybackaccount = [];
  res.status(204).end();
};

export const deleteAccountFailed = (req, res) => {
  res.status(400).send(buildErrorResponse('Mock message: payback number deletion fails...'));
};

export const accountUnbindSucceeded = (req, res) => {
  mockDb.paybackaccount[0]._embedded.accountBindingExists = false;
  res.status(200).end();
};

export const accountUnbindFailed = (req, res) => {
  res.status(400).send(buildErrorResponse('Mock: account unbind failed'));
};

export const toggleEwe16Succeeded = (req, res) => {
  const { optIn } = req.body;
  if (!optIn && !mockDb.paybackaccount[0]._embedded.groupEwe.optIn) {
    mockDb.paybackaccount[0]._embedded.bonusCoupon.optIn = optIn;
  }

  mockDb.paybackaccount[0]._embedded.ewe16.optIn = optIn;
  res.status(200).send(mockDb.paybackaccount[0]._embedded.ewe16);
};

export const toggleEwe16Failed = (req, res) => {
  const { optIn } = req.body;
  mockDb.paybackaccount[0]._embedded.ewe16.optIn = optIn;
  res.status(400).send(buildErrorResponse('Mock: ewe16 toggle failed'));
};

exports.ebonOptInAvailable = (req, res) => {
  res.status(200).end();
};

exports.ebonOptInNotAvailable = (req, res) => {
  res.status(409).send(buildErrorResponse('Mock: ebon is not available'));
};

export const toggleEbonSucceeded = (req, res) => {
  const { optIn } = req.body;
  mockDb.paybackaccount[0]._embedded.ebon.optIn = optIn;
  res.status(200).send(mockDb.paybackaccount[0]._embedded.ebon);
};

export const toggleEbonFailed = (req, res) => {
  res.status(400).send(buildErrorResponse('Mock: ebon toggle failed'));
};

export const ebonReceiptDeletionFailes = (req, res) => {
  res.status(400).send(buildErrorResponse('Mock: receipt deletion failed'));
};

export const ebonReceiptDeletionSucceeds = (req, res) => {
  res.status(200).send(buildErrorResponse('Mock: receipt deletion succeeds'));
};

export const toggleBonusCouponSucceeded = (req, res) => {
  const { optIn } = req.body;
  mockDb.paybackaccount[0]._embedded.bonusCoupon.optIn = optIn;
  res.status(200).send(mockDb.paybackaccount[0]._embedded.bonusCoupon);
};

export const toggleBonusCouponFailed = (req, res) => {
  res.status(400).send(buildErrorResponse('Mock: bonus coupon toggle failed'));
};

export const getPointsSucceeded = (req, res) => {
  res.status(200).send(mockDb.paybackaccount[0].paybackPoints);
};

export const getPointsFailed = (req, res) => {
  res.status(400).end();
};

export const getGroupEweSucceeded = (req, res) => {
  res.status(200).send(groupEweMock);
};

export const getGroupEweFailed = (req, res) => {
  res.status(400).end();
};
