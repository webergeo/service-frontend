const authTokenMock = require('../../mock-data/authTokenMock');

exports.authToken = (req, res) => {
  res.status(200).send(authTokenMock);
};
