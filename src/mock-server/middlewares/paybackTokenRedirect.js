// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import { loadConfiguration } from '../../../dev-settings/dev-config';
import { getEnvironmentConfig as getEnv } from '../../config/appConfig';
import { generateToken } from '../../api/AuthTokenClient';
import { get } from '../../api/ApiClient';

loadConfiguration('preprod');

// generate Payback weak session token
const generateSessionToken = async () => {
  const { token: sessionToken } = await get(getEnv('URL_PAYBACK_SESSION_STRONG_TOKEN'), {}, await generateToken({}));
  return sessionToken;
};

// CommonJS DAFUQ????
exports.tokenRedirect = async (req, res) => {
  // step 1) retrieve PB-initialized redirect URL
  // console.info(`PB redirect URL template: ${req.query.redirectUrlTemplate}`);

  // step 2) retrieve PB (initially weak) sesson token
  const sessionToken = await generateSessionToken();
  // console.info(`PB (weak) session token: ${sessionToken}`);

  // step 3) replace the {token} placeholder in the URL template with the token value
  const redirectUrlTemplate = req.query.redirectUrlTemplate.replace('{token}', sessionToken);
  // console.info(`Target URL: ${redirectUrlTemplate}`);

  // step 4) redirect to the resulting URL -> MAGIC!!!!!!!! IT'S A-L-I-V-E!!!!!!
  res.redirect(redirectUrlTemplate);
};

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
