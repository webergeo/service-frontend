import test from 'ava';
import httpGet from './xml-http-request';

test('should be a function', (t) => {
  t.is(typeof httpGet, 'function');
});
