import test from 'ava';
import nock from 'nock';
import authTokenMock from '../mock-data/authTokenMock';
import { loadConfiguration } from '../../dev-settings/dev-config';
import { deleteAllReceipts } from './Ebons';

const authOptions = {
  payload: {
    scope: 'read',
    grant_type: 'client_credentials'
  },
  token: {
    Authorization: 'Basic 1235483453345',
    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
  }
};

test.before(() => {
  loadConfiguration('test');
  nock('http://localhost:3004')
    .defaultReplyHeaders({
      'Content-Type': 'application/json'
    })
    .persist()
    .post('/api/token', {
      scope: 'read',
      grant_type: 'client_credentials'
    })
    .reply(200, authTokenMock)
    .delete('/api/receipts')
    .query(true)
    .reply(204, '', { 'Content-Type': 'text/plain' });
});

test('API# deleteAllReceipts response check', async (t) => {
  const response = await deleteAllReceipts();
  t.is(response.status, 204);
});