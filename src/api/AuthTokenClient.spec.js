import test from 'ava';
import nock from 'nock';
import cache from 'memory-cache';
import uuid from 'uuid/v1';
import { stringify } from 'query-string';
import { loadConfiguration } from '../../dev-settings/dev-config';
import { generateToken } from './AuthTokenClient';

/**
 * Notes:
 *
 * - need to run these tests serially due to side effects on Nock and memory-cache
 *
 * - cache hits() statistic cannot be used in assertions due to cummulative effects by multiple tests on the global cache
 * instance.
 *
 * - Nock semantics are a mess! The effects of persist() cannot be negated by subsequent re-specification of
 * responses unless cleanAll() is called on the >global< nock scope!
 */
const inputToken = {
  Authorization: `Bearer ${uuid()}`,
  'auth-info-user-id': uuid()
};

const PAYLOAD = stringify({
  scope: 'read',
  grant_type: 'client_credentials'
});

// generates a mocked "machine token" normally issued by rewe-auth-service as we need varying access_token values
const makeAuthToken = () => ({
  access_token: uuid(),
  token_type: 'bearer',
  expires_in: 1,
  scope: 'read'
});

test.before(() => {
  loadConfiguration('test');
  nock('http://localhost:3004')
    .persist()
    .post('/api/token', PAYLOAD)
    .reply(200, makeAuthToken());
  process.env.USE_EXPLICIT_AUTHENTICATION = true;
});

test.serial('generateToken() Should produce a new token if EXP.AUTH=true', async (t) => {
  const outputToken = await generateToken(inputToken);
  t.notDeepEqual(outputToken, inputToken);
  t.true(outputToken.Authorization !== undefined);
  t.true(outputToken['auth-info-user-id'] !== undefined);
});

/* eslint-disable no-plusplus,no-await-in-loop */
test.serial('generateToken() should return cached token for multiple calls within TTL limit', async (t) => {
  // produce initial cached value
  cache.clear();
  const initialToken = await generateToken(inputToken);
  t.notDeepEqual(initialToken, inputToken);
  t.is(cache.size(), 1);

  for (let i = 0; i < 10; i++) {
    const nextToken = await generateToken(inputToken);
    t.deepEqual(initialToken, nextToken);
  }
});

/* eslint-disable no-plusplus,no-await-in-loop */
test.serial('generateToken() should get a new token after TTL expires', async (t) => {
  cache.clear();
  cache.debug(false);

  // produce initial cached value and ensure it is cached
  const lastToken = await generateToken(inputToken);
  t.notDeepEqual(lastToken, inputToken);

  let nextToken = await generateToken(inputToken);
  t.deepEqual(lastToken, nextToken);
  t.is(cache.size(), 1);

  // wait for expiration and ensure cached value is removed from cache
  await new Promise(resolve => setTimeout(resolve, 1010)); // padded by 10ms to ensure reliable expiration should VM jitter interfere slightly
  t.is(cache.size(), 0);

  // make nock forget the previous spec and re-init the authToken response with a new value
  nock.cleanAll();
  nock('http://localhost:3004')
    .post('/api/token', PAYLOAD)
    .reply(200, makeAuthToken());

  nextToken = await generateToken(inputToken);
  t.is(cache.size(), 1);
  t.notDeepEqual(lastToken, nextToken);
});

test.serial('generateToken() Should survive an error inside getMachineToken() if EXP.AUTH=true', async (t) => {
  nock.cleanAll();
  nock('http://localhost:3004')
    .post('/api/token', PAYLOAD)
    .reply(500);

  cache.clear();
  const outputToken = await generateToken(inputToken);
  t.notDeepEqual(outputToken, inputToken);
  t.true(outputToken.Authorization === 'Bearer undefined');
  t.true(outputToken['auth-info-user-id'] !== undefined);
});

test.serial('generateToken() Should pass through input token (NO-OP) if EXP.AUTH=false', async (t) => {
  process.env.USE_EXPLICIT_AUTHENTICATION = false;
  const outputToken = await generateToken(inputToken);
  t.deepEqual(outputToken, inputToken);
});
