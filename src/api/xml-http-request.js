import logger from '../utils/ducLogger';
/**
 * Old approach of doing the client side AJAX request.
 * Currently only used for synchronous request to open the popup window (EBonBlock Component).
 *
 * @param  {String} apiResource   - api URL endpoint
 * @param  {Boolean} asynchronous - flag for asynchronous request
 * @return {XMLHttpRequest}       [description]
 */
const httpGet = ({ apiResource, asynchronous }) => {
  const url = apiResource;
  const xmlHttp = new XMLHttpRequest();
  xmlHttp.open('GET', url, asynchronous);
  xmlHttp.send(null);
  xmlHttp.onerror = function (error) {
    logger.error(`Failed to open ebonBlock popup, error: ${error.message}`);
  };
  return xmlHttp;
};

export default httpGet;
