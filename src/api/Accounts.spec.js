import test from 'ava';
import nock from 'nock';
import authTokenMock from '../mock-data/authTokenMock';
import paybackPointMock from '../mock-data/paybackPointMock';
import groupEweMock from '../mock-data/groupEweMock';
import paybackAccountMock from '../mock-data/paybackAccountMock';
import { loadConfiguration } from '../../dev-settings/dev-config';

import {
  getAuthToken,
  getPaybackAccountInfo,
  updatePaybackAccountInfo,
  deletePaybackAccountInfo,
  deletePaybackAccountBinding,
  getPaybackPoints,
  getGroupEwe,
  ewe16OptToggle,
  ebonOptToggle
} from './Accounts';

const authOptions = {
  payload: {
    scope: 'read',
    grant_type: 'client_credentials'
  },
  token: {
    Authorization: 'Basic 1235483453345',
    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
  }
};

test.before(() => {
  loadConfiguration('test');
  nock('http://localhost:3004')
    .defaultReplyHeaders({
      'Content-Type': 'application/json'
    })
    .persist()
    .post('/api/token', {
      scope: 'read',
      grant_type: 'client_credentials'
    })
    .reply(200, authTokenMock)
    .get('/api/customers/me/paybackaccount')
    .reply(200, paybackAccountMock)
    .put('/api/customers/me/paybackaccount', { paybackNumber: '1234567890' })
    .reply(200, { ...paybackAccountMock, paybackNumber: '1234567890' })
    .delete('/api/customers/me/paybackaccount')
    .reply(204, '', { 'Content-Type': 'text/plain' })
    .delete('/api/customers/me/paybackaccount/accountbinding')
    .reply(200, '', { 'Content-Type': 'text/plain' })
    .get('/api/customers/me/paybackaccount/points')
    .reply(200, paybackPointMock)
    .get('/api/customers/me/paybackaccount/group-ewe')
    .reply(200, groupEweMock)
    .put('/api/customers/me/paybackaccount/ewe16', { optIn: false })
    .reply(200, { optIn: false })
    .put('/api/customers/me/paybackaccount/ebon', { optIn: true })
    .reply(200, { optIn: true });
});

// TODO: Consider to refactor tests and be sure, that the correct stuff is tested: Do we need nock here? Why?
test('API# getAuthToken response check', async (t) => {
  const response = await getAuthToken({ url: 'http://localhost:3004/api/token', payload: authOptions.payload });
  t.deepEqual(response, authTokenMock);
});

test('API# getPaybackAccountInfo response check', async (t) => {
  const response = await getPaybackAccountInfo();
  t.deepEqual(response, paybackAccountMock);
});

test('API# updatePaybackAccountInfo response check', async (t) => {
  const response = await updatePaybackAccountInfo({ payload: { paybackNumber: '1234567890' } });
  t.deepEqual(response, { ...paybackAccountMock, paybackNumber: '1234567890' });
});

test('API# deletePaybackAccountInfo response check', async (t) => {
  const response = await deletePaybackAccountInfo();
  t.is(response.status, 204);
});

test('API# deletePaybackAccountBinding response check', async (t) => {
  const response = await deletePaybackAccountBinding();
  t.is(response.status, 200);
});

test('API# getPaybackPoints response check', async (t) => {
  const response = await getPaybackPoints();
  t.deepEqual(response, paybackPointMock);
});

test('API# getGroupEwe response check', async (t) => {
  const response = await getGroupEwe();
  t.deepEqual(response, groupEweMock);
});

test('API# ewe16OptToggle response check', async (t) => {
  const response = await ewe16OptToggle({ payload: { optIn: false } });
  t.deepEqual(response, { optIn: false });
});

test('API# ebonOptToggle response check', async (t) => {
  const response = await ebonOptToggle({ payload: { optIn: true } });
  t.deepEqual(response, { optIn: true });
});
