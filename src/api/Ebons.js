import { remove } from './ApiClient';
import { generateToken } from './AuthTokenClient';
import { getEnvironmentConfig as getEnv, getEnvironmentConfig } from '../config/appConfig';

export const deleteAllReceipts = async ({ token } = {}) =>
  remove(getEnvironmentConfig('URL_RECEIPTS'), { customerId: getEnv('CUSTOMER_UUID') }, await generateToken(token));
