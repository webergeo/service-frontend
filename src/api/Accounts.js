import { get, post, put, remove } from './ApiClient';
import httpGet from './xml-http-request';
import { generateToken } from './AuthTokenClient';

import { getEnvironmentConfig } from '../config/appConfig';
import { HTTP_OK } from '../utils/constants';

export const getAuthToken = ({ url, payload, token }) => post(url, payload, token);

export const getInitialStatePaybackAccountInfo = ({ url, params, token } = {}) => get(url, params, token);

export const getPaybackAccountInfo = async ({ params, token } = {}) =>
  get(getEnvironmentConfig('URL_PAYBACK_ACCOUNT'), params, await generateToken(token));

export const updatePaybackAccountInfo = async ({ payload, token } = {}) =>
  put(getEnvironmentConfig('URL_PAYBACK_ACCOUNT'), payload, await generateToken(token));

export const deletePaybackAccountInfo = async ({ token } = {}) => remove(getEnvironmentConfig('URL_PAYBACK_ACCOUNT'), {}, await generateToken(token));

export const deletePaybackAccountBinding = async ({ token } = {}) =>
  remove(getEnvironmentConfig('URL_PAYBACK_ACCOUNT_BINDING'), {}, await generateToken(token));

export const getPaybackPoints = async ({ params, token } = {}) => get(getEnvironmentConfig('URL_PAYBACK_POINTS'), params, await generateToken(token));

export const getGroupEwe = async ({ params, token } = {}) => get(getEnvironmentConfig('URL_PAYBACK_GROUP_EWE'), params, await generateToken(token));

export const ewe16OptToggle = async ({ payload, token } = {}) => put(getEnvironmentConfig('URL_PAYBACK_EWE16'), payload, await generateToken(token));

export const ebonOptToggle = async ({ payload, token } = {}) => put(getEnvironmentConfig('URL_PAYBACK_EBON'), payload, await generateToken(token));

export const bonusCouponOptToggle = async ({ payload, token } = {}) => put(getEnvironmentConfig('URL_PAYBACK_BONUS_COUPON'), payload, await generateToken(token));

export const getSyncEbonOptInAvailability = () => httpGet({ apiResource: getEnvironmentConfig('URL_PAYBACK_EBON_OPT_IN_AVAILABILITY'), asynchronous: false });

export const generateSyncCsrfToken = (ebon = false) => httpGet({
  apiResource: getEnvironmentConfig('URL_PAYBACK_CSRF_TOKEN_WEB') + (ebon ? '?ebon=true' : ''),
  asynchronous: false
});

export const getAccountBinding = async (authCode) => {
  const xhr = generateSyncCsrfToken();
  if (xhr.status !== HTTP_OK) {
    return;
  }

  const { csrfToken } = JSON.parse(xhr.response);
  const accountBindingUrl = getEnvironmentConfig('URL_PAYBACK_ACCOUNT_BINDING_REDIRECTION_ENDPOINT');
  window.location.href = `${accountBindingUrl}?state=${csrfToken}&code=${authCode}`;
};

