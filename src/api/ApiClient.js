import fetch from 'isomorphic-fetch';
import { stringify } from 'query-string';

/** Generic Rest API Client consist of CRUD operations */

/**
 * use the fetch api to perform the ajax request
 * @param {string} url - the request url
 * @param {string} method - the REST method type
 * @param {Object} params - the request parameters
 * @param {Object} body - the request body
 * @param {Object} token - list of tokens
 */
const request = async ({ url, method, params = {}, body, token = {} }) => {
  if (typeof url === 'undefined') {
    throw new Error('[url] required');
  }
  const urlWithQuery = Object.keys(params).length ? `${url}?${stringify(params)}` : url;
  let headers = {};

  if (token && Object.keys(token).length) {
    headers = {
      ...headers,
      ...token
    };
  }

  headers = {
    'Content-Type': 'application/json; charset=UTF-8',
    ...headers
  };

  const options = {
    method,
    headers,
    credentials: 'same-origin'
  };

  if (method === 'POST' || method === 'PUT') {
    options.body = typeof body === 'string' ? body : JSON.stringify(body);
  }
  try {
    const response = await fetch(urlWithQuery, options);
    if (!response.ok || response.status >= 400 || response.status === 204) {
      return response;
    }
    // For empty body
    const responseText = await response.text();
    if (responseText === '') {
      return response;
    }

    try {
      return JSON.parse(responseText);
    } catch (e) {
      return response;
    }
  } catch (e) {
    throw new Error(e);
  }
};

/**
 * GET Request
 * @param {string} requestUrl -The endpoint
 * @param {Object} params - get request parameters
 * @param {Object} token - list of tokens
 */
export const get = (requestUrl, params = {}, token = {}) =>
  request({
    url: requestUrl,
    method: 'GET',
    params,
    token
  });

/**
 * POST request
 * @param {string} requestUrl -The endpoint
 * @param {Object} payload - request body
 * @param {Object} token - list of tokens
 */
export const post = (requestUrl, payload = {}, token = {}) =>
  request({
    url: requestUrl,
    method: 'POST',
    body: payload,
    token
  });

/**
 * PUT Request
 * @param {string} requestUrl -The endpoint
 * @param {Object} payload - request body
 * @param {Object} token - list of tokens
 */
export const put = (requestUrl, payload = {}, token = {}) =>
  request({
    url: requestUrl,
    method: 'PUT',
    body: payload,
    token
  });

/**
 * DELETE Request
 * @param {string} requestUrl -The endpoint
 * @param {Object} token - list of tokens
 */
export const remove = (requestUrl, params = {}, token = {}) =>
  request({
    url: requestUrl,
    method: 'DELETE',
    params,
    token
  });
