import { stringify } from 'query-string';
import cache from 'memory-cache';
import { post } from './ApiClient';
import { getEnvironmentConfig as getEnv, isEnvParameterDefined as isEnv } from '../config/appConfig';

const CACHE_KEY = 'pb-auth-token';

const PAYLOAD = stringify({
  scope: 'read',
  grant_type: 'client_credentials'
});

const EMPTY_TOKEN = {
  access_token: ''
};

const getBase64EncodedAuthHeader = () => {
  const clientId = getEnv('AUTH_SERVICE_CLIENT_ID');
  const clientSecret = getEnv('AUTH_SERVICE_CLIENT_SECRET');
  return Buffer.from(`${clientId}:${clientSecret}`).toString('base64');
};

/** retrieve auth token from rewe-auth-service.
 *
 *  - The retrieved token is cached locally based on its TTL value, and a new one will be acquired automatically
 *    upon next call to this method once the previous token expires.
 *
 *  - The logger instance is passed externally due to two slightly different public versions of the generateToken()
 *    method - one is for local use, while the other is used in the SSR code and utilizes an ELK-compliant logger.
 */
export const getMachineToken = async (logger) => {
  let tokenResponse = EMPTY_TOKEN;
  try {
    tokenResponse = cache.get(CACHE_KEY);
    if (!tokenResponse) {
      const requestToken = {
        Authorization: `Basic ${getBase64EncodedAuthHeader()}`,
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
      };

      tokenResponse = await post(`${getEnv('URL_AUTH_SERVICE')}/token`, PAYLOAD, requestToken);
      if (tokenResponse.status >= 400) {
        throw new Error(`HTTP access error in getMachineToken(), status: ${tokenResponse.status}`);
      }

      const ttl = tokenResponse.expires_in * 1000;
      cache.put(CACHE_KEY, tokenResponse, ttl, () => {
        if (logger) {
          logger.info('Cached auth token expired, getting a new one...');
        }
      });
    }
  } catch (err) {
    if (logger) {
      logger.error(err.toString());
    }
  }
  return tokenResponse;
};

/**
 * This transparently produces a valid authentication token by requesting one from rewe-auth-service in local
 * development mode, or simply passing through the existing one in production mode.
 */
export const generateToken = async (token) => {
  let authToken = {};
  if (isEnv('USE_EXPLICIT_AUTHENTICATION') && getEnv('USE_EXPLICIT_AUTHENTICATION') === 'true') {
    const { access_token: accessToken } = await getMachineToken(console);
    authToken = {
      Authorization: `Bearer ${accessToken}`,
      'auth-info-user-id': getEnv('CUSTOMER_UUID')
    };
  }
  return {
    ...token,
    ...authToken
  };
};
