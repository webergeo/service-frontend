const uuid = require('uuid/v1');

module.exports = {
  access_token: uuid(),
  token_type: 'bearer',
  expires_in: 10,
  scope: 'read'
};
