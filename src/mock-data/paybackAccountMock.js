const pointMock = require('./paybackPointMock');

module.exports = {
  id: '1234567890',
  paybackNumber: '1234567890',
  paybackPoints: { ...pointMock },
  _embedded: {
    ebon: {
      optIn: true
    },
    bonusCoupon: {
      optIn: false
    },
    ewe16: {
      optIn: false,
      lastChange: null
    },
    groupEwe: {
      optIn: false
    },
    tokenStatus: 'valid',
    accountBindingExists: true
  }
};
