import React, { Component } from 'react';
import MESSAGES from './messages';
import pbPointee from '../../assets/images/payback-2013-0158-pointee-png.png';
import './RegistrationBanner.scss';
import logger from '../../utils/ducLogger';
import { initializePbClient } from '../../utils/commons';
import { trackPaybackRegistration } from '../../utils/tracking';

// Registration Banner used as meso-content for Team Honeybadger's aftersale page
class RegistrationBanner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pbClient: null
    };
    this.clientErrorHandler = this.clientErrorHandler.bind(this);
    this.clientSuccessHandler = this.clientSuccessHandler.bind(this);
    this.createOverlay = this.createOverlay.bind(this);
  }

  componentDidMount() {
    initializePbClient(this.clientSuccessHandler, this.clientErrorHandler);
  }

  /* eslint-disable class-methods-use-this */
  clientSuccessHandler(client) {
    this.setState({
      pbClient: client
    });
  }

  clientErrorHandler(error) {
    logger.error(`failed to initialize pb-client for overlay: ${error.message}`);
  }

  createOverlay() {
    trackPaybackRegistration();
    if (this.state.pbClient !== null) {
      this.state.pbClient.createModuleInOverlay(
        {
          moduleType: 'enrollment',
          contextKey: 'emptyContext',
          options: {
            customerHasCard: true,
            partnerProvidesOwnCard: true
          }
        },
        {
          backdropDisabled: true
        }
      );
    }
  }

  render() {
    return (
      <div className="rs-payback-banner-content">
        <div className="rs-payback-banner-content-header">
          <span className="rs-payback-banner-content-header-headline"> {MESSAGES.headline} </span>
          <img src={pbPointee} alt="" className="rs-payback-banner-content-header-image" />
        </div>
        <div className="rs-payback-banner-content-footer">
          <span className="rs-payback-banner-content-footer-text"> {MESSAGES.footer}</span>
          <button className="rs-payback-banner-content-footer-button" type="submit" onClick={this.createOverlay}> {MESSAGES.button} </button>
        </div>
      </div>
    );
  }
}

export default RegistrationBanner;
