import test from 'ava';
import React from 'react';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';
import RegistrationBanner from '../RegistrationBanner';

configure({ adapter: new Adapter() });

test('render the default component snapshot', (t) => {
  const regisBanner = <RegistrationBanner />;
  const modalTree = render.create(regisBanner).toJSON();
  t.snapshot(modalTree);
});
