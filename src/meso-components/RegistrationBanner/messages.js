const MESSAGES = {
  headline: 'Noch kein PAYBACK Mitglied?',
  footer: 'Einkaufen bei REWE lohnt sich noch mehr. Jetzt täglich Punkte sammeln, einlösen und sparen.',
  button: 'Bei PAYBACK anmelden'
};

export default MESSAGES;
