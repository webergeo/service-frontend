import test from 'ava';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';
import ButtonNumber from '../ButtonNumber';

configure({ adapter: new Adapter() });

const buttonComponent = props => (
  <ButtonNumber label="label" {...props && { ...props }} />
);

test('compare snapshots to identify changes', (t)=>{
  const buttonTree = render.create(buttonComponent()).toJSON();
  t.snapshot(buttonTree);
});

test('render default button', (t) => {
  const wrapper = shallow(buttonComponent());
  t.true(wrapper.hasClass('rs-payback-number__button'));
});

test('render result should contain a button-element', (t) => {
  const wrapper = shallow(buttonComponent());
  t.is(wrapper.find('button').length, 1);
});

test('renders disabled button', (t) => {
  const wrapper = mount(buttonComponent({ disabled: true }));
  t.is(wrapper.props().disabled, true);
  t.is(wrapper.find('button').disabled)
});

test('renders Button with svg checkmark', (t) => {
  const wrapper = shallow(buttonComponent({ svg: true }));
  t.is(wrapper.find('svg').length, 1);
});
