import PropTypes from 'prop-types';
import React from 'react';
import cn from 'classnames';
import TRIANGLE_IMG from '../../assets/images/ic-triangle-rechts-red.svg';

import './Button.scss';

/**
 * Create a Button component
 * @param {bool}                    big         - Creates a large button
 * @param {oneOf(['green', 'red'])} color       - Set color from config
 * @param {function() {}}           handleClick - Click event callback
 * @param {String}                  href        - Pass an href prop to make the
 *                                                 Button an <a> tag instead of a <button>
 * @param {bool}                    disabled    - flag to disable/enable
 * @param {String}                  label       - Button text
 * @param {String}                  qaClassName - optional css classname used for UI tests
 * @param {String}                  type        - type for a <button>
 * @param {String}                  target      - target attribute for <a> tag
 * @param {String}                  title       - title attribute for tooltip
 */
const ButtonNumber = ({
  big, color, disabled, handleClick, href, label, qaClassName, type, target, title, link, stretched, className, svg
}) => {
  const Component = href ? 'a' : 'button';
  const buttonClassName = link ? 'rs-payback-number__button-link' : cn('rs-payback-number__button', qaClassName, {
    'rs-payback-number__button--large': big,
    [`rs-payback-number__button--${color}`]: color,
    'rs-payback-number__button--stretched': stretched
  }, className);
  const props = {
    href: href || null,
    className: buttonClassName,
    disabled,
    onClick: handleClick,
    type: !href ? type : null,
    target: href ? target : null,
    title
  };
  return (
    <Component {...props}>
      {svg ?
        <svg className="rs-payback-number__button-checkmark" width="20" height="20" viewBox="0 0 150 150">
          <path id="check" d="M10,50 l25,40 l95,-70" />
        </svg> : null
      }
      {link ? <img src={TRIANGLE_IMG} className="rs-payback-number__button-link-image" alt="" /> : null}
      {label}
    </Component>
  );
};

ButtonNumber.propTypes = {
  big: PropTypes.bool,
  color: PropTypes.oneOf(['green', 'white', 'green--animation']),
  disabled: PropTypes.bool,
  handleClick: PropTypes.func,
  /** Pass an href prop to make the Button an <a> tag instead of <button> */
  href: PropTypes.string,
  qaClassName: PropTypes.string,
  label: PropTypes.string.isRequired,
  link: PropTypes.bool,
  target: PropTypes.oneOf(['_blank', '_self']),
  title: PropTypes.string,
  type: PropTypes.oneOf(['submit', 'reset', 'button']),
  stretched: PropTypes.bool,
  className: PropTypes.string,
  svg: PropTypes.bool
};

ButtonNumber.defaultProps = {
  big: false,
  color: 'green',
  disabled: false,
  handleClick: () => {},
  href: '',
  link: false,
  qaClassName: '',
  target: '_self',
  title: '',
  type: 'submit',
  stretched: false,
  className: null,
  svg: false
};
export default ButtonNumber;
