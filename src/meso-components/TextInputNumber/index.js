
import InputMask from 'react-input-mask';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classNames from 'classnames';

import './TextInputNumber.scss';
import { noop } from '../../utils/commons';


class TextInputNumber extends Component {
  constructor(props) {
    super(props);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  handleKeyPress(e) {
    const { validation, onSubmit } = this.props;
    if (e.key === 'Enter') {
      e.preventDefault();
      const isValid = !(validation && validation.error);
      if (isValid) {
        onSubmit(e);
      }
    }
  }

  render() {
    const {
      name, value, onChange, validation, placeHolderText, personal, empty, onIconClick, disabled, mask
    } = this.props;
    const isValid = !(validation && validation.error);
    const isEmpty = empty;
    const inputClassNames = classNames('rs-payback-number__textInput-input', {
      'rs-payback-number__textInput-input--invalid': !isValid,
      'no-mouseflow': personal
    });
    const optionalLabelParams = !isValid
      ? { title: validation.message }
      : {};
    return (
      <div className="rs-payback-number__textInput">
        <label data-label={placeHolderText} {...optionalLabelParams}>
          <InputMask
            disabled={disabled}
            mask={mask}
            maskChar=""
            className={inputClassNames}
            type="text"
            name={name}
            onChange={onChange}
            onKeyPress={this.handleKeyPress}
            value={value}
            {...(personal && { 'data-mf-replace': '' })}
          />
          <div className="rs-payback-number__textInput-label">
            {placeHolderText}
          </div>
          {!isEmpty && !disabled ? <button className="rs-payback-number__icon-close" onClick={onIconClick} > Schließen </button> : null}
        </label>
      </div>
    );
  }
}

TextInputNumber.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  placeHolderText: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func,
  validation: PropTypes.shape({
    error: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired
  }),
  empty: PropTypes.bool,
  onIconClick: PropTypes.func.isRequired,
  personal: PropTypes.bool,
  disabled: PropTypes.bool,
  mask: PropTypes.string.isRequired
};

TextInputNumber.defaultProps = {
  value: '',
  validation: {
    error: false,
    message: ''
  },
  onSubmit: noop,
  empty: false,
  personal: false,
  disabled: false
};

export default TextInputNumber;
