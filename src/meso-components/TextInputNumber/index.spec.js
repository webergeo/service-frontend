import test from 'ava';
import React from 'react';
import render from 'react-test-renderer';
import { spy } from 'sinon';
import { configure, mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import TextInputNumber from '../TextInputNumber';

configure({ adapter: new Adapter() });

const textInput = props => (
  <TextInputNumber
    placeHolderText="Placeholder"
    name="right-example"
    value="true value"
    onIconClick={() => {}}
    mask="999 999 9999 9999"
    onChange={() => {}}
    {...props && { ...props }}
  />
);

test('renders the default snapshot', (t) => {
  const modalTree = render.create(textInput).toJSON();
  t.snapshot(modalTree);
});

test('enter key press submits', (t) => {
  const submitHandler = spy();
  const wrapper = mount(textInput({ onSubmit: submitHandler }));
  wrapper.find('input').simulate('keypress', { key: 'Enter' });

  t.true(submitHandler.calledOnce);
});

test('enter key press does not submit on error', (t) => {
  const submitHandler = spy();
  const wrapper = mount(textInput({ onSubmit: submitHandler, validation: { error: true, message: '' } }));
  wrapper.find('input').simulate('keypress', { key: 'Enter' });

  t.false(submitHandler.calledOnce);
});

test('applies the data-mf-replace attribute and no-mouseflow class if input is personal', (t) => {
  const wrapper = mount(textInput({ personal: true }));
  const input = wrapper.find('input');
  t.true(input.props()['data-mf-replace'] === '');
  t.true(input.hasClass('no-mouseflow'));

  const wrapper2 = mount(textInput({ personal: false }));
  const input2 = wrapper2.find('input');
  t.true(input2.props()['data-mf-replace'] === undefined);
  t.false(input2.hasClass('no-mouseflow'));
});
