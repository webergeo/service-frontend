const MESSAGES = {
  header: {
    title: 'PAYBACK Kundennummer'
  },
  error: {
    notAvaible: 'Leider steht der PAYBACK-Dienst aktuell nicht zur Verfügung. Bitte versuchen Sie es später erneut.',
    invalid: 'Bitte geben Sie eine gültige 10-stellige PAYBACK Kundennummer ein.',
    failed: 'Speichern fehlgeschlagen.'
  },
  footer: {
    hint: 'PAYBACK Kundennummer dauerhaft im Kundenkonto speichern, um bei jedem Einkauf Punkte zu sammeln.',
    checkboxLabel: 'Für diese Bestellung PAYBACK Punkte sammeln.'
  },
  assets: {
    inputPlaceholder: 'Kundennummer eingeben',
    buttonLabel: 'Speichern',
    buttonLabelSaved: 'Gespeichert!'
  }
};

export default MESSAGES;
