import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { removeSpaces, maskPaybackNumber } from '../../utils/commons';
import { trackPaybackNumberAddition } from '../../utils/tracking';
import TextInputNumber from '../TextInputNumber';
import ButtonNumber from '../ButtonNumber';
import { updatePaybackAccountInfo } from '../../api/Accounts';
import logger from '../../utils/ducLogger';

import './PaybackNumberIncludeComponent.scss';
import MESSAGES from './messages';

const validatePaybackNumber = (value) => {
  const validation = {
    error: false,
    message: ''
  };

  if (removeSpaces(value).length > 10) {
    validation.error = true;
    validation.message = MESSAGES.error.invalid;
  }

  return validation;
};

class PaybackNumberIncludeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.defaultValue,
      validation: props.defaultValidation,
      empty: true,
      disabled: null,
      placeHolderText: MESSAGES.assets.inputPlaceholder,
      mask: '999 999 9999 9999',
      saved: false,
      label: MESSAGES.assets.buttonLabel,
      svg: false,
      color: 'green',
      /* max height is needed to "cheat" the use of transition property so the
      div height has a smooth movement when button disapears */
      height: { maxHeight: '300px' },
      isChecked: true,
      failure: false,
      failureMessage: '',
      personalInput: true
    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.isSaveReady = this.isSaveReady.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onCloseIconClick = this.onCloseIconClick.bind(this);
    this.hasPaybackNumber = this.hasPaybackNumber.bind(this);
    this.toggleCheckboxClick = this.toggleCheckboxClick.bind(this);
    this.handleResponseError = this.handleResponseError.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
    this.renderInputMask = this.renderInputMask.bind(this);
    this.renderInfoAndButton = this.renderInfoAndButton.bind(this);
    this.renderFailureMessage = this.renderFailureMessage.bind(this);
    this.renderCheckbox = this.renderCheckbox.bind(this);
  }

  componentDidMount() {
    this.hasPaybackNumber();
  }

  onChangeHandler(e) {
    this.setState({
      value: e.target.value,
      validation: validatePaybackNumber(e.target.value),
      empty: e.target.value === ''
    });
  }

  onCloseIconClick(e) {
    this.setState({
      value: '',
      validation: validatePaybackNumber(e.target.value),
      empty: e.target.value === ''
    });
  }

  setSaveState(number) {
    this.setState({
      placeHolderText: '',
      disabled: true,
      mask: '\\*\\*\\* \\*\\*\\* 9999',
      value: maskPaybackNumber(number),
      personalInput: false
    });
  }

  isSaveReady() {
    const { value, validation } = this.state;
    return !(validation && validation.error) && removeSpaces(value).length === 10 && !this.state.disabled;
  }

  /* eslint-disable class-methods-use-this */
  async saveNumber(paybackNumber) {
    try {
      const payload = { paybackNumber };
      const response = await updatePaybackAccountInfo({ payload });
      if (response.status >= 400) {
        this.handleResponseError(response);
      } else {
        trackPaybackNumberAddition();
        this.setSaveState(this.state.value);
        this.saveAnimation();
      }
    } catch (error) {
      logger.error(`Failed to add a payback number, error: ${error.message}`);
      this.setState({
        failure: true,
        failureMessage: MESSAGES.error.notAvaible
      });
    }
  }

  handleResponseError(response) {
    if (response.status < 500 && response.status >= 400) {
      this.setState({
        placeHolderText: MESSAGES.assets.inputPlaceholder,
        disabled: false,
        color: 'green',
        validation: {
          error: true,
          message: MESSAGES.error.invalid
        }
      });
    } else if (response.status >= 500) {
      this.setState({
        failure: true,
        failureMessage: MESSAGES.error.notAvaible
      });
    }
  }

  toggleCheckboxClick() {
    this.setState({
      isChecked: !this.state.isChecked
    });
  }

  saveAnimation() {
    // timer when "Gespeichert" white button should appear
    setTimeout(
      () => {
        this.setState({
          color: 'white',
          label: MESSAGES.assets.buttonLabelSaved,
          svg: true
        });
      }, 600
    );
    // timer when the div should move up
    setTimeout(
      () => {
        this.setState({
          height: { maxHeight: '167px' }
        });
      }, 1800
    );
    // timer when "Gespeichert" disapears
    setTimeout(
      () => {
        this.setState({
          saved: true
        });
      }, 2050
    );
  }

  async handleSubmit(event) {
    event.preventDefault();
    if (this.isSaveReady()) {
      this.setState({
        color: 'green--animation',
        placeHolderText: '',
        disabled: true
      });
      await this.saveNumber(removeSpaces(this.state.value));
    }
  }

  hasPaybackNumber() {
    if (this.props.accountNumber !== '') {
      this.setSaveState(this.props.accountNumber);
      this.setState({
        personalInput: false,
        saved: true
      });
    }
  }

  renderHeader() {
    return (
      <div className="rs-payback-number__component-header">
        <svg className="rs-payback-number__component-header-logo" xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28">
          <path d="M28 3.5v21.04A3.46 3.46 0 0 1 24.54 28H3.46A3.46 3.46 0 0 1 0 24.54V3.47A3.46 3.46 0 0 1 3.46 0l21.09.03C26.45.04 28 1.6 28 3.5zM14 8.76a1.32 1.32 0 1 1 0-2.65 1.32 1.32 0 0 1 0 2.65zm0 .87a2.19 2.19 0 1 0 0-4.38 2.19 2.19 0 0 0 0 4.38zm0 6.56a2.19 2.19 0 1 0 0-4.38 2.19 2.19 0 0 0 0 4.38zm0 5.7a1.32 1.32 0 1 1 0-2.65 1.32 1.32 0 0 1 0 2.65zm0 .86a2.19 2.19 0 1 0 0-4.37 2.19 2.19 0 0 0 0 4.37zm6.56-13.13a2.19 2.19 0 1 0 0-4.37 2.19 2.19 0 0 0 0 4.38zM7.44 8.77a1.32 1.32 0 1 1 0-2.65 1.32 1.32 0 0 1 0 2.65zm0 .87a2.19 2.19 0 1 0 0-4.38 2.19 2.19 0 0 0 0 4.38zm13.12 6.56a2.19 2.19 0 1 0 0-4.38 2.19 2.19 0 0 0 0 4.38zm-13.12-.87a1.32 1.32 0 1 1 0-2.64 1.32 1.32 0 0 1 0 2.64zm0 .87a2.19 2.19 0 1 0 0-4.38 2.19 2.19 0 0 0 0 4.38zm13.12 6.56a2.19 2.19 0 1 0 0-4.37 2.19 2.19 0 0 0 0 4.37zm-13.12-.86a1.32 1.32 0 1 1 0-2.65 1.32 1.32 0 0 1 0 2.65zm0 .86a2.19 2.19 0 1 0 0-4.37 2.19 2.19 0 0 0 0 4.37z" fill="#1B59A2" fillRule="nonzero" />
        </svg>
        <div className="rs-payback-number__component-header-title">{MESSAGES.header.title}</div>
      </div>);
  }

  renderInputMask() {
    return (
      <div className="rs-payback-number__component-content">
        <TextInputNumber
          name=""
          onChange={this.onChangeHandler}
          onSubmit={this.handleSubmit}
          placeHolderText={this.state.placeHolderText}
          value={this.state.value}
          validation={this.state.validation}
          empty={this.state.empty}
          onIconClick={this.onCloseIconClick}
          disabled={this.state.disabled}
          mask={this.state.mask}
          personal={this.state.personalInput}
        />
      </div>);
  }

  renderCheckbox() {
    return (
      <div className="rs-payback-number__component-footer-checkbox-group" >
        <input
          className="rs-payback-number__component-footer-checkbox"
          type="checkbox"
          id="collect"
          name="usePayback"
          onChange={this.toggleCheckboxClick}
          value={this.state.isChecked}
          defaultChecked
        />
        <label className="rs-payback-number__component-footer-checkbox-label" htmlFor="collect"> {MESSAGES.footer.checkboxLabel} </label>
      </div>);
  }

  renderInfoAndButton() {
    return (
      <div>
        <div className="rs-payback-number__component-footer-hint">
          <span> { MESSAGES.footer.hint } </span>
        </div>
        <div>
          <ButtonNumber
            svg={this.state.svg}
            color={this.state.color}
            big
            stretched
            label={this.state.label}
            handleClick={this.handleSubmit}
            disabled={!this.isSaveReady()}
            type="button"
          />
        </div>
      </div>);
  }

  renderInvalidPaybackNumberMessage() {
    return <div className="rs-payback-number__component-footer-limit">{this.state.validation.message}</div>;
  }

  renderFailureMessage() {
    return <div className="rs-payback-number__component-footer-error">{this.state.failureMessage}</div>;
  }

  render() {
    const {
      validation, empty, saved, height, failure
    } = this.state;
    const invalidNumber = (validation && validation.error);
    return (
      <div className="rs-payback-number__component" style={height}>
        {this.renderHeader()}
        {!failure ? this.renderInputMask() : null}
        <div className="rs-payback-number__component-footer">
          {invalidNumber ? this.renderInvalidPaybackNumberMessage() : null}
          {!failure && saved ? this.renderCheckbox() : null}
          {!failure && !saved && !empty ? this.renderInfoAndButton() : null }
          {failure ? this.renderFailureMessage() : null}
        </div>
      </div>
    );
  }
}

PaybackNumberIncludeComponent.propTypes = {
  defaultValue: PropTypes.string,
  defaultValidation: PropTypes.shape({
    message: PropTypes.string
  }),
  accountNumber: PropTypes.string
};

PaybackNumberIncludeComponent.defaultProps = {
  defaultValue: '',
  defaultValidation: {
    error: false,
    message: ''
  },
  accountNumber: ''
};

export default PaybackNumberIncludeComponent;
