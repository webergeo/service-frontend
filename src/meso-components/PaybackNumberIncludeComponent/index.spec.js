import test from 'ava';
import React from 'react';
import render from 'react-test-renderer';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import PaybackNumberIncludeComponent from '.';

configure({ adapter: new Adapter() });

test('renders the default snapshot', (t) => {
  const modalTree = render.create(<PaybackNumberIncludeComponent />).toJSON();
  t.snapshot(modalTree);
});

test('render with existing payback number disables button and displays checkbox', (t) => {
  const wrapper = mount(<PaybackNumberIncludeComponent accountNumber="1234567890" />);
  const input = wrapper.find('.rs-payback-number__textInput-input').at(0);
  t.true(input.props().value === '*** *** 7890');
  t.true(input.props().disabled === true);

  const checkbox = wrapper.find('.rs-payback-number__component-footer-checkbox').at(0);
  t.true(checkbox.props().value === true);
  t.true(true);
});

test('too long input triggers hint message', (t) => {
  const wrapper = mount(<PaybackNumberIncludeComponent />);
  const input = wrapper.find('.rs-payback-number__textInput-input').at(0);
  input.simulate('change', { target: { value: '12345678901' } });

  t.is(wrapper.find('.rs-payback-number__component-footer-limit').length, 1);
});

test('render without existing payback number', (t) => {
  // default render has no button
  const wrapper = mount(<PaybackNumberIncludeComponent />);
  t.is(wrapper.find('.rs-payback-number__button').length, 0);

  // input not empty makes button appear
  const input = wrapper.find('.rs-payback-number__textInput-input').at(0);
  input.simulate('change', { target: { value: '1234567890' } });
  t.is(wrapper.find('.rs-payback-number__button').length, 1);

  // clicking Icon close empties input field and makes button disappear
  wrapper.find('.rs-payback-number__icon-close').simulate('click');
  t.true(input.props().value === '');
  t.is(wrapper.find('.rs-payback-number__button').length, 0);
});
