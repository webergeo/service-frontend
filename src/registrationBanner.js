import 'core-js/features/object/values';
import ReactDOM from 'react-dom';
import React from 'react';
import logger from './utils/ducLogger';
import RegistrationBannerApp from './containers/RegistrationBannerApp';

/* eslint-disable global-require */
if (process.env.NODE_ENV !== 'production') {
  require('./assets/scss/_development_fonts.scss');
  require('./assets/scss/_development_box_model.scss');
}
/* eslint-enable */

window.addEventListener('DOMContentLoaded', () => {
  try {
    const appMountPoints = document.getElementsByClassName('rs-payback-banner');
    for (let i = 0; i < appMountPoints.length; i += 1) {
      // eslint-disable-next-line no-underscore-dangle
      const elem = appMountPoints[i];
      const layout = elem.getAttribute('data-layout');
      ReactDOM.render(<RegistrationBannerApp layout={layout} />, elem);
    }
  } catch (error) {
    logger.error(`Error during registration-banner-app rendering. URL: ${window.location.href} Message: ${error.message}`);
  }
});
