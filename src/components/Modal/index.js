import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Button } from '../../widgets';

import './Modal.scss';

class Modal extends Component {
  constructor(props) {
    super(props);
    this.renderHeader = this.renderHeader.bind(this);
    this.renderBody = this.renderBody.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
  }

  renderHeader() {
    const { handleClose, title, showClose } = this.props;
    return (
      <div className="rs-modal__header">
        <h2 className="rs-modal__title rs-qa-paybackoverlay__headline">{title}</h2>
        { showClose && <button className="rs-modal__closelink" onClick={handleClose}>Schließen</button> }
      </div>
    );
  }

  renderBody() {
    const { content, contentClassName } = this.props;
    const ContentBlockType = typeof content === 'string' ? 'p' : 'div';
    return (
      content !== '' && (
        <div className="rs-modal__body">
          <ContentBlockType className={`rs-modal__content ${contentClassName}`}>{content}</ContentBlockType>
        </div>
      )
    );
  }

  renderFooter() {
    const {
      type, handleSubmit, handleCancel, footerConfig: { confirmAction, dismissAction }
    } = this.props;
    if (type === 'success' || type === 'error') {
      return (
        <div className="rs-modal__footer">
          <Button big color="beige" label="Weiter" handleClick={handleSubmit} qaClassName="rs-qa-payback-lightbox-close" />
        </div>
      );
    }
    return (
      <div className="rs-modal__footer">
        <div>
          {dismissAction && <Button
            big
            color="pampas"
            label={dismissAction.label}
            handleClick={handleCancel}
            qaClassName="rs-qa-payback-lightbox-cancel"
          />}
          {confirmAction && <Button
            big
            color="beige"
            label={confirmAction.label}
            handleClick={handleSubmit}
            qaClassName="rs-qa-payback-submit"
          />}
        </div>
      </div>
    );
  }

  render() {
    const { contentClassName, type } = this.props;

    return (
      <div className={`rs-modal rs-modal--${type}`}>
        <div className={`rs-modal__holder ${contentClassName}`}>
          {this.renderHeader()}
          {this.renderBody()}
          {this.renderFooter()}
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  contentClassName: PropTypes.string,
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  type: PropTypes.oneOf(['error', 'warning', 'success']),
  showClose: PropTypes.bool,
  handleCancel: PropTypes.func,
  handleClose: PropTypes.func,
  handleSubmit: PropTypes.func,
  footerConfig: PropTypes.shape({
    confirmAction: PropTypes.shape({
      label: PropTypes.string.isRequired
    }),
    dismissAction: PropTypes.shape({
      label: PropTypes.string.isRequired
    })
  })
};

Modal.defaultProps = {
  content: '',
  contentClassName: '',
  type: 'success',
  showClose: true,
  handleCancel: undefined,
  handleSubmit: undefined,
  handleClose: undefined,
  footerConfig: {
    confirmAction: {
      label: 'Ja'
    },
    dismissAction: {
      label: 'Nein'
    }
  }
};

export default Modal;
