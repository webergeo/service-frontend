```
initialState = { inputValue: '' };

<TextInput
  placeHolderText="Placeholder Lorem"
  name="example"
  value={state.inputValue}
  onChange={(e) => setState({ inputValue: e.target.value })}
/>
```

With error
```
initialState = { inputValue: 'Wrong input' };

<TextInput
  placeHolderText="Placeholder Lorem"
  name="example"
  value={state.inputValue}
  onChange={(e) => setState({ inputValue: e.target.value })}
  validation={{ error: true, message: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor' }}
/>
```
