import test from 'ava';
import React from 'react';
import render from 'react-test-renderer';
import { spy } from 'sinon';
import {configure, mount, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import TextInput from '.';

configure({ adapter: new Adapter() });

test('renders the default snapshot', (t) => {
  const textInput = (
    <TextInput
      placeHolderText="Placeholder Lorem"
      name="right-example"
      value="true value"
      onChange={() => {}}
    />
  );
  const modalTree = render.create(textInput).toJSON();
  t.snapshot(modalTree);
});

test('renders the text input with error', (t) => {
  const textInput = (
    <TextInput
      placeHolderText="Placeholder Lorem"
      name="wrong-example"
      value="wrong value"
      onChange={() => {}}
      validation={{
        error: true,
        message: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr'
      }}
    />
  );
  const modalTree = render.create(textInput).toJSON();
  t.snapshot(modalTree);
});

test('Fires submit handler on enter key press', (t) => {
  const submitHandler = spy();
  const value = 'true value';

  const wrapper = mount(
    <TextInput
      placeHolderText="Placeholder Lorem"
      name="right-example"
      value={value}
      onChange={() => {}}
      onSubmit={submitHandler}
    />
  );
  wrapper.find('.rs-payback__textInput-input').simulate('keypress', { key: 'Enter' });

  t.true(submitHandler.calledOnce);
  t.true(submitHandler.calledWith(value));
});

test('Does not fire submit handler on key press with error', (t) => {
  const submitHandler = spy();
  const value = 'false value';

  const wrapper = mount(
    <TextInput
      placeHolderText="Placeholder Lorem"
      name="right-example"
      value={value}
      onChange={() => {}}
      onSubmit={submitHandler}
      validation={{
        error: true,
        message: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr'
      }}
    />
  );
  wrapper.find('.rs-payback__textInput-input').simulate('keypress', { key: 'Enter' });

  t.false(submitHandler.called);
});

test('applies the data-mf-replace attribute and no-mouseflow class iff input is personal', (t) => {
  const error = () => { throw new Error('must not call onChange'); };

  const wrapper = shallow(
    <TextInput
      placeHolderText="Placeholder Lorem"
      name="right-example"
      value="1234"
      onChange={error}
      personal
    />
  );
  const input = wrapper.find('input');
  t.true(input.props()['data-mf-replace'] === '');
  t.true(input.hasClass('no-mouseflow'));

  const wrapper2 = shallow(
    <TextInput
      placeHolderText="Placeholder Lorem"
      name="right-example"
      value="1234"
      onChange={error}
    />
  );
  const input2 = wrapper2.find('input');
  t.true(input2.props()['data-mf-replace'] === undefined);
  t.false(input2.hasClass('no-mouseflow'));
});
