import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classNames from 'classnames';

import './TextInput.scss';
import { noop } from '../../utils/commons';

class TextInput extends Component {
  constructor(props) {
    super(props);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  handleKeyPress(e) {
    const { validation, onSubmit } = this.props;
    const isValid = !(validation && validation.error);

    if (e.key === 'Enter' && isValid) {
      onSubmit(e.target.value);
    }
  }

  render() {
    const {
      name, value, onChange, validation, placeHolderText, personal
    } = this.props;
    const isValid = !(validation && validation.error);
    const inputClassNames = classNames('rs-payback__textInput-input', {
      'rs-payback__textInput-input--invalid': !isValid,
      'no-mouseflow': personal
    });
    const optionalLabelParams = !isValid
      ? { title: validation.message }
      : {};
    return (
      <div className="rs-payback__textInput">
        <label data-label={placeHolderText} {...optionalLabelParams}>
          <input
            className={inputClassNames}
            type="text"
            name={name}
            onChange={onChange}
            onKeyPress={this.handleKeyPress}
            value={value}
            required
            {...(personal && { 'data-mf-replace': '' })}
          />
          <div className="rs-payback__textInput-label">
            {!isValid ? validation.message : placeHolderText}
          </div>
        </label>
      </div>
    );
  }
}

TextInput.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  placeHolderText: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func,
  validation: PropTypes.shape({
    error: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired
  }),
  personal: PropTypes.bool
};

TextInput.defaultProps = {
  value: '',
  validation: {
    error: false,
    message: ''
  },
  onSubmit: noop,
  personal: false
};

export default TextInput;
