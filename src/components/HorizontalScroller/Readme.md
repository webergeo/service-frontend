```
const div1 =
<div style={{display: 'flex'}}>
    <div style={{width: "300px", backgroundColor: "red", height: "400px"}}></div>
    <div style={{width: "300px", backgroundColor: "green", height: "400px"}}></div>
    <div style={{width: "300px", backgroundColor: "blue", height: "400px"}}></div>
</div>;

<div style={{height: "350px"}}>
<HorizontalScroller stepSizeInPixel={300} maxSteps={2}>
    {div1}
</HorizontalScroller>
</div>
```
