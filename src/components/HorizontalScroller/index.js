import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TriangleButton from '../TriangleButton';

import './HorizontalScroller.scss';

/**
 * The HorizontalScroller takes a horizontal box (`children`) and allows to move (scroll) through it stepwise.
 */
class HorizontalScroller extends Component {
  constructor(props) {
    super(props);
    this.state = {
      left: 0
    };
    this.nextSlide = this.nextSlide.bind(this);
    this.prevSlide = this.prevSlide.bind(this);
  }

  nextSlide() {
    const { stepSizeInPixel, maxSteps } = this.props;
    this.setState(prevState => ({
      left: Math.max(prevState.left - stepSizeInPixel, maxSteps * -stepSizeInPixel)
    }));
  }

  prevSlide() {
    const { stepSizeInPixel } = this.props;
    this.setState(prevState => ({
      left: Math.min(prevState.left + stepSizeInPixel, 0)
    }));
  }

  render() {
    const { left } = this.state;
    return (
      <div className="rs-payback__horizontalScroller">
        <div className="rs-payback__horizontalScroller-content" style={{ left }}>
          {this.props.children}
        </div>
        <TriangleButton
          onClick={this.prevSlide}
          className="rs-payback__horizontalScroller-button rs-payback__horizontalScroller-button-left"
        />
        <TriangleButton
          direction="right"
          onClick={this.nextSlide}
          className="rs-payback__horizontalScroller-button rs-payback__horizontalScroller-button-right"
        />
      </div>
    );
  }
}

HorizontalScroller.propTypes = {
  stepSizeInPixel: PropTypes.number.isRequired,
  maxSteps: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired
};

export default HorizontalScroller;
