import test from 'ava';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import HorizontalScroller from '.';

configure({ adapter: new Adapter() });

const moveToSmallestLeftPosition = (t, wrapper, maxLeftInPixel) => {
  // move to max steps
  for (let i = 0; i < 2; i += 1) {
    wrapper.find('TriangleButton[direction="right"]').simulate('click');
  }
  t.is(wrapper.find('.rs-payback__horizontalScroller-content').get(0).props.style.left, maxLeftInPixel);
};

test('renders the default snapshot', (t) => {
  const horizontalScroller = (
    <HorizontalScroller stepSizeInPixel={300} maxSteps={2}>
      <div>Test</div>
    </HorizontalScroller>
  );
  const testHorizontalScroller = render.create(horizontalScroller).toJSON();
  t.snapshot(testHorizontalScroller);
});

test('changes left position on next (right) button click', (t) => {
  const horizontalScroller = (
    <HorizontalScroller stepSizeInPixel={300} maxSteps={2}>
      <div>Test</div>
    </HorizontalScroller>
  );
  const wrapper = mount(horizontalScroller);
  t.is(wrapper.find('.rs-payback__horizontalScroller-content').get(0).props.style.left, 0);

  wrapper.find('TriangleButton[direction="right"]').simulate('click');
  t.is(wrapper.find('.rs-payback__horizontalScroller-content').get(0).props.style.left, -300);

  wrapper.find('TriangleButton[direction="right"]').simulate('click');
  t.is(wrapper.find('.rs-payback__horizontalScroller-content').get(0).props.style.left, -600);
});

test('does not change left position on next (right), if max steps are reached', (t) => {
  const horizontalScroller = (
    <HorizontalScroller stepSizeInPixel={300} maxSteps={1}>
      <div>Test</div>
    </HorizontalScroller>
  );
  const wrapper = mount(horizontalScroller);

  for (let i = 0; i < 3; i += 1) {
    wrapper.find('TriangleButton[direction="right"]').simulate('click');
    t.is(wrapper.find('.rs-payback__horizontalScroller-content').get(0).props.style.left, -300);
  }
});

test('changes left position on previous (left) button click', (t) => {
  const horizontalScroller = (
    <HorizontalScroller stepSizeInPixel={300} maxSteps={2}>
      <div>Test</div>
    </HorizontalScroller>
  );
  const wrapper = mount(horizontalScroller);

  moveToSmallestLeftPosition(t, wrapper, -600);

  wrapper.find('TriangleButton[direction="left"]').simulate('click');
  t.is(wrapper.find('.rs-payback__horizontalScroller-content').get(0).props.style.left, -300);

  wrapper.find('TriangleButton[direction="left"]').simulate('click');
  t.is(wrapper.find('.rs-payback__horizontalScroller-content').get(0).props.style.left, 0);
});

test('does not change left position on previous (left), if min steps are reached', (t) => {
  const horizontalScroller = (
    <HorizontalScroller stepSizeInPixel={300} maxSteps={1}>
      <div>Test</div>
    </HorizontalScroller>
  );
  const wrapper = mount(horizontalScroller);

  moveToSmallestLeftPosition(t, wrapper, -300);

  for (let i = 0; i < 3; i += 1) {
    wrapper.find('TriangleButton[direction="left"]').simulate('click');
    t.is(wrapper.find('.rs-payback__horizontalScroller-content').get(0).props.style.left, 0);
  }
});
