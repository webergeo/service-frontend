import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames'

import './StackLayout.scss';

const StackLayout = ({ children, className }) => (
  <div className={cn('rs-payback__stackLayout', className)}>{children}</div>
);

StackLayout.propTypes = {
  className: PropTypes.string,
  children: PropTypes.arrayOf(PropTypes.element).isRequired
};

StackLayout.defaultProps = {
  className: null
};

export default StackLayout;
