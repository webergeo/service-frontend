Single, nothing happens
```
<StackLayout>
  <div>Some Stack</div>
</StackLayout>
```

With > 2 nodes, adds separator
```
<StackLayout>
  <div>Some Stack</div>
  <div>Some Stack</div>
  <div>Some Stack</div>
</StackLayout>
```
