import test from 'ava';
import React from 'react';
import render from 'react-test-renderer';

import StackLayout from '.';

test('renders the wrapped children', (t) => {
  const example = (
    <StackLayout>
      <div>content1</div>
      <div>content2</div>
    </StackLayout>
  );
  const borderLayoutTree = render.create(example).toJSON();
  t.snapshot(borderLayoutTree);
});
