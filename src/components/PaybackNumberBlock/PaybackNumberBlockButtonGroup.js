import PropTypes from 'prop-types';
import React from 'react';

import { Button } from '../../widgets';

const PaybackNumberBlockButtonGroup = ({ buttonItems, className }) => {
  const buttonListItems = buttonItems.map(buttonProps => (
    <li key={`${buttonProps.label}`} className="rs-simplenav__listitem">
      <Button {...buttonProps} />
    </li>
  ));
  const buttonGroupClassName = `rs-payback-form__form-row ${className}`;
  return (
    <div className={buttonGroupClassName}>
      <nav className="rs-simplenav">
        <ul className="rs-simplenav__list">{buttonListItems}</ul>
      </nav>
    </div>
  );
};

PaybackNumberBlockButtonGroup.propTypes = {
  buttonItems: PropTypes.arrayOf(
    PropTypes.shape({
      big: PropTypes.bool,
      color: PropTypes.string,
      disabled: PropTypes.bool,
      label: PropTypes.string.isRequired,
      handleClick: PropTypes.func,
      title: PropTypes.string.isRequired,
      type: PropTypes.oneOf(['submit', 'reset'])
    })
  ).isRequired,
  className: PropTypes.string.isRequired
};

PaybackNumberBlockButtonGroup.defaultProps = {
  buttonItems: [
    {
      type: 'submit',
      big: true,
      color: 'beige',
      disabled: 'false',
      handleClick: () => {}
    }
  ]
};

export default PaybackNumberBlockButtonGroup;
