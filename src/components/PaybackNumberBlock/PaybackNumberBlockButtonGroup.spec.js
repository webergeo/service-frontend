import test from 'ava';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';
import { Button } from '../../widgets';
import PaybackNumberBlockButtonGroup from './PaybackNumberBlockButtonGroup';

configure({ adapter: new Adapter() });
const paybackBlockButtonGroupComponent = (props) => {
  const defaultButtonItemProps = {
    label: 'Speichern',
    title: 'Kundennummer speichern',
    ...(props && { ...props })
  };

  return <PaybackNumberBlockButtonGroup className="rs-payback-qa-dummy-form-group" buttonItems={[{ ...defaultButtonItemProps }]} />;
};

test('renders the default snapshot', (t) => {
  const modalTree = render.create(paybackBlockButtonGroupComponent()).toJSON();
  t.snapshot(modalTree);
});

test('renders the default PaybackNumberBlockButtonGroup', (t) => {
  const wrapper = mount(paybackBlockButtonGroupComponent());
  t.is(wrapper.props().className, 'rs-payback-qa-dummy-form-group');
  t.is(wrapper.props().buttonItems.length, 1);
  t.is(wrapper.props().buttonItems[0].label, 'Speichern');
  t.is(wrapper.props().buttonItems[0].title, 'Kundennummer speichern');
});

test('always renders the single parent div"', (t) => {
  const wrapper = shallow(paybackBlockButtonGroupComponent());
  t.true(wrapper.find('div.rs-payback-qa-dummy-form-group').length > 0);
});

test('renders the single Button component', (t) => {
  const wrapper = shallow(paybackBlockButtonGroupComponent());
  t.is(wrapper.find(Button).length, 1);
});

test('renders the disabled single button item', (t) => {
  const wrapper = mount(paybackBlockButtonGroupComponent({ disabled: true }));
  t.is(wrapper.find(Button).length, 1);
  t.true(wrapper.props().buttonItems[0].disabled);
});
