const MESSAGES = {
  error: {
    fallbackTitle: 'Es sind Fehler aufgetreten.',
    fallbackMessage: 'Unbekannter Fehler. Bitte versuchen Sie es erneut.',
    nonNumeric: 'Die Kundennummer darf nur aus Ziffern bestehen.',
    numberLimit: 'Die Kundennummer muss aus 10 Ziffern bestehen.',
    whiteSpace: 'Die Kundennummer darf keine Leerzeichen beinhalten.'
  },
  edit: {
    title: 'Möchten Sie Ihre PAYBACK Kundennummer wirklich bearbeiten?',
    main: 'Sie können keine weiteren PAYBACK Services auf REWE.de nutzen:',
    items: ['Punktestand einsehen', 'persönliche eCoupons erhalten', 'Einkaufsgutschein aktivieren', 'REWE eBon'],
    bottom: 'Falls Sie ein Marketing Einverständnis gegenüber REWE erteilt haben, wird dieses hiermit widerrufen.',
    success: 'Ihre PAYBACK Kundennummer wurde erfolgreich gespeichert!',
    fail: 'Ihre PAYBACK Kundennummer konnte nicht gespeichert werden',
    button: {
      label: 'Ändern',
      tooltip: 'PAYBACK Kundennummer bearbeiten'
    }
  },
  delete: {
    button: {
      label: 'Entfernen',
      tooltip: 'PAYBACK Kundennummer entfernen'
    }
  }
};

export default MESSAGES;
