import test from 'ava';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import { noop } from '../../utils/commons';
import PaybackNumberBlock from '../PaybackNumberBlock';
import PaybackNumberBlockContent from './PaybackNumberBlockContent';

configure({ adapter: new Adapter() });

const paybackNumberBlockComponent = () => {
  return <PaybackNumberBlock paybackNumber="0123456789" onPaybackNumberDeleteRequest={noop} />;
};

test('renders the default snapshot', (t) => {
  const modalTree = render.create(paybackNumberBlockComponent()).toJSON();
  t.snapshot(modalTree);
});

test('should show PaybackNumberBlockContent', (t) => {
  const wrapper = shallow(paybackNumberBlockComponent());
  t.is(wrapper.find(PaybackNumberBlockContent).length, 1);
});
