import PropTypes from 'prop-types';
import React from 'react';

import PaybackNumberBlockContent from './PaybackNumberBlockContent';
import { getFormattedPaybackNumber } from '../../utils/commons';

import './PaybackNumberBlock.scss';

const PaybackNumberBlock = ({ paybackNumber, onPaybackNumberDeleteRequest }) => (
  <div className="rs-payback__section--payback bg-pbblue">
    <div className="rs-payback__section-wrappable-head">Ihre PAYBACK Kundennummer</div>
    <div className="rs-payback__section-content">
      <PaybackNumberBlockContent
        // Caution: Add personal flag to TextInput in PaybackNumberBlockContent if you do not obfuscate the PAYBACK number in the future.
        paybackTextInputValue={getFormattedPaybackNumber(paybackNumber)}
        handleDeletePaybackNumber={onPaybackNumberDeleteRequest}
      />
    </div>
  </div>
);

PaybackNumberBlock.propTypes = {
  paybackNumber: PropTypes.string.isRequired,
  onPaybackNumberDeleteRequest: PropTypes.func.isRequired
};

export default PaybackNumberBlock;
