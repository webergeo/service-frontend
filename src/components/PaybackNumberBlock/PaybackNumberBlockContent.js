import PropTypes from 'prop-types';
import React from 'react';
import cn from 'classnames';

import { TextInput } from '../../widgets';
import PaybackNumberBlockButtonGroup from './PaybackNumberBlockButtonGroup';
import MESSAGES from './messages';

const PaybackNumberBlockContent = ({ paybackTextInputValue, handleDeletePaybackNumber }) => {
  const renderRowButtonGroup = () => {
    const deleteButtonProps = {
      big: true,
      color: 'pampas',
      label: `${MESSAGES.delete.button.label}`,
      handleClick: (e) => {
        e.preventDefault();
        handleDeletePaybackNumber();
      },
      qaClassName: 'rs-qa-paybackform-deletenumber',
      title: `${MESSAGES.delete.button.tooltip}`
    };

    const buttonItems = [deleteButtonProps];

    return (
      <PaybackNumberBlockButtonGroup
        className={cn('rs-payback-form__form-row--submit', {
          'rs-payback-form__form-row--submit-single': buttonItems.length === 1
        })}
        buttonItems={buttonItems}
      />
    );
  };

  const paybackTextInputProps = {
    uniqueID: 'rs-payback__text-input',
    placeHolderText: 'PAYBACK Kundennummer',
    textInputValue: paybackTextInputValue,
    isDisabled: true,
    qaClassName: 'rs-qa-payback-input',
    onTextValueChange: () => {}
  };

  return (
    <div>
      {/* TODO: rs-mydata__form--payback used for UI tests need to be renamed later */}
      <form
        action="#"
        className="rs-data__form--payback rs-data__form--inactive rs-mydata__form--payback"
        method="post"
        noValidate
        autoComplete="off"
      >
        <TextInput {...paybackTextInputProps} />
        {renderRowButtonGroup()}
      </form>
    </div>
  );
};

PaybackNumberBlockContent.propTypes = {
  paybackTextInputValue: PropTypes.string.isRequired,
  handleDeletePaybackNumber: PropTypes.func.isRequired
};

export default PaybackNumberBlockContent;
