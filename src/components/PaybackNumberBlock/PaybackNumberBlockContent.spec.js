import test from 'ava';
import React from 'react';
import render from 'react-test-renderer';
import PaybackNumberBlockContent from './PaybackNumberBlockContent';

test('renders the default snapshot', (t) => {
  const modalTree = render
    .create(
      <PaybackNumberBlockContent
        paybackTextInputValue="1234567890"
        handleDeletePaybackNumber={() => {}}
      />
    ).toJSON();
  t.snapshot(modalTree);
});
