import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './MainTitle.scss';

const MainTitle = ({ children, className }) => (<h1 className={cn('rs-payback__mainTitle', className)}>{children}</h1>);

MainTitle.propTypes = {
  children: PropTypes.string.isRequired,
  className: PropTypes.string
};

MainTitle.defaultProps = {
  className: null
};

export default MainTitle;
