import test from 'ava';
import React from 'react';

import render from 'react-test-renderer';

import MainTitle from '.';

test('renders default snapshot', (t) => {
  const tree = render.create(<MainTitle>My Title</MainTitle>).toJSON();
  t.snapshot(tree);
});
