import PropTypes from 'prop-types';
import React from 'react';

import { withKeyFromProps } from '../../utils/hoc';
import MESSAGES from './messages_ewe16';

const Ewe16TermsItem = term => <li className="rs-ewe-terms__listelement" dangerouslySetInnerHTML={{ __html: term.message }} />;
const EWE16TermsItemWithId = withKeyFromProps(Ewe16TermsItem, 'id');
const Ewe16TermsParagraph = ({ children: paragraph }) => <p className="rs-ewe-terms__paragraph" dangerouslySetInnerHTML={{ __html: paragraph }} />;

Ewe16TermsParagraph.propTypes = {
  children: PropTypes.string.isRequired
};

const Ewe16TermsModalContent = () => (
  <div className="rs-ewe-terms">
    <Ewe16TermsParagraph>{MESSAGES.ewe16.terms.paragraph1}</Ewe16TermsParagraph>
    <Ewe16TermsParagraph>{MESSAGES.ewe16.terms.list1_title}</Ewe16TermsParagraph>
    <Ewe16TermsParagraph>{MESSAGES.ewe16.terms.list1_intro}</Ewe16TermsParagraph>
    <ul className="rs-ewe-terms__list">{MESSAGES.ewe16.terms.list1.map(EWE16TermsItemWithId)}</ul>
    <Ewe16TermsParagraph>{MESSAGES.ewe16.terms.paragraph2_title}</Ewe16TermsParagraph>
    <Ewe16TermsParagraph>{MESSAGES.ewe16.terms.paragraph2}</Ewe16TermsParagraph>
    <Ewe16TermsParagraph>{MESSAGES.ewe16.terms.paragraph3_title}</Ewe16TermsParagraph>
    <Ewe16TermsParagraph>{MESSAGES.ewe16.terms.paragraph3}</Ewe16TermsParagraph>
    <ul className="rs-ewe-terms__list">{MESSAGES.ewe16.terms.list2.map(EWE16TermsItemWithId)}</ul>
    <Ewe16TermsParagraph>{MESSAGES.ewe16.terms.paragraph4_title}</Ewe16TermsParagraph>
    <Ewe16TermsParagraph>{MESSAGES.ewe16.terms.paragraph4_1}</Ewe16TermsParagraph>
    <Ewe16TermsParagraph>{MESSAGES.ewe16.terms.paragraph4_2}</Ewe16TermsParagraph>
    <Ewe16TermsParagraph>{MESSAGES.ewe16.terms.paragraph5_title}</Ewe16TermsParagraph>
    <Ewe16TermsParagraph>{MESSAGES.ewe16.terms.paragraph5}</Ewe16TermsParagraph>
    <Ewe16TermsParagraph>{MESSAGES.ewe16.terms.paragraph6}</Ewe16TermsParagraph>
    <Ewe16TermsParagraph>{MESSAGES.ewe16.terms.paragraph7}</Ewe16TermsParagraph>
  </div>
);

export default Ewe16TermsModalContent;
