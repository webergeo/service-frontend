import React from 'react';
import MESSAGES from './messages';

const PaybackAccountBindStatusContent = () => (
  <div className="rs-payback-accordion__content">
    <p>
      {MESSAGES.account_binding.status.description}
      <br />
      <a href={MESSAGES.account_binding.status.url} target="_blank" rel="noopener noreferrer" title="Mehr Informationen">
        Mehr Informationen.
      </a>
    </p>
  </div>
);

export default PaybackAccountBindStatusContent;
