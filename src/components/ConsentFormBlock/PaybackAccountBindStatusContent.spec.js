import test from 'ava';
import React from 'react';

import render from 'react-test-renderer';

import PaybackAccountBindStatusContent from './PaybackAccountBindStatusContent';

test('renders the default snapshot', (t) => {
  const modalTree = render.create(<PaybackAccountBindStatusContent />).toJSON();
  t.snapshot(modalTree);
});
