/**
 * Messages for the ConsentFormBlock and its constituent components. This is one ugly muddah..... Kan't be helped....
 */
const MESSAGES = {
  headline: 'Meine PAYBACK Vorteile bei REWE im Überblick',
  subline: 'Hier können Sie jederzeit Ihre erteilten Einverständniserklärung sehen und aktualisieren.',
  account_binding: {
    status: {
      label: 'Ja, ich habe PAYBACK Services bei REWE freigeschaltet. Somit kann ich zusätzliche Services, wie Guthaben, eCoupons oder den eBon nutzen.',
      description: `Mit der Freischaltung von PAYBACK bei REWE können PAYBACK Services direkt auf REWE.de genutzt werden.
                    Die PAYBACK Services wie eCoupons, eGutscheine und das Einsehen des aktuellen PAYBACK
                    Punktestands stehen direkt auf REWE.de zur Verfügung.`,
      url: 'https://www.rewe.de/payback/leistungsbeschreibung'
    }
  },
  ewe16: {
    opt_paragraph: 'Durch die Erteilung meines Marketing Einverständnisses erlaube ich die Weitergabe von erforderlichen Daten aus dem PAYBACK Programm an REWE. Somit erhalte ich persönliche Angebote und Services.',
    opt_in: {
      status_label: 'Ja, ich habe mein Marketing Einverständnis gegenüber REWE erteilt. Somit erhalte ich persönliche Angebote.',
      description: 'Klicken Sie auf „Einwilligen“, um der REWE Markt GmbH Ihr Marketing Einverständnis zu erteilen.'
    },

    opt_out: {
      message: 'Möchten Sie Ihr erteiltes Marketing Einverständnis gegenüber REWE widerrufen und somit keine persönlichen Angebote mehr erhalten?',
      success_message: 'Sie haben das Marketing Einverständnis gegenüber der REWE Markt GmbH erfolgreich widerrufen.'
    },

    terms: {
      conditions_url: 'https://www.rewe.de/payback/einverstaendniserklaerung'
    }
  },
};

export default MESSAGES;
