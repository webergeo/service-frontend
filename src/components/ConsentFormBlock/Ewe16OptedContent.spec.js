import test from 'ava';
import React from 'react';

import render from 'react-test-renderer';

import Ewe16OptedContent from './Ewe16OptedContent';

test('renders the default snapshot', (t) => {
  const ewe16OptedContentTree = render.create(<Ewe16OptedContent />).toJSON();
  t.snapshot(ewe16OptedContentTree);
});
