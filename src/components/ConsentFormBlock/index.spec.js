import test from 'ava';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';
import { stub, spy } from 'sinon';
import waitForExpect from 'wait-for-expect';
import * as accountsApi from '../../api/Accounts';
import MESSAGES from './messages';
import MESSAGES_EWE16 from './messages_ewe16';
import * as tracking from '../../utils/tracking';
import { noop } from '../../utils/commons';

import ConsentFormBlock from '.';

configure({ adapter: new Adapter() });

// initializer
let onUpdateParentViewSpy = spy();

const consentFormBlock = (props) => {
  const accountInfo = {
    paybackNumber: '1234567890',
    paybackAccountBound: false,
    ewe16OptInExists: false,
    ...props
  };

  // dummy view update handler
  return (
    <ConsentFormBlock
      accountInfo={accountInfo}
      onUpdateParentView={onUpdateParentViewSpy}
      onUnbindPaybackAccount={noop}
    />
  );
};

test('renders the default snapshot', (t) => {
  const getGroupEweStub = stub(accountsApi, 'getGroupEwe');
  getGroupEweStub.returns({
    status: 200,
    json: () => ({ status: true })
  });

  const modalTree = render.create(consentFormBlock()).toJSON();
  t.snapshot(modalTree);
  getGroupEweStub.restore();
});

test('renders the default snapshot and check properties', (t) => {
  const getGroupEweStub = stub(accountsApi, 'getGroupEwe');
  getGroupEweStub.returns({
    status: 200,
    json: () => ({ status: true })
  });
  const wrapper = shallow(consentFormBlock());
  t.is(wrapper.instance().props.accountInfo.paybackNumber, '1234567890');
  t.is(wrapper.instance().props.accountInfo.paybackAccountBound, false);
  getGroupEweStub.restore();
});

test('renders the snapshot with account binding', (t) => {
  const getGroupEweStub = stub(accountsApi, 'getGroupEwe');
  getGroupEweStub.returns({
    status: 200,
    json: () => ({ status: true })
  });
  const modalTree = render.create(consentFormBlock({ paybackNumber: '1234567890', paybackAccountBound: true })).toJSON();
  t.snapshot(modalTree);
  getGroupEweStub.restore();
});

test.serial('Ewe16OptOut request leads to an ewe16OptToggle call and a success message', async (t) => {
  const getGroupEweStub = stub(accountsApi, 'getGroupEwe');
  getGroupEweStub.returns({
    status: 200,
    json: () => ({ status: true })
  });
  const ewe16OptToggleStub = stub(accountsApi, 'ewe16OptToggle');
  ewe16OptToggleStub.returns({
    status: 200
  });

  const wrapper = mount(consentFormBlock({
    paybackAccountBound: true,
    ewe16OptInExists: true
  }));
  wrapper.find('input[id="rs-ewe16-status"]').simulate('change');
  wrapper.update();

  const optOutRequestModal = wrapper.find(`Modal[title="${MESSAGES_EWE16.ewe16.opt_out.message_title}"]`);
  t.is(optOutRequestModal.length, 1);

  await optOutRequestModal.find('Button[label="Ja"]').simulate('click');
  t.true(ewe16OptToggleStub.calledOnce);
  wrapper.update();

  const optOutResponseModal = wrapper.find(`Modal[title='${MESSAGES.ewe16.opt_out.success_message}']`);
  t.is(optOutResponseModal.length, 1);

  ewe16OptToggleStub.restore();
  getGroupEweStub.restore();
});

test.serial('HTTP_FORBIDDEN during Ewe16 optOut request leads to page refresh', async (t) => {
  onUpdateParentViewSpy = spy();
  const getGroupEweStub = stub(accountsApi, 'getGroupEwe');
  getGroupEweStub.returns({
    status: 200,
    json: () => ({ status: true })
  });
  const ewe16OptToggleStub = stub(accountsApi, 'ewe16OptToggle');
  ewe16OptToggleStub.returns({
    status: 403
  });

  const wrapper = mount(consentFormBlock({
    paybackAccountBound: true,
    ewe16OptInExists: true
  }));
  wrapper.find('input[id="rs-ewe16-status"]').simulate('change');
  wrapper.update();

  const optOutRequestModal = wrapper.find(`Modal[title="${MESSAGES_EWE16.ewe16.opt_out.message_title}"]`);
  t.is(optOutRequestModal.length, 1);

  await optOutRequestModal.find('Button[label="Ja"]').simulate('click');
  t.true(ewe16OptToggleStub.calledOnce);
  wrapper.update();

  t.true(onUpdateParentViewSpy.calledOnce);

  ewe16OptToggleStub.restore();
  getGroupEweStub.restore();
});

test.serial('HTTP_BAD_REQUEST response after an ewe16OptOut request leads to a generic error message', async (t) => {
  const errorMessage = 'some error';
  const getGroupEweStub = stub(accountsApi, 'getGroupEwe');
  getGroupEweStub.returns({
    status: 200,
    json: () => ({ status: true })
  });
  const ewe16OptToggleStub = stub(accountsApi, 'ewe16OptToggle');
  ewe16OptToggleStub.returns({
    status: 400,
    json: () => ({
      _status: {
        validationMessages: [{ message: errorMessage }]
      }
    })
  });

  const wrapper = mount(consentFormBlock({
    paybackAccountBound: true,
    ewe16OptInExists: true
  }));
  wrapper.find('input[id="rs-ewe16-status"]').simulate('change');
  wrapper.update();

  const optOutRequestModal = wrapper.find(`Modal[title="${MESSAGES_EWE16.ewe16.opt_out.message_title}"]`);
  t.is(optOutRequestModal.length, 1);

  await optOutRequestModal.find('Button[label="Ja"]').simulate('click');
  t.true(ewe16OptToggleStub.calledOnce);
  await wrapper.update();
  wrapper.update();// called twice, one call will fail the test

  const optOutResponseModal = wrapper.find(`Modal[title='${MESSAGES_EWE16.generic_error_fallback_title}']`);
  t.is(optOutResponseModal.length, 1);

  ewe16OptToggleStub.restore();
  getGroupEweStub.restore();
});

test.serial('when user cancels an ewe16 optOut request, no modal is displayed and no external call was made', async (t) => {
  const getGroupEweStub = stub(accountsApi, 'getGroupEwe');
  getGroupEweStub.returns({
    status: 200,
    json: () => ({ status: true })
  });
  const ewe16OptToggleStub = stub(accountsApi, 'ewe16OptToggle');
  const wrapper = mount(consentFormBlock({
    paybackAccountBound: true,
    ewe16OptInExists: true
  }));

  wrapper.find('input[id="rs-ewe16-status"]').simulate('change');
  wrapper.update();

  const optOutRequestModal = wrapper.find(`Modal[title="${MESSAGES_EWE16.ewe16.opt_out.message_title}"]`);
  t.is(optOutRequestModal.length, 1);

  await optOutRequestModal.find('Button[label="Nein"]').simulate('click');
  wrapper.update();

  t.true(ewe16OptToggleStub.notCalled);
  t.true(wrapper.instance().state.modal === undefined);
  ewe16OptToggleStub.restore();
  getGroupEweStub.restore();
});

test.serial('An ewe16Optin request leads to an external ewe16OptToggle call and to a success message', async (t) => {
  const trackSpy = spy(tracking, 'trackEwe16OptInEvent');
  const getGroupEweStub = stub(accountsApi, 'getGroupEwe');
  getGroupEweStub.returns({
    status: 200,
    json: () => ({ status: true })
  });
  const ewe16OptToggleStub = stub(accountsApi, 'ewe16OptToggle');
  ewe16OptToggleStub.returns({
    status: 200
  });

  const wrapper = mount(consentFormBlock({
    paybackAccountBound: true,
    ewe16OptInExists: false
  }));
  wrapper.find('Button[label="Einwilligen"]').simulate('click');
  wrapper.update();

  const optInRequestModal = wrapper.find("Modal[type='warning']");
  t.is(optInRequestModal.length, 1);

  await optInRequestModal.find('Button[label="Ja, ich stimme zu"]').simulate('click');
  wrapper.update();

  t.true(ewe16OptToggleStub.calledOnce);
  t.true(trackSpy.called);

  const optOutRequestModal = wrapper.find(`Modal[title="${MESSAGES_EWE16.ewe16.opt_in.success_message}"]`);
  t.is(optOutRequestModal.length, 1);

  ewe16OptToggleStub.restore();
  trackSpy.restore();
  getGroupEweStub.restore();
});

test.serial('HTTP_FORBIDDEN during Ewe16 optIn request leads to page refresh', async (t) => {
  const trackSpy = spy(tracking, 'trackEwe16OptInEvent');
  onUpdateParentViewSpy = spy();
  const getGroupEweStub = stub(accountsApi, 'getGroupEwe');
  getGroupEweStub.returns({
    status: 200,
    json: () => ({ status: true })
  });
  const ewe16OptToggleStub = stub(accountsApi, 'ewe16OptToggle');
  ewe16OptToggleStub.returns({
    status: 403
  });

  const wrapper = mount(consentFormBlock({
    paybackAccountBound: true,
    ewe16OptInExists: false
  }));
  wrapper.find('Button[label="Einwilligen"]').simulate('click');
  wrapper.update();

  const optInRequestModal = wrapper.find("Modal[type='warning']");
  t.is(optInRequestModal.length, 1);

  await optInRequestModal.find('Button[label="Ja, ich stimme zu"]').simulate('click');
  t.true(ewe16OptToggleStub.calledOnce);
  wrapper.update();

  t.true(onUpdateParentViewSpy.calledOnce);
  t.true(trackSpy.called);

  ewe16OptToggleStub.restore();
  trackSpy.restore();
  getGroupEweStub.restore();
});

test.serial('HTTP_BAD_REQUEST response after an ewe16OptIn request leads to a generic error message', async (t) => {
  const trackSpy = spy(tracking, 'trackEwe16OptInEvent');
  onUpdateParentViewSpy = spy();
  const getGroupEweStub = stub(accountsApi, 'getGroupEwe');
  getGroupEweStub.returns({
    status: 200,
    json: () => ({ status: true })
  });
  const errorMessage = 'some error';
  const ewe16OptToggleStub = stub(accountsApi, 'ewe16OptToggle');
  ewe16OptToggleStub.returns({
    status: 400,
    json: () => ({
      _status: {
        validationMessages: [{ message: errorMessage }]
      }
    })
  });

  const wrapper = mount(consentFormBlock({
    paybackAccountBound: true,
    ewe16OptInExists: false
  }));
  wrapper.find('Button[label="Einwilligen"]').simulate('click');
  wrapper.update();

  const optInRequestModal = wrapper.find("Modal[type='warning']");
  t.is(optInRequestModal.length, 1);

  await optInRequestModal.find('Button[label="Ja, ich stimme zu"]').simulate('click');
  wrapper.update();

  t.true(ewe16OptToggleStub.calledOnce);
  t.true(trackSpy.called);
  wrapper.update();

  await waitForExpect(() => {
    wrapper.update();
    const optOutRequestModal = wrapper.find(`Modal[title="${MESSAGES_EWE16.generic_error_fallback_title}"]`);
    t.is(optOutRequestModal.length, 1);
  });

  ewe16OptToggleStub.restore();
  trackSpy.restore();
  getGroupEweStub.restore();
});

test.serial('when user cancels an ewe16optIn request, no modal is displayed and no external call was made', async (t) => {
  const trackSpy = spy(tracking, 'trackEwe16OptInEvent');
  const getGroupEweStub = stub(accountsApi, 'getGroupEwe');
  getGroupEweStub.returns({
    status: 200,
    json: () => ({ status: true })
  });
  const wrapper = mount(consentFormBlock({
    paybackAccountBound: true,
    ewe16OptInExists: false
  }));

  wrapper.find('Button[label="Einwilligen"]').simulate('click');
  wrapper.update();

  const optInRequestModal = wrapper.find("Modal[type='warning']");
  t.is(optInRequestModal.length, 1);

  await optInRequestModal.find('Button[label="Abbrechen"]').simulate('click');
  wrapper.update();
  t.true(wrapper.instance().state.modal === undefined);
  t.false(trackSpy.called);
  trackSpy.restore();
  getGroupEweStub.restore();
});

test.serial('Render GroupEWE block if request status is false', async (t) => {
  const getGroupEweStub = stub(accountsApi, 'getGroupEwe');
  getGroupEweStub.returns({
    status: 200,
    json: () => ({ status: false, rewe: 'REWE' })
  });

  const wrapper = mount(consentFormBlock({
    paybackAccountBound: true,
    ewe16OptInExists: true
  }));

  await waitForExpect(() => {
    wrapper.update();
    t.true(wrapper.text().includes('Klicken Sie auf „Einwilligen“'));
  });

  getGroupEweStub.restore();
});
