import React from 'react';
import MESSAGES from './messages';

const Ewe16OptedContent = () => (
  <div className="rs-payback-accordion__content">
    <p>
      {MESSAGES.ewe16.opt_paragraph}
      <br />
      <a href={MESSAGES.ewe16.terms.conditions_url} target="_blank" rel="noopener noreferrer" title="Mehr Informationen">
        Mehr Informationen.
      </a>
    </p>
  </div>
);

export default Ewe16OptedContent;
