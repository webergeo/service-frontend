import test from 'ava';
import React from 'react';

import render from 'react-test-renderer';

import Ewe16TermsModalContent from './Ewe16TermsModalContent';

test('renders the default snapshot', (t) => {
  const ewe16TermsModalContentTree = render.create(<Ewe16TermsModalContent />).toJSON();
  t.snapshot(ewe16TermsModalContentTree);
});
