import PropTypes from 'prop-types';
import React, { Component } from 'react';

import { ContainerBlock, LoadingSpinner, OptedBlock, OptedInBlock, OptedOutBlock, CheckboxGroup } from '../../widgets';
import Modal from '../Modal';
import PaybackAccountBindStatusContent from './PaybackAccountBindStatusContent';
import Ewe16OptedContent from './Ewe16OptedContent';

import { ewe16OptToggle, getGroupEwe } from '../../api/Accounts';
import { parsePaybackErrorMessage } from '../../utils/apiResponseFormatter';
import { initializePbClient } from '../../utils/commons';

import { HTTP_FORBIDDEN } from '../../utils/constants';

import MESSAGES from './messages';
import MESSAGES_EWE16 from './messages_ewe16';
import './ConsentFormBlock.scss';

import logger from '../../utils/ducLogger';

import { trackEwe16OptInEvent } from '../../utils/tracking';
import Ewe16TermsModalContent from './Ewe16TermsModalContent';

class ConsentFormBlock extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // pending indicates a pending response from the back-end.
      // If true, render shows a full-screen LoadingSpinner.
      pending: false,
      // The groupEwe response.
      groupEwe: undefined,
      // modal is the active modal component, if any.
      // If truthy, render shows it.
      modal: undefined,
      // The pb client
      pbClient: null
    };

    this.clientErrorHandler = this.clientErrorHandler.bind(this);
    this.clientSuccessHandler = this.clientSuccessHandler.bind(this);
    this.updateGroupEwe = this.updateGroupEwe.bind(this);
    this.handleGroupEweClick = this.handleGroupEweClick.bind(this);
    this.optInOut = this.optInOut.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.closeModalUpdateParent = this.closeModalUpdateParent.bind(this);
    this.renderOptInRequest = this.renderOptInRequest.bind(this);
    this.renderOptInResponse = this.renderOptInResponse.bind(this);
    this.renderOptOutRequest = this.renderOptOutRequest.bind(this);
    this.renderOptOutResponse = this.renderOptOutResponse.bind(this);
    this.renderUnbindBlock = this.renderUnbindBlock.bind(this);
    this.renderGroupEweBlock = this.renderGroupEweBlock.bind(this);
    this.renderOptInBlock = this.renderOptInBlock.bind(this);
    this.renderOptOutBlock = this.renderOptOutBlock.bind(this);
    this.renderPaybackInfoBlock = this.renderPaybackInfoBlock.bind(this);
  }

  componentDidMount() {
    initializePbClient(this.clientSuccessHandler, this.clientErrorHandler);
    this.updateGroupEwe();
  }

  async updateGroupEwe() {
    let response = await getGroupEwe();

    // In test we get a Response here.
    if (typeof response.json === 'function') {
      response = await response.json();
    }

    this.setState({
      groupEwe: response
    });
  }

  /* eslint-disable class-methods-use-this */
  clientSuccessHandler(client) {
    this.setState({ pbClient: client });
  }

  clientErrorHandler(error) {
    logger.error(`failed to initialize pb-client for overlay: ${error.message}`);
  }

  handleGroupEweClick() {
    if (this.state.pbClient !== null) {
      this.state.pbClient.createModuleInOverlay({
        moduleType: 'entitlement',
        contextKey: 'overlayContext',
        options: {
          entitlementGroup: 'PROGRAM'
        },
        onSuccess: () => {
          const observer = new MutationObserver((mutations) => {
            mutations.forEach((mutation) => {
              // Refetch group-ewe after layer was closed.
              const isLayerOpen = document.body.classList.contains('payback-online-integration-overlay-underlying');
              if (mutation.attributeName === 'class' && !isLayerOpen) {
                this.updateGroupEwe();
                observer.disconnect();
              }
            });
          });

          observer.observe(document.body, { attributes: true });
        }
      }, {
        backdropDisabled: true
      });
    }
  }

  // optInOut asks the back-end to opt-in or -out of EWE16 and opens a modal to show the response.
  async optInOut(optIn) {
    const nextModal = optIn ? this.renderOptInResponse : this.renderOptOutResponse;
    if (optIn) {
      // Track opt-in request before we know whether it succeeds, see PBINT-458.
      trackEwe16OptInEvent();
    }
    try {
      this.setState({ pending: true });
      let error;
      const response = await ewe16OptToggle({ payload: { optIn } });
      if (response.status >= 400) {
        if (response.status === HTTP_FORBIDDEN) {
          await this.props.onUpdateParentView();
          return;
        }
        const body = await response.json();
        error = parsePaybackErrorMessage(body);
      }
      this.setState({
        pending: false,
        modal: nextModal(error)
      });
    } catch (error) {
      logger.error(`Failed to toggle ewe16 opt status, error: ${error.message}`);
      this.setState({
        pending: false,
        modal: nextModal(parsePaybackErrorMessage(error))
      });
    }
  }

  closeModal() {
    this.setState({ modal: undefined });
  }

  closeModalUpdateParent() {
    this.setState({ modal: undefined });
    this.props.onUpdateParentView();
  }

  renderOptInRequest() {
    const footerConfig = {
      confirmAction: { label: 'Ja, ich stimme zu' },
      dismissAction: { label: 'Abbrechen' }
    };
    return (
      <Modal
        title={
          <div>
            {MESSAGES_EWE16.ewe16.terms.title1}
            <br />
            {MESSAGES_EWE16.ewe16.terms.title2}
          </div>
        }
        contentClassName="scrollable"
        content={<Ewe16TermsModalContent />}
        type="warning"
        handleSubmit={() => {
          this.setState({ modal: undefined });
          this.optInOut(true);
        }}
        handleCancel={this.closeModal}
        handleClose={this.closeModal}
        footerConfig={footerConfig}
      />
    );
  }

  renderOptInResponse(error) {
    return (
      <Modal
        title={error ? MESSAGES_EWE16.generic_error_fallback_title : MESSAGES_EWE16.ewe16.opt_in.success_message}
        content={error}
        type={error ? 'error' : 'success'}
        handleSubmit={this.closeModalUpdateParent}
        handleClose={this.closeModalUpdateParent}
      />
    );
  }

  renderOptOutRequest() {
    return (
      <Modal
        title={MESSAGES_EWE16.ewe16.opt_out.message_title}
        content={
          <div>
            <p>{MESSAGES_EWE16.ewe16.opt_out.message_hint}</p>
            <p>{MESSAGES_EWE16.ewe16.opt_out.message_content}</p>
          </div>
          }
        type="warning"
        handleSubmit={() => {
          this.setState({ modal: undefined });
          this.optInOut(false);
        }}
        handleCancel={this.closeModal}
        handleClose={this.closeModal}
      />
    );
  }

  renderOptOutResponse(error) {
    return (
      <Modal
        title={error ? MESSAGES_EWE16.generic_error_fallback_title : MESSAGES.ewe16.opt_out.success_message}
        content={error}
        type={error ? 'error' : 'success'}
        handleSubmit={this.closeModalUpdateParent}
        handleClose={this.closeModalUpdateParent}
      />
    );
  }

  renderUnbindBlock() {
    const { paybackAccountBound } = this.props.accountInfo;
    const accordionProperties = {
      alternateTitle: 'Weniger Informationen',
      title: 'Mehr Informationen'
    };
    const checkboxGroup = (
      <CheckboxGroup
        labelText={MESSAGES.account_binding.status.label}
        checkboxId="rs-pb-account-binding-status"
        isChecked={paybackAccountBound}
        handleCheckboxClick={this.props.onUnbindPaybackAccount}
      />
    );
    return (
      <OptedInBlock accordionProperties={accordionProperties} checkboxGroup={checkboxGroup}>
        <PaybackAccountBindStatusContent />
      </OptedInBlock>
    );
  }

  renderGroupEweBlock() {
    const accordionProperties = {
      alternateTitle: 'Weniger Informationen',
      title: 'Mehr Informationen'
    };

    const blockInfo = this.state.groupEwe.type === 'REWE' ? (
      <span>Klicken Sie auf „Einwilligen“, um Ihre Einwilligung in Werbung und Marktforschung gegenüber PAYBACK sowie Unternehmen der REWE Group abzugeben. Sie erhalten Zugang zu eCoupons, vielen Extra-Punkte Chancen, persönlichen Coupons und weiteren Angeboten.</span>
    ) : (
      <span>Klicken Sie auf „Einwilligen“, um Ihre Einwilligung in Werbung und Marktforschung gegenüber PAYBACK und dem Partnerunternehmen, von dem Sie Ihre Karte erhalten haben, abzugeben. Sie erhalten Zugang zu eCoupons, vielen Extra-Punkte Chancen, persönlichen Coupons und weiteren Angeboten.</span>
    );

    return (
      <OptedOutBlock
        accordionProperties={accordionProperties}
        optedOutButtonLabel="Einwilligen"
        handleOptedOutButtonClick={this.handleGroupEweClick}
        blockDescription={blockInfo}
      >
        {this.state.groupEwe.type === 'REWE' && <span>Weiterführende Informationen zu Ihrer Einwilligung in Werbung und Marktforschung gegenüber REWE, PENNY, Zooroyal und REWE Wein online können Sie jederzeit auf <a href="https://www.payback.de/info/ewe09" target="_blank" rel="noopener">payback.de</a> einsehen.</span>}
        {this.state.groupEwe.type !== 'REWE' && <span>Weiterführende Informationen zu Ihrer Einwilligung in Werbung und Marktforschung können Sie jederzeit auf <a href="https://www.payback.de/info/ewe09" target="_blank" rel="noopener">payback.de</a> einsehen.</span>}
      </OptedOutBlock>
    );
  }

  renderOptInBlock() {
    const accordionProperties = {
      alternateTitle: 'Weniger Informationen',
      title: 'Mehr Informationen'
    };

    return (
      <OptedOutBlock
        accordionProperties={accordionProperties}
        optedOutButtonLabel="Einwilligen"
        handleOptedOutButtonClick={() => { this.setState({ modal: this.renderOptInRequest() }); }}
        blockDescription={MESSAGES.ewe16.opt_in.description}
      >
        <Ewe16OptedContent />
      </OptedOutBlock>
    );
  }

  renderOptOutBlock() {
    const { ewe16OptInExists } = this.props.accountInfo;
    const accordionProperties = {
      alternateTitle: 'Weniger Informationen',
      title: 'Mehr Informationen'
    };
    const checkboxGroup = (
      <CheckboxGroup
        labelText={MESSAGES.ewe16.opt_in.status_label}
        checkboxId="rs-ewe16-status"
        isChecked={ewe16OptInExists}
        handleCheckboxClick={() => { this.setState({ modal: this.renderOptOutRequest() }); }}
      />
    );
    return (
      <OptedInBlock accordionProperties={accordionProperties} checkboxGroup={checkboxGroup}>
        <Ewe16OptedContent />
      </OptedInBlock>
    );
  }

  renderPaybackInfoBlock() {
    return (
      <OptedBlock>
        <span>Ihre PAYBACK Einwilligungen können Sie jederzeit auf <a href="https://www.payback.de/info/mein-payback/einwilligungen" target="_blank" rel="noopener noreferrer">payback.de</a> einsehen und verwalten.</span>
      </OptedBlock>
    );
  }

  render() {
    const { paybackAccountBound, ewe16OptInExists } = this.props.accountInfo;
    const {
      pending,
      modal,
      groupEwe
    } = this.state;


    return (
      <div className="rs-payback__consentFormBlock">
        {/* Render a full-screen spinner, if pending. */}
        {pending && <LoadingSpinner />}

        {/* Render the active modal, if any. */}
        {modal}

        <ContainerBlock title={MESSAGES.headline} subtitle={MESSAGES.subline}>
          <div className="rs-payback__consentFormBlock-content">
            {paybackAccountBound && this.renderUnbindBlock()}
            {paybackAccountBound && (ewe16OptInExists ? this.renderOptOutBlock() : this.renderOptInBlock())}
            {paybackAccountBound && groupEwe && !groupEwe.optIn && this.renderGroupEweBlock()}
            {paybackAccountBound && this.renderPaybackInfoBlock()}
          </div>
        </ContainerBlock>
      </div>
    );
  }
}

ConsentFormBlock.propTypes = {
  accountInfo: PropTypes.shape({
    paybackAccountBound: PropTypes.bool,
    ewe16OptInExists: PropTypes.bool
  }).isRequired,
  onUpdateParentView: PropTypes.func.isRequired,
  onUnbindPaybackAccount: PropTypes.func.isRequired
};

export default ConsentFormBlock;
