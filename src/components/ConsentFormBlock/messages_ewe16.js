/**
 * Messages for the ConsentFormBlock and its constituent components. This is one ugly muddah..... Kan't be helped....
 */
const MESSAGES = {
  generic_error_fallback_title: 'Es sind Fehler aufgetreten.',

  ewe16: {
    opt_in: {
      success_message: 'Die Einwilligung Ihres Marketing Einverständnisses gegenüber REWE war erfolgreich.'
    },

    opt_out: {
      message_title: 'Möchten Sie Ihr erteiltes Marketing Einverständnis gegenüber REWE widerrufen und somit keine persönlichen Angebote mehr erhalten?',
      message_hint: 'Wichtiger Hinweis für Teilnehmer am REWE Bonus Coupon und Nutzer der personalisierten REWE Wochenangebote am REWE Service-Punkt:',
      message_content: 'Wenn Sie Ihre Zustimmung zum Marketing Einverständnis gegenüber REWE widerrufen und nicht die aktuelle Einwilligung zu Werbung und Marktforschung zugunsten von REWE abgegeben haben, wird auch Ihre Teilnahme am REWE Bonus Coupon widerrufen. Dadurch verfällt Ihr gesammelter Monatsumsatz und Sie haben keinen Anspruch auf einen möglichen Bonus Coupon im Folgemonat. Gleichzeitig stehen Ihnen die personalisierten Wochenangebote am REWE Service-Punkt nicht mehr zur Verfügung.',
      success_message: 'Sie haben das Marketing Einverständnis gegenüber der REWE Markt GmbH erfolgreich widerrufen.'
    },

    terms: {
      title1: 'Dürfen wir personalisierte Inhalte anzeigen?',
      title2: 'PAYBACK Services bei REWE Marketing Einverständnis',
      conditions_url: 'https://www.rewe.de/payback/einverstaendniserklaerung',

      paragraph1: 'Ich bin einverstanden, dass meine Daten zusätzlich wie folgt verarbeitet werden:',

      paragraph2_title: '<strong>Falls ich PAYBACK PAY nutze:</strong>',
      paragraph2: `Weiter bin ich einverstanden, dass die PAYBACK GmbH an REWE die Information übermittelt,
        ob ich zu PAYBACK PAY angemeldet bin und ggf. mein gesondert erteiltes <a href="https://www.payback.de/site-mobile/paydatenschutz" target="_blank">PAY Marketing Einverständnis</a> vorliegt. Wenn dies der Fall ist, 
        darf REWE diese beiden Umstände, und meine bei REWE anfallenden PAY Transaktionsdaten (d.h. Zahlbetrag, Währung, 
        Ort, Kassen- oder Terminal-ID und Zeitpunkt und Nummer des Vorgangs sowie ggf. Filiale), speichern und zu den unten beschriebenen 
        Zwecken verarbeiten.`,

      paragraph3_title: '<strong>Welche Zwecke:</strong>',
      paragraph3: 'REWE verarbeitet alle vorstehend genannten Daten zu folgenden Zwecken:',

      paragraph4_title: '<strong>Nutzung elektronischer Medien von REWE und PAYBACK Werbung:</strong>',
      paragraph4_1: `Sofern zusätzlich meine <a href="https://www.payback.de/pb/id/424758/" target="_blank">Einwilligung in Werbung und Marktforschung</a>
        vorliegt, darf REWE die bei meiner Nutzung der elektronischen Medien von REWE anfallenden Daten an die PAYBACK GmbH übermitteln. Dies betrifft
        mein Surf- und Nutzungsverhalten bei REWE, solange ich dort eingeloggt bin, auch unabhängig von PAYBACK Services bei REWE.`,
      paragraph4_2: `Diese Daten und die bei der Nutzung von PAYBACK Services bei REWE anfallende Daten darf die PAYBACK GmbH zu meinen bereits vorhandenen Daten 
        hinzuspeichern und zusammen mit diesen Daten für die in der Einwilligung in Werbung und Marktforschung beschriebenen Zwecke sowie zudem zur 
        Ausspielung in den elektronischen Medien von Partnerunternehmen verarbeiten.`,

      paragraph5_title: '<strong>Weitere Hinweise:</strong>',
      paragraph5: 'Einzelheiten finde ich in den <a href="https://www.payback.de/info/datenschutzhinweise/rewe" target="_blank">Hinweisen zum Datenschutz</a>.',

      paragraph6: `Dieses Marketing Einverständnis kann ich jederzeit widerrufen, z.B. gegenüber REWE, telefonisch im 
        PAYBACK Service Center oder schriftlich an PAYBACK, Postfach 23 21 03, 85330 München.`,

      paragraph7: `Andere Einwilligungserklärungen, die ich ggf. bereits gegenüber REWE und/oder der PAYBACK GmbH abgegeben habe, bleiben unberührt. 
        Solche anderen Einwilligungserklärungen gelten also auch dann weiter, wenn ich diesem Marketing Einverständnis nicht zustimme. Das gleiche gilt, wenn 
        ich diesem Marketing Einverständnis zunächst zustimme und es später widerrufe.`,

      list1_title: '<strong>Welche Daten:</strong>',
      list1_intro: 'Die REWE Markt GmbH („REWE“) darf meine folgenden Daten speichern und zu den unten beschriebenen Zwecken verarbeiten:',
      list1: [
        {
          id: 'ewe16-terms-element-1',
          message: `Die von der PAYBACK GmbH übermittelten Daten zur Nutzung von PAYBACK Services bei REWE, nämlich Anrede, 
            Name, Geburtsdatum, E-Mail-Adresse, Telefonnummer(n), Wohn- und Lieferanschrift(en), Punktestand, eCoupons, Kartennummer(n) und 
            von mir bestellte PAYBACK Vorteilservices,`
        },
        {
          id: 'ewe16-terms-element-2',
          message: `Daten über meine bei REWE getätigten Einkäufe (d.h. von mir – bisher oder in Zukunft – im Ladengeschäft, 
              in elektronischen Medien oder anderweitig erworbene Waren/Dienstleistungen, Zahlbetrag, Währung, Ort, Kassen- oder Terminal-ID 
              und Zeitpunkt und Nummer des Vorgangs sowie ggf. Filiale, ggf. der PAYBACK Rabattbetrag),`
        },
        {
          id: 'ewe16-terms-element-3',
          message: `die bei meiner Nutzung von PAYBACK Services bei REWE anfallenden Daten (d.h. welchen PAYBACK
            Service habe ich bei REWE wann in Anspruch genommen), und`
        },
        {
          id: 'ewe16-terms-element-4',
          message: `die Daten, die – auch unabhängig von PAYBACK Services bei REWE - bei meiner Nutzung der elektronischen 
            Medien, des Service Centers und gesondert bestellter E-Mail Newsletter und mobiler Push-Nachrichten von REWE anfallen
            (d.h. welches dieser Medien habe ich wann genutzt, welche der dort unterbreiteten Angebote habe ich mir wann angesehen
            und ggf. in Anspruch genommen).`
        }
      ],

      list2: [
        {
          id: 'ewe16-terms-element-1',
          message: 'Marktforschung'
        },
        {
          id: 'ewe16-terms-element-2',
          message: `Personalisierung von Werbeflächen, Angeboten und anderen Informationen von REWE in seinen elektronischen
             Medien und seinem Service Center`
        },
        {
          id: 'ewe16-terms-element-3',
          message: `Personalisierung von E-Mail Newsletter und mobile Push-Nachrichten von REWE, soweit ich diese gesondert 
            bestellt habe`
        },
        {
          id: 'ewe16-terms-element-4',
          message: `Personalisierung von REWE Briefwerbung, jedoch nur wenn REWE zugleich der Partner ist,
             von dem ich meine PAYBACK Karte erhalten habe und meine <a href="https://www.payback.de/pb/id/424758/" target="_blank">Einwilligung in Werbung und 
             Marktforschung</a> vorliegt.`
        }
      ]
    }
  }
};

export default MESSAGES;
