/* eslint-disable jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */
import PropTypes from 'prop-types';
import React, { Component } from 'react';

import './Dialog.scss';
import { noop } from '../../utils/commons';

class Dialog extends Component {
  constructor(props) {
    super(props);
    this.onClickHandler = this.onClickHandler.bind(this);
  }

  onClickHandler(e) {
    if (e.target === e.currentTarget) {
      this.props.onCancel();
    }
  }

  render() {
    return (
      <div className="rs-payback__dialog" onClick={this.onClickHandler}>
        <div className="rs-payback__dialog-content">
          {this.props.children}
        </div>
      </div>
    );
  }
}

Dialog.propTypes = {
  children: PropTypes.node.isRequired,
  onCancel: PropTypes.func
};

Dialog.defaultProps = {
  onCancel: noop
};

export default Dialog;
