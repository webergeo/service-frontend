import test from 'ava';
import React from 'react';
import render from 'react-test-renderer';
import { configure, mount } from 'enzyme';
import { spy } from 'sinon';
import Adapter from 'enzyme-adapter-react-16';

import Dialog from '.';

configure({ adapter: new Adapter() });

test('renders the default', (t) => {
  const modalTree = render.create(<Dialog><div>some content</div></Dialog>).toJSON();
  t.snapshot(modalTree);
});

test('triggers onCancel handler only if the main block is clicked', (t) => {
  const handleOnCancel = spy();
  const wrapper = mount(<Dialog onCancel={handleOnCancel}><div>some content</div></Dialog>);

  wrapper.find('.rs-payback__dialog-content').simulate('click');
  t.false(handleOnCancel.calledOnce);

  wrapper.find('.rs-payback__dialog').simulate('click');
  t.true(handleOnCancel.calledOnce);
});
