```
const initialState = {
  showDialog: false
};

const openDialog = () => setState({ showDialog: true  });
const closeDialog =() => setState({ showDialog: false  });

const exampleDialog = (
  <Dialog onCancel={closeDialog}>
    <div style={{ padding: '16px', textAlign: 'center' }}>Click Outside the Box to Close!</div>
  </Dialog>
);

<div>
  <Button label="Open Dialog" handleClick={openDialog} />
  {state.showDialog && exampleDialog}
</div>
```
