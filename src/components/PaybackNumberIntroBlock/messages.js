const MESSAGES = {
  headline: 'Ihre PAYBACK Kundennummer: ',
  actions: {
    edit: {
      label: 'Ändern'
    },
    delete: {
      label: 'Löschen'
    }
  }
};

export default MESSAGES;
