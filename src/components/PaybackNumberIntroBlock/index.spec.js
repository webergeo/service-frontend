import test from 'ava';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';
import { spy } from 'sinon';

import PaybackNumberIntroBlock from '.';
import MESSAGES from './messages';
import { NBSP } from '../../utils/constants';
import Button from '../Button';

configure({ adapter: new Adapter() });

test('renders default snapshot', (t) => {
  const numberBlockTree = render
    .create(
      <PaybackNumberIntroBlock
        paybackNumber="0123456789"
        onEditButtonClick={() => { }}
        onDeleteButtonClick={() => { }}
      />
    ).toJSON();

  t.snapshot(numberBlockTree);
});

test('renders component with obfuscated payback number', (t) => {
  const wrapper = mount(
    <PaybackNumberIntroBlock
      paybackNumber="0123456789"
      onEditButtonClick={() => { }}
      onDeleteButtonClick={() => { }}
    />
  );

  t.is(wrapper.find('h2').length, 1);
  t.is(wrapper.find('h2').text(), `${MESSAGES.headline}***${NBSP}***${NBSP}6789`);
});

test('edit button click produces an event', (t) => {
  const clickHandlerDummy = spy();
  const wrapper = mount(
    <PaybackNumberIntroBlock
      paybackNumber="0123456789"
      onEditButtonClick={clickHandlerDummy}
      onDeleteButtonClick={() => { }}
    />
  );
  wrapper
    .find(Button)
    .at(0)
    .simulate('click');
  t.true(clickHandlerDummy.calledOnce);
});

test('delete button click produces an event', (t) => {
  const clickHandlerDummy = spy();
  const wrapper = mount(
    <PaybackNumberIntroBlock
      paybackNumber="0123456789"
      onEditButtonClick={() => { }}
      onDeleteButtonClick={clickHandlerDummy}
    />
  );
  wrapper
    .find(Button)
    .at(1)
    .simulate('click');
  t.true(clickHandlerDummy.calledOnce);
});
