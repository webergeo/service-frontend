import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';
import BlockHeader from '../BlockHeader';
import BorderLayout from '../BorderLayout';
import { getFormattedPaybackNumber as obfuscatePaybackNumber } from '../../utils/commons';
import MESSAGES from './messages';

/**
 * This block is used in the activation screen (i.e. PRIOR to account binding) to provide a customer
 * the option of editing or completely removing his/her payback number from the account data
 * @param {paybackNumber} The plain-text payback number
 * @param {onEditButtonClick} Handler method for the edit button click
 * @param {onDeleteButtonClick} Handler method for the delete button click
 */
const PaybackNumberIntroBlock = ({ paybackNumber, onEditButtonClick, onDeleteButtonClick }) => {
  const content = (
    <div>
      {/* Caution: Add personal flag to TextInput in BlockHeader if you do not obfuscate the PAYBACK number in the future. */}
      <BlockHeader>{MESSAGES.headline + obfuscatePaybackNumber(paybackNumber)}</BlockHeader>
      <Button label={MESSAGES.actions.edit.label} link handleClick={onEditButtonClick} />
      <Button label={MESSAGES.actions.delete.label} link handleClick={onDeleteButtonClick} />
    </div>
  );

  return <BorderLayout content={content} />;
};

PaybackNumberIntroBlock.propTypes = {
  paybackNumber: PropTypes.string.isRequired,
  onEditButtonClick: PropTypes.func.isRequired,
  onDeleteButtonClick: PropTypes.func.isRequired
};

export default PaybackNumberIntroBlock;
