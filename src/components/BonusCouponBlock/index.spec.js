import test from 'ava';
import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import render from 'react-test-renderer';
import MESSAGES from './messages';

import { LoadingSpinner, ContainerBlock, OptedInBlock, OptedOutBlock } from '../../widgets';
import Modal from '../Modal';
import BonusCouponBlock from '../BonusCouponBlock';

// import { EBONBLOCK_OPTEDOUT_MESSAGE, EBONBLOCK_OPTEDOUT_MESSAGE_CONTENT } from '../../utils/constants';

const PAYBACK_INTROBLOCK_PRIMARY_CLASS = 'rs-qa-payback__section--bonus-coupon';

configure({ adapter: new Adapter() });

const bonusCouponBlockComponent = (props) => {
  const accountInfo = {
    bonusCouponOptInExists: true,
    ...props
  };

  // dummy view update handler
  const onUpdateView = () => {};
  return <BonusCouponBlock accountInfo={accountInfo} onUpdateParentView={onUpdateView} />;
};

test('renders BonusCouponBlock without BonusCouponOptInFlag and has correct class and subcomponent', (t) => {
  const bonusCouponBlockTree = render.create(bonusCouponBlockComponent({ bonusCouponOptInExists: false })).toJSON();
  t.snapshot(bonusCouponBlockTree);
  const wrapper = shallow(bonusCouponBlockComponent({ bonusCouponOptInExists: false }));
  t.is(wrapper.instance().props.accountInfo.bonusCouponOptInExists, false);
  t.true(wrapper.hasClass(PAYBACK_INTROBLOCK_PRIMARY_CLASS));
});

test('renders initial BonusCouponBlock with default state', (t) => {
  const wrapper = shallow(bonusCouponBlockComponent({ bonusCouponOptInExists: false }));
  const initialState = {
    isResponsePending: false,
    openEwe16OptInModal: false,
    openBonusCouponOptInStatusModal: false,
    openBonusCouponOptOutModal: false,
    openBonusCouponOptOutStatusModal: false,
    bonusCouponOptErrorMessage: '',
    bonusCouponOptStatusType: 'success'
  };
  t.deepEqual(wrapper.state(), initialState);
  t.is(wrapper.state().openBonusCouponOptInStatusModal, initialState.openBonusCouponOptInStatusModal);
  t.is(wrapper.state().bonusCouponOptErrorMessage, initialState.bonusCouponOptErrorMessage);
  t.deepEqual(wrapper.state().bonusCouponAccountBindingStatus, initialState.bonusCouponAccountBindingStatus);
});

test('renders BonusCouponBlock with ContainerBlock subcomponent', (t) => {
  const wrapper = shallow(bonusCouponBlockComponent({ bonusCouponOptInExists: false }));
  t.is(wrapper.find(ContainerBlock).length, 1);
});

test('renders BonusCouponBlock with OptedInBlock subcomponent', (t) => {
  // const bonusCouponBlockTree = render.create(bonusCouponBlockComponent({ bonusCouponOptInExists: true })).toJSON();
  const wrapper = shallow(bonusCouponBlockComponent({ bonusCouponOptInExists: true }));
  t.is(wrapper.find(OptedInBlock).length, 1);
});

test('renders default BonusCouponBlock with OptedOutBlock subcomponent', (t) => {
  const wrapper = shallow(bonusCouponBlockComponent({ bonusCouponOptInExists: false }));
  t.is(wrapper.find(OptedOutBlock).length, 1);
});

test('When toggled openBonusCouponOptInStatusModal state and bonusCouponOptStatusType is of type *success*, shows the success modal window', (t) => {
  const wrapper = shallow(bonusCouponBlockComponent({ bonusCouponOptInExists: false }));
  wrapper.setState({ openBonusCouponOptInStatusModal: true, bonusCouponOptStatusType: 'success' });
  t.is(wrapper.state().openBonusCouponOptInStatusModal, true);

  const modalWindow = wrapper.find(Modal);
  t.is(modalWindow.length, 1);
  t.is(modalWindow.props().type, 'success');
});

test('When optIn without ewe16 and groupEwe is clicked ewe16 modal is shown', (t) => {
  const wrapper = mount(bonusCouponBlockComponent({ bonusCouponOptInExists: false, ewe16OptInExists: false, groupEweOptInExists: false }));
  const optInButton = wrapper.find('button');
  t.is(optInButton.length, 1);
  optInButton.simulate('click')
  t.is(wrapper.state().openEwe16OptInModal, true);

  const modalWindow = wrapper.find(Modal);
  t.is(modalWindow.length, 1);
});

test('When optIn with groupEwe is clicked ewe16 modal is not shown', (t) => {
  const wrapper = mount(bonusCouponBlockComponent({ bonusCouponOptInExists: false, ewe16OptInExists: false, groupEweOptInExists: true }));
  const optInButton = wrapper.find('button');
  t.is(optInButton.length, 1);
  optInButton.simulate('click')
  t.is(wrapper.state().openEwe16OptInModal, false);
});


test('When optIn with ewe16 is clicked ewe16 modal is not shown', (t) => {
  const wrapper = mount(bonusCouponBlockComponent({ bonusCouponOptInExists: false, ewe16OptInExists: true, groupEweOptInExists: false }));
  const optInButton = wrapper.find('button');
  t.is(optInButton.length, 1);
  optInButton.simulate('click')
  t.is(wrapper.state().openEwe16OptInModal, false);
});

test('When optIn with ewe16 and groupEwe is clicked ewe16 modal is not shown', (t) => {
  const wrapper = mount(bonusCouponBlockComponent({ bonusCouponOptInExists: false, ewe16OptInExists: true, groupEweOptInExists: true }));
  const optInButton = wrapper.find('button');
  t.is(optInButton.length, 1);
  optInButton.simulate('click')
  t.is(wrapper.state().openEwe16OptInModal, false);
});


test('When toggled openBonusCouponOptInStatusModal state and bonusCouponOptStatusType is of type *error*, shows the error modal window', (t) => {
  const wrapper = shallow(bonusCouponBlockComponent({ bonusCouponOptInExists: false }));
  wrapper.setState({ openBonusCouponOptInStatusModal: true, bonusCouponOptStatusType: 'error' });
  t.is(wrapper.state().openBonusCouponOptInStatusModal, true);

  const modalWindow = wrapper.find(Modal);
  t.is(modalWindow.length, 1);
  t.is(modalWindow.props().type, 'error');
});

test('should show LoadingSpinner when response is still pending', (t) => {
  const wrapper = shallow(bonusCouponBlockComponent({ bonusCouponOptInExists: true }));
  wrapper.setState({ isResponsePending: true });
  const loadingSpinner = wrapper.find(LoadingSpinner);
  t.is(loadingSpinner.length, 1);
});

test('should show bonusCoupon OptOut Modal when openBonusCouponOptOutModal state is true', (t) => {
  const wrapper = shallow(bonusCouponBlockComponent({ bonusCouponOptInExists: true }));
  wrapper.setState({ openBonusCouponOptOutModal: true });
  const modalWindow = wrapper.find(Modal);
  t.is(modalWindow.length, 1);
  t.is(modalWindow.props().title, MESSAGES.optedout.message);
  t.is(modalWindow.props().content, MESSAGES.optedout.content);
  t.is(modalWindow.props().type, 'warning');
});

test('When toggled openBonusCouponOptOutStatusModal state and bonusCouponOptStatusType is of type *success*, shows the error modal window', (t) => {
  const wrapper = shallow(bonusCouponBlockComponent({ bonusCouponOptInExists: false }));
  wrapper.setState({ openBonusCouponOptOutStatusModal: true, bonusCouponOptStatusType: 'success' });
  t.is(wrapper.state().openBonusCouponOptOutStatusModal, true);
  const modalWindow = wrapper.find(Modal);
  t.is(modalWindow.length, 1);
  t.is(modalWindow.props().type, 'success');
});

test('When toggled openBonusCouponOptOutStatusModal state and bonusCouponOptStatusType is of type *error*, shows the error modal window', (t) => {
  const wrapper = shallow(bonusCouponBlockComponent({ bonusCouponOptInExists: false }));
  wrapper.setState({ openBonusCouponOptOutStatusModal: true, bonusCouponOptStatusType: 'error' });
  t.is(wrapper.state().openBonusCouponOptOutStatusModal, true);
  const modalWindow = wrapper.find(Modal);
  t.is(modalWindow.length, 1);
  t.is(modalWindow.props().type, 'error');
});

test('Show ewe16 modal', (t) => {
  const wrapper = shallow(bonusCouponBlockComponent({ bonusCouponOptInExists: false }));
  wrapper.setState({ openEwe16OptInModal: true });
  const modalWindow = wrapper.find(Modal);
  t.is(modalWindow.length, 1);
  t.is(modalWindow.props().type, 'warning');
});
