import test from 'ava';
import React from 'react';

import render from 'react-test-renderer';

import BonusCouponContent from './BonusCouponContent';

test('renders the default snapshot', (t) => {
  const modalTree = render.create(<BonusCouponContent />).toJSON();
  t.snapshot(modalTree);
});
