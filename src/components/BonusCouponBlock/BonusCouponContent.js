import React from 'react';
import {BONUS_COUPON_URL} from "../../utils/constants";

const BonusCouponContent = () => (
  <div className="rs-payback-accordion__content">
    <p>
      Sichern Sie sich ab sofort monatlich Ihren persönlichen
      REWE Bonus Coupon als unser Dankeschön für Ihre Treue.
    </p>
    <br />

    <p>
      Je nach gesammeltem Monatsumsatz erhalten Sie für den Folgemonat Ihren
      persönlichen Bonus Coupon, den Sie bei einem Ihrer Einkäufe
      einsetzen können.
    </p>
    <br />

    <p>
      Ab 100 € Umsatz im Monat = 5fach Punkte auf einen Einkauf im Folgemonat
      <br />
      Ab 200 € Umsatz im Monat = 10fach Punkte bei einem Einkauf im Folgemonat
      <br />
      Ab 400 € Umsatz im Monat = 20fach Punkte bei einem Einkauf im Folgemonat
    </p>
    <br />

    <p>
      Ihren persönlichen REWE Bonus Coupon erhalten Sie am REWE ServicePunkt im Markt.
    </p>
    <br />

    <p>
      Übrigens: Den aktuellen Stand Ihres persönlichen Monatsumsatzes
      erfahren Sie auf Ihrem Kassenbon oder jederzeit am REWE Service-Punkt im Markt.
    </p>
    <br />

    <p>
      Ausführliche Informationen zu den Teilnahmebedingungen zum
      REWE Bonus Coupon finden Sie <a href="https://rewe.de/teilnahme-bonuscoupon" target="_blank">hier</a>.
      Bei weiteren Fragen empfehlen wir Ihnen einen Blick in den Bereich{' '}
      <a href={BONUS_COUPON_URL} target="_blank" rel="noopener noreferrer">
        Fragen und Antworten
      </a>.
    </p>
  </div>
);

BonusCouponContent.propTypes = {
};

export default BonusCouponContent;
