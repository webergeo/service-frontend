const MESSAGES = {
  generic_error_fallback_title: 'Es sind Fehler aufgetreten.',
  content: {
    headline: 'REWE Bonus Coupon',
    subline: 'Unser Dankeschön für Ihre Treue',
    optedout: {
      description: 'Klicken Sie auf „Freischalten“, um mit dem REWE Bonus Coupon monatlich bis zu 20fach zu punkten.'
    }
  },
  optedin: {
    status: {
      label: 'Ja, ich möchte REWE Bonus Coupons zukünftig erhalten.'
    },
    success: {
      message: 'Ihre Freischaltung für den REWE Bonus Coupon war erfolgreich!'
    }
  },
  optedout: {
    message: 'Möchten Sie sich vom REWE Bonus Coupon abmelden?',
    content: 'Wenn Sie sich vom REWE Bonus Coupon abmelden, nehmen Sie nicht mehr an diesem ' +
             'PAYBACK Service bei REWE teil. Ihr gesammelter Monatsumsatz verfällt ' +
             'und Sie haben keinen Anspruch auf einen Bonus Coupon im Folgemonat.',
    success: {
      message: 'Ihre Abmeldung vom REWE Bonus Coupon war erfolgreich!'
    }
  }
};

export default MESSAGES;
