import PropTypes from 'prop-types';
import React, { Component } from 'react';

import { LoadingSpinner, ContainerBlock, CheckboxGroup, OptedInBlock, OptedOutBlock } from '../../widgets';
import Modal from '../Modal';
import BonusCouponContent from './BonusCouponContent';
import Ewe16TermsModalContent from '../ConsentFormBlock/Ewe16TermsModalContent';

import { bonusCouponOptToggle, ewe16OptToggle } from '../../api/Accounts';
import { parsePaybackErrorMessage } from '../../utils/apiResponseFormatter';
import { hasProp } from '../../utils/commons';

import { HTTP_FORBIDDEN } from '../../utils/constants';
import MESSAGES from './messages';
import MESSAGES_EWE16 from '../ConsentFormBlock/messages_ewe16';
import './BonusCouponBlock.scss';

import logger from '../../utils/ducLogger';

const bonusCouponBlockClassName = 'rs-payback__section--bonus-coupon rs-qa-payback__section--bonus-coupon';

class BonusCouponBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isResponsePending: false,
      openEwe16OptInModal: false,
      openBonusCouponOptInStatusModal: false,
      openBonusCouponOptOutModal: false,
      openBonusCouponOptOutStatusModal: false,
      bonusCouponOptStatusType: 'success',
      bonusCouponOptErrorMessage: ''
    };
    this.handleBonusCouponOptIn = this.handleBonusCouponOptIn.bind(this);
    this.handleBonusCouponOptOut = this.handleBonusCouponOptOut.bind(this);
    this.handleEwe16OptOutModalSubmit = this.handleEwe16OptOutModalSubmit.bind(this);
    this.handleBonusCouponBlockModalClose = this.handleBonusCouponBlockModalClose.bind(this);
  }

  handleBonusCouponOptIn() {
    const { ewe16OptInExists, groupEweOptInExists } = this.props.accountInfo;

    if (!ewe16OptInExists && !groupEweOptInExists) {
      this.setState({ openEwe16OptInModal: true });
      return;
    }

    this.toggleBonusCouponOptStatus({ optIn: true });
  }

  handleEwe16OptInModalClose() {
    this.setState(() => ({ openEwe16OptInModal: false }));
  }

  async handleEwe16OptInModalSubmit() {
    try {
      this.activateLoadingSpinner(true);
      this.setState({ openEwe16OptInModal: false });

      const response = await ewe16OptToggle({ payload: { optIn: true } });
      if (response.status >= 400) {
        if (response.status === HTTP_FORBIDDEN) {
          await this.props.onUpdateParentView();
          return;
        }

        const body = await response.json();
        this.setState({
          isResponsePending: false,
          openBonusCouponOptInStatusModal: true,
          bonusCouponOptErrorMessage: parsePaybackErrorMessage(body),
          bonusCouponOptStatusType: 'error'
        });
      } else {
        this.activateLoadingSpinner(false);
        this.toggleBonusCouponOptStatus({ optIn: true });
      }
    } catch (error) {
      logger.error(`Failed to toggle ewe16 opt status, error: ${error.message}`);
      this.setState({
        isResponsePending: false,
        bonusCouponOptErrorMessage: parsePaybackErrorMessage(error),
        bonusCouponOptStatusType: 'error'
      });
    }
  }

  handleBonusCouponOptOut() {
    this.setState({ openBonusCouponOptOutModal: true });
  }

  activateLoadingSpinner(toggleFlag) {
    this.setState(() => ({
      isResponsePending: toggleFlag
    }));
  }

  handleEwe16OptOutModalSubmit() {
    this.setState({ openBonusCouponOptOutModal: false });
    this.toggleBonusCouponOptStatus({ optIn: false });
  }

  async toggleBonusCouponOptStatus({ optIn }) {
    try {
      this.activateLoadingSpinner(true);
      const payload = { optIn };
      const response = await bonusCouponOptToggle({ payload });
      if (response.status >= 400) {
        if (response.status === HTTP_FORBIDDEN) {
          await this.props.onUpdateParentView();
          return;
        }

        const errorMessage = await response.json();
        this.setState(() => ({
          [optIn === true ? 'openBonusCouponOptInStatusModal' : 'openBonusCouponOptOutStatusModal']: true,
          bonusCouponOptErrorMessage: parsePaybackErrorMessage(errorMessage),
          bonusCouponOptStatusType: 'error'
        }));
      } else {
        this.setState({
          [optIn === true ? 'openBonusCouponOptInStatusModal' : 'openBonusCouponOptOutStatusModal']: true,
          bonusCouponOptStatusType: 'success'
        });
        await this.props.onUpdateParentView();
      }
      // Finally disable the spinner
      this.activateLoadingSpinner(false);
    } catch (error) {
      logger.error(`Failed to toggle bonus coupon opt status, error: ${error.message}`);
      this.activateLoadingSpinner(false);
      this.setState({
        [optIn === true ? 'openBonusCouponOptInStatusModal' : 'openBonusCouponOptOutStatusModal']: true,
        bonusCouponOptErrorMessage: parsePaybackErrorMessage(error),
        bonusCouponOptStatusType: 'error'
      });
    }
  }

  handleBonusCouponBlockModalClose(e, stateName) {
    e.preventDefault();
    if (!hasProp(this.state, stateName)) {
      return;
    }
    this.setState(() => ({ [stateName]: false }));
  }

  renderEwe16OptInModal() {
    const footerConfig = {
      confirmAction: { label: 'Ja, ich stimme zu' },
      dismissAction: { label: 'Abbrechen' }
    };
    return (
      <Modal
        title={
          <div>
            {MESSAGES_EWE16.ewe16.terms.title1}
            <br />
            {MESSAGES_EWE16.ewe16.terms.title2}
          </div>
        }
        contentClassName="scrollable"
        content={<Ewe16TermsModalContent />}
        type="warning"
        handleSubmit={() => this.handleEwe16OptInModalSubmit()}
        handleCancel={e => this.handleEwe16OptInModalClose(e)}
        handleClose={e => this.handleEwe16OptInModalClose(e)}
        footerConfig={footerConfig}
      />
    );
  }

  renderBonusCouponOptedOutBlock() {
    const accordionProperties = {
      alternateTitle: 'Weniger Informationen',
      title: 'Mehr Informationen'
    };

    return (
      <OptedOutBlock
        accordionProperties={accordionProperties}
        optedOutButtonLabel="Freischalten"
        handleOptedOutButtonClick={this.handleBonusCouponOptIn}
        blockDescription={MESSAGES.content.optedout.description}
      >
        <BonusCouponContent />
      </OptedOutBlock>
    );
  }

  renderBonusCouponOptedInBlock() {
    const { bonusCouponOptInExists } = this.props.accountInfo;
    const accordionProperties = {
      alternateTitle: 'Weniger Informationen',
      title: 'Mehr Informationen'
    };
    const checkboxGroup = (
      <CheckboxGroup
        labelText={MESSAGES.optedin.status.label}
        isChecked={bonusCouponOptInExists}
        checkboxId="rs-bonus-coupon-status"
        handleCheckboxClick={this.handleBonusCouponOptOut}
      />
    );
    return (
      <OptedInBlock accordionProperties={accordionProperties} checkboxGroup={checkboxGroup}>
        <BonusCouponContent />
      </OptedInBlock>
    );
  }

  renderBonusCouponOptInStatusModal() {
    const { bonusCouponOptStatusType, bonusCouponOptErrorMessage } = this.state;
    const modalTitle = bonusCouponOptStatusType === 'error' ? MESSAGES.generic_error_fallback_title : MESSAGES.optedin.success.message;
    const modalContent = bonusCouponOptStatusType === 'error' ? bonusCouponOptErrorMessage : '';
    return (
      <Modal
        title={modalTitle}
        content={modalContent}
        type={bonusCouponOptStatusType}
        handleClose={e => this.handleBonusCouponBlockModalClose(e, 'openBonusCouponOptInStatusModal')}
        handleSubmit={e => this.handleBonusCouponBlockModalClose(e, 'openBonusCouponOptInStatusModal')}
      />
    );
  }

  renderBonusCouponOptOutModal() {
    return (
      <Modal
        title={MESSAGES.optedout.message}
        content={MESSAGES.optedout.content}
        type="warning"
        handleClose={e => this.handleBonusCouponBlockModalClose(e, 'openBonusCouponOptOutModal')}
        handleCancel={e => this.handleBonusCouponBlockModalClose(e, 'openBonusCouponOptOutModal')}
        handleSubmit={this.handleEwe16OptOutModalSubmit}
      />
    );
  }

  renderBonusCouponOptOutStatusModal() {
    const { bonusCouponOptStatusType, bonusCouponOptErrorMessage } = this.state;
    const modalTitle = bonusCouponOptStatusType === 'error' ? MESSAGES.generic_error_fallback_title : MESSAGES.optedout.success.message;
    const modalContent = bonusCouponOptStatusType === 'error' ? bonusCouponOptErrorMessage : '';
    return (
      <Modal
        title={modalTitle}
        content={modalContent}
        type={bonusCouponOptStatusType}
        handleClose={e => this.handleBonusCouponBlockModalClose(e, 'openBonusCouponOptOutStatusModal')}
        handleSubmit={e => this.handleBonusCouponBlockModalClose(e, 'openBonusCouponOptOutStatusModal')}
      />
    );
  }

  render() {
    const {
      isResponsePending,
      openBonusCouponOptInStatusModal,
      openBonusCouponOptOutModal,
      openBonusCouponOptOutStatusModal,
      openEwe16OptInModal
    } = this.state;
    const { bonusCouponOptInExists } = this.props.accountInfo;
    return (
      <div className={bonusCouponBlockClassName}>
        {isResponsePending ? <LoadingSpinner /> : undefined}
        <ContainerBlock title={MESSAGES.content.headline} subtitle={MESSAGES.content.subline}>
          <div className="rs-payback__section-content">
            {bonusCouponOptInExists ? this.renderBonusCouponOptedInBlock() : this.renderBonusCouponOptedOutBlock()}
          </div>
        </ContainerBlock>
        {openEwe16OptInModal && this.renderEwe16OptInModal()}
        {openBonusCouponOptInStatusModal && this.renderBonusCouponOptInStatusModal()}
        {openBonusCouponOptOutModal && this.renderBonusCouponOptOutModal()}
        {openBonusCouponOptOutStatusModal && this.renderBonusCouponOptOutStatusModal()}
      </div>
    );
  }
}

BonusCouponBlock.propTypes = {
  accountInfo: PropTypes.shape({
    bonusCouponOptInExists: PropTypes.bool,
    ewe16OptInExists: PropTypes.bool,
    groupEweOptInExists: PropTypes.bool
  }).isRequired,
  onUpdateParentView: PropTypes.func.isRequired
};

export default BonusCouponBlock;
