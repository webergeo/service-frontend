import React from 'react';
import PropTypes from 'prop-types';

import PaybackError from '../../components/PaybackError';
import ActivationView from '../../views/ActivationView/ActivationView';
import PointsView from '../../views/PointsView/PointsView';
import { HTTP_FORBIDDEN } from '../../utils/constants';

// View returns either a PaybackError, an ActivationView, a ServiceView, or
// null. The last case, null, is a legacy resulting from the fact that App
// initializes both, the errorStatus and the paybackAccountBound flag as
// undefined.
const Points = (props) => {
  const { errorStatus, paybackAccountBound, tokenStatus } = props.accountInfo;
  if (errorStatus === HTTP_FORBIDDEN) {
    window.location.reload();
  } else if (errorStatus !== undefined) {
    return <PaybackError {...props} />;
  } else if (tokenStatus === 'invalid') {
    return <ActivationView {...props} />;
  } else if (tokenStatus === 'expired') {
    return <ActivationView {...props} />;
  } else if (paybackAccountBound === false) {
    return <ActivationView {...props} />;
  } else if (paybackAccountBound === true) {
    return <PointsView {...props} />;
  }
  return null;
};

Points.propTypes = {
  environment: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  accountInfo: PropTypes.shape({
    errorStatus: PropTypes.any,
    paybackAccountBound: PropTypes.bool
  }).isRequired
};

export default Points;
