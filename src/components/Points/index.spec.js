import test from 'ava';
import { stub } from 'sinon';
import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { noop } from '../../utils/commons';
import View from '.';
import PaybackError from '../PaybackError';
import ServiceView from '../../views/ServiceView/ServiceView';
import ActivationView from '../../views/ActivationView/ActivationView';
import { HTTP_FORBIDDEN } from '../../utils/constants';

configure({ adapter: new Adapter() });

/* eslint-disable no-multi-spaces */
test('renders correct component', (t) => {
  // Beware that View renders PaybackError if errorStatus !== undefined. Not only if falsy.
  [
    { errorStatus: true,      paybackAccountBound: true,  want: PaybackError },
    { errorStatus: true,      paybackAccountBound: false, want: PaybackError },
    { errorStatus: undefined, paybackAccountBound: true,  want: ServiceView },
    { errorStatus: undefined, paybackAccountBound: false, want: ActivationView }
  ].forEach((testCase) => {
    const wrapper = shallow(
      <View
        environment={{}}
        accountInfo={testCase}
        onUpdateParentView={noop}
        onDeletePaybackNumber={noop}
        onUnbindPaybackAccount={noop}
      />
    );
    t.true(wrapper.find(testCase.want).length > 0);
  });
});

test('show ActivationView if token status is invalid', (t) => {
  const testCase = { tokenStatus: 'invalid', paybackAccountBound: true };
  const wrapper = shallow(
    <View
      environment={{}}
      accountInfo={testCase}
      onUpdateParentView={noop}
      onDeletePaybackNumber={noop}
      onUnbindPaybackAccount={noop}
    />
  );

  t.true(wrapper.find(ActivationView).length === 1);
});

test('reloads page if user session is invalid', (t) => {
  const reloadMock = stub(window.location, 'reload');
  const testCase = { errorStatus: HTTP_FORBIDDEN, paybackAccountBound: false };
  shallow(
    <View
      environment={{}}
      accountInfo={testCase}
      onUpdateParentView={noop}
      onDeletePaybackNumber={noop}
      onUnbindPaybackAccount={noop}
    />
  );
  t.true(reloadMock.called);
});
