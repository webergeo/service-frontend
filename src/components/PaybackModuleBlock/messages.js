const MESSAGES = {
  voucher: {
    title: 'PAYBACK Punkte einlösen!',
    subtitle: 'Mein persönliches REWE Guthaben - REWE Einkauf einfach mit PAYBACK Punkten bezahlen!',
    text: `Lösen Sie hier Ihre gesammelten PAYBACK Punkte ein. Einfach Einlösebetrag wählen
          und  damit Ihr persönliches REWE Guthaben aufladen. So zahlen Sie Ihren nächsten Einkauf mit PAYBACK Punkten.`
  },
  voucher_error: {
    title: 'Die Aufladung von REWE Guthaben ist momentan aus technischen Gründen auf unserer Website sowie in der REWE App nicht möglich.',
    subtitle: '',
    text: 'Sie können jedoch weiterhin wie gewohnt REWE Guthaben an allen REWE Service-Punkten in unseren Märkten aufladen. Wir entschuldigen uns für die  Unannehmlichkeiten und hoffen, dass Sie diesen Service bald wieder nutzen können.'
  },
  coupon: {
    title: 'PAYBACK Punkte sammeln!',
    subtitle: 'Meine persönlichen eCoupons – jetzt Extra-Punkte sammeln mit REWE eCoupons!',
    text: 'Aktivieren Sie hier Ihre persönlichen eCoupons und sammeln Sie mit Ihrem nächsten Einkauf wertvolle Extra-Punkte.'
  }
};

export default MESSAGES;
