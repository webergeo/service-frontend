import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { ContainerBlock } from '../../widgets';
import { isEnvParameterTruthy } from '../../config/appConfig';
import voucherOptions from '../../config/voucher.config';
import { initializePbClient } from '../../utils/commons';
import { VOUCHER_CREATED_EVENT } from '../../utils/constants';
import {
  PAYBACK_COUPON_ACTIVATION_EVENT,
  trackCouponActivationEvent,
  PAYBACK_VOUCHER_ACTIVATION_EVENT,
  trackVoucherActivationEvent
} from '../../utils/tracking';
import logger from '../../utils/ducLogger';
import MESSAGES from './messages';

import './PaybackModuleBlock.scss';
import ImgPBPoint from '../../assets/images/payback-2014-1075-pointee.png';
import ImgPBGuthaben from '../../assets/images/pointee-rewe-coin-flip.png';
import PaybackError from '../PaybackError';

const ECOUPON_BLOCK_ID = 'payback_coupon';
const ECOUPON_BLOCK_CLASS_NAME = 'rs-payback__paybackModuleBlock rs-qa-payback__ecouponblock';
const EVOUCHER_BLOCK_ID = 'payback_voucher';
const EVOUCHER_BLOCK_CLASS_NAME = 'rs-payback__paybackModuleBlock rs-qa-payback__evoucherblock';

class PaybackModuleBlock extends Component {
  constructor(props) {
    super(props);
    this.clientSuccessHandler = this.clientSuccessHandler.bind(this);
    this.clientErrorHandler = this.clientErrorHandler.bind(this);
    this.createCouponModule = this.createCouponModule.bind(this);
    this.registerEvents = this.registerEvents.bind(this);
    this.showVoucher = isEnvParameterTruthy('FEATURE_TOGGLE_EVOUCHER');
  }

  componentDidMount() {
    initializePbClient(this.clientSuccessHandler, this.clientErrorHandler);
  }

  /* eslint-disable class-methods-use-this */

  clientSuccessHandler(client) {
    this.createCouponModule(client);
    this.registerEvents(client);
    if (this.showVoucher) {
      this.createVoucherModule(client);
    }
  }

  clientErrorHandler(error) {
    logger.error(`failed to initialize pb-client: ${error.message}`);
  }

  createCouponModule(client) {
    client.createModule({
      moduleType: 'coupon',
      selector: `#${ECOUPON_BLOCK_ID}`,
      width: '100%',
      height: 300,
      contextKey: 'partnerWithPaybackContext',
      options: {},
      onError(error) {
        logger.error(`failed to create coupon: ${error.message}`);
      }
    });
  }

  createVoucherModule(client) {
    client.createModule({
      moduleType: 'voucher',
      selector: `#${EVOUCHER_BLOCK_ID}`,
      width: '100%',
      height: 300,
      contextKey: 'partnerWithoutPaybackContext',
      options: voucherOptions,
      onError(error) {
        logger.error(`failed to create voucher: ${error.message}`);
      }
    });
  }

  registerEvents(client) {
    const { eventBus } = this.props;
    client.subscribe('PB-VoucherCreatedEvent', (event) => {
      eventBus.send(VOUCHER_CREATED_EVENT, event);
    });
    client.subscribe(PAYBACK_COUPON_ACTIVATION_EVENT, trackCouponActivationEvent);
    client.subscribe(PAYBACK_VOUCHER_ACTIVATION_EVENT, trackVoucherActivationEvent);
  }

  renderVoucherBlock() {
    return (
      <ContainerBlock
        title={MESSAGES.voucher.title}
        subtitle={MESSAGES.voucher.subtitle}
        paragraph={MESSAGES.voucher.text}
        img={ImgPBGuthaben}
        nodivider
      >
        {this.showVoucher ?
          <div id={EVOUCHER_BLOCK_ID} className={EVOUCHER_BLOCK_CLASS_NAME} /> :
          <PaybackError messages={{
            title: MESSAGES.voucher_error.title,
            subtitle: MESSAGES.voucher_error.subtitle,
            text: MESSAGES.voucher_error.text
          }}
          />
        }
      </ContainerBlock>
    );
  }

  renderCouponBlock() {
    return (
      <ContainerBlock title={MESSAGES.coupon.title} subtitle={MESSAGES.coupon.subtitle} paragraph={MESSAGES.coupon.text} img={ImgPBPoint} nodivider>
        <div id={ECOUPON_BLOCK_ID} className={ECOUPON_BLOCK_CLASS_NAME} />
      </ContainerBlock>
    );
  }

  render() {
    return (
      <div>
        {this.renderVoucherBlock()}
        {this.renderCouponBlock()}
      </div>
    );
  }
}

PaybackModuleBlock.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  eventBus: PropTypes.object.isRequired
};

export default PaybackModuleBlock;
