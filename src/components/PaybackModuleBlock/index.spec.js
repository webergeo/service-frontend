import test from 'ava';
import React from 'react';
import { configure, shallow } from 'enzyme';
import { spy } from 'sinon';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';
import { createBus } from 'suber';
import { loadConfiguration } from '../../../dev-settings/dev-config';
import PaybackError from '../PaybackError';

import PaybackModuleBlock from '../PaybackModuleBlock';
import {
  PAYBACK_COUPON_ACTIVATION_EVENT,
  trackCouponActivationEvent,
  PAYBACK_VOUCHER_ACTIVATION_EVENT,
  trackVoucherActivationEvent
} from '../../utils/tracking';

const ECOUPON_BLOCK_CLASS_NAME = '.rs-qa-payback__ecouponblock';
const EVOUCHER_BLOCK_CLASS_NAME = '.rs-qa-payback__evoucherblock';

configure({ adapter: new Adapter() });
const eventBus = createBus();

test('renders PaybackModuleBlock ECoupon and has correct class', (t) => {
  const eCouponBlockTree = render.create(<PaybackModuleBlock eventBus={eventBus} />).toJSON();
  t.snapshot(eCouponBlockTree);
  const wrapper = shallow(<PaybackModuleBlock eventBus={eventBus} />);
  t.is(wrapper.find(ECOUPON_BLOCK_CLASS_NAME).length, 1);
});

test('render the evoucher with inactive feature toggle', (t) => {
  const eVoucherBlockTree = render.create(<PaybackModuleBlock eventBus={eventBus} />).toJSON();
  t.snapshot(eVoucherBlockTree);
  const wrapper = shallow(<PaybackModuleBlock eventBus={eventBus} />);
  t.is(wrapper.find(EVOUCHER_BLOCK_CLASS_NAME).length, 0);
  t.is(wrapper.find(PaybackError).length, 1);
});

test('render the evoucher with active feature toggle', (t) => {
  loadConfiguration('test');
  const eVoucherBlockTree = render.create(<PaybackModuleBlock eventBus={eventBus} />).toJSON();
  t.snapshot(eVoucherBlockTree);
  const wrapper = shallow(<PaybackModuleBlock eventBus={eventBus} />);
  t.is(wrapper.find(EVOUCHER_BLOCK_CLASS_NAME).length, 1);
  t.is(wrapper.find(PaybackError).length, 0);
});

test('registers to payback client event to track coupon activation', (t) => {
  const wrapper = shallow(<PaybackModuleBlock eventBus={eventBus} />).instance();
  const someClient = { subscribe: () => {} };
  const subscribe = spy(someClient, 'subscribe');
  wrapper.registerEvents(someClient);
  t.true(subscribe.calledWith(PAYBACK_COUPON_ACTIVATION_EVENT, trackCouponActivationEvent));
});

test('registers to payback client event to track guthaben activation', (t) => {
  const wrapper = shallow(<PaybackModuleBlock eventBus={eventBus} />).instance();
  const someClient = { subscribe: () => {} };
  const subscribe = spy(someClient, 'subscribe');
  wrapper.registerEvents(someClient);
  t.true(subscribe.calledWith(PAYBACK_VOUCHER_ACTIVATION_EVENT, trackVoucherActivationEvent));
});
