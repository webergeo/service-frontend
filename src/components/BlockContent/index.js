import React from 'react';
import PropTypes from 'prop-types';
import './BlockContent.scss';

const BlockContent = ({ children }) => <p className="rs-payback__blockContent">{children}</p>;

BlockContent.propTypes = {
  children: PropTypes.string.isRequired
};

export default BlockContent;
