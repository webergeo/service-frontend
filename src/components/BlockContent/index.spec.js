import test from 'ava';
import React from 'react';
import render from 'react-test-renderer';
import BlockContent from '.';

test('renders the component with correct component', (t) => {
  const tree = render.create(<BlockContent>Lorem Ipsum</BlockContent>).toJSON();
  t.snapshot(tree);
});
