import test from 'ava';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';
import Error from '../Error';

configure({ adapter: new Adapter() });

test('renders the default Error Component', (t) => {
  const errorTree = render.create(<Error />).toJSON();
  t.snapshot(errorTree);
  const wrapper = shallow(<Error />);
  t.is(wrapper.find('h1').length, 0);
  t.is(wrapper.find('h2').text(), 'Es ist ein unerwarteter Fehler aufgetreten.');
});

test('renders the Error with statusCode 404', (t) => {
  const wrapper = shallow(<Error statusCode={404} />);
  t.is(wrapper.find('h2').text(), 'Die angeforderte Ressource konnte nicht gefunden werden.');
});

test('renders the Error with statusCode other than 404', (t) => {
  const wrapper = shallow(<Error statusCode={503} />);
  t.is(wrapper.find('h2').text(), 'Interner Server Fehler.');
});
