import PropTypes from 'prop-types';
import React from 'react';
import { FOUR_OH_FOUR } from '../../utils/constants';

import './Error.scss';

const Error = ({ statusCode }) => {
  // eslint-disable-next-line
  const title = statusCode === FOUR_OH_FOUR
    ? 'Die angeforderte Ressource konnte nicht gefunden werden'
    : (statusCode ? 'Interner Server Fehler' : 'Es ist ein unerwarteter Fehler aufgetreten');

  return (
    <div className="rs-error">
      <div>
        <div className="rs-error__desc">
          <h2>{title}.</h2>
        </div>
      </div>
    </div>
  );
};

Error.propTypes = {
  statusCode: PropTypes.number
};

Error.defaultProps = {
  statusCode: 0
};
export default Error;
