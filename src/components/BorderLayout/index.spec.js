import test from 'ava';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import BorderLayout from '../BorderLayout';

configure({ adapter: new Adapter() });

test('render default BorderLayout', (t) => {
  const borderLayoutTree = render.create(<BorderLayout />).toJSON();
  t.snapshot(borderLayoutTree);
});

test('render default BorderLayout with header', (t) => {
  const borderLayoutTree = render.create(<BorderLayout header="Header" content="Content"/>).toJSON();
  t.snapshot(borderLayoutTree);
});

test('render default BorderLayout with footer', (t) => {
  const borderLayoutTree = render.create(<BorderLayout content="Content" footer="Footer,"/>).toJSON();
  t.snapshot(borderLayoutTree);
});

test('render default BorderLayout with footer & header', (t) => {
  const borderLayoutTree = render.create(<BorderLayout header="Header" content="Content" footer="Footer,"/>).toJSON();
  t.snapshot(borderLayoutTree);
});