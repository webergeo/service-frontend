BorderLayout with just content
```
<div>
  <BorderLayout
    content="Content"
  />
</div>
```

Borderlayout with content & footer
```
<div>
  <BorderLayout
    content="Content"
    footer="Footer"
  />
</div>
```

BorderLayout with header & content
```
<div>
  <BorderLayout
    header="header"
    content="content"
  />
</div>
```

BorderLayout with header & content & footer
```
<div>
  <BorderLayout
    header="header"
    content="content"
    footer="footer"
  />
</div>
```