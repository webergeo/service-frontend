import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './BorderLayout.scss';

const BorderLayout = ({ header, content, footer }) => (
  <div className="rs-payback__borderlayout rs-qa-payback__borderlayout">
    {header != null && <div className="rs-payback__borderlayout-header">{header}</div>}
    <div className={'rs-payback__borderlayout-root'}>
      {content}
    </div>
    {footer != null && <div className="rs-payback__borderlayout-footer">{footer}</div>}
  </div>
);

BorderLayout.propTypes = {
  header: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  footer: PropTypes.oneOfType([PropTypes.string, PropTypes.node])
};

BorderLayout.defaultProps = {
  header: null,
  content: null,
  footer: null
};

export default BorderLayout;
