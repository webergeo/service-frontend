import React from 'react';
import PropTypes from 'prop-types';
import './BlockHeader.scss';

/**
 * A generic inner header (or headline, if you prefer) for individual block elements. This components primarily aims to
 * provide centralized styling to such headlines in order to avoid copy-pasted CSS styles.
 */
const BlockHeader = ({ children }) => <h2 className="rs-payback__blockHeader">{children}</h2>;

BlockHeader.propTypes = {
  children: PropTypes.string.isRequired
};

export default BlockHeader;
