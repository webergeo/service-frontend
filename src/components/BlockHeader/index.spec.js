import test from 'ava';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';
import BlockHeader from '../BlockHeader';

configure({ adapter: new Adapter() });

test('renders the component with correct title', (t) => {
  const blockHeader = <BlockHeader>Lorem Ipsum</BlockHeader>;
  const errorTree = render.create(blockHeader).toJSON();
  t.snapshot(errorTree);

  const wrapper = shallow(blockHeader);
  t.is(wrapper.find('h2').length, 1);
  t.is(wrapper.find('h2').text(), 'Lorem Ipsum');
});
