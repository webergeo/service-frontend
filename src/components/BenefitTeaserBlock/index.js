import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';
import BorderLayout from '../BorderLayout';

import MESSAGES from './messages';
import './BenefitTeaserBlock.scss';
import BlockHeader from '../BlockHeader';
import BlockContent from '../BlockContent';
import rewePointee from '../../assets/images/pointee-rewe-cap-2x.png';

/**
 * A  teaser block for the "Sie nutzen bereits PAYBACK?" section
 */
const BenefitTeaserBlock = ({ onClick }) => {
  const contentBlock = (
    <div className="rs-payback__benefitTeaserBlock-content">
      <div className="rs-payback__benefitTeaserBlock-content-desc">
        <BlockHeader>{MESSAGES.headline}</BlockHeader>
        <BlockContent>{MESSAGES.paragraph}</BlockContent>
      </div>
      <img src={rewePointee} alt="" className="rs-payback__benefitTeaserBlock-content-img" />
    </div>
  );

  const footer = (
    <div className="rs-payback__benefitTeaserBlock-footer">
      <Button
        big
        color="green"
        label={MESSAGES.action.text}
        target="_blank"
        handleClick={onClick}
        className="rs-payback__benefitTeaserBlock-button"
      />
    </div>
  );

  return <BorderLayout content={contentBlock} footer={footer} />;
};

BenefitTeaserBlock.propTypes = {
  onClick: PropTypes.func.isRequired
};

export default BenefitTeaserBlock;
