const MESSAGES = {
  headline: 'Sie nutzen PAYBACK bereits?',
  paragraph: 'Jetzt PAYBACK Services einmalig im Kundenkonto freischalten und bequem online nutzen und beim REWE Lieferservice automatisch punkten.',
  action: {
    text: 'Jetzt Services freischalten'
  }
};

export default MESSAGES;
