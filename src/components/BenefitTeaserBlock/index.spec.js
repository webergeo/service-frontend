import test from 'ava';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';
import { spy } from 'sinon';

import BenefitTeaserBlock from '../BenefitTeaserBlock';
import Button from '../Button';
import BlockHeader from '../BlockHeader';

const PAYBACK_INTROBLOCK_PRIMARY_CLASS = '.rs-payback__borderlayout-root';
const PAYBACK_INTROBLOCK_SECONDARY_CLASS = '.rs-payback__intro--haspayback';

configure({ adapter: new Adapter() });

function createTeaser() {
  return (
    <BenefitTeaserBlock
      onClick={() => {
        console.info('clicked!');
      }}
    />
  );
}

test('renders BenefitTeaserBlock without payback info and has correct class', (t) => {
  const introBlockTree = render.create(createTeaser()).toJSON();
  t.snapshot(introBlockTree);
  const wrapper = mount(createTeaser());
  t.is(wrapper.find(PAYBACK_INTROBLOCK_PRIMARY_CLASS).length, 1);
  t.is(wrapper.find(PAYBACK_INTROBLOCK_SECONDARY_CLASS).length, 0);
});

test('renders the correct headline', (t) => {
  const wrapper = mount(createTeaser());
  t.is(wrapper.find(BlockHeader).length, 1);
});

test('renders the button component', (t) => {
  const wrapper = mount(createTeaser());
  t.is(wrapper.find(Button).length, 1);
});

test('button click produces click event', (t) => {
  const clickHandlerDummy = spy();
  const teaser = <BenefitTeaserBlock onClick={clickHandlerDummy} />;
  const wrapper = mount(teaser);
  wrapper.find(Button).simulate('click');
  t.true(clickHandlerDummy.calledOnce);
});
