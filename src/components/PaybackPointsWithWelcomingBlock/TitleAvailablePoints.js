import React from 'react';
import MESSAGES from './messages';

const TitleAvailablePoints = () => (
  <div className="payback-sub-container">
    <div className="payback-sub-container-box">
      <div className="rs-point-block__row--available">
        <div className="rs-point-block__cell--label">{MESSAGES.available}:</div>
      </div>
    </div>
  </div>
);

export default TitleAvailablePoints;
