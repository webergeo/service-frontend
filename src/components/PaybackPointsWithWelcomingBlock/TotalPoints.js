import React from 'react';
import PropTypes from 'prop-types';

const TotalPoints = ({ totalPoints }) => <div className="payback-title-totalpoints">{totalPoints} °P</div>;

TotalPoints.propTypes = {
  totalPoints: PropTypes.string.isRequired
};
export default TotalPoints;
