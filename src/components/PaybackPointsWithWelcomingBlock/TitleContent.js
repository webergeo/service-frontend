import React from 'react';
import PropTypes from "prop-types";

const TitleContent = ({ welcoming }) => <div className="payback-title-headline">{welcoming}</div>;

TitleContent.propTypes = {
  welcoming: PropTypes.string.isRequired
};
export default TitleContent;