import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { formatNumber } from '../../utils/commons';
import { getPoints } from '../../services/PaybackAccountService';
import { hasError } from '../../services/RequestErrorWrapper';

import TitleContent from './TitleContent';
import TotalPoints from './TotalPoints';
import TitleAvailablePoints from './TitleAvailablePoints';

import PaybackError from '../PaybackError';

import { VOUCHER_CREATED_EVENT, HTTP_FORBIDDEN } from '../../utils/constants';

import './PaybackPointsWithWelcomingBlock.scss';

import LoadingIndicator from '../../widgets/LoadingIndicator';

class PaybackPointsWithWelcomingBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      responseStatus: 'loading',
      forceShowSpinner: true
    };
    this.getPoints = this.getPoints.bind(this);
    this.subscribeToEvents = this.subscribeToEvents.bind(this);
    this.unblockSpinnerOnTimeout = this.unblockSpinnerOnTimeout.bind(this);
    this.subscribeToEvents();
  }

  componentDidMount() {
    this.unblockSpinnerOnTimeout();
    this.getPoints();
  }

  // eslint-disable-next-line
  async getPoints() {
    const pointResponse = await getPoints();
    if (pointResponse.errorStatus === HTTP_FORBIDDEN) {
      await this.props.onUpdateParentView();
      return;
    }

    this.setState({
      welcoming: "Guten Morgen, Max!",
      totalPoints: pointResponse.totalPoints,
      responseStatus: `${hasError(pointResponse) ? 'error' : 'loaded'}`
    });
  }

  // eslint-disable-next-line class-methods-use-this
  unblockSpinnerOnTimeout() {
    window.setTimeout(() => {
      this.setState({
        forceShowSpinner: false
      });
    }, 1000);
  }

  subscribeToEvents() {
    // for now this just reloads the current PB points from backend
    // eslint-disable-next-line no-unused-vars
    this.props.eventBus.take(VOUCHER_CREATED_EVENT, (event) => {
      this.setState({
        forceShowSpinner: true,
        responseStatus: 'loading'
      });
      this.unblockSpinnerOnTimeout();
      this.getPoints();
    });
  }

  render() {
    if (this.state.forceShowSpinner || this.state.responseStatus === 'loading') {
      return (
        <div className="rs-payback__section--payback-points bg-pbblue">
          <TitleContent welcoming={this.state.welcoming}/>
          <LoadingIndicator />
        </div>
      );
    }

    if (this.state.responseStatus === 'error') {
      return (
        <div className="rs-payback__section--payback-points bg-pbblue">
          <PaybackError showImage={false} showSubtitle={false} />
        </div>
      );
    }

    return (
      <div className="rs-payback__section--payback-points bg-pbblue">
        <TitleContent welcoming={this.state.welcoming}/>

        <TitleAvailablePoints />

        <TotalPoints totalPoints={formatNumber(this.state.totalPoints)} />
      </div>
    );
  }
}

PaybackPointsWithWelcomingBlock.propTypes = {
  // eslint-disable-next-line
  eventBus: PropTypes.object.isRequired,
  onUpdateParentView: PropTypes.func.isRequired
};

export default PaybackPointsWithWelcomingBlock;
