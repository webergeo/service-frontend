```
const openDialog = () => setState({ showDialog: true  });
const closeDialog =() => setState({ showDialog: false  });

<div>
  <Button label="Open Dialog" handleClick={openDialog} />
  {state.showDialog && <PaybackNumberModal onCancel={closeDialog} onSubmit={() => console.log('Submitted')} />}
</div>
```
