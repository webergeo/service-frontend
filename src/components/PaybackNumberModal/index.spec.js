import React from 'react';
import test from 'ava';
import render from 'react-test-renderer';
import { spy } from 'sinon';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import PaybackNumberModal from '.';
import { noop } from '../../utils/commons';

configure({ adapter: new Adapter() });

test('renders the default snapshot', (t) => {
  const modalTree = render.create(
    <PaybackNumberModal
      onCancel={noop}
      onSubmit={noop}
    />
  ).toJSON();
  t.snapshot(modalTree);
});

test('renders the snapshot in editNumberMode', (t) => {
  const modalTree = render.create(
    <PaybackNumberModal
      activateService={false}
      onCancel={noop}
      onSubmit={noop}
    />
  ).toJSON();
  t.snapshot(modalTree);
});

test('submit handler does not propagate if value is too short', (t) => {
  const onSubmit = spy();
  const valueTooShort = '01234';
  const wrapper = shallow(
    <PaybackNumberModal
      onCancel={noop}
      onSubmit={onSubmit}
      defaultValue={valueTooShort}
    />
  );
  wrapper.instance().handleSubmit();
  t.false(onSubmit.called);
});

test('submit handler propagates if all constraints are met', (t) => {
  const onSubmit = spy();
  const validValue = '0123456789';
  const wrapper = shallow(
    <PaybackNumberModal
      onCancel={noop}
      onSubmit={onSubmit}
      defaultValue={validValue}
    />
  );
  wrapper.instance().handleSubmit();
  t.true(onSubmit.calledOnce);
  t.true(onSubmit.calledWith(validValue));
});

test('submit handler does not propagate submit if any validation error exists', (t) => {
  const onSubmit = spy();
  const value = '0123456789';
  const validation = { error: true, message: 'any error message' };
  const wrapper = shallow(
    <PaybackNumberModal
      onCancel={noop}
      onSubmit={onSubmit}
      defaultValue={value}
      defaultValidation={validation}
    />
  );
  wrapper.instance().handleSubmit();
  t.false(onSubmit.called);
});
