import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './PaybackNumberModal.scss';
import Button from '../Button';
import TextInput from '../TextInput';
import InfoPopover from '../InfoPopover';
import paybackCardExplanationImage from '../../assets/images/img-payback-card-explanation.jpg';
import { WHITE_SPACE } from '../../utils/constants';
import MESSAGES from './messages';
import { isNumeric } from '../../utils/commons';
import StackLayout from '../StackLayout';
import Dialog from '../Dialog';

const validatePaybackNumber = (value) => {
  const validation = {
    error: false,
    message: ''
  };

  if (value.indexOf(WHITE_SPACE) !== -1) {
    validation.error = true;
    validation.message = MESSAGES.error.whiteSpace;
  } else if (!isNumeric(value)) {
    validation.error = true;
    validation.message = MESSAGES.error.nonNumeric;
  } else if (value.trim().length > 10) {
    validation.error = true;
    validation.message = MESSAGES.error.numberLimit;
  }

  return validation;
};

class PaybackNumberModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.defaultValue,
      validation: props.defaultValidation
    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.isSaveReady = this.isSaveReady.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillReceiveProps(props) {
    this.setState ({
      value: props.defaultValue,
      validation: props.defaultValidation
    });
  }

  onChangeHandler(e) {
    const { value } = e.target;
    this.setState({
      value,
      validation: validatePaybackNumber(value)
    });
  }

  isSaveReady() {
    const { value, validation } = this.state;
    return !(validation && validation.error) && value.length === 10;
  }

  handleSubmit() {
    if (this.isSaveReady()) {
      this.props.onSubmit(this.state.value);
    }
  }

  render() {
    const { onCancel } = this.props;
    const { value, validation } = this.state;

    return (
      <Dialog onCancel={onCancel}>
        <StackLayout className="rs-payback__paybackNumberModal">
          <div className="rs-payback__paybackNumberModal-header">
            <span> Ihre PAYBACK Kundennummer </span>
            <button className="rs-payback__paybackNumberModal-header-button rs-payback__paybackNumberModal-header-button-close" onClick={onCancel}>
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path
                  fill="#4A4A4A"
                  fillRule="evenodd"
                  d="M6.455 18.75a.852.852 0 1 1-1.205-1.205L10.795 12 5.25 6.455A.852.852 0 0 1 6.455 5.25L12 10.795l5.545-5.545a.852.852 0 1 1 1.205
                  1.205L13.205 12l5.545 5.545a.852.852 0 1 1-1.205 1.205L12 13.205 6.455 18.75z"
                />
              </svg>
            </button>
          </div>
          <div className="rs-payback__paybackNumberModal-content">
            <span>{(this.props.activateService) ? MESSAGES.modalText.activateService : MESSAGES.modalText.changeNumber}</span>
            <TextInput
              name="paybackNumber"
              onChange={this.onChangeHandler}
              onSubmit={this.handleSubmit}
              placeHolderText="PAYBACK Kundennummer"
              value={value}
              validation={validation}
              personal
            />
            <div className="rs-payback__paybackNumberModal-content-info">
              <span>Wo finde ich meine PAYBACK Kundennummer?</span>
              <InfoPopover>
                Ihre Kundennummer finden Sie auf der Rückseite Ihrer PAYBACK Karte:
                <img src={paybackCardExplanationImage} alt="payback card back" />
              </InfoPopover>
            </div>
          </div>
          <div className="rs-payback__paybackNumberModal-footer">
            <Button big stretched label="Abbrechen" color="grey" handleClick={onCancel} />
            <Button big stretched label="Speichern" handleClick={this.handleSubmit} disabled={!this.isSaveReady()} />
          </div>
        </StackLayout>
      </Dialog>
    );
  }
}

PaybackNumberModal.propTypes = {
  defaultValue: PropTypes.string,
  activateService: PropTypes.bool,
  defaultValidation: PropTypes.shape({
    error: PropTypes.bool.isRequired,
    message: PropTypes.string
  }),
  onCancel: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
};

PaybackNumberModal.defaultProps = {
  defaultValue: '',
  activateService: true,
  defaultValidation: {
    error: false,
    message: ''
  }
};

export default PaybackNumberModal;
