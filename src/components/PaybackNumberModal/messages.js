const MESSAGES = {
  modalText: {
    activateService: 'Sie können bei REWE noch einfacher Punkte sammeln, wenn Sie Ihre PAYBACK Nummer speichern. ' +
      'Bei Abbruch der Freischaltung bleibt Ihre PAYBACK Nummer im Kundenkonto gespeichert. Sie können sie jederzeit im REWE Kundenkonto löschen.',

    changeNumber: 'Wenn Sie eine andere PAYBACK Kundennummer für Ihr REWE Kundenkonto verwenden möchten, geben Sie diese bitte ein. ' +
      'Tipp: Vergessen Sie nicht, anschließend Ihre PAYBACK Services freizuschalten.'
  },
  error: {
    nonNumeric: 'Die Kundennummer darf nur aus Ziffern bestehen.',
    numberLimit: 'Die Kundennummer muss aus 10 Ziffern bestehen.',
    whiteSpace: 'Die Kundennummer darf keine Leerzeichen beinhalten.'
  }
};

export default MESSAGES;
