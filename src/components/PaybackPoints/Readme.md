default Modal (success)

```
const openModal = () => setState({ showModal: true  });
const closeModal =() => setState({ showModal: false  });

const exampleModal = 
  <Modal
    handleClose={closeModal}
    title="Lorem ipsum dolor"
    content="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam"
    handleSubmit={closeModal}
  />;

<div>
  <Button label="Open Modal" handleClick={openModal} />
  {state.showModal && exampleModal}
</div>
```

warning Modal

```
const openModal = () => setState({ showModal: true  });
const closeModal =() => setState({ showModal: false  });

const exampleModal = 
  <Modal
    handleClose={closeModal}
    title="Lorem ipsum dolor"
    content="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam"
    type="warning"
    handleSubmit={closeModal}
    handleCancel={closeModal}
  />;

<div>
  <Button label="Open Modal" handleClick={openModal} />
  {state.showModal && exampleModal}
</div>
```

error Modal

```
const openModal = () => setState({ showModal: true  });
const closeModal =() => setState({ showModal: false  });

const exampleModal = 
  <Modal
    handleClose={closeModal}
    title="Lorem ipsum dolor"
    content="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam"
    type="error"
    handleSubmit={closeModal}
  />;

<div>
  <Button label="Open Modal" handleClick={openModal} />
  {state.showModal && exampleModal}
</div>
```
