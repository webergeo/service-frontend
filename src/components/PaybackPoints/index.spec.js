import test from 'ava';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import Modal from '.';

const defaultModal = () => <Modal title="Awesome Default Modal" />;

configure({ adapter: new Adapter() });

test('renders the default Modal', (t) => {
  const modalTree = render.create(defaultModal()).toJSON();
  t.snapshot(modalTree);
  const wrapper = shallow(defaultModal());
  t.is(wrapper.instance().props.title, 'Awesome Default Modal');
  t.is(wrapper.instance().props.content, '');
  t.is(wrapper.instance().props.type, 'success');
  t.is(wrapper.instance().props.showClose, true);
  t.is(wrapper.find('Button').length, 1);
});

test('renders the Modal with no close button', (t) => {
  const wrapper = shallow(<Modal title="Modal with Content" content="I am a content" showClose={false} />);
  const closeButton = 'rs-modal__closelink';
  t.is(wrapper.find('Button').length, 1);
  t.is(wrapper.find(`button[className="${closeButton}"]`).length, 0);
});

test('renders the Modal with content', (t) => {
  const wrapper = shallow(<Modal title="Modal with Content" content="I am a content" />);
  t.is(wrapper.instance().props.title, 'Modal with Content');
  t.is(wrapper.instance().props.content, 'I am a content');
});

test('renders the warning type Modal', (t) => {
  const wrapper = shallow(<Modal title="Warning Modal" type="warning" />);
  t.is(wrapper.instance().props.type, 'warning');
  t.is(wrapper.find('Button').length, 2);
});

test('renders the warning type Modal with scrollable content', (t) => {
  const content = <div>Hello scrollable content</div>;
  const scrollableModal = () => <Modal content={content} title="Warning Modal" contentClassName="scrollable" type="warning" />;
  const modalTree = render.create(scrollableModal()).toJSON();
  t.snapshot(modalTree);
  const wrapper = shallow(scrollableModal());
  t.is(wrapper.find('.rs-modal__holder').hasClass('scrollable'), true);
  t.is(wrapper.find('.rs-modal__content').hasClass('scrollable'), true);
});

test('renders the warning type Modal with provided Footer Buttons order', (t) => {
  const content = <div>Hello scrollable content</div>;
  const footerConfig = {
    confirmAction: {
      label: 'Yes, click me!!'
    },
    dismissAction: {
      label: 'Get lost!!'
    }
  };
  const scrollableModal = () =>
    <Modal content={content} title="Warning Modal" contentClassName="scrollable" type="warning" footerConfig={footerConfig} />;
  const modalTree = render.create(scrollableModal()).toJSON();
  t.snapshot(modalTree);
  const wrapper = shallow(scrollableModal());
  t.is(wrapper.find('Button').at(1).prop('label'), 'Yes, click me!!');
  t.is(wrapper.find('Button').at(0).prop('label'), 'Get lost!!');
});
