const MESSAGES = {
  text: 'Auf unseren ausführlichen Info-Seiten finden Sie viel Wissenswertes, wenn es um die Vorteile von PAYBACK bei REWE geht.',
  action: {
    label: 'So funktioniert PAYBACK bei REWE',
    url: 'https://www.rewe.de/payback/'
  }
};

export default MESSAGES;
