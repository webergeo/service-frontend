import React from 'react';

import './QuestionsBlock.scss';
import Button from '../Button';
import BorderLayout from '../BorderLayout';
import MESSAGES from './messages';

const QuestionsBlock = () => {
  const content = (
    <div>
      <p className="rs-payback__questions--content">{MESSAGES.text}</p>
      <Button label={MESSAGES.action.label} link href={MESSAGES.action.url} target="_blank" />
    </div>
  );

  return <BorderLayout content={content} />;
};

export default QuestionsBlock;
