import test from 'ava';
import React from 'react';

import render from 'react-test-renderer';

import QuestionsBlock from '.';

test('renders the default snapshot', (t) => {
  const modalTree = render.create(<QuestionsBlock />).toJSON();
  t.snapshot(modalTree);
});
