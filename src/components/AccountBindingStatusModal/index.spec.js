import test from 'ava';
import React from 'react';
import render from 'react-test-renderer';
import AccountBindingStatusModal from '../AccountBindingStatusModal';

test('renders success modal', (t) => {
  const component = render.create(
    <AccountBindingStatusModal
      environment={{}}
      paybackNumber="1234567890"
      accountBindingStatus="success"
    />
  ).toJSON();

  t.snapshot(component);
});

test('renders success with new content if toggle is active', (t) => {
  const component = render.create(
    <AccountBindingStatusModal
      environment={{ FEATURE_TOGGLE_KV_ENROLLMENT: 'true' }}
      paybackNumber="1234567890"
      accountBindingStatus="success"
    />
  ).toJSON();

  t.snapshot(component);
});

test('do not render success modal without card if toggle is active', (t) => {
  const component = render.create(
    <AccountBindingStatusModal
      environment={{ FEATURE_TOGGLE_KV_ENROLLMENT: 'true' }}
      paybackNumber={null}
      accountBindingStatus="success"
    />
  ).toJSON();
  t.snapshot(component);
});

test('renders failure modal', (t) => {
  const component = render.create(
    <AccountBindingStatusModal
      environment={{}}
      accountBindingStatus="failure"
    />
  ).toJSON();
  t.snapshot(component);
});

test('renders number_mismatch modal', (t) => {
  const component = render.create(
    <AccountBindingStatusModal
      environment={{}}
      accountBindingStatus="number_mismatch"
    />
  ).toJSON();
  t.snapshot(component);
});

test('renders binding_limit_exceeded modal', (t) => {
  const component = render.create(
    <AccountBindingStatusModal
      environment={{}}
      accountBindingStatus="binding_limit_exceeded"
    />
  ).toJSON();
  t.snapshot(component);
});

test('renders unauthorized modal', (t) => {
  const component = render.create(
    <AccountBindingStatusModal
      environment={{}}
      accountBindingStatus="unauthorized"
    />
  ).toJSON();
  t.snapshot(component);
});
