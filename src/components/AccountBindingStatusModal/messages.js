const MESSAGES = {
  success: {
    type: 'success',
    title: 'Ihre Anmeldung für die PAYBACK Services bei REWE war erfolgreich!',
    content: 'Sie haben PAYBACK Services bei REWE freigeschaltet. Damit können Sie zukünftig persönliche PAYBACK Angebote auf REWE.de nutzen.',
    newContent: cardNumber => `Sie haben PAYBACK Services bei REWE für Ihre PAYBACK Kundennummer ${cardNumber || ''} freigeschaltet. So können Sie ab sofort alle PAYBACK Vorteile bei REWE nutzen. Wir wünschen viel Spaß beim Einkaufen und Punkten!`
  },
  failure: {
    type: 'error',
    title: 'Es sind Fehler aufgetreten.',
    content: 'Eine Anmeldung zu PAYBACK Services (z.B. REWE eBon) ist zur Zeit nicht möglich, bitte versuchen Sie es später erneut.'
  },
  number_mismatch: {
    type: 'error',
    title: 'Es sind Fehler aufgetreten.',
    content: 'Eine Anmeldung zu PAYBACK Services (z.B. REWE eBon) ist nicht möglich. Die PAYBACK Kundennummer stimmt' +
    ' nicht mit der bei REWE.de hinterlegten PAYBACK Kundennummer überein.'
  },
  binding_limit_exceeded: {
    type: 'error',
    title: 'Es sind Fehler aufgetreten.',
    content: 'Die PAYBACK Services für dieses PAYBACK Konto sind bereits in einem anderen REWE Konto aktiviert.'
  },
  unauthorized: {
    type: 'error',
    title: 'Es sind Fehler aufgetreten.',
    content: 'Eine Anmeldung zu PAYBACK Services (z.B. REWE eBon) ist nicht möglich. Unauthorized'
  }
};

export default MESSAGES;
