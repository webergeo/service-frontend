import React from 'react';
import PropTypes from 'prop-types';

import MESSAGES from './messages';
import Modal from '../Modal';
import { noop } from '../../utils/commons';
import { isEnvParameterDefined, isEnvParameterTruthy } from '../../config/appConfig';

const AccountBindingStatusModal = ({
  environment,
  paybackNumber,
  accountBindingStatus,
  onSubmit,
  onClose
}) => {
  const kvEnrollment = isEnvParameterDefined('FEATURE_TOGGLE_KV_ENROLLMENT') ?
    isEnvParameterTruthy('FEATURE_TOGGLE_KV_ENROLLMENT') :
    environment.FEATURE_TOGGLE_KV_ENROLLMENT;

  if (kvEnrollment && accountBindingStatus === 'success' && !paybackNumber) {
    return null;
  }

  const content = kvEnrollment && accountBindingStatus === 'success' ?
    MESSAGES.success.newContent(paybackNumber) :
    MESSAGES[accountBindingStatus].content;

  return (
    <Modal
      title={MESSAGES[accountBindingStatus].title}
      content={content}
      type={MESSAGES[accountBindingStatus].type}
      handleClose={onClose}
      handleSubmit={onSubmit}
    />
  );
};

AccountBindingStatusModal.propTypes = {
  environment: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  paybackNumber: PropTypes.string,
  accountBindingStatus: PropTypes.oneOf(Object.keys(MESSAGES)).isRequired,
  onSubmit: PropTypes.func,
  onClose: PropTypes.func
};

AccountBindingStatusModal.defaultProps = {
  paybackNumber: null,
  onSubmit: noop,
  onClose: noop
};

export default AccountBindingStatusModal;
