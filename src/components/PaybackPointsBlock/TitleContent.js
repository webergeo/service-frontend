import React from 'react';
import MESSAGES from './messages';

export default function TitleContent() {
  return (
    <div className="payback-title-headline">
      {MESSAGES.title}
      <span className="payback-title-headline-colon">:</span>
    </div>
  );
}
