import test from 'ava';
import React from 'react';

import render from 'react-test-renderer';

import TooltipPointsInfoContent from './TooltipPointsInfoContent';

test('test the default snapshot', (t) => {
  const tooltipPointsInfoContent = render.create(<TooltipPointsInfoContent />).toJSON();
  t.snapshot(tooltipPointsInfoContent);
});
