import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatNumber } from '../../utils/commons';
import { getPoints } from '../../services/PaybackAccountService';
import { hasError } from '../../services/RequestErrorWrapper';
import TitleContent from './TitleContent';
import TotalPoints from './TotalPoints';
import BlockedAndAvailablePoints from './BlockedAndAvailablePoints';
import PaybackError from '../PaybackError';
import { VOUCHER_CREATED_EVENT, HTTP_FORBIDDEN } from '../../utils/constants';
import MESSAGES from './messages';
import './PaybackPointsBlock.scss';

import LoadingIndicator from '../../widgets/LoadingIndicator';

class PaybackPointsBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      responseStatus: 'loading',
      forceShowSpinner: true
    };
    this.getPoints = this.getPoints.bind(this);
    this.subscribeToEvents = this.subscribeToEvents.bind(this);
    this.unblockSpinnerOnTimeout = this.unblockSpinnerOnTimeout.bind(this);
    this.subscribeToEvents();
  }

  componentDidMount() {
    this.unblockSpinnerOnTimeout();
    this.getPoints();
  }

  // eslint-disable-next-line
  async getPoints() {
    const pointResponse = await getPoints();
    if (pointResponse.errorStatus === HTTP_FORBIDDEN) {
      await this.props.onUpdateParentView();
      return;
    }

    this.setState({
      totalPoints: pointResponse.totalPoints,
      availablePoints: pointResponse.availablePoints,
      blockedPoints: pointResponse.blockedPoints,
      responseStatus: `${hasError(pointResponse) ? 'error' : 'loaded'}`
    });
  }

  // eslint-disable-next-line class-methods-use-this
  unblockSpinnerOnTimeout() {
    window.setTimeout(() => {
      this.setState({
        forceShowSpinner: false
      });
    }, 1000);
  }

  subscribeToEvents() {
    // for now this just reloads the current PB points from backend
    // eslint-disable-next-line no-unused-vars
    this.props.eventBus.take(VOUCHER_CREATED_EVENT, (event) => {
      this.setState({
        forceShowSpinner: true,
        responseStatus: 'loading'
      });
      this.unblockSpinnerOnTimeout();
      this.getPoints();
    });
  }

  render() {
    if (this.state.forceShowSpinner || this.state.responseStatus === 'loading') {
      return (
        <div className="rs-payback__section--payback-points bg-pbblue">
          <TitleContent />
          <LoadingIndicator />
        </div>
      );
    }

    if (this.state.responseStatus === 'error') {
      return (
        <div className="rs-payback__section--payback-points bg-pbblue">
          <PaybackError showImage={false} showSubtitle={false} />
        </div>
      );
    }

    return (
      <div className="rs-payback__section--payback-points bg-pbblue">
        <TitleContent />
        <TotalPoints totalPoints={formatNumber(this.state.totalPoints)} />
        <BlockedAndAvailablePoints
          blockedPoints={formatNumber(this.state.blockedPoints)}
          availablePoints={formatNumber(this.state.availablePoints)}
        />
        <div className="rs-payback__points-link-container">
          <a className="rs-payback__points-link" href={MESSAGES.tooltip.url} target="_blank">
            {MESSAGES.button}
          </a>
        </div>
      </div>
    );
  }
}

PaybackPointsBlock.propTypes = {
  // eslint-disable-next-line
  eventBus: PropTypes.object.isRequired,
  onUpdateParentView: PropTypes.func.isRequired
};

export default PaybackPointsBlock;
