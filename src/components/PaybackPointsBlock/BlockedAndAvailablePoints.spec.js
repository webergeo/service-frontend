import test from 'ava';
import React from 'react';

import render from 'react-test-renderer';

import BlockedAndAvailablePoints from './BlockedAndAvailablePoints';

test('test the default snapshot', (t) => {
  const blockedAndAvailablePoints = render.create(<BlockedAndAvailablePoints blockedPoints={'0'} availablePoints={'2500'} />).toJSON();
  t.snapshot(blockedAndAvailablePoints);
});
