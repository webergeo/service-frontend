import test from 'ava';
import React from 'react';

import render from 'react-test-renderer';

import TitleContent from './TitleContent';

test('test the default snapshot', (t) => {
  const titleContent = render.create(<TitleContent />).toJSON();
  t.snapshot(titleContent);
});
