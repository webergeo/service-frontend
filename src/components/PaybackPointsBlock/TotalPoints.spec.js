import test from 'ava';
import React from 'react';
import render from 'react-test-renderer';
import TotalPoints from './TotalPoints';

test('test the default snapshot', (t) => {
  const totalPoints = render.create(<TotalPoints totalPoints="1" />).toJSON();
  t.snapshot(totalPoints);
});
