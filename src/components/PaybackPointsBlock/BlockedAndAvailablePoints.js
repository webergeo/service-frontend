import React from 'react';
import PropTypes from 'prop-types';
import { RSInfoIcon } from '../../widgets';
import TooltipPointsInfoContent from './TooltipPointsInfoContent';
import MESSAGES from './messages';

const BlockedAndAvailablePoints = ({ blockedPoints, availablePoints }) => (
  <div className="payback-sub-container">
    <div className="payback-sub-container-box">
      <div className="rs-point-block__row">
        <div className="rs-point-block__cell--label">{MESSAGES.locked}:</div>
        <div className="rs-point-block__cell--points">
          <div className="rs-point-block__locked-wrapper">
            <div className="rs-point-block__icon rs-payback__hint">
              <p className="rs-payback__hint-paragraph">
                <RSInfoIcon height={22} width={22} styles="rs-points-infoicon" stroke={30} />
              </p>
              <div className="rs-bubble">
                <div>
                  <div className="rs-points-tooltip-content">
                    <div className="rs-bubble__triangle rs-bubble__triangle--outer" />
                    <div className="rs-bubble__triangle rs-bubble__triangle--inner" />
                    <TooltipPointsInfoContent />
                  </div>
                </div>
              </div>
            </div>
            <div className="rs-point-block__blocked-points">{blockedPoints} °P</div>
          </div>
        </div>
      </div>

      <div className="rs-point-block__row--available">
        <div className="rs-point-block__cell--label">{MESSAGES.available}:</div>
        <div className="rs-point-block__cell rs-point-block__available-points">{availablePoints} °P</div>
      </div>
    </div>
  </div>
);

BlockedAndAvailablePoints.propTypes = {
  blockedPoints: PropTypes.string.isRequired,
  availablePoints: PropTypes.string.isRequired
};

export default BlockedAndAvailablePoints;
