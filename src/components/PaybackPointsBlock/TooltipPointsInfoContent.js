import React from 'react';
import MESSAGES from './messages';

const TooltipPointsInfoContent = () => (
  <div className="rs-bubble-content--points">
    {MESSAGES.tooltip.title}
  </div>
);

export default TooltipPointsInfoContent;
