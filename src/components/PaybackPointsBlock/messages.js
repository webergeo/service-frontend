const MESSAGES = {
  title: 'PAYBACK Punktestand',
  locked: 'Gesperrte Punkte',
  available: 'Einlösbare Punkte',
  button: "Hier geht's zur Punkteübersicht",
  tooltip: {
    title: `Die gesammelten Punkte bei den PAYBACK Online Partnern werden auf Ihrem Punktekonto 
    in gesperrter Form gutgeschrieben und stehen für die Einlösung erst nach bis zu 6 Wochen zur Verfügung. 
    Diese Zeit benötigt der Betreiber des Online Shops, um den Einkauf zu bestätigen.`,
    url: 'https://www.payback.de/pb/punktekonto/id/13598/'
  }
};

export default MESSAGES;
