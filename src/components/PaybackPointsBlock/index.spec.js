import test from 'ava';
import React from 'react';
import render from 'react-test-renderer';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import nock from 'nock';
import { createBus } from 'suber';
import PaybackPointsBlock from '../PaybackPointsBlock';
import { loadConfiguration } from '../../../dev-settings/dev-config';
import authTokenMock from '../../mock-data/authTokenMock';
import paybackPointMock from '../../mock-data/paybackPointMock';

const eventBus = createBus();
const updateParentView = () => { };
const paybackPointsBlock = props => <PaybackPointsBlock {...props && { ...props }} eventBus={eventBus} onUpdateParentView={updateParentView} />;

configure({ adapter: new Adapter() });

test.before(() => {
  loadConfiguration('test');
  // mock possible HTTP requests
  nock('http://localhost:3004')
    .persist()
    .post('/api/token', {
      scope: 'read',
      grant_type: 'client_credentials'
    })
    .reply(200, authTokenMock)
    .get('/api/customers/me/paybackaccount/points')
    .reply(200, paybackPointMock);
});

test('renders the default snapshot', (t) => {
  const testPaybackPointsBlock = render.create(paybackPointsBlock()).toJSON();
  t.snapshot(testPaybackPointsBlock);
});
