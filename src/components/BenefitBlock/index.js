import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '../Button';
import './BenefitBlock.scss';
import BorderLayout from '../BorderLayout';
import BenefitItem from '../BenefitItem';
import HorizontalScroller from '../HorizontalScroller';
import MESSAGES from './messages';
import BlockHeader from '../BlockHeader';

class BenefitBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      items, numCols, withFirstHighlighted, onClick
    } = this.props;

    // Group items into rows for given number of columns
    const numRows = Math.ceil(items.length / numCols);
    const groupedItems = [];
    for (let i = 0; i < numRows; i += 1) {
      const rowItems = [];
      for (let j = 0; j < numCols; j += 1) {
        const nextIdx = (i * numCols) + j;
        if (items[nextIdx] !== undefined) {
          let newItem = Object.assign({}, items[nextIdx]);
          if (withFirstHighlighted) {
            newItem = Object.assign(newItem, nextIdx === 0
              ? { selected: true }
              : { disabled: true });
          }
          rowItems.push(newItem);
        }
      }
      groupedItems.push(rowItems);
    }

    const content = (
      <div className="rs-payback__benefitBlock-content" onClick={onClick} onKeyPress={null} role="button" tabIndex={-1}>
        {
          groupedItems.map((rowItems, idx) => (
            // eslint-disable-next-line react/no-array-index-key
            <div key={idx} className="rs-payback__benefitBlock-row">
              {rowItems.map(item => (
                <div key={item.title} className="rs-payback__benefitBlock-item">
                  <BenefitItem {...item} />
                </div>))}
            </div>
          ))
        }
      </div>
    );

    const finalContent =
        <div className="rs-payback__benefitBlock-scroller">
          <HorizontalScroller stepSizeInPixel={203} maxSteps={5}>
            {content}
          </HorizontalScroller>
        </div>
;

    const header = withFirstHighlighted
      ? (
        <div className="rs-payback__benefitBlock-header">
          <BlockHeader>{MESSAGES.title}</BlockHeader>
          <Button big label={MESSAGES.action.text} handleClick={onClick} className="rs-payback__benefitBlock-button" />
        </div>
      )
      : null;

    const footer = !withFirstHighlighted
      ? (
        <div className="rs-payback__benefitBlock-footer">
          <Button big label={MESSAGES.action.text} handleClick={onClick} className="rs-payback__benefitBlock-button" />
        </div>
      )
      : null;

    return (
      <BorderLayout content={finalContent} footer={footer} header={header} />
    );
  }
}

BenefitBlock.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape(BenefitItem.propTypes)),
  numCols: PropTypes.number,
  withFirstHighlighted: PropTypes.bool,
  onClick: PropTypes.func.isRequired
};

BenefitBlock.defaultProps = {
  items: MESSAGES.items,
  numCols: 3,
  withFirstHighlighted: false
};

export default BenefitBlock;
