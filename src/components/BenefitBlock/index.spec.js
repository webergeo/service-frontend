import test from 'ava';
import React from 'react';

import render from 'react-test-renderer';

import BenefitBlock from '.';
import { noop } from '../../utils/commons';

test('renders the default snapshot', (t) => {
  const modalTree = render.create(<BenefitBlock onClick={noop} />).toJSON();
  t.snapshot(modalTree);
});

test('renders the snapshot with first highlighted', (t) => {
  const modalTree = render.create(<BenefitBlock withFirstHighlighted onClick={noop} />).toJSON();
  t.snapshot(modalTree);
});
