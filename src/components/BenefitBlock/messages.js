/* eslint-disable global-require */
const MESSAGES = {
  title: 'Sie nutzen erst 1 von 6 Vorteilen',
  action: {
    text: 'Jetzt alle Vorteile freischalten',
    url: ''
  },
  items: [
    {
      title: 'Online keinen Punkt verpassen',
      content: 'Bei jeder Bestellung beim REWE Lieferservice sammeln Sie automatisch Punkte.',
      image: require('../../assets/images/payback-2013-0816-pointee.png')
    },
    {
      title: 'Punktestand abfragen',
      content: 'Ihr aktueller PAYBACK Punktestand ist jederzeit für Sie verfügbar.',
      image: require('../../assets/images/payback-2013-0313-pointee.png')
    },
    {
      title: 'eCoupons nutzen',
      content: 'Mit Ihren eCoupons sammeln Sie im REWE Markt oder beim REWE Lieferservice Extra-Punkte.',
      image: require('../../assets/images/payback-2014-1075-pointee.png'),
      imageStyle: { maxWidth: '100%', margin: '16px 0px 0px 0px' }
    },
    {
      title: 'REWE eBon',
      content: 'Auf Wunsch erhalten Sie Ihren REWE Kassenbon einfach per E-Mail.',
      image: require('../../assets/images/pointee-ebon.png')
    },
    {
      title: 'REWE Service-Punkt',
      content: 'Mit attraktiven PAYBACK Aktionen auch immer im Markt punkten.',
      image: require('../../assets/images/rewe-service-punkt-pointees-w400.png'),
      imageStyle: { width: '152px', height: '100px' }
    },
    {
      title: 'REWE Guthaben aufladen',
      content: 'PAYBACK Punkte können Sie bequem online in Guthaben umwandeln und Ihren nächsten Einkauf damit bezahlen.',
      image: require('../../assets/images/pointee-rewe-coin-flip.png'),
      imageStyle: { width: '101px', height: '116px' }
    }
  ]
};

export default MESSAGES;
