Without first highlighted flag
```
<BenefitBlock />
```

With first highlighted flag
```
<BenefitBlock withFirstHighlighted />
```
