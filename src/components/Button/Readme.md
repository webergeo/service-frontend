Big buttons

```
<div style={{display: 'flex', justifyContent: 'space-around'}}>
  <Button big label="Default"/>
  <Button big color="red" label="Red"/>
  <Button big color="pampas" label="Pampas"/>
  <Button big color="grey" label="Grey"/>
  <Button big color="white" label="White"/>
</div>
```

Big buttons (as <a>)

```
<Button big label="Follow Anchor" href="shop.rewe.de"/>
```

Streched big button

```
<Button big stretched label="Follow Anchor" type="reset"/>
```

Button as link

```
<Button label="So funktioniert PAYBACK bei REWE" link href="shop.rewe.de"/>
```
