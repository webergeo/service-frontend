import test from 'ava';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import Button from '.';

const PAYBACK_BUTTON_CLASS = 'rs-payback__button';
const PAYBACK_BUTTON_SIZE_CLASS = 'rs-payback__button--large';
const PAYBACK_BUTTON_COLOR_CLASS_PREFIX = 'rs-payback__button--';

configure({ adapter: new Adapter() });

const buttonComponent = props => (
  <Button label="i am an awesome label" {...props && { ...props }} />
);

test('renders the default Button', (t) => {
  const buttonTree = render.create(buttonComponent()).toJSON();
  t.snapshot(buttonTree);
  const wrapper = shallow(buttonComponent());
  t.true(wrapper.hasClass(PAYBACK_BUTTON_CLASS));
});

test('should have a default <button> type of Component', (t) => {
  const wrapper = shallow(buttonComponent());
  t.is(wrapper.find('button').length, 1);
});

test('should have a default green color prop', (t) => {
  const wrapper = shallow(buttonComponent());
  t.true(wrapper.hasClass(`${PAYBACK_BUTTON_COLOR_CLASS_PREFIX}green`));
});

test('render big size Button Component', (t) => {
  const wrapper = shallow(buttonComponent({ big: true }));
  t.true(wrapper.hasClass(PAYBACK_BUTTON_SIZE_CLASS));
});

test('should have <a> tag type of Component', (t) => {
  const wrapper = shallow(buttonComponent({ href: 'https://www.some-domain.de/uri' }));
  t.is(wrapper.find('a').length, 1);
});

test('renders the disabled Button', (t) => {
  const wrapper = mount(buttonComponent({ disabled: true }));
  t.is(wrapper.props().disabled, true);
});

test('should have a tooltip for Button', (t) => {
  const wrapper = mount(buttonComponent({ title: 'I am a title' }));
  t.is(wrapper.props().title, 'I am a title');
});
