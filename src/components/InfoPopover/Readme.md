Default closed
```
<div style={{ display: 'flex', justifyContent: 'center' }}>
  <InfoPopover>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
       dolore magna aliquyam erat, sed diam voluptua.
  </InfoPopover>
</div>
```

Opened below and without auto positioning
```
<div style={{ display: 'flex', justifyContent: 'center', height: '180px' }}>
  <InfoPopover defaultOpened={true} autoPositionDisabled>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
       dolore magna aliquyam erat, sed diam voluptua.
  </InfoPopover>
</div>
```

Opened above and without auto positioning
```
<div style={{ display: 'flex', justifyContent: 'center', height: '180px', alignItems: 'flex-end' }}>
  <InfoPopover defaultOpened defaultShowAbove autoPositionDisabled>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
       dolore magna aliquyam erat, sed diam voluptua.
  </InfoPopover>
</div>
```