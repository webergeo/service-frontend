import test from 'ava';
import React from 'react';

import render from 'react-test-renderer';

import InfoPopover from '.';

test('renders the default snapshot', (t) => {
  const modalTree = render.create(<InfoPopover>Test</InfoPopover>).toJSON();
  t.snapshot(modalTree);
});

test('renders with content opened below', (t) => {
  const modalTree = render.create(<InfoPopover defaultOpened>Test</InfoPopover>).toJSON();
  t.snapshot(modalTree);
});

test('renders with content opened above', (t) => {
  const modalTree = render.create(<InfoPopover defaultOpened defaultShowAbove>Test</InfoPopover>).toJSON();
  t.snapshot(modalTree);
});
