import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './InfoPopover.scss';
import { debounce } from '../../utils/commons';

const minEdgeDistance = 16;
const defaultBoxLeft = -219;

class InfoPopover extends Component {
  constructor(props) {
    super(props);
    this.state = {
      opened: props.defaultOpened,
      showAbove: props.defaultShowAbove,
      boxStyle: { left: defaultBoxLeft }
    };
    this.togglePopover = this.togglePopover.bind(this);
    this.closePopover = this.closePopover.bind(this);
    this.setContentBoxRef = this.setContentBoxRef.bind(this);
    this.updateVerticalBoxPosition = this.updateVerticalBoxPosition.bind(this);
    this.updateHorizontalBoxPosition = this.updateHorizontalBoxPosition.bind(this);
  }

  componentDidMount() {
    this.debouncedScrollHandler = debounce(this.updateVerticalBoxPosition, 250);
    if (!this.props.autoPositionDisabled) {
      window.addEventListener('scroll', this.debouncedScrollHandler);
    }
  }

  componentWillUnmount() {
    if (!this.props.autoPositionDisabled) {
      window.removeEventListener('scroll', this.debouncedScrollHandler);
    }
  }

  setContentBoxRef(ref) {
    this.contentBox = ref;
  }

  updateVerticalBoxPosition() {
    const clientRect = this.contentBox.getBoundingClientRect();

    if (clientRect.bottom > window.innerHeight - minEdgeDistance) {
      this.setState({ showAbove: true });
    } else if (clientRect.top < minEdgeDistance) {
      this.setState({ showAbove: false });
    }
  }

  updateHorizontalBoxPosition() {
    const clientRect = this.contentBox.getBoundingClientRect();

    if (window.innerWidth < 768) {
      if (clientRect.left < 16) {
        this.setState({
          boxStyle: { left: (defaultBoxLeft + 16) - clientRect.left }
        });
      }
    } else {
      this.setState({
        boxStyle: { left: defaultBoxLeft }
      });
    }
  }

  togglePopover() {
    if (!this.state.opened && !this.props.autoPositionDisabled) {
      this.updateVerticalBoxPosition();
    }
    this.updateHorizontalBoxPosition();

    this.setState(prevState => ({
      opened: !prevState.opened
    }));
  }

  closePopover() {
    this.setState({ opened: false });
  }

  render() {
    const { opened, showAbove, boxStyle } = this.state;

    return (
      <div className={cn('rs-payback__infoPopover', { 'rs-payback__infoPopover--opened': opened })}>
        <button className="rs-payback__infoPopover-button rs-payback__infoPopover-button-info" onClick={this.togglePopover}>
          {!opened &&
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
            <path
              fill="#1D97E2"
              fillRule="evenodd"
              d="M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0zm-1 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-7.938-2.984c-.616 0-1.108-.439-1.108-1.001
                0-.562.492-1.015 1.123-1.015.63 0 1.123.439 1.123 1.015 0 .562-.492 1.001-1.138 1.001zM11.692 17c-.261
                0-.477-.192-.477-.425v-6.091c0-.233.216-.425.477-.425h.785c.261 0 .477.192.477.425v6.09c0 .234-.216.426-.477.426h-.785z"
            />
          </svg>
          }
          {opened &&
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
            <path
              fill="#1D97E2"
              fillRule="evenodd"
              d="M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0zm-8.938-2.984c.646 0 1.138-.439 1.138-1.001C13.2 7.439 12.708 7 12.077 7c-.63 0-1.123.453-1.123
                1.015 0 .562.492 1.001 1.108 1.001zM11.692 17h.785c.261 0 .477-.192.477-.425v-6.091c0-.233-.216-.425-.477-.425h-.785c-.261
                0-.477.192-.477.425v6.09c0 .234.216.426.477.426z"
            />
          </svg>
          }
        </button>
        <div
          className={cn(
            'rs-payback__infoPopover-popover',
            'rs-payback__infoPopover-content-box',
            `rs-payback__infoPopover-content-box-${showAbove ? 'above' : 'below'}`
          )}
          ref={this.setContentBoxRef}
          style={boxStyle}
        >
          {this.props.children}
          <button className="rs-payback__infoPopover-button rs-payback__infoPopover-button-close" onClick={this.closePopover}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <path
                fill="#1D97E2"
                fillRule="nonzero"
                d="M6.455 18.75a.852.852 0 1 1-1.205-1.205L10.795 12 5.25 6.455A.852.852 0 0 1 6.455 5.25L12 10.795l5.545-5.545a.852.852 0 1 1 1.205
                  1.205L13.205 12l5.545 5.545a.852.852 0 1 1-1.205 1.205L12 13.205 6.455 18.75z"
              />
            </svg>
          </button>
        </div>
        <div
          className={cn(
            'rs-payback__infoPopover-popover',
            'rs-payback__infoPopover-arrow',
            `rs-payback__infoPopover-arrow-${showAbove ? 'down' : 'up'}`
          )}
        />
      </div>
    );
  }
}

InfoPopover.propTypes = {
  children: PropTypes.node.isRequired,
  defaultShowAbove: PropTypes.bool,
  defaultOpened: PropTypes.bool,
  autoPositionDisabled: PropTypes.bool
};

InfoPopover.defaultProps = {
  defaultShowAbove: false,
  defaultOpened: false,
  autoPositionDisabled: false
};

export default InfoPopover;
