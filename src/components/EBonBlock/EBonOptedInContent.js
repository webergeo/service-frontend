import React from 'react';

import {EBON_TERMS_CONDITIONS_URL, EBON_Q_AND_A_URL, EBON_PRIVACY_URL, EBON_SHOP_URL} from '../../utils/constants';

const EBonOptedInContent = () => (
  <div className="rs-payback-accordion__content">
    <p>
      Einfach die PAYBACK Karte beim Kauf an der Kasse vorlegen und der digitale Kassenbon kommt per E-Mail an die im
      REWE Kundenkonto hinterlegte E-Mail-Adresse. Diese E-Mail-Adresse kann im Kundenkonto unter »Meine Daten«
      bearbeitet werden. Des Weiteren finden Sie Ihre eBons als PDFs{' '}
      <a href={EBON_SHOP_URL} target="_blank" rel="noopener noreferrer">hier</a> im Kundenkonto.
    </p>
    <br/>
    <p>
      Weitere Informationen finden Sie unter {' '}
      <span>
        <a href={EBON_Q_AND_A_URL} target="_blank" rel="noopener noreferrer">Fragen und Antworten</a>.
      </span>
    </p>
    <br/>
    <p>
      Bitte beachten Sie: Seit dem 02.12.2019 gelten neue Teilnahmebedingungen sowie Datenschutzhinweise für den REWE
      eBon. Mit dieser Aktualisierung wird der zusätzliche automatische Papierausdruck des Kassenbons im REWE Markt
      bei Nutzung des eBons eingestellt. Des Weiteren können Sie Ihre eBons ab diesem Zeitpunkt als PDFs im Kundenkonto
      {' '}
        <a href={EBON_SHOP_URL} target="_blank" rel="noopener noreferrer">hier</a>
      {' '} aufrufen. Sie können sich jederzeit hier vom eBon abmelden.
      Hier finden Sie die seit 02.12.2019 geltenden {' '}
      <span>
        <a href={EBON_TERMS_CONDITIONS_URL} target="_blank" rel="noopener noreferrer" title="Teilnahmebedingungen">
          Teilnahmebedingungen
        </a>
      </span>
      {' '} und {' '}
      <a href={EBON_PRIVACY_URL} target="_blank" rel="noopener noreferrer" title="Datenschutzhinweise">
        Datenschutzhinweise
      </a>.
    </p>
  </div>);

EBonOptedInContent.propTypes = {};

export default EBonOptedInContent;
