import test from 'ava';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import render from 'react-test-renderer';
import MESSAGES from './messages';

import { LoadingSpinner, ContainerBlock, OptedInBlock, OptedOutBlock } from '../../widgets';
import Modal from '../Modal';
import EBonBlock from '../EBonBlock';

// import { EBONBLOCK_OPTEDOUT_MESSAGE, EBONBLOCK_OPTEDOUT_MESSAGE_CONTENT } from '../../utils/constants';

const PAYBACK_INTROBLOCK_PRIMARY_CLASS = 'rs-qa-payback__section--ebon';

configure({ adapter: new Adapter() });

const eBonBlockComponent = (props) => {
  const accountInfo = {
    eBonOptInExists: true,
    ...props
  };
  const { deleteBonCheckBox } = props;
  // dummy view update handler
  const onUpdateView = () => {};
  return <EBonBlock accountInfo={accountInfo} onUpdateParentView={onUpdateView} deleteBonCheckBox={deleteBonCheckBox} />;
};

test('renders EBonBlock without EBonOptInFlag and has correct class and subcomponent', (t) => {
  const eBonBlockTree = render.create(eBonBlockComponent({ eBonOptInExists: false })).toJSON();
  t.snapshot(eBonBlockTree);
  const wrapper = shallow(eBonBlockComponent({ eBonOptInExists: false }));
  t.is(wrapper.instance().props.accountInfo.eBonOptInExists, false);
  t.true(wrapper.hasClass(PAYBACK_INTROBLOCK_PRIMARY_CLASS));
});

test('renders initial EbonBlock with default state', (t) => {
  const wrapper = shallow(eBonBlockComponent({ eBonOptInExists: false }));
  const initialState = {
    isResponsePending: false,
    openEbonOptInStatusModal: false,
    openEbonOptOutModal: false,
    openEbonOptOutStatusModal: false,
    ebonOptErrorMessage: '',
    deleteEbon: false,
    ebonOptStatusType: 'success'
  };
  t.deepEqual(wrapper.state(), initialState);
  t.is(wrapper.state().openEbonOptInStatusModal, initialState.openEbonOptInStatusModal);
  t.is(wrapper.state().ebonOptErrorMessage, initialState.ebonOptErrorMessage);
  t.deepEqual(wrapper.state().ebonAccountBindingStatus, initialState.ebonAccountBindingStatus);
});

test('renders EBonBlock with ContainerBlock subcomponent', (t) => {
  const wrapper = shallow(eBonBlockComponent({ eBonOptInExists: false }));
  t.is(wrapper.find(ContainerBlock).length, 1);
});

test('renders EBonBlock with OptedInBlock subcomponent', (t) => {
  const ebonBlockTree = render.create(eBonBlockComponent({ eBonOptInExists: true })).toJSON();
  t.snapshot(ebonBlockTree);
  const wrapper = shallow(eBonBlockComponent({ eBonOptInExists: true }));
  t.is(wrapper.find(OptedInBlock).length, 1);
});

test('renders default EBonBlock with OptedOutBlock subcomponent', (t) => {
  const wrapper = shallow(eBonBlockComponent({ eBonOptInExists: false }));
  t.is(wrapper.find(OptedOutBlock).length, 1);
});

test('When toggled openEbonOptInStatusModal state and ebonOptStatusType is of type *success*, shows the success modal window', (t) => {
  const wrapper = shallow(eBonBlockComponent({ eBonOptInExists: false }));
  wrapper.setState({ openEbonOptInStatusModal: true, ebonOptStatusType: 'success' });
  t.is(wrapper.state().openEbonOptInStatusModal, true);

  const modalWindow = wrapper.find(Modal);
  t.is(modalWindow.length, 1);
  t.is(modalWindow.props().type, 'success');
});

test('When toggled openEbonOptInStatusModal state and ebonOptStatusType is of type *error*, shows the error modal window', (t) => {
  const wrapper = shallow(eBonBlockComponent({ eBonOptInExists: false }));
  wrapper.setState({ openEbonOptInStatusModal: true, ebonOptStatusType: 'error' });
  t.is(wrapper.state().openEbonOptInStatusModal, true);

  const modalWindow = wrapper.find(Modal);
  t.is(modalWindow.length, 1);
  t.is(modalWindow.props().type, 'error');
});

test('should show LoadingSpinner when response is still pending', (t) => {
  const wrapper = shallow(eBonBlockComponent({ eBonOptInExists: true }));
  wrapper.setState({ isResponsePending: true });
  const loadingSpinner = wrapper.find(LoadingSpinner);
  t.is(loadingSpinner.length, 1);
});

test('should show eBon OptOut Modal when openEbonOptOutModal state is true', (t) => {
  const wrapper = shallow(eBonBlockComponent({ eBonOptInExists: true }));
  wrapper.setState({ openEbonOptOutModal: true });
  const modalWindow = wrapper.find(Modal);
  t.is(modalWindow.length, 1);
  t.is(modalWindow.props().title, MESSAGES.optedout.message);
  t.is(modalWindow.props().content, MESSAGES.optedout.content);
  t.is(modalWindow.props().type, 'warning');
});

test.only('should show eBon OptOut Modal witjh ebon delete checkbox when openEbonOptOutModal state is true deleteEbonCheckbox is true', (t) => {
  const wrapper = shallow(eBonBlockComponent({ eBonOptInExists: true, deleteBonCheckBox: true }));
  wrapper.setState({ openEbonOptOutModal: true });
  const modalWindow = wrapper.find(Modal);
  const modelTree = modalWindow.render()
  t.snapshot(modelTree);
  t.is(modalWindow.length, 1);
  t.is(modalWindow.props().title, MESSAGES.optedoutdeloption.message);
  t.is(modalWindow.props().type, 'warning');
});

test('When toggled openEbonOptOutStatusModal state and ebonOptStatusType is of type *success*, shows the error modal window', (t) => {
  const wrapper = shallow(eBonBlockComponent({ eBonOptInExists: false }));
  wrapper.setState({ openEbonOptOutStatusModal: true, ebonOptStatusType: 'success' });
  t.is(wrapper.state().openEbonOptOutStatusModal, true);
  const modalWindow = wrapper.find(Modal);
  t.is(modalWindow.length, 1);
  t.is(modalWindow.props().type, 'success');
});

test('When toggled openEbonOptOutStatusModal state and ebonOptStatusType is of type *error*, shows the error modal window', (t) => {
  const wrapper = shallow(eBonBlockComponent({ eBonOptInExists: false }));
  wrapper.setState({ openEbonOptOutStatusModal: true, ebonOptStatusType: 'error' });
  t.is(wrapper.state().openEbonOptOutStatusModal, true);
  const modalWindow = wrapper.find(Modal);
  t.is(modalWindow.length, 1);
  t.is(modalWindow.props().type, 'error');
});
