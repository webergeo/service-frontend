import test from 'ava';
import React from 'react';

import render from 'react-test-renderer';

import EBonOptedOutContent from './EBonOptedOutContent';

test('renders the default snapshot', (t) => {
  const modalTree = render.create(<EBonOptedOutContent />).toJSON();
  t.snapshot(modalTree);
});
