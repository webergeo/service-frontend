import test from 'ava';
import React from 'react';

import render from 'react-test-renderer';

import EBonOptedInContent from './EBonOptedInContent';

test('renders the default snapshot', (t) => {
  const modalTree = render.create(<EBonOptedInContent paybackNumber="*** *** 7890" />).toJSON();
  t.snapshot(modalTree);
});
