import PropTypes from 'prop-types';
import React, { Component } from 'react';

import { LoadingSpinner, ContainerBlock, CheckboxGroup, IconTickCheckbox, OptedInBlock, OptedOutBlock } from '../../widgets';
import Modal from '../Modal';
import EBonOptedInContent from './EBonOptedInContent';
import EBonOptedOutContent from './EBonOptedOutContent';

import { ebonOptToggle } from '../../api/Accounts';
import { deleteAllReceipts } from '../../api/Ebons';
import { parsePaybackErrorMessage } from '../../utils/apiResponseFormatter';
import { hasProp } from '../../utils/commons';

import { HTTP_FORBIDDEN } from '../../utils/constants';
import MESSAGES from './messages';
import './EBonBlock.scss';
import logger from '../../utils/ducLogger';

const eBonBlockClassName = 'rs-payback__section--ebon rs-qa-payback__section--ebon';

class EBonBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isResponsePending: false,
      openEbonOptInStatusModal: false,
      openEbonOptOutModal: false,
      openEbonOptOutStatusModal: false,
      deleteEbon: false,
      ebonOptStatusType: 'success',
      ebonOptErrorMessage: ''
    };
    this.handleEbonOptOut = this.handleEbonOptOut.bind(this);
    this.handleEbonOptOutModalSubmit = this.handleEbonOptOutModalSubmit.bind(this);
    this.handleEbonOptInAvailablityCheck = this.handleEbonOptInAvailablityCheck.bind(this);
    this.handleEbonBlockModalClose = this.handleEbonBlockModalClose.bind(this);
    this.handleDeleteEbonsOption = this.handleDeleteEbonsOption.bind(this);
  }

  async getEbonOptInAvailability() {
    try {
      await this.toggleEbonOptStatus({ optIn: true });
      this.setState({ openEbonOptInStatusModal: true });
    } catch (error) {
      logger.error(`Failed to get ebon opt availability, error: ${error.message}`);
      this.setState({
        openEbonOptInStatusModal: true,
        ebonOptErrorMessage: parsePaybackErrorMessage(error),
        ebonOptStatusType: 'error'
      });
    }
  }

  handleEbonOptOut() {
    this.setState({ openEbonOptOutModal: true });
  }

  activateLoadingSpinner(toggleFlag) {
    this.setState(() => ({
      isResponsePending: toggleFlag
    }));
  }

  handleEbonOptOutModalSubmit(e) {
    e.preventDefault();
    this.setState({ openEbonOptOutModal: false });
    this.toggleEbonOptStatus({ optIn: false });
  }

  /**
   * Performs the Ebon optIn/OptOut operation based on optIn flag
   * @param {Object} optIn - optIn flag true/false
   */
  async toggleEbonOptStatus({ optIn }) {
    try {
      this.activateLoadingSpinner(true);
      const { deleteEbon } = this.state;
      if (deleteEbon) {
        this.setState({ deleteEbon: false });
        const deleteResponse = await deleteAllReceipts();
        if (deleteResponse.status >= 400) {
          if (deleteResponse.status === HTTP_FORBIDDEN) {
            await this.props.onUpdateParentView();
            this.activateLoadingSpinner(false);
            return;
          }
          const errorMessage = await deleteResponse.json();
          this.setState(() => ({
            [optIn === true ? 'openEbonOptInStatusModal' : 'openEbonOptOutStatusModal']: true,
            ebonOptErrorMessage: parsePaybackErrorMessage(errorMessage),
            ebonOptStatusType: 'error'
          }));
          this.activateLoadingSpinner(false);
          return;
        }
      }

      const payload = { optIn };
      const response = await ebonOptToggle({ payload });
      if (response.status >= 400) {
        if (response.status === HTTP_FORBIDDEN) {
          await this.props.onUpdateParentView();
          this.activateLoadingSpinner(false);
          return;
        }

        const errorMessage = await response.json();
        this.setState(() => ({
          [optIn === true ? 'openEbonOptInStatusModal' : 'openEbonOptOutStatusModal']: true,
          ebonOptErrorMessage: parsePaybackErrorMessage(errorMessage),
          ebonOptStatusType: 'error'
        }));
      } else {
        this.setState({
          [optIn === true ? 'openEbonOptInStatusModal' : 'openEbonOptOutStatusModal']: true,
          ebonOptStatusType: 'success'
        });

        await this.props.onUpdateParentView();
      }
      // Finally disable the spinner
      this.activateLoadingSpinner(false);
    } catch (error) {
      logger.error(`Failed to toggle ebon opt status, error: ${error.message}`);
      this.activateLoadingSpinner(false);
      this.setState({
        [optIn === true ? 'openEbonOptInStatusModal' : 'openEbonOptOutStatusModal']: true,
        ebonOptErrorMessage: parsePaybackErrorMessage(error),
        ebonOptStatusType: 'error'
      });
    }
  }

  handleEbonOptInAvailablityCheck(e) {
    e.preventDefault();
    this.getEbonOptInAvailability();
  }

  handleEbonBlockModalClose(e, stateName) {
    e.preventDefault();
    this.setState({ deleteEbon: false });
    if (!hasProp(this.state, stateName)) {
      return;
    }
    this.setState(() => ({ [stateName]: false }));
  }

  renderEBonOptedOutBlock() {
    const accordionProperties = {
      alternateTitle: 'Weniger Informationen',
      title: 'Mehr Informationen'
    };

    return (
      <OptedOutBlock
        accordionProperties={accordionProperties}
        optedOutButtonLabel={MESSAGES.content.optedout.label}
        handleOptedOutButtonClick={this.handleEbonOptInAvailablityCheck}
        blockDescription={MESSAGES.content.optedout.description}
      >
        <EBonOptedOutContent />
      </OptedOutBlock>
    );
  }

  renderEBonOptedInBlock() {
    const { eBonOptInExists } = this.props.accountInfo;
    const accordionProperties = {
      alternateTitle: 'Weniger Informationen',
      title: 'Mehr Informationen'
    };
    const checkboxGroup = (
      <CheckboxGroup
        labelText={MESSAGES.optedin.status.label}
        isChecked={eBonOptInExists}
        checkboxId="rs-ebon-status"
        handleCheckboxClick={this.handleEbonOptOut}
      />
    );
    return (
      <OptedInBlock accordionProperties={accordionProperties} checkboxGroup={checkboxGroup}>
        <EBonOptedInContent />
      </OptedInBlock>
    );
  }

  renderEbonOptInStatusModal() {
    const { ebonOptStatusType, ebonOptErrorMessage } = this.state;
    const modalTitle = ebonOptStatusType === 'error' ? MESSAGES.generic_error_fallback_title : MESSAGES.optedin.success.message;
    const modalContent = ebonOptStatusType === 'error' ? ebonOptErrorMessage : '';
    return (
      <Modal
        title={modalTitle}
        content={modalContent}
        type={ebonOptStatusType}
        handleClose={e => this.handleEbonBlockModalClose(e, 'openEbonOptInStatusModal')}
        handleSubmit={e => this.handleEbonBlockModalClose(e, 'openEbonOptInStatusModal')}
      />
    );
  }

  renderEbonOptOutModal() {
    const { deleteBonCheckBox } = this.props;
    const title = (deleteBonCheckBox) ? MESSAGES.optedoutdeloption.message : MESSAGES.optedout.message;
    const content = (deleteBonCheckBox) ? this.renderEbonReceiptDeleteOption() : MESSAGES.optedout.content;
    const footerConfig = {
      confirmAction: { label: MESSAGES.optedoutdeloption.confirmActionLabel },
      dismissAction: { label: MESSAGES.optedoutdeloption.dismissActionLabel }
    };
    return (
      <Modal
        title={title}
        content={content}
        type="warning"
        handleClose={e => this.handleEbonBlockModalClose(e, 'openEbonOptOutModal')}
        handleCancel={e => this.handleEbonBlockModalClose(e, 'openEbonOptOutModal')}
        handleSubmit={this.handleEbonOptOutModalSubmit}
        footerConfig={footerConfig}
      />
    );
  }

  renderEbonReceiptDeleteOption() {
    const { deleteEbon } = this.state;
    return (
      <div>
        <div>
          {MESSAGES.optedoutdeloption.content}
          <IconTickCheckbox
            checkboxId="rs-delete-ebons"
            isChecked={deleteEbon}
            labelText={MESSAGES.optedoutdeloption.deleteEbonLabelText}
            handleCheckboxClick={this.handleDeleteEbonsOption}
          />
        </div>
      </div>
    );
  }

  handleDeleteEbonsOption(e) {
    this.setState({ deleteEbon: e.checked });
  }

  renderEbonOptOutStatusModal() {
    const { ebonOptStatusType, ebonOptErrorMessage } = this.state;
    const modalTitle = ebonOptStatusType === 'error' ? MESSAGES.generic_error_fallback_title : MESSAGES.optedout.success.message;
    const modalContent = ebonOptStatusType === 'error' ? ebonOptErrorMessage : '';
    return (
      <Modal
        title={modalTitle}
        content={modalContent}
        type={ebonOptStatusType}
        handleClose={e => this.handleEbonBlockModalClose(e, 'openEbonOptOutStatusModal')}
        handleSubmit={e => this.handleEbonBlockModalClose(e, 'openEbonOptOutStatusModal')}
      />
    );
  }

  render() {
    const { isResponsePending, openEbonOptInStatusModal, openEbonOptOutModal, openEbonOptOutStatusModal } = this.state;
    const { eBonOptInExists } = this.props.accountInfo;
    return (
      <div className={eBonBlockClassName}>
        {isResponsePending ? <LoadingSpinner/> : undefined}
        <ContainerBlock title={MESSAGES.content.headline} subtitle={MESSAGES.content.subline}>
          <div className="rs-payback__section-content">{eBonOptInExists ? this.renderEBonOptedInBlock() : this.renderEBonOptedOutBlock()}</div>
        </ContainerBlock>
        {openEbonOptInStatusModal && this.renderEbonOptInStatusModal()}
        {openEbonOptOutModal && this.renderEbonOptOutModal()}
        {openEbonOptOutStatusModal && this.renderEbonOptOutStatusModal()}
      </div>
    );
  }
}

EBonBlock.propTypes = {
  accountInfo: PropTypes.shape({
    eBonOptInExists: PropTypes.bool
  }).isRequired,
  onUpdateParentView: PropTypes.func.isRequired,
  deleteBonCheckBox: PropTypes.bool.isRequired
};

export default EBonBlock;
