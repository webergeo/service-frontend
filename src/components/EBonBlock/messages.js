const MESSAGES = {
  generic_error_fallback_title: 'Es sind Fehler aufgetreten.',
  content: {
    headline: 'Der REWE eBon',
    subline: 'Ganz schön praktisch: Ihre Kassenbons gibt es jetzt auch per E-Mail.',
    optedout: {
      description: 'Klicken Sie auf "Freischalten", um Ihre Kassenbons zukünftig per E-Mail zu erhalten.',
      label: 'Freischalten'
    },
    information: {
      more: 'Mehr Informationen',
      less: 'Weniger Informationen'
    }
  },
  optedin: {
    status: {
      label: 'Ja, ich möchte meine Kassenbons zukünftig per E-Mail erhalten.'
    },
    definitionlist: {
      title: 'PAYBACK Kundennummer für REWE eBon:'
    },
    success: {
      message: 'Ihre Anmeldung für den REWE eBon war erfolgreich!'
    }
  },
  optedout: {
    message: 'Möchten Sie sich vom REWE eBon abmelden?',
    content: 'Wenn Sie sich vom REWE eBon abmelden, erhalten Sie Ihren REWE Kassenbon nicht länger per E-Mail.',
    success: {
      message: 'Ihre Abmeldung vom REWE eBon war erfolgreich!'
    },
    paragraph: `Wenn Sie sich für den REWE eBon anmelden, erhalten Sie alle Kassenbons auch digital. 
    Damit haben Sie alles an einem Ort, behalten den Überblick über Ihre Einkäufe und 
    können jeden Kassenbon schnell wiederfinden, wenn Sie ihn benötigen.`
  },
  optedoutdeloption: {
    message: 'Vom REWE eBon abmelden',
    content: 'Möchten Sie Ihre eBons für diese PAYBACK Karte nach der eBon Abmeldung unwiderruflich löschen?',
    deleteEbonLabelText: 'Ja, alle eBons löschen',
    confirmActionLabel: 'Jetzt abmelden',
    dismissActionLabel: 'Abbrechen',
  }
};

export default MESSAGES;
