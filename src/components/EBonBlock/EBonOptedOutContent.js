import React from 'react';

import {EBON_PRIVACY_URL, EBON_Q_AND_A_URL, EBON_TERMS_CONDITIONS_URL, EBON_SHOP_URL} from '../../utils/constants';
import MESSAGES from './messages';

const EBonOptedOutContent = () => (
  <div className="rs-payback-accordion__content">
    <p>
      {MESSAGES.optedout.paragraph}
      <br/>So funktioniert’s:
    </p>
    <ol className="rs-payback-advantages-list">
      <li className="rs-payback-advantages-list__listelement">
        Melden Sie sich hier mit Klick auf „Freischalten“ zum eBon an.
      </li>
      <li className="rs-payback-advantages-list__listelement">
        Scannen Sie Ihre PAYBACK Karte wie gewohnt an der Kasse in Ihrem REWE Markt.
      </li>
      <li className="rs-payback-advantages-list__listelement">
        Jetzt erhalten Sie Ihren Kassenbon via E-Mail. Die E-Mail-Adresse entspricht der Ihres REWE Kundenkontos.
        Des Weiteren finden Sie Ihre eBons als PDFs {' '}
        <span>
          <a href={EBON_SHOP_URL} target="_blank" rel="noopener noreferrer">hier</a>
        </span> im Kundenkonto.
      </li>
    </ol>
    <p>
      Bitte beachten Sie: Seit dem 02.12.2019 gelten neue Teilnahmebedingungen sowie Datenschutzhinweise
      für den REWE eBon, denen Sie mit der Anmeldung zustimmen. Mit dieser Aktualisierung wird der zusätzliche
      automatische Papierausdruck des Kassenbons im REWE Markt bei Nutzung des eBons eingestellt. Des Weiteren
      können Sie Ihre eBons ab diesem Zeitpunkt als PDFs im Kundenkonto {' '}
      <span>
        <a href={EBON_SHOP_URL} target="_blank" rel="noopener noreferrer">hier</a>
      </span> {' '}
      aufrufen. Sie können sich jederzeit im Kundenkonto vom eBon abmelden. Hier finden Sie die seit 02.12.2019 geltenden {' '}
      <a href={EBON_TERMS_CONDITIONS_URL} target="_blank" rel="noopener noreferrer" title="Teilnahmebedingungen">Teilnahmebedingungen</a>
      {' '} und {' '}
      <span>
        <a href={EBON_PRIVACY_URL} target="_blank" rel="noopener noreferrer" title="Datenschutzhinweise">Datenschutzhinweise</a>
      </span>.
    </p>
    <br/>
    <p>
      Bei weiteren Fragen empfehlen wir Ihnen einen Blick in den Bereich {' '}
      <a href={EBON_Q_AND_A_URL} target="_blank" rel="noopener noreferrer" title="Fragen und Antworten">Fragen und Antworten</a>.
    </p>
  </div>
);

export default EBonOptedOutContent;
