import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './TriangleButton.scss';

const TriangleButton = ({ direction, onClick, className }) => (
  <button className={cn('rs-payback__triangleButton', className)} onClick={onClick}>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      className={cn('rs-payback__triangleButton-img', `rs-payback__triangleButton--${direction}`)}
    >
      <path
        fillRule="evenodd"
        d="M9 17.557V6.443C9 6.2 9.192 6 9.428 6c.115 0 .224.047.305.131l5.019 5.243c.33.345.33.902 0 1.248l-5.02 5.247a.418.418 0 0
         1-.605.003.451.451 0 0 1-.127-.315z"
      />
    </svg>
  </button>
);

TriangleButton.propTypes = {
  direction: PropTypes.oneOf(['left', 'right']),
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string
};

TriangleButton.defaultProps = {
  direction: 'left',
  className: null
};
export default TriangleButton;
