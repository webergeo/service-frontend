import test from 'ava';
import React from 'react';
import render from 'react-test-renderer';

import TriangleButton from '.';
import { noop } from '../../utils/commons';

test('renders the default snapshot', (t) => {
  const testTriangleButton = render.create(
    <TriangleButton onClick={noop} />
  ).toJSON();
  t.snapshot(testTriangleButton);
});

test('renders with direction right', (t) => {
  const testTriangleButton = render.create(
    <TriangleButton onClick={noop} direction="right" />
  ).toJSON();
  t.snapshot(testTriangleButton);
});
