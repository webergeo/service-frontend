import test from 'ava';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';

import PaybackError from '../PaybackError';

const PAYBACK_ERROR = '.rs-payback__error';
const PAYBACK_ERROR_IMAGE = '.rs-payback__error-img';
const PAYBACK_ERROR_HEADLINE = '.rs-payback__error-headline';
const PAYBACK_ERROR_SUBHEADLINE = '.rs-payback__error-subheadline';
const PAYBACK_ERROR_TEXT = '.rs-payback__error-paragraph';

configure({ adapter: new Adapter() });
test('renders PaybackError and has correct classes', (t) => {
  const paybackErrorTree = render.create(<PaybackError />).toJSON();
  t.snapshot(paybackErrorTree);
  const wrapper = mount(<PaybackError />);
  t.is(wrapper.props().showImage, true);
  t.is(wrapper.props().showSubtitle, true);
  t.is(wrapper.find(PAYBACK_ERROR).length, 1);
  t.is(wrapper.find(PAYBACK_ERROR_IMAGE).length, 1);
  t.is(wrapper.find(PAYBACK_ERROR_HEADLINE).length, 1);
  t.is(wrapper.find(PAYBACK_ERROR_SUBHEADLINE).length, 1);
  t.is(wrapper.find(PAYBACK_ERROR_TEXT).length, 1);
});

test('renders PaybackError without image', (t) => {
  const paybackErrorTree = render.create(<PaybackError showImage={false} />).toJSON();
  t.snapshot(paybackErrorTree);
  const wrapper = mount(<PaybackError showImage={false} />);
  t.is(wrapper.props().showImage, false);
  t.is(wrapper.props().showSubtitle, true);
  t.is(wrapper.find(PAYBACK_ERROR).length, 1);
  t.is(wrapper.find(PAYBACK_ERROR_IMAGE).length, 0);
  t.is(wrapper.find(PAYBACK_ERROR_HEADLINE).length, 1);
  t.is(wrapper.find(PAYBACK_ERROR_SUBHEADLINE).length, 1);
  t.is(wrapper.find(PAYBACK_ERROR_TEXT).length, 1);
});

test('renders PaybackError without subheadline', (t) => {
  const paybackErrorTree = render.create(<PaybackError showSubtitle={false} />).toJSON();
  t.snapshot(paybackErrorTree);
  const wrapper = mount(<PaybackError showSubtitle={false} />);
  t.is(wrapper.props().showImage, true);
  t.is(wrapper.props().showSubtitle, false);
  t.is(wrapper.find(PAYBACK_ERROR).length, 1);
  t.is(wrapper.find(PAYBACK_ERROR_IMAGE).length, 1);
  t.is(wrapper.find(PAYBACK_ERROR_HEADLINE).length, 1);
  t.is(wrapper.find(PAYBACK_ERROR_SUBHEADLINE).length, 0);
  t.is(wrapper.find(PAYBACK_ERROR_TEXT).length, 1);
});

test('renders PaybackError without subheadline and without image', (t) => {
  const paybackErrorTree = render.create(<PaybackError showSubtitle={false} showImage={false} />).toJSON();
  t.snapshot(paybackErrorTree);
  const wrapper = mount(<PaybackError showSubtitle={false} showImage={false} />);
  t.is(wrapper.props().showImage, false);
  t.is(wrapper.props().showSubtitle, false);
  t.is(wrapper.find(PAYBACK_ERROR).length, 1);
  t.is(wrapper.find(PAYBACK_ERROR_IMAGE).length, 0);
  t.is(wrapper.find(PAYBACK_ERROR_HEADLINE).length, 1);
  t.is(wrapper.find(PAYBACK_ERROR_SUBHEADLINE).length, 0);
  t.is(wrapper.find(PAYBACK_ERROR_TEXT).length, 1);
});
