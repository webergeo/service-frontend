const MESSAGES = {
  title: 'PAYBACK ist zur Zeit nicht verfügbar.',
  subtitle: 'Die Verfügbarkeit wird gerade wieder hergestellt',
  text: 'Bitte versuchen Sie es später erneut!'
};

export default MESSAGES;
