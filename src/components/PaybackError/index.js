import PropTypes from 'prop-types';
import React from 'react';
import './PaybackError.scss';

import MESSAGES from './messages';

const PaybackError = ({ showSubtitle, showImage, messages }) => {
  const msg = {
    ...MESSAGES,
    ...messages
  };
  return (
    <div className="rs-payback__error rs-qa-payback__error">
      <div className="rs-payback__error-content">
        {showImage !== undefined && showImage === true && <div className="rs-payback__error-img rs-qa-payback__error-img hidden-xs" />}
        <div className="rs-payback__error-text-container">
          <h2 className="rs-payback__error-headline rs-qu-payback__error-headline">{msg.title}</h2>
          {showSubtitle !== undefined &&
          showSubtitle === true && (
            <p className="rs-payback__error-subheadline rs-qu-payback__error-subheadline hidden-xs">{msg.subtitle}</p>
          )}
          <p className="rs-payback__error-paragraph rs-qu-payback__error-paragraph">{msg.text}</p>
        </div>
      </div>
    </div>
  );
};

PaybackError.defaultProps = {
  showImage: true,
  showSubtitle: true,
  messages: {}
};

PaybackError.propTypes = {
  showImage: PropTypes.bool,
  showSubtitle: PropTypes.bool,
  messages: PropTypes.shape({
    title: PropTypes.string,
    subtitle: PropTypes.string,
    text: PropTypes.string
  })
};

export default PaybackError;
