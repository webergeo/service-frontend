import test from 'ava';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import render from 'react-test-renderer';
import { spy } from 'sinon';
import * as tracker from '../../utils/tracking';

import PaybackTeaserBlock from '../PaybackTeaserBlock';
import Button from '../Button';
import BlockHeader from '../BlockHeader';

const PAYBACK_INTROBLOCK_PRIMARY_CLASS = '.rs-payback__borderlayout-root';
const PAYBACK_INTROBLOCK_SECONDARY_CLASS = '.rs-payback__intro--haspayback';

configure({ adapter: new Adapter() });

test('renders PaybackTeaserBlock without payback info and has correct class', (t) => {
  const introBlockTree = render.create(<PaybackTeaserBlock />).toJSON();
  t.snapshot(introBlockTree);
  const wrapper = mount(<PaybackTeaserBlock />);
  t.is(wrapper.find(PAYBACK_INTROBLOCK_PRIMARY_CLASS).length, 1);
  t.is(wrapper.find(PAYBACK_INTROBLOCK_SECONDARY_CLASS).length, 0);
});

test('renders the correct headline', (t) => {
  const wrapper = mount(<PaybackTeaserBlock />);
  t.is(wrapper.find(BlockHeader).length, 1);
});

test('renders the button component', (t) => {
  const wrapper = mount(<PaybackTeaserBlock />);
  t.is(wrapper.find(Button).length, 1);
});

// sequential execution here due to possible side effects while spying on globally available functions!
test.serial('button click produces tracking event', (t) => {
  // note: must call spy() before mount()!
  const trackSpy = spy(tracker, 'trackPaybackRegistration');
  const wrapper = mount(<PaybackTeaserBlock />);
  wrapper.find(Button).simulate('click');

  t.true(trackSpy.called);
  trackSpy.restore();
});
