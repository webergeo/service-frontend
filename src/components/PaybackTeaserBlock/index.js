import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';
import { trackPaybackRegistration } from '../../utils/tracking';
import logger from '../../utils/ducLogger';
import { initializePbClient } from '../../utils/commons';
import './PaybackTeaserBlock.scss';

import rewePaybackCard from '../../assets/images/rewe-payback-card@2x.png';
import MESSAGES from './messages';

import BorderLayout from '../BorderLayout';
import BlockHeader from '../BlockHeader';
import BlockContent from '../BlockContent';

import { getAccountBinding } from '../../api/Accounts';

class PaybackTeaserBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pbClient: null
    };
    this.clientErrorHandler = this.clientErrorHandler.bind(this);
    this.clientSuccessHandler = this.clientSuccessHandler.bind(this);
    this.createOverlay = this.createOverlay.bind(this);
  }

  componentDidMount() {
    initializePbClient(this.clientSuccessHandler, this.clientErrorHandler);
  }

  /* eslint-disable class-methods-use-this */
  clientSuccessHandler(client) {
    this.setState({
      pbClient: client
    });
  }

  clientErrorHandler(error) {
    logger.error(`failed to initialize pb-client for overlay: ${error.message}`);
  }

  createOverlay() {
    trackPaybackRegistration();
    if (this.state.pbClient !== null) {
      this.state.pbClient.subscribe('PB-AuthorizationCodeEvent', (e) => {
        if (!e.data && !e.data.authorizationCode) {
          logger.error(`did not get authorization code: (event=${e})`);
          return;
        }

        const authCode = e.data.authorizationCode;
        const observer = new MutationObserver((mutations) => {
          mutations.forEach((mutation) => {
            const isLayerOpen = document.body.classList.contains('payback-online-integration-overlay-underlying');
            if (mutation.attributeName === 'class' && !isLayerOpen) {
              observer.disconnect();
              getAccountBinding(authCode);
            }
          });
        });

        observer.observe(document.body, { attributes: true });
      });

      this.state.pbClient.createModuleInOverlay({
        moduleType: 'enrollment',
        contextKey: 'overlayContext',
        options: {
          customerHasCard: true,
          partnerProvidesOwnCard: true,
          sendAuthCode: this.props.kv,
          showAccountBinding: this.props.kv
        }
      }, {
        backdropDisabled: true
      });
    }
  }

  renderContentblock() {
    return (
      <div className="rs-payback__paybackTeaserBlock-content">
        <div className="rs-payback__paybackTeaserBlock-content-desc">
          <BlockHeader>{MESSAGES.headline}</BlockHeader>
          <BlockContent>{ this.props.kv ? MESSAGES.paragraph_kv : MESSAGES.paragraph }</BlockContent>
        </div>
        <img src={rewePaybackCard} alt="" className="rs-payback__paybackTeaserBlock-content-img" />
      </div>
    );
  }

  renderFooter() {
    return (
      <div className="rs-payback__paybackTeaserBlock-footer">
        <Button
          big
          color="grey"
          label={ this.props.kv ? MESSAGES.action.text_kv : MESSAGES.action.text }
          handleClick={this.createOverlay}
          className="rs-payback__paybackTeaserBlock-button"
        />
      </div>
    );
  }

  render() {
    return (
      <BorderLayout content={this.renderContentblock()} footer={this.renderFooter()} />
    );
  }
}

PaybackTeaserBlock.propTypes = {
  kv: PropTypes.bool
};

PaybackTeaserBlock.defaultProps = {
  kv: false
};

export default PaybackTeaserBlock;
