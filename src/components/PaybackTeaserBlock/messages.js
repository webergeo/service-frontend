const MESSAGES = {
  headline: 'Du bist noch kein PAYBACK Kunde?',
  paragraph: 'Dann melde dich jetzt kostenlos an und punkte direkt los.',
  paragraph_kv: 'Dann melden Sie sich jetzt kostenlos bei PAYBACK an und schalten Sie die PAYBACK Services im REWE Kundenkonto einmalig frei. Im Anschluss können Sie die PAYBACK Services bequem online nutzen und direkt lospunkten.',
  action: {
    text: 'Jetzt bei PAYBACK anmelden',
    text_kv: 'Anmelden und freischalten'
  }
};

export default MESSAGES;
