Unselected and enabled
```
const imageProps = {
  image: require('../../assets/images/payback-2014-1075-pointee.png'),
  imageStyle: { maxWidth: '100%'}
};
<div style={{ maxWidth: '301px' }}>
  <BenefitItem
    {...imageProps}
    title="eCoupons"
    content="Mit eCoupons im Markt oder REWE Onlineshop Extra-Punkte sammeln."
  />
</div>
```

Selected and enabled
```
const imageProps = {
  image: require('../../assets/images/payback-2014-1075-pointee.png'),
  imageStyle: { maxWidth: '100%'}
};
<div style={{ maxWidth: '301px' }}>
  <BenefitItem
    {...imageProps}
    title="eCoupons"
    content="Mit eCoupons im Markt oder REWE Onlineshop Extra-Punkte sammeln."
    selected
  />
</div>
```

Unselected and disabled
```
const imageProps = {
  image: require('../../assets/images/payback-2014-1075-pointee.png'),
  imageStyle: { maxWidth: '100%'}
};
<div style={{ maxWidth: '301px' }}>
  <BenefitItem
    {...imageProps}
    title="eCoupons"
    content="Mit eCoupons im Markt oder REWE Onlineshop Extra-Punkte sammeln."
    disabled
  />
</div>
```
