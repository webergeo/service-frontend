import test from 'ava';
import React from 'react';

import render from 'react-test-renderer';

import BenefitItem from '.';

const testProps = {
  image: 'image.png',
  title: 'Some Title',
  content: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam'
};

test('renders the default snapshot', (t) => {
  const modalTree = render.create(<BenefitItem {...testProps} />).toJSON();
  t.snapshot(modalTree);
});

test('renders the default snapshot for selected component', (t) => {
  const modalTree = render.create(<BenefitItem {...testProps} selected />).toJSON();
  t.snapshot(modalTree);
});

test('renders the default snapshot for disabled component', (t) => {
  const modalTree = render.create(<BenefitItem {...testProps} disabled />).toJSON();
  t.snapshot(modalTree);
});
