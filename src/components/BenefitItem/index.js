import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './BenefitItem.scss';
import BlockHeader from '../BlockHeader';
import BlockContent from '../BlockContent';

const BenefitItem = ({
  image, imageStyle, title, content, selected, disabled
}) => (
  <div className={cn('rs-payback__benefitItem', {
    'rs-payback__benefitItem-selected': selected,
    'rs-payback__benefitItem-disabled': disabled
  })}
  >
    {selected && <div className="rs-payback__benefitItem-selected-triangle" />}
    {selected &&
      <svg className="rs-payback__benefitItem-selected-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 11">
        <path className="cls-1" d="M0 6.6l1.47-1.65 4.41 3.02L13.23 0 15 1.38 6.47 11 0 6.6z" />
      </svg>
    }
    <div className="rs-payback__benefitItem-img-box">
      <img src={image} alt="" className="rs-payback__benefitItem-img rs-qa-payback__benefitItem-img" {...(imageStyle ? { style: imageStyle } : {})} />
    </div>
    <div className="rs-payback__benefitItem-desc-box">
      <BlockHeader>{title}</BlockHeader>
      <BlockContent>{content}</BlockContent>
    </div>
  </div>
);

BenefitItem.propTypes = {
  image: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
  imageStyle: PropTypes.objectOf(PropTypes.string),
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  selected: PropTypes.bool,
  disabled: PropTypes.bool
};

BenefitItem.defaultProps = {
  selected: false,
  disabled: false,
  imageStyle: null
};

export default BenefitItem;
