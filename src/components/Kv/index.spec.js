import test from 'ava';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { stub } from 'sinon';
import waitForExpect from 'wait-for-expect';
import * as accountsApi from '../../api/Accounts';
import Kv from '.';

configure({ adapter: new Adapter() });

test('show unlock button', (t) => {
  const wrapper = mount(
    <Kv
      accountBindingStatus={undefined}
      paybackAccountBound={false}
      paybackNumber={undefined}
    />
  );

  const btn = wrapper.find('Button[label="Freischalten"]');
  t.is(btn.length, 1);
});

test('show unlock button with predefind paybackNumber', (t) => {
  const wrapper = mount(
    <Kv
      accountBindingStatus={undefined}
      paybackAccountBound={false}
      paybackNumber="1234567890"
    />
  );

  // TODO check other button if allow different labels.
  const btn = wrapper.find('Button[label="Freischalten"]');
  t.is(btn.length, 1);
});

test('fetch data and show button', async (t) => {
  const getPaybackAccountInfoStub = stub(accountsApi, 'getPaybackAccountInfo');
  getPaybackAccountInfoStub.returns({
    paybackNumber: '1234567890',
    _embedded: {
      accountBindingExists: false
    }
  });

  const wrapper = mount(
    <Kv
      accountBindingStatus={undefined}
      paybackAccountBound={undefined}
      paybackNumber={undefined}
    />
  );

  await waitForExpect(() => {
    t.is(getPaybackAccountInfoStub.called, true);
  });

  wrapper.update();
  const btn = wrapper.find('Button[label="Freischalten"]');
  t.is(btn.length, 1);

  getPaybackAccountInfoStub.restore();
});

test('render success status', (t) => {
  const wrapper = mount(
    <Kv
      accountBindingStatus="success"
      paybackAccountBound
      paybackNumber="1234567890"
    />
  );

  const modal = wrapper.find('AccountBindingStatusModal[accountBindingStatus="success"]');
  t.is(modal.length, 1);
});

test('render failure status', (t) => {
  const wrapper = mount(
    <Kv
      accountBindingStatus="failure"
      paybackAccountBound={false}
      paybackNumber="1234567890"
    />
  );

  const modal = wrapper.find('AccountBindingStatusModal[accountBindingStatus="failure"]');
  t.is(modal.length, 1);
});

test('show error if client error', async (t) => {
  const updatePaybackAccountInfoStub = stub(accountsApi, 'updatePaybackAccountInfo');
  updatePaybackAccountInfoStub.returns({
    status: 400,
    json: () => ({
      _status: {
        validationMessages: [{ message: 'Whoops' }]
      }
    })
  });

  const wrapper = mount(
    <Kv
      accountBindingStatus={undefined}
      paybackAccountBound={false}
      paybackNumber=""
    />
  );

  wrapper.find('Button[label="Freischalten"]').simulate('click');

  const modal = wrapper.find('PaybackNumberModal');
  t.is(modal.length, 1);

  modal.find('TextInput').find('input').simulate('change', { target: { value: '1234567890' } });
  modal.find('Button[label="Speichern"]').simulate('click');

  await waitForExpect(() => {
    wrapper.update();
    const error = wrapper.find('Modal[content="Whoops"]');
    t.is(error.length, 1);
  });

  updatePaybackAccountInfoStub.restore();
});
