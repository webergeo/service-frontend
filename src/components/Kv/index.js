import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { parse, stringify } from 'query-string';
import { getAccountInfo } from '../../services/PaybackAccountService';
import { generateSyncCsrfToken, updatePaybackAccountInfo } from '../../api/Accounts';
import { parsePaybackErrorMessage } from '../../utils/apiResponseFormatter';
import { getEnvironmentConfig } from '../../config/appConfig';
import PaybackNumberModal from '../PaybackNumberModal';
import AccountBindingStatusModal from '../AccountBindingStatusModal';
import Button from '../Button';
import Modal from '../Modal';

const App = ({
  accountBindingStatus,
  paybackAccountBound,
  paybackNumber
}) => {
  const [state, setState] = useState({
    binding: paybackAccountBound,
    cardNumber: paybackNumber,
    showCardNumberModal: false,
    showPaybackPopup: false,
    status: accountBindingStatus,
    error: undefined
  });

  const onUnlock = async () => {
    if (state.cardNumber) {
      // Open payback kv flow in popup. Load payback in iframe is not possible at
      // the moment, the page after the captcha does not contain the required
      // frame-options.
      const csrfTokenResponse = await generateSyncCsrfToken(true);
      const { csrfToken } = JSON.parse(csrfTokenResponse.response);

      const paybackOauthUrl = getEnvironmentConfig('PAYBACK_ACCOUNT_BINDING_URL');
      const paybackOauthClientId = getEnvironmentConfig('PAYBACK_ACCOUNT_BINDING_CLIENT_ID');
      const redirectUri = `${window.location.origin}/mydata/payback/redirectionendpoint`;

      const url = paybackOauthUrl +
        '?response_type=code' +
        `&client_id=${paybackOauthClientId}` +
        `&state=${csrfToken}` +
        `&cardNumber=${state.cardNumber}` +
        `&redirect_uri=${redirectUri}`;

      const options = 'height=650,width=800,location=1,menubar=0,resizable=0,scrollbars=1,status=1,titlebar=1';
      const popup = window.open(url, 'Payback Popup', options);
      if (popup == null) {
        setState({ ...state, error: 'Ihr Browser hat das Öffnen des PAYBACK Pop-ups verhindert. Bitte erstellen Sie für PAYBACK eine Ausnahme in Ihren Browsereinstellungen, um eine vollständige Nutzung der Services zu ermöglichen.' });
      }

      popup.focus();
    } else {
      setState({ ...state, showCardNumberModal: true });
    }
  };

  const onCardNumberModalClose = () => {
    setState({ ...state, showCardNumberModal: false });
  };

  const onCardNumberSave = async (cardNumber) => {
    const data = { payload: { paybackNumber: cardNumber } };
    const response = await updatePaybackAccountInfo(data);

    if (response.status === 403) {
      // Unauthenticated? Make reload!
      window.location.reload();
    } else if (response.status >= 400) {
      const responseData = await response.json();
      setState({ ...state, error: parsePaybackErrorMessage(responseData) });
    } else {
      setState({
        ...state,
        cardNumber,
        showCardNumberModal: false,
        showPaybackPopup: true
      });
    }
  };

  const onStatusModalClose = () => {
    const query = parse(window.location.search);
    delete query.accountBindingStatus;
    window.history.replaceState({}, '', `${window.location.pathname}?${stringify(query)}`);
    setState({ ...state, status: undefined });
  };

  const onErrorModalClose = () => {
    setState({ ...state, error: undefined });
  };

  useEffect(() => {
    // Fetch data when ssr preloading did not work.
    if (state.binding === undefined) {
      getAccountInfo().then((response) => {
        setState({
          ...state,
          binding: response.paybackAccountBound,
          cardNumber: response.paybackNumber
        });
      });
    }
  }, []);

  useEffect(() => {
    if (state.showPaybackPopup) {
      onUnlock();
    }
  }, [state.showPaybackPopup]);

  if ((!state.status && state.binding) || state.binding === undefined) {
    return null;
  }

  const showStatus =
    (state.status === 'success' && state.binding) ||
    (state.status !== 'success' && state.status);

  return (
    <div className="rs-payback__kv">
      {state.showCardNumberModal ? (
        <PaybackNumberModal
          onCancel={onCardNumberModalClose}
          onSubmit={onCardNumberSave}
          defaultValidation={{ error: false, message: '' }}
        />
      ) : ''}
      {!state.binding ? (
        <Button
          big
          color="green"
          handleClick={onUnlock}
          label="Freischalten"
        />
      ) : ''}
      {showStatus ? (
        <AccountBindingStatusModal
          environment={{}}
          paybackNumber={state.cardNumber}
          accountBindingStatus={state.status}
          onClose={onStatusModalClose}
          onSubmit={onStatusModalClose}
        />
      ) : ''}
      {state.error ? (
        <Modal
          title="Ihre PAYBACK Kundennummer konnte nicht gespeichert werden"
          content={state.error}
          type="error"
          handleCancel={onErrorModalClose}
          handleSubmit={onErrorModalClose}
        />
      ) : ''}
    </div>
  );
};

App.propTypes = {
  paybackAccountBound: PropTypes.bool,
  paybackNumber: PropTypes.string,
  accountBindingStatus: PropTypes.string
};

App.defaultProps = {
  paybackAccountBound: undefined,
  paybackNumber: undefined,
  accountBindingStatus: undefined
};

export default App;
