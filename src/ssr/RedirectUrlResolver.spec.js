import test from 'ava';
import { loadConfiguration } from '../../dev-settings/dev-config';
import RedirectUrlResolver from './RedirectUrlResolver';

test.before(() => {
  loadConfiguration('test');
});

test('load configuration should pass OK', (t) => {
  try {
    // eslint-disable-next-line no-unused-vars
    const resolver = new RedirectUrlResolver();
    t.pass();
  } catch (err) {
    t.fail(err);
  }
});

test('resolveUrl() should resolve a known URL mapping', (t) => {
  const resolver = new RedirectUrlResolver();
  const target = resolver.resolveUrl('/ui/mobile/coupons');
  t.is(target, '/mydata/mobile/payback/coupons');
});

test('resolveUrl() should resolve mydata-paybackcontent', (t) => {
  const resolver = new RedirectUrlResolver();
  const target = resolver.resolveUrl('/ui/mydata-paybackcontent');
  t.is(target, '/mydata/payback');
});

test('resolveUrl() should handle unmapped URL', (t) => {
  const resolver = new RedirectUrlResolver();
  const target = resolver.resolveUrl('/ui/mydata');
  t.is(target, '');
});

test('resolveUrl() should leave query params intact', (t) => {
  const resolver = new RedirectUrlResolver();
  const target = resolver.resolveUrl('/ui/mobile/coupons?param1=testParam1&param2=testParam2');
  t.is(target, '/mydata/mobile/payback/coupons?param1=testParam1&param2=testParam2');
});