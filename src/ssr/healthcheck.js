/**
 * These are simple stubs for now, but can be extended to include specific
 * health indications when such become available.
 */

const HEALTH_OK = {
  status: 'UP',
  enabled: {
    name: 'enabled',
    status: 'UP',
    info: 'Payback frontend service is enabled',
    error: ''
  }
};

export const healthCheck = (req, res) => {
  res.status(200).end();
};

export const healthStatus = (req, res) => {
  res
    .status(200)
    .send(HEALTH_OK)
    .end();
};
