/**
 * View handler definitions
 */

import { env } from 'process';
import React from 'react';
import { renderToString } from 'react-dom/server';
import serialize from 'serialize-javascript';
import logger from './logger';

import { loadInitialState, loadInitialPaybackAccount } from './initial-state';
import { hasPaybackAccountBinding } from '../utils/apiResponseFormatter';
import App from '../containers/App';
import NumberApp from '../containers/NumberApp';
import Kv from '../components/Kv';
import RegistrationBannerApp from '../containers/RegistrationBannerApp';

export const renderWebView = viewName => async (req, res) => {
  let initialState;
  try {
    initialState = await loadInitialState({ headers: req.headers });
  } catch (err) {
    logger.error(err.toString());
    initialState = {
      errorStatusCode: 500
    };
  }

  // extra protection for possible rendering erors
  try {
    const markup = renderToString(<App {...initialState} defaultQueryParams={req.query} />);
    res.render(viewName, {
      markup,
      initialState: serialize(initialState),
      paybackMcApiScript: `<script type="application/javascript" src="${env.PAYBACK_MCAPI_URL}" meso-options="place-in-header"></script>`
    });
  } catch (err) {
    logger.error(err.toString());
    res
      .status(500)
      .send('A web rendering error has ocurred!')
      .end();
  }
};

export const renderMobileView = (viewName, moduleOptions) => async (req, res) => {
  try {
    const response = await loadInitialPaybackAccount({ headers: req.headers });
    const accountBindingExists = hasPaybackAccountBinding(response);
    if (response.status >= 400) {
      res.status(response.status).end();
    } else if (!accountBindingExists) {
      res.status(200).end();
    } else {
      const renderOptions = {
        PAYBACK_MCAPI_URL: env.PAYBACK_MCAPI_URL,
        PAYBACK_API_KEY: env.PAYBACK_API_KEY,
        URL_PAYBACK_SESSION_TOKEN_REDIRECT: env.URL_PAYBACK_SESSION_TOKEN_REDIRECT
      };

      if (moduleOptions !== undefined) {
        renderOptions.MODULE_OPTIONS = JSON.stringify(moduleOptions);
      }

      res.render(viewName, renderOptions);
    }
  } catch (err) {
    logger.error(err.toString());
    res
      .status(500)
      .send('Es ist ein unbekannter Fehler aufgetreten.')
      .end();
  }
};

export const renderNumberAppView = viewName => async (req, res) => {
  let initialState;
  try {
    initialState = await loadInitialState({ headers: req.headers });
  } catch (err) {
    logger.error(err.toString());
    initialState = {
      errorStatusCode: 500
    };
  }

  // extra protection for possible rendering errors
  try {
    const markup = renderToString(<NumberApp {...initialState} />);
    res.render(viewName, {
      markup,
      initialState: serialize(initialState),
      REWE_ENVIRONMENT: env.REWE_ENVIRONMENT
    });
  } catch (err) {
    logger.error(err.toString());
    res
      .status(500)
      .send('A number-app rendering error has ocurred!')
      .end();
  }
};

export const renderKvAppView = (viewName) => async (req, res) => {
  let initialState;
  try {
    initialState = await loadInitialState({ headers: req.headers });
  } catch (err) {
    logger.error(err.toString());
    initialState = {
      errorStatusCode: 500
    };
  }

  try {
    const markup = renderToString(
      /* eslint-disable-next-line */
      <Kv {...initialState} />
    );
    res.render(viewName, {
      markup,
      initialState: serialize(initialState),
      REWE_ENVIRONMENT: env.REWE_ENVIRONMENT
    });
  } catch (err) {
    logger.error(err.toString());
    res
      .status(500)
      .send('A kv-app rendering error has ocurred!')
      .end();
  }
};

export const renderRegistrationBannerAppView = viewName => async (req, res) => {
  let initialState;
  try {
    initialState = await loadInitialState({ headers: req.headers });
  } catch (err) {
    logger.error(err.toString());
    initialState = {
      errorStatusCode: 500
    };
  }

  // extra protection for possible rendering errors
  try {
    const layout = req.query.layout ? req.query.layout : 'checkout';
    const markup = renderToString(<RegistrationBannerApp layout={layout} />);
    res.render(viewName, {
      markup,
      layout,
      paybackMcApiScript: `<script type="application/javascript" src="${env.PAYBACK_MCAPI_URL}" meso-options="place-in-header"></script>`,
      initialState: serialize(initialState),
      REWE_ENVIRONMENT: env.REWE_ENVIRONMENT
    });
  } catch (err) {
    logger.error(err.toString());
    res
      .status(500)
      .send('A registration-banner-app rendering error has ocurred!')
      .end();
  }
};
