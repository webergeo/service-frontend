import test from 'ava';
import path from 'path';
import request from 'supertest';
import nock from 'nock';

import app from './index';
import { loadConfiguration } from '../../dev-settings/dev-config';
import paybackAccountMock from '../mock-data/paybackAccountMock';
import authTokenMock from '../mock-data/authTokenMock';

const fetchRequest = request(app);

const mockOneSucceededPaybackAccountResponse = () => nock('http://localhost:3004')
  .get('/api/customers/me/paybackaccount')
  .reply(200, paybackAccountMock);

/**
 * Please note that during these tests, the redirect resolver always falls back to its default route map as the ENV
 * parameters are initialized too late - the redirect resolver instance in the SSR "app" module is initialzed before
 * the before() method below triggers!
 */
test.before(() => {
  loadConfiguration('test');

  // explicitly set the view path
  app.set('views', path.join(__dirname, '..', 'templates'));

  // mock possible HTTP requests
  nock('http://localhost:3004')
    .persist()
    .post('/api/token', {
      scope: 'read',
      grant_type: 'client_credentials'
    })
    .reply(200, authTokenMock)
    .get('/admin/health')
    .reply(200, {
      status: 'UP',
      enabled: {
        name: 'enabled',
        status: 'UP',
        info: 'This service is enabled',
        error: ''
      }
    })
    .post('/api/check_token', { token: 'valid' })
    .reply(200, {
      aud: ['oauth2-resource'],
      exp: 1872239462,
      active: true,
      client_id: 'duc',
      scope: ['read'],
      authorities: [
        'ROLE_CLIENT',
        'ROLE_TRUSTED_CLIENT'
      ]
    })
    .post('/api/check_token', { token: 'invalid' })
    .reply(400, {
      error: 'invalid_token',
      error_description: 'Token was not recognised'
    });
});

test.serial('SSR# check response type of /ui/mydata-paybackcontent endpoint - happy path', async (t) => {
  mockOneSucceededPaybackAccountResponse();
  const response = await fetchRequest
    .get('/ui/mydata-paybackcontent')
    .set('auth-info-user-id', 'some-user-id')
    .set('authorization', 'Bearer valid');
  t.is(response.statusCode, 200);
  t.is(response.type, 'text/html');
});

test('SSR# /ui/mydata-paybackcontent endpoint returns 400 status code when token is missing', async (t) => {
  const response = await fetchRequest
    .get('/ui/mydata-paybackcontent')
    .set('auth-info-user-id', 'some-user-id')
  t.is(response.statusCode, 400);
});

test('SSR# /ui/mydata-paybackcontent endpoint returns 401 status code when token is invalid', async (t) => {
  const response = await fetchRequest
    .get('/ui/mydata-paybackcontent')
    .set('auth-info-user-id', 'some-user-id-specific')
    .set('authorization', 'Bearer invalid');
  t.is(response.statusCode, 401);
});

test('SSR# /ui/mydata-paybackcontent endpoint returns 302 status code(redirect) when auth-info-user-id is not present ', async (t) => {
  const response = await fetchRequest
    .get('/ui/mydata-paybackcontent')
    .set('authorization', 'Bearer valid');
  t.is(response.statusCode, 302);
  t.is(response.get('location'), '/mydata/login?redirectUrl=/mydata/payback');
  t.is(response.type, 'text/plain');
});

test('SSR# /ui/mydata-paybackcontent endpoint returns 302 status code(redirect) with intact query params when auth-info-user-id is not present ', async (t) => {
  const response = await fetchRequest
    .get('/ui/mydata-paybackcontent?tparam1=test1&tparam2=test2')
    .set('authorization', 'Bearer valid');
  t.is(response.statusCode, 302);
  t.is(response.get('location'), '/mydata/login?redirectUrl=/mydata/payback?tparam1=test1&tparam2=test2');
  t.is(response.type, 'text/plain');
});

test.serial('SSR# /ui/mobile/coupons endpoint returns 200 - happy path', async (t) => {
  mockOneSucceededPaybackAccountResponse();
  const response = await fetchRequest
    .get('/ui/mobile/coupons')
    .set('authorization', 'Bearer valid')
    .set('auth-info-user-id', 'some-user-id');
  t.is(response.statusCode, 200);
  t.is(response.type, 'text/html');
});

test('SSR# /ui/mobile/coupons endpoint returns 302 status code (redirect) when auth-info-user-id is not present ', async (t) => {
  const response = await fetchRequest
    .get('/ui/mobile/coupons')
    .set('authorization', 'Bearer valid');

  t.is(response.statusCode, 302);
  t.is(response.get('location'), '/mydata/login?redirectUrl=/mydata/mobile/payback/coupons');
  t.is(response.type, 'text/plain');
});

test('SSR# /ui/mobile/coupons?tparam1=test1&tparam2=test2 endpoint returns 302 status code (redirect) with intact query params when auth-info-user-id is not present ', async (t) => {
  const response = await fetchRequest
    .get('/ui/mobile/coupons?tparam1=test1&tparam2=test2')
    .set('authorization', 'Bearer valid');
  t.is(response.statusCode, 302);
  t.is(response.get('location'), '/mydata/login?redirectUrl=/mydata/mobile/payback/coupons?tparam1=test1&tparam2=test2');
  t.is(response.type, 'text/plain');
});

test.serial('SSR# /ui/mobile/coupons endpoint returns error status code when payback account cannot be loaded', async (t) => {
  nock('http://localhost:3004')
    .get('/api/customers/me/paybackaccount')
    .reply(404);

  const notFoundErrorResponse = await fetchRequest
    .get('/ui/mobile/coupons')
    .set('authorization', 'Bearer valid')
    .set('auth-info-user-id', 'some-user-id');
  t.is(notFoundErrorResponse.statusCode, 404);

  nock('http://localhost:3004')
    .get('/api/customers/me/paybackaccount')
    .reply(500);

  const internalServerErrorResponse = await fetchRequest
    .get('/ui/mobile/coupons')
    .set('authorization', 'Bearer valid')
    .set('auth-info-user-id', 'some-user-id');
  t.is(internalServerErrorResponse.statusCode, 500);
});

test.serial('SSR# /ui/mobile/coupons endpoint returns 200 and no body when no account binding', async (t) => {
  nock('http://localhost:3004')
    .get('/api/customers/me/paybackaccount')
    .reply(200, {});

  const response = await fetchRequest
    .get('/ui/mobile/coupons')
    .set('authorization', 'Bearer valid')
    .set('auth-info-user-id', 'some-user-id');
  t.is(response.statusCode, 200);
  t.is(response.text, '');
});

test.serial('SSR# /ui/mobile/vouchers endpoint returns 200 - happy path', async (t) => {
  mockOneSucceededPaybackAccountResponse();
  const response = await fetchRequest
    .get('/ui/mobile/vouchers')
    .set('authorization', 'Bearer valid')
    .set('auth-info-user-id', 'some-user-id');
  t.is(response.statusCode, 200);
});

test('SSR# /ui/mobile/vouchers endpoint returns 302 status code(redirect) when auth-info-user-id is not present ', async (t) => {
  const response = await fetchRequest
    .get('/ui/mobile/vouchers')
    .set('authorization', 'Bearer valid');
  t.is(response.statusCode, 302);
  t.is(response.get('location'), '/mydata/login?redirectUrl=/mydata/mobile/payback/vouchers');
  t.is(response.type, 'text/plain');
});

test('SSR# /ui/mobile/vouchers?tparam1=test1&tparam2=test2 endpoint returns 302 status code (redirect) with intact query params when auth-info-user-id is not present', async (t) => {
  const response = await fetchRequest
    .get('/ui/mobile/vouchers?tparam1=test1&tparam2=test2')
    .set('authorization', 'Bearer valid');
  t.is(response.statusCode, 302);
  t.is(response.get('location'), '/mydata/login?redirectUrl=/mydata/mobile/payback/vouchers?tparam1=test1&tparam2=test2');
  t.is(response.type, 'text/plain');
});

test.serial('SSR# /ui/mobile/vouchers endpoint returns error status code when payback account cannot be loaded', async (t) => {
  nock('http://localhost:3004')
    .get('/api/customers/me/paybackaccount')
    .reply(404);

  const notFoundErrorResponse = await fetchRequest
    .get('/ui/mobile/vouchers')
    .set('authorization', 'Bearer valid')
    .set('auth-info-user-id', 'some-user-id');
  t.is(notFoundErrorResponse.statusCode, 404);

  nock('http://localhost:3004')
    .get('/api/customers/me/paybackaccount')
    .reply(500);

  const internalServerErrorResponse = await fetchRequest
    .get('/ui/mobile/vouchers')
    .set('authorization', 'Bearer valid')
    .set('auth-info-user-id', 'some-user-id');
  t.is(internalServerErrorResponse.statusCode, 500);
});

test.serial('SSR# /ui/mobile/vouchers endpoint returns 200 and no body when no account binding', async (t) => {
  nock('http://localhost:3004')
    .get('/api/customers/me/paybackaccount')
    .reply(200, {});

  const response = await fetchRequest
    .get('/ui/mobile/vouchers')
    .set('authorization', 'Bearer valid')
    .set('auth-info-user-id', 'some-user-id');
  t.is(response.statusCode, 200);
});

test('SSR# /admin/healthcheck endpoint response OK', async (t) => {
  const response = await fetchRequest.get('/admin/healthcheck');
  t.is(response.statusCode, 200);
});

test('SSR# /admin/health endpoint response OK', async (t) => {
  const response = await fetchRequest.get('/admin/health');
  t.is(response.statusCode, 200);
  t.is(response.type, 'application/json');
});
