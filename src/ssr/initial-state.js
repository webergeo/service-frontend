import { env } from 'process';
import { getMachineToken } from '../api/AuthTokenClient';
import { getInitialStatePaybackAccountInfo } from '../api/Accounts';
import {
  hasEBonOptInFlag,
  hasBonusCouponOptInFlag,
  hasEwe16OptInFlag,
  hasPaybackAccountBinding,
  getPaybackNumber,
  hasGroupEweOptInFlag
} from '../utils/apiResponseFormatter';
import { generateAccountBindingUrl } from '../utils/commons';
import logger from './logger';

/**
 * Initial state retrieval has been deliberately factored into a separate unit to reduce clutter in the main SSR module.
 * The actual rational for having a separate getInitialStatePaybackAccountInfo and the surrounding implementation is this:
 *
 * All "usual" methods in Accounts.js that perform AJAX requests do so via the absolute URLs exposed through the "shop", using
 * an active DUC session for authentication. The SSR code runs in the docker environment >behind< the UI composition layer etc.
 * and therefore has to perform direct REST calls to backend to retrieve the initial state. Therefore, explicit authentication
 * becomes necessary AND we also can't use the URL employed by the "normal" getPaybackAccountInfo() method.
 *
 * Also, the resulting initial state object carries some additional information such as all the URLs configured externally via
 * the deployment descriptor.
 *
 * Therefore, despite the fact the code belows looks very similar to the implemenation in PaybackAccountService it has to be
 * duplicated here due to the aforementioned differences.
 */

const ACCOUNT_BINDING_URL = generateAccountBindingUrl({
  paybackAccountBindingUrl: env.PAYBACK_ACCOUNT_BINDING_URL,
  paybackAccountBindingClientId: env.PAYBACK_ACCOUNT_BINDING_CLIENT_ID,
  paybackAccountBindingRedirectUri: env.URL_PAYBACK_ACCOUNT_BINDING_REDIRECTION_ENDPOINT
});

export const loadInitialPaybackAccount = async ({ headers }) => {
  const { access_token: accessToken } = await getMachineToken(logger);
  return getInitialStatePaybackAccountInfo({
    url: env.URL_PAYBACK_ACCOUNT_INITIAL_STATE,
    token: {
      Authorization: `Bearer ${accessToken}`,
      'auth-info-user-id': headers['auth-info-user-id']
    }
  });
};

export const loadInitialState = async ({ headers }) => {
  let initialState;

  try {
    const customerId = headers['auth-info-user-id'];
    const isTestGroupActive = (name) => (headers['a-b-test-groups'] || '').indexOf(name) !== -1;

    initialState = {
      ACCOUNT_BINDING_URL,
      environment: {
        NODE_ENV: env.NODE_ENV,
        SERVICE_PORT: env.SERVICE_PORT,
        URL_PAYBACK_ACCOUNT: env.URL_PAYBACK_ACCOUNT,
        URL_PAYBACK_EWE16: env.URL_PAYBACK_EWE16,
        URL_PAYBACK_CSRF_TOKEN_WEB: env.URL_PAYBACK_CSRF_TOKEN_WEB,
        URL_PAYBACK_EBON: env.URL_PAYBACK_EBON,
        URL_PAYBACK_BONUS_COUPON: env.URL_PAYBACK_BONUS_COUPON,
        URL_PAYBACK_EBON_OPT_IN_AVAILABILITY: env.URL_PAYBACK_EBON_OPT_IN_AVAILABILITY,
        URL_PAYBACK_ACCOUNT_BINDING: env.URL_PAYBACK_ACCOUNT_BINDING,
        URL_PAYBACK_ACCOUNT_BINDING_REDIRECTION_ENDPOINT: env.URL_PAYBACK_ACCOUNT_BINDING_REDIRECTION_ENDPOINT,
        URL_PAYBACK_POINTS: env.URL_PAYBACK_POINTS,
        URL_PAYBACK_GROUP_EWE: env.URL_PAYBACK_GROUP_EWE,
        URL_RECEIPTS: env.URL_RECEIPTS,
        PAYBACK_ACCOUNT_BINDING_URL: env.PAYBACK_ACCOUNT_BINDING_URL,
        PAYBACK_ACCOUNT_BINDING_CLIENT_ID: env.PAYBACK_ACCOUNT_BINDING_CLIENT_ID,
        FEATURE_TOGGLE_EBON: env.FEATURE_TOGGLE_EBON,
        FEATURE_TOGGLE_BONUS_COUPON: isTestGroupActive('payback-bonus-coupon'),
        FEATURE_TOGGLE_ECOUPON: env.FEATURE_TOGGLE_ECOUPON,
        FEATURE_TOGGLE_EVOUCHER: env.FEATURE_TOGGLE_EVOUCHER,
        FEATURE_TOGGLE_KV_ENROLLMENT: isTestGroupActive('payback-kv-enrollment'),
        FEATURE_TOGGLE_DELETE_EBON_CHECKBOX: isTestGroupActive('payback-delete-ebon'),
        PAYBACK_API_KEY: env.PAYBACK_API_KEY,
        PAYBACK_MCAPI_URL: env.PAYBACK_MCAPI_URL,
        URL_PAYBACK_SESSION_TOKEN_REDIRECT: env.URL_PAYBACK_SESSION_TOKEN_REDIRECT,
        CUSTOMER_UUID: customerId,
      }
    };

    if (customerId) {
      const response = await loadInitialPaybackAccount({ headers });
      if (response.status >= 400) {
        logger.error(`Payback-Service NodeSSR: ${response.statusText}:${response.status}`);
        initialState.errorStatus = response.status;
      }

      initialState.paybackNumber = getPaybackNumber(response);
      initialState.eBonOptInExists = hasEBonOptInFlag(response);
      initialState.bonusCouponOptInExists = hasBonusCouponOptInFlag(response);
      initialState.groupEweOptInExists = hasGroupEweOptInFlag(response);
      initialState.ewe16OptInExists = hasEwe16OptInFlag(response);
      initialState.paybackAccountBound = hasPaybackAccountBinding(response);
    }
  } catch (error) {
    logger.error(`Payback-Service NodeSSR: ${error.toString()}`);
  }

  return initialState;
};
