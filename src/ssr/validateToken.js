import cache from 'memory-cache';
import logger from './logger';
import { getEnvironmentConfig as getEnv } from '../config/appConfig';
import { post } from '../api/ApiClient';


const tokenCache = new cache.Cache();

const checkAndCacheToken = (token, responseBody) => {
  if (responseBody.active) {
    const ttl = (responseBody.exp * 1000) - Math.ceil((new Date()).valueOf());
    tokenCache.put(token, responseBody, ttl);
    return;
  }
  throw Error('Token has no active flag');
};

const authServiceCheckToken = token =>
  post(
    `${getEnv('URL_AUTH_SERVICE')}/check_token`,
    `token=${token}`, {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  );

const validateToken = async (req, res, next) => {
  const requestHeaders = req.headers;
  const authHeader = req.header('Authorization');
  if (!authHeader) {
    logger.error('Authorization header not found', { requestHeaders });
    return res.sendStatus(400);
  }
  const token = authHeader.replace('Bearer ', '');

  if (tokenCache.get(token)) {
    return next();
  }
  return authServiceCheckToken(token)
    .then(response => checkAndCacheToken(token, response))
    .then(() => next())
    .catch((err) => {
      logger.error('Authentication token is invalid.', { requestHeaders, error: err.message });
      res.sendStatus(401);
    });
};

export default validateToken;
