import fs from 'fs';
import bunyan from 'bunyan';
import Middlewares from './ExpressMiddlewares';
import LoggerRawStream from './LoggerRawStream';

const LOG_LEVELS = ['TRACE', 'DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL'];

/**
 * - Original code patched to remove google cloud logging option
 * - Removes dependence on the big-ass google-cloud NPM dependency along with its (partially native) gRPC stuff
 * - These classes are ignored by ESLINT!
 */

/* eslint-disable no-param-reassign, prefer-destructuring */
class ServiceLogger {
  constructor(options) {
    if (typeof options === 'undefined') {
      throw new Error('configuration is required!');
    }

    this.productionMode = options.production || options.productionMode || false;
    this.loggerName = options.loggerName || 'logger';
    this.logDir = options.logDir || 'logs';
    this.varKey = options.varKey || 'LOG';
    this.silence = options.silence || false;
    this.dockerMode = options.docker || options.dockerMode || false;
    this.logFieldOptions = options.logFieldOptions || null;
    this.level = options.level || options.logLevel || 'INFO'; // support fallback to older key named "logLevel"
    this.serviceName = options.serviceName || 'undefined';
    this.caller = options.caller || false;

    if (this.level && LOG_LEVELS.indexOf(this.level) === -1) {
      console.warn(`[log4bro] level is not a supported logLevel: ${this.level}, defaulting to INFO.`);
      this.level = 'INFO';
    }

    this.skipDebug = this.silence || (this.productionMode && !(this.logLevel === 'TRACE' || this.logLevel === 'DEBUG'));

    this.streams = null;
    this.LOG = this.createLogger();
    const sdInitText = '';

    this.LOG.info(`Created logger, name=${this.loggerName} prod=${this.productionMode}${sdInitText}, docker=${this.dockerMode}, level=${
      this.level
    }, skipDebug=${this.skipDebug}`);

    this.setGlobal();
  }

  createLogger() {
    this.streams = []; // clear

    this.streams.push({
      type: 'raw',
      level: this.logLevel,
      stream: new LoggerRawStream(null, this.logFieldOptions, this.dockerMode) // will only write to console/stdout
    });

    if (!this.dockerMode) {
      // console.log("[log4bro] Logger is not in docker mode.");
      this.createLoggingDir();

      this.streams.push({
        type: 'raw',
        level: this.logLevel,
        stream: new LoggerRawStream(`${this.logDir}/service-log.json`, this.logFieldOptions) // will only write to logfile
      });
    }

    return bunyan.createLogger({
      name: this.loggerName,
      streams: this.streams,
      src: false
    });
  }

  createChild(defaultAdditionalFields = {}) {
    const self = this;
    defaultAdditionalFields.child = true;
    return {
      trace: (message, additionalFields) => self.trace(message, Object.assign({}, additionalFields, defaultAdditionalFields)),
      debug: (message, additionalFields) => self.debug(message, Object.assign({}, additionalFields, defaultAdditionalFields)),
      info: (message, additionalFields) => self.info(message, Object.assign({}, additionalFields, defaultAdditionalFields)),
      warn: (message, additionalFields) => self.warn(message, Object.assign({}, additionalFields, defaultAdditionalFields)),
      error: (message, additionalFields) => self.error(message, Object.assign({}, additionalFields, defaultAdditionalFields)),
      fatal: (message, additionalFields) => self.fatal(message, Object.assign({}, additionalFields, defaultAdditionalFields))
    };
  }

  changeLogLevel(level) {
    if (level && LOG_LEVELS.indexOf(level) === -1) {
      this.LOG.error(`[log4bro] level is not a supported logLevel: ${level}, defaulting to INFO.`);
      return;
    }

    this.skipDebug = !(level === 'DEBUG' || level === 'TRACE');

    this.LOG.info(`[log4bro] changing loglevel from ${this.logLevel} to ${level}.`);
    this.logLevel = level;
    this.LOG = this.createLogger();
  }

  createLoggingDir() {
    if (!fs.existsSync(this.logDir)) {
      // console.log("[log4bro] Logs folder does not exists creating " + this.logDir + " make sure to set path in blammo.xml.");
      fs.mkdirSync(this.logDir);
      return;
    }

    // console.log("[log4bro] Logs folder exists, clearing " + this.logDir);

    let files = null;
    try {
      files = fs.readdirSync(this.logDir);
    } catch (e) {
      return;
    }

    if (files.length > 0) {
      // eslint-disable-next-line no-plusplus
      for (let i = 0; i < files.length; i++) {
        const filePath = `${this.logDir}/${files[i]}`;
        if (fs.statSync(filePath).isFile()) fs.unlinkSync(filePath);
        else fs.rmDir(filePath);
      }
    }
  }

  applyMiddlewareAccessLog(expressApp, opts) {
    if (!expressApp || typeof expressApp !== 'function') {
      throw new Error('[log4bro] ExpressApp is null or not an object, make sure you pass an instance of express() to applyMiddleware.');
    }

    expressApp.use(Middlewares.accessLogMiddleware(this.serviceName, this.dockerMode, opts));
    return expressApp;
  }

  static applyMiddlewareAccessLogFile(expressApp, logFilePath) {
    if (!expressApp || typeof expressApp !== 'function') {
      throw new Error('[log4bro] ExpressApp is null or not an object, make sure you pass an instance of express() to applyMiddleware.');
    }

    if (!logFilePath) {
      throw new Error('[log4bro] logFilePath is empty on applyMiddlewareAccessLogFile.');
    }

    expressApp.use(Middlewares.accessLogMiddlewareFile(logFilePath));
    return expressApp;
  }

  setGlobal() {
    global[this.varKey] = this;
  }

  trace(message, additionalFields) {
    if (this.skipDebug) return; // save memory & cpu
    this.moveMsgObject('trace', message, additionalFields);
  }

  debug(message, additionalFields) {
    if (this.skipDebug) return; // save memory & cpu
    this.moveMsgObject('debug', message, additionalFields);
  }

  info(message, additionalFields) {
    if (this.silence) return;
    this.moveMsgObject('info', message, additionalFields);
  }

  warn(message, additionalFields) {
    if (this.silence) return;
    this.moveMsgObject('warn', message, additionalFields);
  }

  error(message, additionalFields) {
    if (this.silence) return;
    this.moveMsgObject('error', message, additionalFields);
  }

  fatal(message, additionalFields) {
    if (this.silence) return;
    this.moveMsgObject('fatal', message, additionalFields);
  }

  moveMsgObject(level, message, additionalFields = {}) {
    // identify if dealing with an object or string message
    // move an object to the msg_json field that can be index by ELK
    // do nothing if dealing with a string
    // it is important to run this step before the message touches bunyan

    const { child, wrapped } = additionalFields;

    if (this.caller) {
      try {
        const index = 3 + (child ? 1 : 0) + (wrapped ? 1 : 0);
        additionalFields.caller = new Error().stack.split('\n')[index].trim();
      } catch (err) {
        additionalFields.caller = 'error';
      }
    }

    if (child) {
      delete additionalFields.child;
    }

    if (wrapped) {
      delete additionalFields.wrapped;
    }

    if (typeof message === 'object') {
      message = {
        msg_json: message
      };

      Object.assign(message, additionalFields);

      if (this.GLOG) this.GLOG[level](message);

      return this.LOG[level](message);
    }

    if (this.GLOG) this.GLOG[level](additionalFields, message);

    return this.LOG[level](additionalFields, message);
  }

  raw(messageObject, support) {
    if (typeof messageObject !== 'object') {
      throw new Error('Logger.raw(obj) must be called with an object.');
    }

    if (this.silence) return;

    support = support || false;

    this.streams.forEach((stream) => {
      if (stream && stream.stream) {
        stream.stream.write(messageObject, support ? LoggerRawStream.OVERWRITE_MODES.ADAPT : LoggerRawStream.OVERWRITE_MODES.NONE);
      }
    });
  }
}

export default ServiceLogger;
