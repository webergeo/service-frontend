import test from 'ava';
import intercept from 'intercept-stdout';
import Logger from './ServiceLogger';

// test is just a "smoke test", does not cover the entire functionality (ported from the original Log.test.js)

/* eslint-disable no-undef, no-new */
test('should log to stdout', (t) => {
  let captured = '';
  const unhookIntercept = intercept((txt) => {
    captured += txt;
  });

  new Logger({
    productionMode: true,
    logDir: 'logs',
    varKey: 'TLOG',
    silence: false,
    dockerMode: true,
    loggerName: 'main logger',
    logFieldOptions: {
      log_type: 'application',
      application_type: 'service',
      service: 'test-service'
    },
    serviceName: 'test-service',
    level: 'INFO'
  });

  const corValue = 'i-am-a-correlation-id';
  const testLogger = TLOG.createChild({ 'correlation-id': corValue });
  testLogger.info({ an: 'object' });
  testLogger.warn('I am an empty string');

  unhookIntercept();

  t.true(captured.length > 0);
  t.true(captured.includes(corValue));
  t.is(captured.split('\n').length, 4);
});
