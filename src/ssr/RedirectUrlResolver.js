/**
 * A redirect URL mapping component that loads DUC route data from the configuration file corresponding to the current
 * environment (i.e. INT, PRE or PROD). Used by the SSR module to redirect unauthorized users to the shop login page.
 *
 * Note: Due to current rendering architecture outside of the scope of this microservice, there is ONE redirect
 * which is completely hardwired below, namely (/ui/mydata-paybackcontent -> /mydata/payback).
 * It is completely independent of the route config files.
 *
 */
import fs from 'fs';
import path from 'path';
import url from 'url';
import { env } from 'process';
import yaml from 'js-yaml';
import logger from './logger';

// Fallback map used for graceful degradation if data cannot be loaded from file for some reason (this should ideally
// never happen)
const FALLBACK_MAP = new Map([
  ['/ui/mydata-paybackcontent', '/mydata/payback'],
  ['/ui/mydata-payback-test', '/mydata/payback'],
  ['/ui/mobile/coupons', '/mydata/mobile/payback/coupons'],
  ['/ui/mobile/vouchers', '/mydata/mobile/payback/vouchers']
]);

class RedirectUrlResolver {
  constructor() {
    // eslint-disable-next-line no-underscore-dangle
    this.ducRouteMap = this._loadConfiguration();
    this.resolveUrl = this.resolveUrl.bind(this);
  }

  /** initializer for lookup map
   * Note on usage of process.cwd() below: because the server process may be started differently(i.e. are we developing
   * locally or are we inside a docker container?), we need a reliable mechanism for figuring out which directory we're
   * currently in - this is enabled by using process.cwd()
   */
  // eslint-disable-next-line class-methods-use-this
  _loadConfiguration() {
    let routeMap;
    try {
      const filePath = path.resolve(process.cwd(), 'config', `route_config_${env.REWE_ENVIRONMENT.toLowerCase()}.yml`);
      logger.info(`Loading DUC route configuration from ${filePath}`);

      const data = yaml.safeLoad(fs.readFileSync(filePath, 'utf8'));
      if (data.route_configuration) {
        routeMap = new Map();
        data.route_configuration.forEach((route) => {
          routeMap.set(route.service_path, route.path);
        });

        // hardwired redirect for mydata-paybackcontent
        routeMap.set('/ui/mydata-paybackcontent', '/mydata/payback');
      } else {
        throw new Error(`Could not find route configuration data in route_config_${env.REWE_ENVIRONMENT.toLowerCase()}.yml`);
      }
      return routeMap;
    } catch (err) {
      logger.warn(`Cannot load DUC route configuration, error: ${err.message}, using fallback map!`);
      routeMap = FALLBACK_MAP;
    }
    return routeMap;
  }

  resolveUrl(src) {
    const srcUrl = url.parse(src);
    let target;
    if (this.ducRouteMap.has(srcUrl.pathname)) {
      target = this.ducRouteMap.get(srcUrl.pathname);
      target = `${target}${srcUrl.search ? srcUrl.search : ''}`;
    } else {
      logger.warn(`Cannot resolve redirect URL for source URL:${src}`);
      target = '';
    }
    return target;
  }
}

export default RedirectUrlResolver;
