import { env } from 'process';
import express from 'express';
import compression from 'compression';

import logger from './logger';
import { renderWebView, renderMobileView, renderNumberAppView, renderRegistrationBannerAppView, renderKvAppView } from './views';
import { healthCheck, healthStatus } from './healthcheck';
import validateToken from './validateToken';
import RedirectUrlResolver from './RedirectUrlResolver';
import voucherOptions from '../config/voucher.config';

logger.info('Initializing expressJS instance...');
const app = express();

app.disable('x-powered-by');
app.use(compression());
app.set('view engine', 'ejs');

const redirectResolver = new RedirectUrlResolver();

const redirectUnauthenticatedUser = (req, res, next) => {
  if (env.NODE_ENV !== 'production' && env.NODE_ENV !== 'test') {
    return next();
  }
  if (typeof req.header('auth-info-user-id') === 'undefined') {
    logger.info('Redirecting unauthenticated user...');
    const target = redirectResolver.resolveUrl(req.url);
    return res.redirect(`/mydata/login?redirectUrl=${target}`);
  }
  return next();
};

const failUnauthenticatedUser = (req, res, next) => {
  if (env.NODE_ENV !== 'production' && env.NODE_ENV !== 'test') {
    return next();
  }
  if (typeof req.header('auth-info-user-id') === 'undefined') {
    return res.status(403).end();
  }
  return next();
};

// ------------------------------------ Health checks --------------------------------------------

app.get('/admin/healthcheck', healthCheck);
app.get('/admin/health', healthStatus);

// ---------------------------------- Content handlers -------------------------------------------

app.use(validateToken);
app.get('/ui/mydata-paybackcontent', redirectUnauthenticatedUser, renderWebView('mydata-paybackcontent'));
app.get('/ui/mobile/coupons', redirectUnauthenticatedUser, renderMobileView('coupons-mobile'));
const voucherTemplate = env.FEATURE_TOGGLE_EVOUCHER === '1' || env.FEATURE_TOGGLE_EVOUCHER === 'true' ? 'voucher' : 'voucher_disabled';
app.get('/ui/mobile/vouchers', redirectUnauthenticatedUser, renderMobileView(voucherTemplate, voucherOptions));

// temporary handler for DEV branch preview on PRE
app.get('/ui/mydata-payback-test', redirectUnauthenticatedUser, renderWebView('mydata-payback-dev'));

app.get('/ui/payback-gallery', (req, res) => res.render('payback-gallery', {
  show: req.query.show
}));

// fragments
app.get('/ui/fragments/number', failUnauthenticatedUser, renderNumberAppView('number'));
app.get('/ui/fragments/registrationBanner', renderRegistrationBannerAppView('registrationBanner'));
app.get('/ui/fragments/kv', failUnauthenticatedUser, renderKvAppView('kv'));
app.get('/ui/fragments/coupons', failUnauthenticatedUser, renderMobileView('coupons'));
// -----------------------------------------------------------------------------------------------

logger.info('ExpressJS initialized!');

export default app;
