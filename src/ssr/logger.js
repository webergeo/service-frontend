import Logger from './logging/ServiceLogger';

/**
 * Logstash logger and configuration.
 */
const loggingOptions = {
  productionMode: process.env.NODE_ENV === 'production',
  silence: false,
  loggerName: 'main',
  dockerMode: true,
  serviceName: 'payback-frontend',
  level: 'INFO',
  logFieldOptions: {
    log_type: 'application',
    application_type: 'service',
    service: 'payback-frontend'
  }
};

// global Logstash logger
const logger = new Logger(loggingOptions);

export const DEBUG = 'debug';
export const INFO = 'info';
export const WARN = 'warn';
export const ERROR = 'error';
export const FATAL = 'fatal';

export default logger;
