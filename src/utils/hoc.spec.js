import test from 'ava';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { withKeyFromProps } from './hoc';

configure({ adapter: new Adapter() });

test('withKeyFromProps', (t) => {
  const DummyChildComponent = () => <li className="dummy-child-class">hello test</li>;
  const EnhancedComponentWithKey = withKeyFromProps(DummyChildComponent, 'uniqueId');
  const wrapper = shallow(<EnhancedComponentWithKey uniqueId="dummyId" message="dummy message" />);
  t.is(wrapper.key(), 'dummyId');
});
