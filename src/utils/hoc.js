import PropTypes from 'prop-types';
import React from 'react';

/* eslint-disable import/prefer-default-export */

export const withKeyFromProps = (Comp, propName) => props => <Comp key={props[propName]} {...props} />;

/* eslint-enable import/prefer-default-export */

withKeyFromProps.propTypes = {
  Comp: PropTypes.element.isRequired,
  propName: PropTypes.string.isRequired
};
