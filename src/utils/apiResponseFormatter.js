/**
 * util methods(Selectors) consisting of response mapping
 * in a single place for every api response recieved
 */
/* eslint-disable no-underscore-dangle */
import MESSAGES from './messages';

/**
 * check if the payback number exists or null.
 * @param  {Object}  paybackData - payback response
 * @return {Boolean}             true/false flag
 */
export function hasPaybackNumber(paybackData) {
  if (paybackData.paybackNumber === null) {
    return false;
  }
  if (typeof paybackData === 'object' && Object.keys(paybackData).length === 0) {
    return false;
  }
  return true;
}

/**
 * get the payback number
 * @param  {Object}  paybackData - payback response
 * @return {String/Null}
 */
export function getPaybackNumber(paybackData) {
  if (paybackData.paybackNumber !== null) {
    return paybackData.paybackNumber;
  }
  return '';
}

/**
 * check if the ebon optIn flag is enabled or not.
 * @param  {Object}  paybackData - payback response
 * @return {Boolean}             true/false flag
 */
export function hasEBonOptInFlag(paybackData) {
  if (paybackData && paybackData._embedded && paybackData._embedded.ebon) {
    const ebonObject = paybackData._embedded.ebon;
    return ebonObject.optIn || false;
  }
  return false;
}

/**
 * check if the bonus coupon optIn flag is enabled or not.
 * @param  {Object}  paybackData - payback response
 * @return {Boolean}             true/false flag
 */
export function hasBonusCouponOptInFlag(paybackData) {
  if (paybackData && paybackData._embedded && paybackData._embedded.bonusCoupon) {
    const bonusCouponObject = paybackData._embedded.bonusCoupon;
    return bonusCouponObject.optIn || false;
  }
  return false;
}

/**
 * check if the groupEWE optIn flag is enabled or not.
 * @param  {Object}  paybackData - payback response
 * @return {Boolean}             true/false flag
 */
export function hasGroupEweOptInFlag(paybackData) {
  if (paybackData && paybackData._embedded && paybackData._embedded.groupEwe) {
    const groupEweObject = paybackData._embedded.groupEwe;
    return groupEweObject.optIn || false;
  }
  return false;
}

/**
 * check if the payback account binding exists.
 * @param  {Object}  paybackData - payback response
 * @return {Boolean}             true/false flag
 */
export function hasPaybackAccountBinding(paybackData) {
  if (paybackData && paybackData._embedded && paybackData._embedded.accountBindingExists) {
    return paybackData._embedded.accountBindingExists || false;
  }
  return false;
}

/**
 * check if the ewe16 optIn flag is enabled or not.
 * @param  {Object}  paybackData - payback response
 * @return {Boolean}             true/false flag
 */
export function hasEwe16OptInFlag(paybackData) {
  if (paybackData && paybackData._embedded && paybackData._embedded.ewe16) {
    const ewe16Object = paybackData._embedded.ewe16;
    return ewe16Object.optIn || false;
  }
  return false;
}

export function parsePaybackErrorMessage(paybackData) {
  if (paybackData && paybackData._status && paybackData._status.validationMessages) {
    return paybackData._status.validationMessages[0].message;
  }

  return MESSAGES.GENERIC_ERROR_FALLBACK_MESSAGE;
}

/**
 * extract paybackPoints from response
 * @param {Object} paybackData
 * @returns {Object}
 */
export function getPointsFromResponse(paybackData) {
  if (paybackData && paybackData.paybackPoints) {
    return paybackData.paybackPoints;
  }

  return MESSAGES.GENERIC_ERROR_FALLBACK_MESSAGE;
}

export function getTokenStatus(paybackData) {
  if (paybackData && paybackData._embedded && paybackData._embedded.tokenStatus) {
    return paybackData._embedded.tokenStatus.toLowerCase() || 'valid';
  }

  return 'valid';
}
