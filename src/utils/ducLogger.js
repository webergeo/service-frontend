/**
 *
 * An adapter for XRD.logging object
 *
 * TODO externalize service name?
 *
 */
const xrdLogger = XRD.logging.getLogger('payback-frontend');

const logger = {
  debug: message => xrdLogger.debug(message),
  info: message => xrdLogger.info(message),
  warn: message => xrdLogger.warn(message),
  error: message => xrdLogger.error(message)
};

export default logger;
