import test from 'ava';
import {
  hasPaybackNumber,
  getPaybackNumber,
  hasEBonOptInFlag,
  hasPaybackAccountBinding,
  parsePaybackErrorMessage,
  hasGroupEweOptInFlag,
  getTokenStatus
} from './apiResponseFormatter';

test.beforeEach((t) => {
  const paybackData = {
    paybackNumber: null,
    _embedded: {
      accountBindingExists: false,
      ebon: { optIn: false, _links: {}, _messages: {} },
      ewe16: { optIn: false, lastChange: null, _links: {}, _messages: {} }
    }
  };
  Object.assign(t.context, { paybackData });
});

test('hasPaybackNumber: current user doesnot have any payback number', (t) => {
  t.false(hasPaybackNumber(t.context.paybackData));
});

test('hasPaybackNumber: current user holds the payback number', (t) => {
  const paybackData = {
    ...t.context.paybackData,
    paybackNumber: '1234567890'
  };
  t.true(hasPaybackNumber(paybackData));
});

test('getPaybackNumber: returns non empty number', (t) => {
  const paybackData = {
    ...t.context.paybackData,
    paybackNumber: '1234567890'
  };
  t.is(getPaybackNumber(paybackData), '1234567890');
});

test('getPaybackNumber: returns empty string when payback Number is null', (t) => {
  t.is(getPaybackNumber(t.context.paybackData), '');
});

test('hasEBonOptInFlag: returns the ebon optInFlag', (t) => {
  const paybackData = {
    ...t.context.paybackData,
    _embedded: {
      // eslint-disable-next-line
      ...t.context.paybackData._embedded,
      ebon: {
        optIn: true
      }
    }
  };
  t.is(hasEBonOptInFlag(paybackData), true);
});

test('hasEBonOptInFlag: returns false when the payback data is not valid', (t) => {
  const paybackData = {
    ...t.context.paybackData,
    _embedded: {}
  };
  t.is(hasEBonOptInFlag(paybackData), false);
});

test('hasGroupEweOptInFlag: returns the groupEwe optInFlag', (t) => {
  const paybackData = {
    ...t.context.paybackData,
    _embedded: {
      // eslint-disable-next-line
      ...t.context.paybackData._embedded,
      groupEwe: {
        optIn: true
      }
    }
  };
  t.is(hasGroupEweOptInFlag(paybackData), true);
});

test('hasGroupEweOptInFlag: returns false when the payback data is not valid', (t) => {
  const paybackData = {
    ...t.context.paybackData,
    _embedded: {}
  };
  t.is(hasGroupEweOptInFlag(paybackData), false);
});

test('hasPaybackAccountBinding: returns payback account binding status', (t) => {
  const paybackData = {
    ...t.context.paybackData,
    _embedded: {
      // eslint-disable-next-line
      ...t.context.paybackData._embedded,
      accountBindingExists: true
    }
  };
  t.is(hasPaybackAccountBinding(paybackData), true);
});

test('getTokenStatus: returns token status', (t) => {
  const paybackData = {
    ...t.context.paybackData,
    _embedded: {
      // eslint-disable-next-line
      ...t.context.paybackData._embedded,
      tokenStatus: 'FOO'
    }
  };
  t.is(getTokenStatus(paybackData), 'foo');
});

test('parsePaybackErrorMessage: returns the server side error message', (t) => {
  const paybackData = {
    _status: {
      validationMessages: [
        {
          message: 'Die Payback Kundennummer ist nicht gültig',
          fieldName: 'paybackCustomerNumber'
        }
      ],
      messages: [
        {
          message: 'validation error',
          referenceId: '58d6c084-f8ad-415e-9acc-8ee07db92f09'
        }
      ]
    }
  };
  t.is(parsePaybackErrorMessage(paybackData), 'Die Payback Kundennummer ist nicht gültig');
});

test('parsePaybackErrorMessage: returns the default client side error message', (t) => {
  const paybackData = {
    _status: {}
  };
  const genericErrorFallbackMessage = 'Unbekannter Fehler. Bitte versuchen Sie es erneut.';
  t.is(parsePaybackErrorMessage(paybackData), genericErrorFallbackMessage);
});
