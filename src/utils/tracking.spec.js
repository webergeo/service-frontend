import test from 'ava';
import { spy } from 'sinon';
import {
  trackCouponActivationEvent,
  trackVoucherActivationEvent,
  trackPaybackNumberAddition,
  trackPaybackRegistration,
  trackEwe16OptInEvent,
  trackAccountBindingStart
} from './tracking';

test.serial('tracks payback number addition', (t) => {
  const trackEventSpy = spy(XRD.tracking, 'trackEvent');
  trackPaybackNumberAddition();
  t.true(trackEventSpy.calledWith('paybackCardAddition'));
  trackEventSpy.restore();
});

test.serial('calling trackEwe16OptInEvent function emitts an paybackApprovalOM event in XRD', (t) => {
  const trackEventSpy = spy(XRD.tracking, 'trackEvent');
  trackEwe16OptInEvent();
  t.true(trackEventSpy.calledWith('paybackApprovalOM'));
  trackEventSpy.restore();
});

test.serial('tracks payback customer registration', (t) => {
  const trackEventSpy = spy(XRD.tracking, 'trackEvent');
  trackPaybackRegistration();
  t.true(trackEventSpy.calledWith('paybackRegistration'));
  trackEventSpy.restore();
});

test.serial('tracks account binding button click', (t) => {
  const trackEventSpy = spy(XRD.tracking, 'trackEvent');
  trackAccountBindingStart();
  t.true(trackEventSpy.calledWith('paybackApprovalStart'));
  trackEventSpy.restore();
});

test.serial('passes transformed payback coupon activation event to tracking api', (t) => {
  const trackEventSpy = spy(XRD.tracking, 'trackEvent');

  const pbEvent = {
    data: {
      couponId: '123456'
    }
  };

  const expectedReweEvent = {
    coupon: ['123456']
  };

  trackCouponActivationEvent(pbEvent);

  t.true(trackEventSpy.calledWith('paybackCouponActivation', expectedReweEvent));

  trackEventSpy.restore();
});

test.serial('passes transformed payback voucher activation event  with predefined voucher to tracking api', (t) => {
  const trackEventSpy = spy(XRD.tracking, 'trackEvent');

  const pbEvent = {
    data: {
      points: 1000
    }
  };

  const expectedReweEvent = {
    amount: '1000',
    type: 'default'
  };

  trackVoucherActivationEvent(pbEvent);

  t.true(trackEventSpy.calledWith('paybackCreditActivation', expectedReweEvent));

  trackEventSpy.restore();
});

test.serial('passes transformed payback voucher activation event with custom voucher to tracking api ', (t) => {
  const trackEventSpy = spy(XRD.tracking, 'trackEvent');

  const pbEvent = {
    data: {
      points: 1200
    }
  };

  const expectedReweEvent = {
    amount: '1200',
    type: 'individually'
  };

  trackVoucherActivationEvent(pbEvent);

  t.true(trackEventSpy.calledWith('paybackCreditActivation', expectedReweEvent));

  trackEventSpy.restore();
});
