const MESSAGES = {
 GENERIC_ERROR_FALLBACK_MESSAGE: 'Unbekannter Fehler. Bitte versuchen Sie es erneut.'
}
export default MESSAGES;
