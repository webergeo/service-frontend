import test from 'ava';
import { spy, match } from 'sinon';
import logger from './ducLogger';

test.serial('test logger.info()', (t) => {
  const ducLoggerSpy = spy(XRD.logging.getLogger(), 'info');
  const message = 'test message';

  logger.info(message);

  t.true(ducLoggerSpy.calledWith(message));
  t.true(ducLoggerSpy.calledOnce);
  ducLoggerSpy.restore();
});

test.serial('test logger.debug()', (t) => {
  const ducLoggerSpy = spy(XRD.logging.getLogger(), 'debug');
  const message = 'test message';

  logger.debug(message);

  t.true(ducLoggerSpy.calledWith(message));
  t.true(ducLoggerSpy.calledOnce);
  ducLoggerSpy.restore();
});

test.serial('test logger.warn()', (t) => {
  const ducLoggerSpy = spy(XRD.logging.getLogger(), 'warn');
  const message = 'test message';

  logger.warn(message);

  t.true(ducLoggerSpy.calledWith(message));
  t.true(ducLoggerSpy.calledOnce);
  ducLoggerSpy.restore();
});

test.serial('test logger.error()', (t) => {
  const ducLoggerSpy = spy(XRD.logging.getLogger(), 'error');
  const message = 'test message';

  logger.error(message);

  t.true(ducLoggerSpy.calledWith(message));
  t.true(ducLoggerSpy.calledOnce);
  ducLoggerSpy.restore();
});
