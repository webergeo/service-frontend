import test from 'ava';
import {
  arrayGenerator,
  getFormattedPaybackNumber,
  hasProp,
  isNumeric,
  generateAccountBindingUrl,
  debounce,
  maskPaybackNumber
} from './commons';

import { NBSP } from './constants';

test('maskPaybackNumber', (t) => {
  t.deepEqual(maskPaybackNumber('1234567890'), '*** *** 7890');
});

test('getFormattedPaybackNumber', (t) => {
  t.deepEqual(getFormattedPaybackNumber('1234567890'), `***${NBSP}***${NBSP}7890`);
});

test('getFormattedPaybackNumber: empty number', (t) => {
  t.is(getFormattedPaybackNumber(''), '');
});

test('getFormattedPaybackNumber: undefined value', (t) => {
  t.is(getFormattedPaybackNumber(undefined), undefined);
});

test('isNumeric: check numeric value', (t) => {
  t.true(isNumeric(19));
});

test('isNumeric: check numeric string value', (t) => {
  t.true(isNumeric('19'));
});

test('isNumeric: check mixed value', (t) => {
  t.false(isNumeric('as19'));
});

test('isNumeric: check numeric value with dot in between', (t) => {
  t.false(isNumeric('1462.512'));
});

test('isNumeric: check numeric value with dot at end', (t) => {
  t.false(isNumeric('12312.'));
});

test('arrayGenerator', (t) => {
  t.is(arrayGenerator(3).length, 3);
});

test('hasProp', (t) => {
  const object = {
    name: 'Foo',
    lastName: 'Bar'
  };
  t.true(hasProp(object, 'lastName'));
});

test('generateAccountBindingUrl: valid URL', (t) => {
  const inputMappingProperties = {
    paybackAccountBindingUrl: 'http://some-url.com',
    paybackAccountBindingClientId: 12,
    paybackAccountBindingRedirectUri: 'http://some-redirect-uri.com'
  };
  const accountBindingUrl = inputMappingProperties.paybackAccountBindingUrl;
  const clientIdParam = `client_id=${inputMappingProperties.paybackAccountBindingClientId}`;
  const requireUriParam = `redirect_uri=${inputMappingProperties.paybackAccountBindingRedirectUri}`;
  const expectedEbonAccountBindingPageUrl = `${accountBindingUrl}?response_type=code&${clientIdParam}&${requireUriParam}`;
  const inputEbonAccountBindingPageUrl = generateAccountBindingUrl(inputMappingProperties);
  t.is(inputEbonAccountBindingPageUrl, expectedEbonAccountBindingPageUrl);
});

test('generateAccountBindingUrl: invalid URL', (t) => {
  const inputMappingProperties = {
    paybackAccountBindingUrl: 'http://some-url.com',
    paybackAccountBindingClientId: 12,
    paybackAccountBindingRedirectUri: ''
  };
  const inputEbonAccountBindingPageUrl = generateAccountBindingUrl(inputMappingProperties);
  t.is('', inputEbonAccountBindingPageUrl);
});

test.cb('debounce', (t) => {
  t.plan(2);
  const fn = debounce((x, y) => {
    t.is(x, 1);
    t.is(y, 2);
  }, 5);

  fn(1, 2);
  fn(1, 2);

  setTimeout(() => t.end(), 20);
});
