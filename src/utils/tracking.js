export const PAYBACK_COUPON_ACTIVATION_EVENT = 'PB-CouponActivatedEvent';
export const PAYBACK_VOUCHER_ACTIVATION_EVENT = 'PB-VoucherCreatedEvent';

export const trackPaybackNumberAddition = () => {
  XRD.tracking.trackEvent('paybackCardAddition');
};

export const trackPaybackRegistration = () => {
  XRD.tracking.trackEvent('paybackRegistration');
};

export const trackCouponActivationEvent = (event) => {
  const { couponId } = event.data;
  XRD.tracking.trackEvent('paybackCouponActivation', {
    coupon: [couponId]
  });
};

export const trackVoucherActivationEvent = (event) => {
  const { points } = event.data;
  const type = points === 200 || points === 500 || points === 1000 || points === 2000 ? 'default' : 'individually';
  XRD.tracking.trackEvent('paybackCreditActivation', {
    amount: points.toString(),
    type
  });
};

export const trackEwe16OptInEvent = () => {
  XRD.tracking.trackEvent('paybackApprovalOM');
};

export const trackAccountBindingStart = () => {
  XRD.tracking.trackEvent('paybackApprovalStart');
};
