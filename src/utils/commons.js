import { ACCOUNTBINDING_POPUP_BREAKPOINT, ACCOUNTBINDING_POPUP_HEIGHT, ACCOUNTBINDING_POPUP_WIDTH, NBSP } from './constants';
import { getEnvironmentConfig } from '../config/appConfig';

const PAYBACK_READABLE_FORMAT_DELIMITER = 6;
const PAYBACK_READABLE_FORMAT_INTERVAL = 3;

/**
 * Utility function to check if a string only contains numbers
 * e.g: input: 123221 output: true
 * input: 123s22 output: false
 * Regex was used because in most functions
 * input: 1232.2 is considered a valid number
 * @param {string} data
 * @returns {boolean}
 */
export const isNumeric = (data) => {
  const regex = /^[0-9\b]+$/;
  return regex.test(data) || data === '';
};

export const arrayGenerator = (length) => [...Array(length)].map(Number.call, Number);
export const hasProp = (obj, prop) => Object.prototype.hasOwnProperty.call(obj, prop);

/**
 * Utility function to remove any kinde of white spaces from a string
 * and return it
 * @param {string} input
 * @returns {string}
 */
export const removeSpaces = (input) => {
  let nospaceInput = input;
  nospaceInput = nospaceInput.replace(/\s+/g, '');
  return nospaceInput;
};

/**
 * Utiltiy function to mask the first 6 digits of a payback number
 * e.g: input: 1234567890 output: *** *** 7890
 * @param {string} value
 * @returns {string}
 */
export const maskPaybackNumber = (value) => {
  const maskVal = `*** *** ${removeSpaces(value).substring(6)}`;
  return maskVal;
};

/**
 * Utility function to format the payback number
 * e.g: input: 1234567890 output: *** *** 7890
 * Warning: this returns the value with Non-breaking spaces '\u00A0' in between
 * @param {string} value
 * @returns {string}
 */
export const getFormattedPaybackNumber = (value) => {
  if (value === undefined || value === '') {
    return value;
  }
  const symbol = '*';
  return value
    .split('')
    .map((element, index) => (index < PAYBACK_READABLE_FORMAT_DELIMITER ? symbol : element))
    .reduce((accumulator, element, index) => {
      const interValPosition = index % PAYBACK_READABLE_FORMAT_INTERVAL === PAYBACK_READABLE_FORMAT_INTERVAL - 1;
      return interValPosition && index < PAYBACK_READABLE_FORMAT_DELIMITER ? accumulator + element + NBSP : accumulator + element;
    }, '');
};

/**
 * Utility function to add a thousand seperator to the payback points
 * e.g: input: 10000 output: 10.000
 */
export const formatNumber = points => new Intl.NumberFormat('de-DE').format(points);

export const generateAccountBindingUrl = ({ paybackAccountBindingUrl, paybackAccountBindingClientId, paybackAccountBindingRedirectUri }) => {
  if (paybackAccountBindingUrl && paybackAccountBindingClientId && paybackAccountBindingRedirectUri) {
    const clientIdParam = `client_id=${paybackAccountBindingClientId}`;
    const requireUriParam = `redirect_uri=${paybackAccountBindingRedirectUri}`;
    return `${paybackAccountBindingUrl}?response_type=code&${clientIdParam}&${requireUriParam}`;
  }
  return '';
};

export const noop = () => { };

/**
 * Inspired/used from https://davidwalsh.name/javascript-debounce-function
 *
 * Returns a function, that, as long as it continues to be invoked, will not
 * be triggered. The function will be called after it stops being called for
 * N milliseconds. If `immediate` is passed, trigger the function on the
 * leading edge, instead of the trailing.
 */
export const debounce = (func, wait, immediate) => {
  let timeout;
  return (...args) => {
    const context = this;
    const later = () => {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

export const popupName = 'paybackPopup';

export const popupOptions = () => {
  const popupDimension = `height=${ACCOUNTBINDING_POPUP_HEIGHT},width=${ACCOUNTBINDING_POPUP_WIDTH}`;
  const popupOtherSettings = ',location=1,menubar=0,resizable=0,scrollbars=1,status=1,titlebar=1';
  return popupDimension + popupOtherSettings;
};

export const renderAccountBindingPopUp = (csrfToken, paybackNumber) => {
  const acountBindingPageUrl = generateAccountBindingUrl({
    paybackAccountBindingUrl: getEnvironmentConfig('PAYBACK_ACCOUNT_BINDING_URL'),
    paybackAccountBindingClientId: getEnvironmentConfig('PAYBACK_ACCOUNT_BINDING_CLIENT_ID'),
    paybackAccountBindingRedirectUri: getEnvironmentConfig('URL_PAYBACK_ACCOUNT_BINDING_REDIRECTION_ENDPOINT')
  });
  const state = `&state=${csrfToken}`;
  const cardNumber = `&cardNumber=${paybackNumber}`;
  const currentAccountBindingPageUrl = acountBindingPageUrl + state + cardNumber;
  // only open popup in non-mobile mode
  if (window.innerWidth > ACCOUNTBINDING_POPUP_BREAKPOINT) {
    const popup = window.open(currentAccountBindingPageUrl, popupName, popupOptions());
    if (popup == null) {
      return false;
    }

    popup.focus();
    return true;
  }
  window.location.href = currentAccountBindingPageUrl;
  return true;
};

export const initializePbClient = (successHandler, errorHandler) => {
  if (!window.PB) return;

  window.PB.mc.core.Bootstrap.createClient({
    apiKey: getEnvironmentConfig('PAYBACK_API_KEY'),
    token: PB.mc.core.Bootstrap.oi.token.FROM_PARTNER_SESSION,
    partnerTokenUrlTemplate: getEnvironmentConfig('URL_PAYBACK_SESSION_TOKEN_REDIRECT'),
    canCreateStrongTokens: true,
    onSuccess(client) {
      successHandler(client);
    },
    onError(errorData) {
      errorHandler(errorData);
    }
  });
};
