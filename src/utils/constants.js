/* eslint-disable max-len */

// http status codes
export const FOUR_OH_FOUR = 404;
export const HTTP_OK = 200;
export const HTTP_NO_CONTENT = 204;
export const HTTP_CONFLICT = 409;
export const HTTP_FORBIDDEN = 403;

export const WHITE_SPACE = ' ';
export const NBSP = '\u00A0';

export const PAYBACK_NUMBER_LIMIT = 10;

export const ACCOUNTBINDING_POPUP_BREAKPOINT = 480;
export const ACCOUNTBINDING_POPUP_HEIGHT = 650;
export const ACCOUNTBINDING_POPUP_WIDTH = 800;

// Event names
export const VOUCHER_CREATED_EVENT = 'voucher-created-event';

// URLs
export const PAYBACK_MCAPI_URL = 'https://www.tst1.pb-nonprod.de/pb/mcapi/oi/bootstrap.js';
export const EBON_Q_AND_A_URL = 'https://www.rewe.de/service/fragen-und-antworten/payback/';
export const EBON_TERMS_CONDITIONS_URL = 'https://www.rewe.de/payback/teilnahmebedingungen-datenschutz-ebon';
export const EBON_PRIVACY_URL = 'https://www.rewe.de/payback/datenschutzhinweise-ebon';
export const BONUS_COUPON_URL = 'https://www.rewe.de/payback/bonus-coupon';
export const EBON_SHOP_URL = '/mydata/meine-einkaeufe/im-markt';