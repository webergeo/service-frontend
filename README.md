# The Frontend Service

Provides the single-page app within the "my data" section of the customer data page.

## Local build & developement

Local build & development in this project are provided by WebPack. See /webpack subdirectory for configuration details.

* Run `yarn install` initially to configure the project dependencies
* Run `yarn dev` if you wish to develop locally (all server responses are mocked in this mode!)
* Run `yarn dev:int` if you wish to develop locally while using the INT environment backend
* Run `yarn dev:preprod` if you wish to develop locally while using the PREPROD environment backend
* Run `yarn dev:number` if you wish to develop locally the number fragment (all server responses are mocked in this mode!)
* Run `yarn dev:banner` if you wish to develop locally the registration banner fragment (all server responses are mocked)
* Run `yarn test` to perform the local AVA test suite
* Run `yarn tdd` to perform the local AVA test suite and have it watch your changes as you develop
* Run `yarn test:update-snapshot` to update rendered output snapshots in the local AVA test suite if necessary
* Run `yarn build` to build the actual production distributable and check the integrity of the build
* Run `yarn ssr:local` to start the SSR module in local dev mode

### Local SSR checks

* Use curl or wget after you've started the SSR mode via `yarn ssr:local`
  * The following URLs are accessible:
    * <http://localhost:8080/admin/health> and <http://localhost:8080/admin/healthcheck>
    * <http://localhost:8080/ui/mydata-content>
    * <http://localhost:8080/ui/mobile/coupons> and <http://localhost:8080/ui/mobile/vouchers>
    * <http://localhost:8080/ui/fragment/number>

## Configuration

* Local development settings are contained in several ENV files inside the /dev-settings directory, corrresponding to different environments.
* Service descriptor and DUC route configuration files are in the /config directory

## Deployment

* [Master build and deploy](https://team.jenkins.firma.com/job/service-frontend/)
* [Dev build and deploy](https://team.jenkins.firma.com/job/service-frontend-dev/)
  How to: Go to your branch and click on "Build with Parameters" and check the 
  "DEPLOY" checkbox. Your branch will be deployed to Int and Pre on a special
  service name which is then routed to `/mydata/service-test`.
